require("dotenv").config({ path: __dirname + `/.env.${process.env.NODE_ENV}` });
const app = require("./app");

const port = process.env.PORT || 4040;

app.listen(port, () => {
  console.log(
    "Listening to port " + port + "\nenv version: " + process.env.ENV_VERSION
  );
});
