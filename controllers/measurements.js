const {
  CreateMeasurement,
  ReadMeasurement,
  ReadMeasurements
} = require("../queries");
const { facets } = require("../helpers");
const map = require("lodash/map");
const isEmpty = require("lodash/isEmpty");
const {
  fullNameObjToFrontEndArray
} = require("../helpers/transformMeasurements");
const { MeasurementProfile, AnonymousUser } = require("../models");

module.exports = {
  createMeasurement(req, res, next) {
    const { _id } = res.locals.user;

    CreateMeasurement({ measurementProps: req.body, _id })
      .then(measurement => {
        res.status(200).json(measurement);
        return next();
      })
      .catch(e => next(new Error(e.toString())));
  },

  async createAnonymousMeasurement(req, res, next) {
    const { firebaseUid } = res.locals.user;

    const measurementProfile = new MeasurementProfile(req.body);

    try {
      const measurement = await measurementProfile.save();
      await AnonymousUser.findOneAndUpdate(
        { firebaseUid: firebaseUid },
        { $push: { profiles: measurement._id } }
      );
      res.status(200).json(measurement);
      return next();
    } catch (e) {
      next(new Error(e.toString()));
    }
  },

  async readMeasurement(req, res) {
    const { measurementId } = req.params;
    const userId = res.locals.user._id;
    try {
      const measurement = await ReadMeasurement(measurementId);
      if (measurement.user.toString() === userId.toString()) {
        return res.status(200).json(measurement);
      } else {
        return res.status(400).json({
          error: "Could not find measurement associated with your profile."
        });
      }
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  },

  async readMeasurements(req, res) {
    const userId = res.locals.user._id;
    try {
      const user = await ReadMeasurements(userId);
      const measurementProfiles = user.profiles;
      /* Transforms Jacket-Waist to Waist for each measurement in the array of objects
      */

      if (isEmpty(measurementProfiles)) {
        return res.sendStatus(204);
      } else {
        const transformedMeasurements = map(
          measurementProfiles,
          singleProfile => fullNameObjToFrontEndArray(singleProfile)
        );

        console.log(measurementProfiles[0]);

        return res.status(200).json(transformedMeasurements);
        // return res.status(200).json(measurementProfiles);
      }
    } catch (err) {
      return res.status(400).json({ error: err.toString() });
    }
  }
};
