const { CreateItem } = require("../../queries");
const facets = require("../../helpers/constants/facets");

const CLOUDINARY_BASE_URL = `https://res.cloudinary.com/${
  process.env.CLOUDINARY_CLOUD_NAME
}/image/upload/`;

module.exports = function(req, res, next) {
  const sellerId = res.locals.user._id;
  const sellerUsername = res.locals.user.username;

  const isMeasurementValid = checkMeasurements(
    req.body.measurement,
    req.body.itemType
  );

  if (!isMeasurementValid) {
    const wrongMeasurements = new Error("Incorrect measurements.");
    return next(wrongMeasurements);
  }

  const photos = req.body.photos.map(
    photo => `${CLOUDINARY_BASE_URL}v${photo}`
  );

  const item = {
    ...req.body,
    photos,
    sellerId,
    sellerUsername
  };

  CreateItem(item)
    .then(function(item) {
      return res.status(200).json({ _id: item._id.toString() });
    })
    .catch(err => next(new Error(`Error saving item. ${err.toString()}`)));
};

function checkMeasurements(measurement, itemType) {
  let isValid = true;

  const userMeasurements = Object.keys(measurement);
  const requiredMeasurements = facets[itemType];

  requiredMeasurements.map(requiredMeasurement => {
    if (!measurement[requiredMeasurement]) {
      console.log(
        "measurement[requiredMeasurement]",
        measurement[requiredMeasurement]
      );
      isValid = false;
    }
  });

  userMeasurements.map(userMeasurement => {
    if (!requiredMeasurements.includes(userMeasurement)) {
      console.log(
        "requiredMeasurements.includes(userMeasurement)",
        requiredMeasurements,
        "\n",
        userMeasurement,
        requiredMeasurements.includes(userMeasurement)
      );
      isValid = false;
    }
  });

  return isValid;
}
