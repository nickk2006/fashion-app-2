// const bcrypt = require("bcryptjs");
// const jwt = require("jsonwebtoken");
// const keys = require("../config/keys");
// const normalizeEmail = require("validator/lib/normalizeEmail");
// const { API_IDENTIFIER } = require("../helpers/constants");
const { User } = require("../models");
const { firebase } = require("../firebase");
const {
  CreateUser,
  ReadUserByEmail,
  UpdateUsernameFirebase
} = require("../queries");
const {
  DISPLAY_NAME_TAKEN,
  EMAIL_TAKEN
} = require("../helpers/constants/error");
// const validateLoginInput = require("../validation/login");
const verifyJwt = require("../firebase/auth/firebaseVerifyJwt");

module.exports = {
  async register(req, res, next) {
    const { username, email, password } = req.body;
    try {
      // const mongodbUser = await new User({
      //   username
      // }).save();

      const isEmailUnique = await User.isEmailUnique(email);

      if (!isEmailUnique) {
        const error = new Error(EMAIL_TAKEN);
        error.status = 409;
        error.code = EMAIL_TAKEN;
        return next(error);
      }
      const user = new User({
        username,
        email
      });

      const savedUser = await user.save();

      await firebase.auth().createUser({
        email,
        displayName: username,
        uid: savedUser._id.toString(),
        password
      });
      res.locals.username = username;
      res.locals.email = email;
      res.status(200).json({ username });
      next();
    } catch (e) {
      return next(e);
    }
  },

  async isUsernameUnique(req, res, next) {
    const { username } = req.query;
    try {
      const isUsernameUnique = await User.isUsernameUnique(username);

      if (isUsernameUnique) {
        return res.sendStatus(200);
      } else {
        const error = new Error(DISPLAY_NAME_TAKEN);
        error.status = 409;
        error.code = DISPLAY_NAME_TAKEN;
        return next(error);
      }
    } catch (e) {
      return next(e);
    }
  },

  async login(req, res, next) {
    try {
      const token = req.header("Authorization");
      const firebaseUser = await verifyJwt(token);

      const {
        _id,
        itemsForSale,
        itemsSold,
        itemsPurchased,
        profiles,
        isConnectedToStripe
      } = await User.findById(firebaseUser.uid).select(
        "_id itemsForSale itemsSold itemsPurchased profiles email username isConnectedToStripe"
      );

      const payload = {
        _id,
        itemsForSale,
        itemsSold,
        itemsPurchased,
        profiles,
        isConnectedToStripe,
        email: firebaseUser.email,
        username: firebaseUser.displayName
      };
      res.status(200).json({ success: true, user: payload });
      next();
    } catch (e) {
      next(new Error(e.toString()));
    }
  },

  async current(req, res) {
    const token = req.header("Authorization");
    await verifyJwt(token);
    const {
      isConnectedToStripe,
      itemsForSale = [],
      itemsSold = [],
      itemsPurchased = [],
      profiles = []
    } = res.locals.user;

    const payload = {
      isConnectedToStripe,
      itemsForSale,
      itemsSold,
      itemsPurchased,
      profiles
    };
    return res.status(200).json(payload);
    // const user = await User.findById(
    //   res.locals.user._id,
    //   "itemsForSale itemsSold itemsPurchased"
    // ).populate({
    //   path: "itemsForSale itemsSold itemsPurchased",
    //   select: "_id facets.designer facets.itemType price photos"
    // });
    // return res.status(200).json(user);
  },

  logout(req, res) {
    req.logout();
    return res.status(200).json({ success: true });
  },

  async getUserProfile(req, res, next) {
    const { username } = req.params;
    try {
      const { itemsForSale } = await User.findOne(
        { username },
        "itemsForSale -_id"
      )
        .lean()
        .populate("itemsForSale", {
          primaryPhoto: 1,
          _id: 1,
          "facets.designer": 1,
          designerLine: 1,
          price: 1
        })
        .exec();
      const transformedItems = itemsForSale.map(item => ({
        primaryPhoto: item.primaryPhoto,
        _id: item._id,
        designer: item.facets.designer,
        designerLine: item.designerLine,
        price: item.price
      }));
      console.log("transf items", transformedItems);
      res.locals.items = { data: transformedItems };
      next();
    } catch (e) {
      return next(e);
    }
  }
};
