const mobile = require("is-mobile");

exports.resizeForCatalog = function(req, res, next) {
  if (!res.locals.items) {
    res.sendStatus(204);
    return next();
  }
  const { data = [], ...rest } = res.locals.items;
  console.log("data is", data);
  const items = data.map(item => {
    const { primaryPhoto, ...rest } = item;
    const transformedPhoto = primaryPhoto.replace("_XX", "_md");
    return {
      primaryPhoto: transformedPhoto,
      ...rest
    };
  });

  const payload = { data: items, ...rest };
  res.status(200).json(payload);
  next();
};

exports.resizeForProductInfo = function(req, res, next) {
  const { photos, ...rest } = res.locals.payload;
  const isMobile = mobile(req);

  const transformedPhotos = photos.map(
    photo =>
      isMobile ? photo.replace("_XX", "_md") : photo.replace("_XX", "_lg")
  );

  const newPayload = {
    photos: transformedPhotos,
    ...rest
  };

  res.status(200).json(newPayload);
};
