const facetSearch = require("../queries/Facet");
const ReadDistinctFacets = require("../queries/Facet/ReadDistinctFacets");
const facets = require("../helpers/constants/facets");
const isEmpty = require("lodash/isEmpty");
const util = require("util");
const { MeasurementProfile } = require("../models");
exports.populateMeasurements = async function(req, res, next) {
  const { profile } = req.query;

  if (!!profile) {
    const profilePromises = profile.map(async p =>
      MeasurementProfile.findById(p).exec()
    );
    Promise.all(profilePromises)
      .then(value => {
        res.locals.profiles = value;
        return next();
      })
      .catch(e => next(new Error(e.toString)));
  } else {
    res.locals.profiles = [];
    return next();
  }
};

exports.facetedSearch = function(req, res, next) {
  const { profiles = [] } = res.locals;
  const { pn = 1, profile, ...filters } = req.query;
  const limit = 30;
  const skip = (pn - 1) * limit;
  const payload = {
    measurementsArray: profiles,
    filters,
    skip,
    limit,
    page: pn
  };
  facetSearch(payload, (err, documents) => {
    if (err) {
      return next(err);
    } else if (isEmpty(documents[0].data)) {
      const payload = {
        data: [],
        metaData: [
          {
            total: 0,
            page: 1,
            offset: 0
          }
        ]
      };
      return res.status(200).json(payload);
    } else {
      const payload = documents[0] ? documents[0] : {};
      return res.status(200).json(payload);
    }
  });
};

exports.distinctFilters = function(req, res, next) {
  const transformedProfiles = [];

  const { profiles = [] } = res.locals;

  const profilesInUserModel = res.locals.user.profiles;

  if (!isEmpty(profilesInUserModel)) {
    profilesInUserModel.map(profile =>
      transformedProfiles.push({
        label: profile.name,
        value: profile._id
      })
    );
  }
  ReadDistinctFacets(profiles, (err, documents) => {
    if (err) {
      next(err);
    }
    const items = documents[0] || {};

    const { material = [], designer = [], itemType = [], color = [] } = items;

    let payload = {
      material: material.sort(),
      designer: designer.sort(),
      itemType: itemType.sort(),
      color: color.sort(),
      myProfile: transformedProfiles
    };

    return res.status(200).json(payload);
  });
};
