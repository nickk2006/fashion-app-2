const {
  ReadMeasurements,
  ReadItemById,
  ReadUserItemsForSale,
  UpdateItemById
} = require("../queries");
const facets = require("../helpers/constants/facets");
const isEmpty = require("lodash/isEmpty");
const isNumber = require("lodash/isNumber");
const map = require("lodash/map");
const mongoose = require("mongoose");
const { User, Item } = require("../models");

module.exports = {
  async readItem(req, res, next) {
    try {
      const select = "-buyer -primaryPhoto -__v ";
      const item = await ReadItemById(req.params.itemId, select);

      const {
        photos,
        _id,
        price,
        sellerId,
        sellerUsername,
        description,
        designerLine,
        createdAt,
        isSold
      } = item;

      const {
        color,
        material,
        designer,
        itemType,
        ...measurements
      } = item.facets;

      const transformedMaterial = material.map(materialName => ({
        name: materialName,
        amount: item.materials[materialName]
      }));

      const transformedMeasurements = [];

      map(facets[itemType], fullName => {
        if (isNumber(measurements[fullName])) {
          transformedMeasurements.push({
            name: facets.itemNameMapping[fullName],
            amount: measurements[fullName]
          });
        }
      });

      const payload = {
        material: transformedMaterial,
        color,
        itemType,
        designer,
        measurement: transformedMeasurements,
        photos,
        _id,
        price,
        sellerId,
        sellerUsername,
        description,
        designerLine,
        createdAt,
        isSold
      };
      return res.status(200).json(payload);
    } catch (e) {
      return res.status(400).json({ error: e.toString() });
    }
  },

  async deleteItem(req, res, next) {
    const { itemId } = req.params;
    const userId = res.locals.user._id;

    try {
      const item = await Item.findOneAndRemove({
        _id: itemId,
        sellerId: userId
      }).exec();

      if (item) {
        const user = await User.findByIdAndUpdate(
          userId,
          { $pull: { itemsForSale: itemId } },
          { new: true, select: "itemsForSale" }
        ).populate("itemsForSale");
        return res.status(200).json({ itemsForSale: user.itemsForSale });
      } else {
        return res.sendStatus(401);
      }
    } catch (e) {
      next(e);
    }
  },

  async readUserItems(req, res, next) {
    try {
      const { itemsForSale } = await ReadUserItemsForSale(req.params.userId);
      if (!isEmpty(itemsForSale)) {
        return res.status(200).json(itemsForSale);
      } else {
        return res.status(204).json();
      }
    } catch (e) {
      return res.status(400).json({ error: e.toString() });
    }
  },

  async readItemAndSetHold(req, res, next) {
    try {
      const itemId = mongoose.Types.ObjectId(req.params.itemId);
      const userId = mongoose.Types.ObjectId(res.locals.user._id);
      // console.log("itemId", itemId, "userId", userId);
      // console.log("reqbody", req.body);
      const select = "holdUntil holderSetBy isSold sellerId";
      const newHoldUntil = Date.now() + 5 * 60 * 1000; // Current date plus five minutes

      const itemOnHoldError = new Error(
        "Item is currently in someone else's checkout. Check back in five minutes."
      );
      itemOnHoldError.status = 403;

      const oldItem = await ReadItemById(itemId, select);
      if (oldItem.isSold) {
        const itemIsSoldError = new Error(
          "We're sorry. The item has been sold."
        );
        itemIsSoldError.status = 403;
        return next(itemIsSoldError);
      } else if (Date.now < oldItem.holdUntil) {
        return next(itemOnHoldError);
      }

      const newItem = await Item.findByIdAndUpdate(
        itemId,
        {
          $set: {
            holdUntil: newHoldUntil,
            holderSetBy: userId
          }
        },
        {
          new: true
        }
      ).exec();

      const holderId = mongoose.Types.ObjectId(newItem.holderSetBy);

      if (!holderId.equals(userId)) {
        return next(itemOnHoldError);
      }

      res.locals.item = newItem;
      next();
    } catch (e) {
      next(new Error(e.toString()));
    }
  },

  async updateUsersAndCatalogForPurchase(req, res, next) {
    const buyerId = mongoose.Types.ObjectId(res.locals.user._id);
    const sellerId = mongoose.Types.ObjectId(res.locals.item.sellerId);
    const itemId = mongoose.Types.ObjectId(res.locals.item._id);
    const payment = res.locals.payment;
    const { shipping } = req.body;
    console.log("shipping is", shipping, "\n");
    const updateBuyer = User.findByIdAndUpdate(
      buyerId,
      {
        $push: { itemsPurchased: itemId }
      },
      { new: true }
    ).exec();

    const updateSeller = User.findByIdAndUpdate(
      sellerId,
      {
        $pull: { itemsForSale: itemId },
        $push: { itemsSold: itemId }
      },
      { new: true }
    ).exec();

    const updateItem = Item.findByIdAndUpdate(
      itemId,
      {
        $set: { isSold: true, buyer: buyerId, shipTo: shipping, payment }
      },
      { new: true }
    ).exec();

    const [updatedBuyer, updatedSeller, updatedItem] = await Promise.all([
      updateBuyer,
      updateSeller,
      updateItem
    ]);

    (res.locals.buyer = updatedBuyer),
      (res.locals.seller = updatedSeller),
      (res.locals.item = updatedItem);
    next();
  },

  async isShipping(req, res, next) {
    const { user } = res.locals;
    const { itemId } = req.params;

    try {
      const item = await Item.findById(
        itemId,
        "isSold buyer isClosed isTrackingSet photos facets.designer facets.itemType _id"
      ).exec();
      console.log("item is", item);
      const canEdit = isAbleToEditShipping(item, user);
      const payload = {
        photos: item.photos,
        designer: item.facets.designer,
        itemType: item.facets.itemType,
        _id: item._id
      };
      if (canEdit) {
        return res.status(200).json({ isSuccess: true, item: payload });
      } else {
        return res.status(200).json({ isSuccess: false, item: {} });
      }
    } catch (e) {
      next(e.toString());
    }
  },

  async verifySeller(req, res, next) {
    const { user } = res.locals;
    const { itemId } = req.params;

    try {
      const item = await Item.findById(itemId).exec();
      res.locals.item = item;
      res.locals.seller = user;
      const canEdit = isAbleToEditShipping(item, user);

      if (canEdit) {
        // const { carrier, tracking_number } = req.body;
        // const updatedItem = await Item.findByIdAndUpdate(
        //   itemId,
        //   {
        //     $set: {
        //       carrier: carrier,
        //       tracking_number: tracking_number,
        //       isTrackingSet: true
        //     }
        //   },
        //   { new: true }
        // );

        // res.locals.item = updatedItem;
        return next();
      } else {
        return next(new Error("Unable to set shipping tracker"));
      }
    } catch (e) {
      return next(e.toString());
    }
  },

  async updateItemForCaptureCharge(req, res, next) {
    const { item } = res.locals;
    try {
      const newItem = await Item.findByIdAndUpdate(
        item._id,
        {
          $set: { "payment.isCaptured": true }
        },
        { new: true }
      ).exec();
      const buyer = await User.findById(newItem.buyer, "email").exec();
      res.locals.item = newItem;
      res.locals.buyerEmail = buyer.email;
      next();
    } catch (e) {
      next(e.toString());
    }
  },

  async setTransferDate(req, res, next) {
    const { deliveryDate, itemId } = res.locals;
    const FIVE_DAYS = 1000 * 60 * 60 * 24 * 5;
    const transferDate = parseInt((deliveryDate + FIVE_DAYS).toFixed(0));

    console.log("deliveryDate");
    try {
      const newItem = await Item.findByIdAndUpdate(
        itemId,
        { $set: { "payment.transferDate": transferDate, isDelivered: true } },
        { new: true }
      ).exec();
      console.log("newitem", newItem);
      res.sendStatus(200);
      next();
    } catch (e) {
      // res.sendStatus(200);
      next(e.toString());
    }
  },

  async findTransfersDue(req, res, next) {
    const now = Date.now();
    const FIVE_DAYS = 1000 * 60 * 60 * 24 * 5;
    const ONE_HOUR = 1000 * 60 * 60;
    const TEN_MINUTES = 1000 * 60 * 10;

    const lowerBound = now - FIVE_DAYS - ONE_HOUR - TEN_MINUTES;
    const upperBound = now - FIVE_DAYS;
    const itemsDue = await Item.find(
      {
        "payment.isDisputed": false,
        isClosed: false,
        "payment.transferDate": { $gte: lowerBound, $lte: upperBound }
      },
      "payment.transfer_group payment.destination payment.amount"
    ).exec();
    if (isEmpty(itemsDue)) {
      return res.sendStatus(200);
    }

    const items = itemsDue.map(
      ({ payment: { transfer_group, destination, amount } }) => ({
        transfer_group,
        destination,
        amount
      })
    );
    console.log(items);

    res.locals.items = items;
    next();
  }
};

const isAbleToEditShipping = (item, user) =>
  !item.isClosed &&
  !!item.buyer &&
  item.buyer.toString() === user._id.toString() &&
  !item.carrier &&
  !item.isTrackingSet;
