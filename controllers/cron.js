exports.checkCronHeader = (req, res, next) => {
  const isValidHeader = req.headers["x-appengine-cron"];
  console.log("header", req.headers);
  if (isValidHeader) {
    res.sendStatus(200);
    return next();
  } else {
    return next(new Error("Auth header not found"));
  }
};
