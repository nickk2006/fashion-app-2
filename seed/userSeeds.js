const jsf = require("json-schema-faker");
const seeder = require("mongoose-seeder");

const userSchema = {
  type: "object",
  properties: {
    email: {
      type: "string",
      format: "email",
      faker: "internet.email"
    },
    username: {
      type: "string",
      faker: "internet.username"
    },
    password: {
      type: "string",
      faker: "internet.password",
      minLength: 6,
      makLength: 15
    },
    profile: {
      type: "object",
      properties: {
        name: {
          type: "string",
          faker: "name.findName"
        },
        gender: {
          type: "string",
          enum: ["male", "female"]
        }
      },
      required: ["name", "gender"]
    }
  },
  required: ["email", "password", "profile"]
};
jsf.extend("faker", () => require("faker"));

jsf.resolve(userSchema).then(sample => console.log(sample));
