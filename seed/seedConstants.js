const DESIGNERS = [
  "APC",
  "Acne Studios",
  "Balenciaga",
  "Brioni",
  "Belvest",
  "Neil Barrett",
  "Thom Browne",
  "Brunello Cucinelli",
  "Calvin Klein",
  "Chanel",
  "Comme des Garcons",
  "Corneliani",
  "Dolce & Gabbana",
  "Etro",
  "Fendi",
  "Gieves & Hawkes",
  "Gucci",
  "Isaia",
  "Jil Sander",
  "Maison Margiela",
  "Rick Owens",
  "Prada",
  "Ralph Lauren",
  "Turnbull & Asser",
  "Versace",
  "Wooyoungmi",
  "Yves Saint Laurent"
];

module.exports = { DESIGNERS };
