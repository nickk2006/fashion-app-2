const _ = require("lodash");
const faker = require("faker");
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const DESIGNERS = require("./seedConstants");
const { User, Item, MeasurementProfile } = require("../models");
const uuid = require("uuid/v4");
// const { Jacket, DressShirt, Pants } = require("../models/itemModels");
// const {
//   JacketProfile,
//   DressShirtProfile,
//   PantsProfile
// } = require("../models/measurementProfiles");
// const keys = require("../config/keys_dev_local");
const keys = require("../config/keys_dev");
const normalizeEmail = require("validator/lib/normalizeEmail");

const USERS_TO_ADD = 1;
const ITEMS_TO_ADD_MIN = 1;
const ITEMS_TO_ADD_MAX = 2;
const MEASUREMENTS_TO_ADD = 1;

const colors = [
  "black",
  "brown",
  "blue",
  "green",
  "grey",
  "orange",
  "pink",
  "purple",
  "red",
  "white",
  "yellow"
];
const materials = [
  { name: "cotton", amount: 85 },
  { name: "cashmere", amount: 5 },
  { name: "silk", amount: 10 }
];

mongoose.connect(keys.mongoURI);
mongoose.connection.once("open", async () => {
  const user1 = await createCompleteUser();
  const user2 = await createCompleteUser();
  const user3 = await createCompleteUser();
  const user4 = await createCompleteUser();
  const user5 = await createCompleteUser();

  console.log("done!");
  // mongoose.connection.close();
});

const createCompleteUser = () => {
  const user = new User({
    username: faker.internet.userName()
  });

  user.save().then(({ _id, username }) => {
    const pantsMeasurementProfile = new MeasurementProfile({
      user: mongoose.Types.ObjectId(_id),
      name: "My Pants Measurements",
      itemType: "Pants",
      measurements: [
        {
          name: "waist",
          min: randomNumber(16, 18, 0.25).toString(),
          max: randomNumber(18.25, 20, 0.25).toString(),
          itemType: "Pants"
        },
        {
          name: "legOpening",
          min: randomNumber(7, 8, 0.25).toString(),
          max: randomNumber(8.25, 9, 0.25).toString(),
          itemType: "Pants"
        }
      ]
    });

    const shirtMeasurementProfile = new MeasurementProfile({
      user: mongoose.Types.ObjectId(_id),
      name: "My Dress Shirt Measurements",
      itemType: "DressShirt",
      measurements: [
        {
          name: "collar",
          min: randomNumber(7, 8, 0.25).toString(),
          max: randomNumber(8.25, 9, 0.25).toString(),
          itemType: "DressShirt"
        },
        {
          name: "sleeve",
          min: randomNumber(24, 25, 0.25).toString(),
          max: randomNumber(25.25, 26, 0.25).toString(),
          itemType: "DressShirt"
        },
        {
          name: "chest",
          min: randomNumber(20, 22.5, 0.25).toString(),
          max: randomNumber(22.75, 23, 0.25).toString(),
          itemType: "DressShirt"
        },
        {
          name: "waist",
          min: randomNumber(16, 18, 0.25).toString(),
          max: randomNumber(18.25, 20, 0.25).toString(),
          itemType: "DressShirt"
        },
        {
          name: "length",
          min: randomNumber(27, 28.5, 0.25).toString(),
          max: randomNumber(28.75, 30, 0.25).toString(),
          itemType: "DressShirt"
        }
      ]
    });

    const jacketMeasurementProfile = new MeasurementProfile({
      user: mongoose.Types.ObjectId(_id),
      name: "My Jacket Shirt Measurements",
      itemType: "Jacket",
      measurements: [
        {
          name: "backLength",
          min: randomNumber(29, 30.5, 0.25).toString(),
          max: randomNumber(30.75, 32, 0.25).toString(),
          itemType: "Jacket"
        },
        {
          name: "sleeve",
          min: randomNumber(24, 25, 0.25).toString(),
          max: randomNumber(25.25, 26, 0.25).toString(),
          itemType: "Jacket"
        },
        {
          name: "chest",
          min: randomNumber(18, 20, 0.25).toString(),
          max: randomNumber(20.25, 22, 0.25).toString(),
          itemType: "Jacket"
        },
        {
          name: "waist",
          min: randomNumber(17, 19.5, 0.25).toString(),
          max: randomNumber(19.75, 22, 0.25).toString(),
          itemType: "Jacket"
        },
        {
          name: "shoulder",
          min: randomNumber(16, 19, 0.25).toString(),
          max: randomNumber(19.25, 22, 0.25).toString(),
          itemType: "Jacket"
        }
      ]
    });

    const arrayOfPhotos = [...Array(randomNumber(1, 10, 1))].map(
      () =>
        `https://picsum.photos/$HEIGHT/$WIDTH/?image=${randomNumber(
          1,
          1084,
          1
        ).toString()}`
    );

    const pants = [
      ...Array(randomNumber(ITEMS_TO_ADD_MIN, ITEMS_TO_ADD_MAX, 1))
    ].map(() => {
      const pant = new Item({
        sellerId: _id,
        sellerUsername: username,
        designer: randomFromArray(DESIGNERS),
        price: faker.commerce.price(75, 2000).toString(),
        itemType: {
          name: "Pants"
        },
        colors: [
          { name: randomFromArray(colors) },
          { name: randomFromArray(colors) },
          { name: randomFromArray(colors) }
        ],
        materials: [
          {
            name: "cotton",
            amount: 50
          },
          {
            name: "linen",
            amount: 50
          }
        ],
        primaryPhoto: `https://picsum.photos/$HEIGHT/$WIDTH/?image=${randomNumber(
          1,
          1084,
          1
        ).toString()}`,
        secondaryPhotos: arrayOfPhotos,
        title: "Lorem ipsum dolor sit pants",
        description:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat mas",
        measurements: [
          {
            name: "waist",
            measurement: randomNumber(16, 20, 0.25).toString(),
            itemType: "Pants"
          },
          {
            name: "legOpening",
            measurement: randomNumber(7, 9, 0.25).toString(),
            itemType: "Pants"
          }
        ]
      });
      return pant.save();
    });

    const shirts = [
      ...Array(randomNumber(ITEMS_TO_ADD_MIN, ITEMS_TO_ADD_MAX, 1))
    ].map(() => {
      const shirt = new Item({
        sellerId: _id,
        sellerUsername: username,
        designer: randomFromArray(DESIGNERS),
        price: faker.commerce.price(75, 2000).toString(),
        itemType: {
          name: "DressShirt"
        },
        colors: [
          { name: randomFromArray(colors) },
          { name: randomFromArray(colors) },
          { name: randomFromArray(colors) }
        ],
        materials: [
          {
            name: "wool",
            amount: 50
          },
          {
            name: "cotton",
            amount: 50
          }
        ],
        primaryPhoto: `https://picsum.photos/$HEIGHT/$WIDTH/?image=${randomNumber(
          1,
          1084,
          1
        ).toString()}`,
        secondaryPhotos: arrayOfPhotos,
        title: "Lorem ipsum dolor si shirt",
        description:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat mas",
        measurements: [
          {
            name: "waist",
            measurement: randomNumber(16, 20, 0.25).toString(),
            itemType: "DressShirt"
          },
          {
            name: "collar",
            measurement: randomNumber(7, 9, 0.25).toString(),
            itemType: "DressShirt"
          },
          {
            name: "sleeve",
            measurement: randomNumber(24, 26, 0.25).toString(),
            itemType: "DressShirt"
          },
          {
            name: "chest",
            measurement: randomNumber(20, 23, 0.25).toString(),
            itemType: "DressShirt"
          },
          {
            name: "length",
            measurement: randomNumber(27, 30, 0.25).toString(),
            itemType: "DressShirt"
          }
        ]
      });
      return shirt.save();
    });

    const jackets = [
      ...Array(randomNumber(ITEMS_TO_ADD_MIN, ITEMS_TO_ADD_MAX, 1))
    ].map(() => {
      const jacket = new Item({
        sellerId: _id,
        sellerUsername: username,
        designer: randomFromArray(DESIGNERS),
        price: faker.commerce.price(75, 2000).toString(),
        itemType: {
          name: "Jacket"
        },
        colors: [
          { name: randomFromArray(colors) },
          { name: randomFromArray(colors) }
        ],
        materials: [
          {
            name: "cotton",
            amount: 50
          },
          {
            name: "silk",
            amount: 25
          },
          {
            name: "cashmere",
            amount: 25
          }
        ],
        primaryPhoto: `https://picsum.photos/$HEIGHT/$WIDTH/?image=${randomNumber(
          1,
          1084,
          1
        ).toString()}`,
        secondaryPhotos: arrayOfPhotos,
        title: "Lorem ipsum dolor s balzer",
        description:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat mas",
        measurements: [
          {
            name: "backLength",
            measurement: randomNumber(29, 32, 0.25).toString(),
            itemType: "Jacket"
          },
          {
            name: "sleeve",
            measurement: randomNumber(24, 26, 0.25).toString(),
            itemType: "Jacket"
          },
          {
            name: "waist",
            measurement: randomNumber(17, 22, 0.25).toString(),
            itemType: "Jacket"
          },
          {
            name: "chest",
            measurement: randomNumber(18, 22, 0.25).toString(),
            itemType: "Jacket"
          },
          {
            name: "shoulder",
            measurement: randomNumber(16, 22, 0.25).toString(),
            itemType: "Jacket"
          }
        ]
      });
      return jacket.save();
    });
    Promise.all([
      ...pants,
      ...jackets,
      ...shirts,
      jacketMeasurementProfile.save(),
      shirtMeasurementProfile.save(),
      pantsMeasurementProfile.save()
    ]).then(() => console.log("done"));
  });
};

//   const users = _.times(USERS_TO_ADD, () => {
//     const salt = bcrypt.genSaltSync(12);
//     const password = bcrypt.hashSync("123123", salt);
//     console.log(password);
//     return {
//       email: normalizeEmail(faker.internet.email()),
//       password,
//       username: faker.internet.userName()
//     };
//   });

//   User.insertMany(users).then(users => {
//     const jacketArray = _.times(ITEMS_TO_ADD, () =>
//       createItem(users, "Jacket")
//     );

//     const dressShirtArray = _.times(ITEMS_TO_ADD, () =>
//       createItem(users, "DressShirt")
//     );

//     const pantsArray = _.times(ITEMS_TO_ADD, () => createItem(users, "Pants"));

//     const jacketMeasurementArray = _.times(MEASUREMENTS_TO_ADD, () =>
//       createProfile(users, "JacketMeasurement")
//     );

//     const dressShirtMeasurementArray = _.times(MEASUREMENTS_TO_ADD, () =>
//       createProfile(users, "DressShirtMeasurement")
//     );

//     const pantsMeasurementArray = _.times(MEASUREMENTS_TO_ADD, () =>
//       createProfile(users, "PantsMeasurement")
//     );

//     Promise.all([
//       Jacket.insertMany(jacketArray),
//       DressShirt.insertMany(dressShirtArray),
//       Pants.insertMany(pantsArray),
//       JacketProfile.insertMany(jacketMeasurementArray)
//       // DressShirtProfile.insertMany(dressShirtMeasurementArray),
//       // PantsProfile.insertMany(pantsMeasurementArray)
//     ]).then(values => {
//       // const profiles = _.flatten([values[3], values[4], values[5]]);
//       const profiles = values[3];

//       const matchedProfiles = profiles.map((measurement, index, array) => {
//         User.findByIdAndUpdate(
//           measurement.user,
//           {
//             $push: { profiles: measurement._id }
//           },
//           function() {
//             if (array.length - 1 === index) {
//               console.log("DONE!");
//               mongoose.connection.close();
//             }
//           }
//         );
//       });
//     });
//   });
// });

// function createItem(users, itemType) {
//   let user = randomUser(users);
//   let item;
//   switch (itemType) {
//     case "Jacket":
//       item = createJacket();
//       break;
//     case "DressShirt":
//       item = createDressShirt();
//       break;
//     case "Pants":
//       item = createPants();
//       break;
//   }
//   item.sellerId = user._id;
//   item.sellerUsername = user.username;
//   return item;
// }
// function createProfile(users, profileType) {
//   let user = randomUser(users);
//   let profile;
//   switch (profileType) {
//     case "JacketMeasurement":
//       profile = createJacketMeasurement();
//       break;
//     case "DressShirtMeasurement":
//       profile = createDressShirtMeasurement();
//       break;
//     case "PantsMeasurement":
//       profile = createPantsMeasurement();
//       break;
//   }
//   profile.user = user._id;
//   return profile;
// }

// function createPantsMeasurement() {
//   return {
//     waist: randomNumber(28, 44, 1).toString(),
//     legOpening: randomNumber(7, 9, 0.25).toString()
//   };
// }

// function createDressShirtMeasurement() {
//   return {
//     collar: randomNumber(14, 16, 0.25).toString(),
//     sleeve: randomNumber(24, 26, 0.25).toString(),
//     chest: randomNumber(20, 23, 0.25).toString(),
//     waist: randomNumber(16, 20, 0.25).toString(),
//     length: randomNumber(27, 30, 0.25).toString()
//   };
// }

// function createJacketMeasurement() {
//   return {
//     backLength: randomNumber(29, 32, 0.25).toString(),
//     chest: randomNumber(18, 22, 0.25).toString(),
//     waist: randomNumber(17, 22, 0.25).toString(),
//     shoulder: randomNumber(16, 22, 0.25).toString(),
//     sleeve: randomNumber(24, 26, 0.25).toString()
//   };
// }

// function createBase() {
//   const arrayOfPhotos = [...Array(randomNumber(1, 10, 1))].map(
//     () =>
//       `https://picsum.photos/$HEIGHT/$WIDTH/?image=${randomNumber(
//         1,
//         1084,
//         1
//       ).toString()}`
//   );
//   return {
//     designer: randomFromArray(DESIGNERS),
//     materials,
//     price: faker.commerce.price(75, 2000).toString(),
//     primaryPhoto: `https://picsum.photos/$HEIGHT/$WIDTH/?image=${randomNumber(
//       1,
//       1084,
//       1
//     ).toString()}`,
//     secondaryPhotos: arrayOfPhotos,
//     description:
//       "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat mas"
//   };
// }

// function createJacket() {
//   let base = createBase();
//   let details = {
//     measurements: {
//       backLength: randomNumber(29, 32, 0.25).toString(),
//       chest: randomNumber(18, 22, 0.25).toString(),
//       waist: randomNumber(17, 22, 0.25).toString(),
//       shoulder: randomNumber(16, 22, 0.25).toString(),
//       sleeve: randomNumber(24, 26, 0.25).toString()
//     },
//     colors: [
//       randomFromArray(colors),
//       randomFromArray(colors),
//       randomFromArray(colors)
//     ]
//   };
//   return Object.assign(base, details);
// }

// function createDressShirt() {
//   let base = createBase();
//   let details = {
//     measurements: {
//       collar: randomNumber(14, 16, 0.25).toString(),
//       sleeve: randomNumber(24, 26, 0.25).toString(),
//       chest: randomNumber(20, 23, 0.25).toString(),
//       waist: randomNumber(16, 20, 0.25).toString(),
//       length: randomNumber(27, 30, 0.25).toString()
//     },
//     colors: [randomFromArray(colors)]
//   };
//   return Object.assign(base, details);
// }

// function createPants() {
//   let base = createBase();
//   let details = {
//     measurements: {
//       waist: randomNumber(28, 44, 1).toString(),
//       legOpening: randomNumber(7, 9, 0.25).toString()
//     },
//     colors: [randomFromArray(colors), randomFromArray(colors)]
//   };
//   return Object.assign(base, details);
// }

function randomFromArray(array) {
  return array[~~(Math.random() * array.length)];
}

function randomNumber(min, max, increment) {
  return (
    Math.floor((Math.random() * (max - min)) / increment) * increment + min
  );
}

// function randomUser(users) {
//   let user = randomFromArray(users);
//   return user;
// }
