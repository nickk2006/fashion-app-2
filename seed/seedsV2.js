const dotenv = require("dotenv");
const path = require("path");
dotenv.config({ path: path.join(__dirname, "../.env.production") });
const faker = require("faker");
const mongoose = require("mongoose");
const { User, Item } = require("../models");
const { CreateMeasurement } = require("../queries");
const keys = require("../config/keys_dev_local");
const facets = require("../helpers/constants/facets");
const seedConstants = require("./seedConstants");
const ITEMS_TO_ADD_MIN = 250;
const ITEMS_TO_ADD_MAX = 500;
const { firebase } = require("../firebase");
const FIREBASE_UIDS = [
  "Gvmso4FipFbz8W05CECdPMgogqm2",
  "aPUXTwERloXchRcyPledFIYN7Sj2",
  "da0LPFDNaPgI26PCb2098OY9tS32",
  "DT1fKA0Qx3fomYTK4tZu63bb0MZ2",
  "VXFDefmc2Eed0LDoKUtq02Yzse43",
  "tHniELDIsdV6NoovZaG5n3VyLWY2",
  "DkYgPLL8U9Rz4e0M0wn2GRvp4Lq2"
];

mongoose.connect(
  process.env.MONGO_URI,
  { useNewUrlParser: true }
);
mongoose.connection.once("open", async () => {
  console.log("done!");
  await createOneProfile("Gvmso4FipFbz8W05CECdPMgogqm2");
  await createOneProfile("aPUXTwERloXchRcyPledFIYN7Sj2");
  // await createOneProfile("da0LPFDNaPgI26PCb2098OY9tS32");
  // await createOneProfile("DT1fKA0Qx3fomYTK4tZu63bb0MZ2");
  // await createOneProfile("VXFDefmc2Eed0LDoKUtq02Yzse43");
  // await createOneProfile("tHniELDIsdV6NoovZaG5n3VyLWY2");
  // await createOneProfile("DkYgPLL8U9Rz4e0M0wn2GRvp4Lq2");
});

const createOneProfile = () => {
  const userSchema = createUserSchema();
  const user = new User(userSchema);

  return user
    .save()
    .then(
      async user =>
        await firebase
          .auth()
          .createUser({
            email: user.email,
            displayName: user.username,
            uid: user._id.toString(),
            password: "123123"
          })
          .then(() => user)
    )
    .then(({ _id, username }) => {
      const jacketMeasurement1 = createJacketMeasurementSchema();
      const jacketMeasurement2 = createJacketMeasurementSchema();
      const dressShirtMeasurement1 = createDressShirtMeasurementSchema();
      const dressShirtMeasurement2 = createDressShirtMeasurementSchema();
      const pantsMeasurement1 = createPantsMeasurementSchema();
      const pantsMeasurement2 = createPantsMeasurementSchema();

      const measurementDocs = [
        jacketMeasurement1,
        jacketMeasurement2,
        dressShirtMeasurement1,
        dressShirtMeasurement2,
        pantsMeasurement1,
        pantsMeasurement2
      ].map(measurementProps => CreateMeasurement({ measurementProps, _id }));

      const items = [];
      [...Array(randomNumber(ITEMS_TO_ADD_MIN, ITEMS_TO_ADD_MAX, 1))].map(
        () => {
          const item = createItemDocument({ _id, username });
          items.push(item);
        }
      );

      console.log("items are", items);
      return Promise.all([...measurementDocs, ...items]);
    })
    .then(res => console.log("done"));
};

const createUserSchema = () => {
  const username = faker.internet.userName();
  const email = faker.internet.email();
  return {
    username,
    email
  };
};

const createJacketMeasurementSchema = () => ({
  itemType: "Jacket",
  name: `${faker.internet.userName()} Jacket`,
  "Jacket-Chest.min": randomNumber(5, 10, 0.25),
  "Jacket-Chest.max": randomNumber(10.25, 20, 0.25),

  "Jacket-Length.min": randomNumber(5, 10, 0.25),
  "Jacket-Length.max": randomNumber(10.25, 20, 0.25),

  "Jacket-Shoulder.min": randomNumber(5, 10, 0.25),
  "Jacket-Shoulder.max": randomNumber(10.25, 20, 0.25),

  "Jacket-Sleeve.min": randomNumber(5, 10, 0.25),
  "Jacket-Sleeve.max": randomNumber(10.25, 20, 0.25),

  "Jacket-Waist.min": randomNumber(5, 10, 0.25),
  "Jacket-Waist.max": randomNumber(10.25, 20, 0.25)
});

const createPantsMeasurementSchema = () => ({
  itemType: "Pants",
  name: `${faker.internet.userName()} Pants`,

  "Pants-Front Rise.min": getRandomInt(9, 10),
  "Pants-Front Rise.max": getRandomInt(11, 12),

  "Pants-Hip.min": getRandomInt(15, 23),
  "Pants-Hip.max": getRandomInt(24, 30),

  "Pants-Inseam.min": getRandomInt(26, 33),
  "Pants-Inseam.max": getRandomInt(34, 40),

  "Pants-Knee.min": getRandomInt(6, 9),
  "Pants-Knee.max": getRandomInt(10, 12),

  "Pants-Leg Opening.min": getRandomInt(5, 10),
  "Pants-Leg Opening.max": getRandomInt(11, 12),

  "Pants-Top Thigh.min": getRandomInt(10, 12),
  "Pants-Top Thigh.max": getRandomInt(13, 15),

  "Pants-Waist.min": getRandomInt(27, 35),
  "Pants-Waist.max": getRandomInt(36, 46)
});

const createDressShirtMeasurementSchema = () => ({
  itemType: "Dress Shirt",
  name: `${faker.internet.userName()} Dress Shirt`,

  "Dress Shirt-Chest.min": randomNumber(5, 10, 0.25),
  "Dress Shirt-Chest.max": randomNumber(10.25, 20, 0.25),

  "Dress Shirt-Length.min": randomNumber(5, 10, 0.25),
  "Dress Shirt-Length.max": randomNumber(10.25, 20, 0.25),

  "Dress Shirt-Neck.min": randomNumber(5, 10, 0.25),
  "Dress Shirt-Neck.max": randomNumber(10.25, 20, 0.25),

  "Dress Shirt-Shoulder.min": randomNumber(5, 10, 0.25),
  "Dress Shirt-Shoulder.max": randomNumber(10.25, 20, 0.25),

  "Dress Shirt-Waist.min": randomNumber(5, 10, 0.25),
  "Dress Shirt-Waist.max": randomNumber(10.25, 20, 0.25)
});

const createItemDocument = ({ _id, username }) => {
  const itemType = randomFromArray(facets.itemTypes);
  const measurements = {};
  facets[itemType].map(
    fullName => (measurements[fullName] = randomNumber(5, 46, 0.25))
  );
  const material = randomMaterialArray();
  const schema = {
    itemType,
    price: randomNumber(50, 2000, 1),
    sellerId: _id,
    sellerUsername: "test User",
    photos: [
      "https://picsum.photos/300/400/?random_XX",
      "https://picsum.photos/300/400/?random_XX",
      "https://picsum.photos/300/400/?random_XX",
      "https://picsum.photos/300/400/?random_XX",
      "https://picsum.photos/300/400/?random_XX",
      "https://picsum.photos/300/400/?random_XX"
    ],
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium q",
    designerLine: "Collection Line",
    material,
    color: randomColorArray(),
    designer: randomFromArray(seedConstants.DESIGNERS),
    measurement: { ...measurements }
  };
  const item = new Item(schema);
  return item.save();
};

function randomFromArray(array) {
  return array[~~(Math.random() * array.length)];
}

function randomNumber(min, max, increment) {
  return (
    Math.floor((Math.random() * (max - min)) / increment) * increment + min
  );
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomMaterialArray() {
  const material1 = randomFromArray(facets.materials);

  return [{ name: [material1], amount: 100 }];
}

function randomColorArray() {
  const color1 = randomFromArray(facets.colors);
  const color2 = randomFromArray(facets.colors);
  const color3 = randomFromArray(facets.colors);

  return [color1, color2, color3];
}
