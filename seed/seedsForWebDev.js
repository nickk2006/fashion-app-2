const _ = require("lodash");
const faker = require("faker");
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const DESIGNERS = require("./seedConstants");
const User = require("../models/User");
const { Jacket, DressShirt, Pants } = require("../models/itemModels");
const {
  JacketProfile,
  DressShirtProfile,
  PantsProfile
} = require("../models/measurementProfiles");
const keys = require("../config/keys_dev");
const normalizeEmail = require("validator/lib/normalizeEmail");

const USERS_TO_ADD = 5;
const ITEMS_TO_ADD = 500;
const MEASUREMENTS_TO_ADD = 8;

const colors = [
  "black",
  "brown",
  "blue",
  "green",
  "grey",
  "orange",
  "pink",
  "purple",
  "red",
  "white",
  "yellow"
];
const materials = [
  { name: "cotton", amount: 85 },
  { name: "cashmere", amount: 5 },
  { name: "silk", amount: 10 }
];

mongoose.connect(keys.mongoURI);
mongoose.connection.once("open", () => {
  const users = _.times(USERS_TO_ADD, () => {
    const salt = bcrypt.genSaltSync(12);
    const password = bcrypt.hashSync("123123", salt);
    console.log(password);
    return {
      email: normalizeEmail(faker.internet.email()),
      password,
      username: faker.internet.userName()
    };
  });

  User.insertMany(users).then(users => {
    const jacketArray = _.times(ITEMS_TO_ADD, () =>
      createItem(users, "Jacket")
    );

    const dressShirtArray = _.times(ITEMS_TO_ADD, () =>
      createItem(users, "DressShirt")
    );

    const pantsArray = _.times(ITEMS_TO_ADD, () => createItem(users, "Pants"));

    const jacketMeasurementArray = _.times(MEASUREMENTS_TO_ADD, () =>
      createProfile(users, "JacketMeasurement")
    );

    const dressShirtMeasurementArray = _.times(MEASUREMENTS_TO_ADD, () =>
      createProfile(users, "DressShirtMeasurement")
    );

    const pantsMeasurementArray = _.times(MEASUREMENTS_TO_ADD, () =>
      createProfile(users, "PantsMeasurement")
    );

    Promise.all([
      Jacket.insertMany(jacketArray),
      DressShirt.insertMany(dressShirtArray),
      Pants.insertMany(pantsArray),
      JacketProfile.insertMany(jacketMeasurementArray)
      // DressShirtProfile.insertMany(dressShirtMeasurementArray),
      // PantsProfile.insertMany(pantsMeasurementArray)
    ]).then(values => {
      // const profiles = _.flatten([values[3], values[4], values[5]]);
      const profiles = values[3];

      const matchedProfiles = profiles.map((measurement, index, array) => {
        User.findByIdAndUpdate(
          measurement.user,
          {
            $push: { profiles: measurement._id }
          },
          function() {
            if (array.length - 1 === index) {
              console.log("DONE!");
              mongoose.connection.close();
            }
          }
        );
      });
    });
  });
});

function createItem(users, itemType) {
  let user = randomUser(users);
  let item;
  switch (itemType) {
    case "Jacket":
      item = createJacket();
      break;
    case "DressShirt":
      item = createDressShirt();
      break;
    case "Pants":
      item = createPants();
      break;
  }
  item.sellerId = user._id;
  item.sellerUsername = user.username;
  return item;
}
function createProfile(users, profileType) {
  let user = randomUser(users);
  let profile;
  switch (profileType) {
    case "JacketMeasurement":
      profile = createJacketMeasurement();
      break;
    case "DressShirtMeasurement":
      profile = createDressShirtMeasurement();
      break;
    case "PantsMeasurement":
      profile = createPantsMeasurement();
      break;
  }
  profile.user = user._id;
  return profile;
}

function createPantsMeasurement() {
  return {
    waist: randomNumber(28, 44, 1).toString(),
    legOpening: randomNumber(7, 9, 0.25).toString()
  };
}

function createDressShirtMeasurement() {
  return {
    collar: randomNumber(14, 16, 0.25).toString(),
    sleeve: randomNumber(24, 26, 0.25).toString(),
    chest: randomNumber(20, 23, 0.25).toString(),
    waist: randomNumber(16, 20, 0.25).toString(),
    length: randomNumber(27, 30, 0.25).toString()
  };
}

function createJacketMeasurement() {
  return {
    backLength: randomNumber(29, 32, 0.25).toString(),
    chest: randomNumber(18, 22, 0.25).toString(),
    waist: randomNumber(17, 22, 0.25).toString(),
    shoulder: randomNumber(16, 22, 0.25).toString(),
    sleeve: randomNumber(24, 26, 0.25).toString()
  };
}

function createBase() {
  const arrayOfPhotos = [...Array(randomNumber(1, 10, 1))].map(
    () =>
      `https://picsum.photos/$HEIGHT/$WIDTH/?image=${randomNumber(
        1,
        1084,
        1
      ).toString()}`
  );
  return {
    designer: randomFromArray(DESIGNERS),
    materials,
    price: faker.commerce.price(75, 2000).toString(),
    primaryPhoto: `https://picsum.photos/$HEIGHT/$WIDTH/?image=${randomNumber(
      1,
      1084,
      1
    ).toString()}`,
    secondaryPhotos: arrayOfPhotos,
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat mas"
  };
}

function createJacket() {
  let base = createBase();
  let details = {
    measurements: {
      backLength: randomNumber(29, 32, 0.25).toString(),
      chest: randomNumber(18, 22, 0.25).toString(),
      waist: randomNumber(17, 22, 0.25).toString(),
      shoulder: randomNumber(16, 22, 0.25).toString(),
      sleeve: randomNumber(24, 26, 0.25).toString()
    },
    colors: [
      randomFromArray(colors),
      randomFromArray(colors),
      randomFromArray(colors)
    ]
  };
  return Object.assign(base, details);
}

function createDressShirt() {
  let base = createBase();
  let details = {
    measurements: {
      collar: randomNumber(14, 16, 0.25).toString(),
      sleeve: randomNumber(24, 26, 0.25).toString(),
      chest: randomNumber(20, 23, 0.25).toString(),
      waist: randomNumber(16, 20, 0.25).toString(),
      length: randomNumber(27, 30, 0.25).toString()
    },
    colors: [randomFromArray(colors)]
  };
  return Object.assign(base, details);
}

function createPants() {
  let base = createBase();
  let details = {
    measurements: {
      waist: randomNumber(28, 44, 1).toString(),
      legOpening: randomNumber(7, 9, 0.25).toString()
    },
    colors: [randomFromArray(colors), randomFromArray(colors)]
  };
  return Object.assign(base, details);
}

function randomFromArray(array) {
  return array[~~(Math.random() * array.length)];
}

function randomNumber(min, max, increment) {
  return (
    Math.floor((Math.random() * (max - min)) / increment) * increment + min
  );
}

function randomUser(users) {
  let user = randomFromArray(users);
  return user;
}
