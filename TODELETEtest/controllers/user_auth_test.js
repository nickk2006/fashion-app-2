// const assert = require("assert");
// const request = require("supertest");
// const mongoose = require("mongoose");
// const app = require("../../app");
// const User = mongoose.model("User");
// const getIdByDecodingJwt = require("../helpers/getIdByDecodingJwt");

// describe.skip("User controller", () => {
//   var id;

//   beforeEach(done => {
//     request(app)
//       .post("/api/users/register")
//       .send({
//         email: "test@test.com",
//         username: "user1",
//         password: "123123"
//       })
//       .end((err, { body }) => {
//         id = getIdByDecodingJwt(body.token);
//         done();
//       });
//   });

//   afterEach(done => {
//     User.remove({}, function() {
//       mongoose.connection.collections["users"].drop(function() {
//         done();
//       });
//     });
//   });

//   it("POST to api/users/register creates a new user", done => {
//     request(app)
//       .post("/api/users/register")
//       .send({
//         email: "test3@test.com",
//         username: "user2",
//         password: "123123",
//         password2: "123123"
//       })
//       .end((err, { body: { token } }) => {
//         var userId = getIdByDecodingJwt(token);
//         User.findById(userId).then(user => {
//           assert(user.email === "test3@test.com");
//           done();
//         });
//       });
//   });

//   it("POST to api/users/login logins a user", done => {
//     request(app)
//       .post("/api/users/login")
//       .send({
//         email: "test@test.com",
//         password: "123123",
//         password2: "123123"
//       })
//       .expect(200)
//       .end((err, { body: { token } }) => {
//         const userId = getIdByDecodingJwt(token);
//         assert(userId === id);
//         done();
//       });
//   });

//   it("GET to api/users/current returns user if logged in", done => {
//     request(app)
//       .post("/api/users/login")
//       .send({
//         email: "test@test.com",
//         password: "123123",
//         password2: "123123"
//       })
//       .end((err, { body }) => {
//         const { token } = body;
//         request(app)
//           .get("/api/users/current")
//           .set("Authorization", `Bearer ${token}`)
//           .end((err, { body }) => {
//             assert(body.username === "user1");
//             done();
//           });
//       });
//   });

//   it("GET to api/users/logout logs out user", done => {
//     request(app)
//       .post("/api/users/login")
//       .send({
//         email: "test@test.com",
//         password: "123123",
//         password2: "123123"
//       })
//       .expect(200)
//       .end((err, { body }) => {
//         const { token } = body;
//         request(app)
//           .get("/api/users/logout")
//           .set("Authorization", token)
//           .end((err, { body }) => {
//             assert(body.success);
//             done();
//           });
//       });
//   });
// });
