// const assert = require("assert");
// const _ = require("lodash");
// const request = require("supertest");
// const mongoose = require("mongoose");
// const app = require("../../app");
// const {
//   addUsers,
//   createItemsFor,
//   loginUserAndGetToken,
//   getIdWithToken
// } = require("../helpers");
// const User = mongoose.model("User");
// const Item = require("../../models/itemModels/Item");
// const DressShirt = require("../../models/itemModels/DressShirt");

// describe.skip("Filtering and aggregating queries", function() {
//   let token, id;
//   this.timeout(10000);
//   before(async () => {
//     try {
//       const users = await addUsers();
//       const items = users.map(({ _id }) => {
//         return createItemsFor(_id);
//       });
//       const itemsToAdd = _.flatten(items);
//       await Promise.all(itemsToAdd);

//       token = await loginUserAndGetToken();
//       id = await getIdWithToken(token);
//     } catch (e) {
//       console.log("error", e);
//     }
//   });

//   after(async () => {
//     await Item.remove({});
//     await User.remove({});
//   });

//   it("POST to /api/items/search filters dataset by price", async () => {
//     const criteria = {
//       price: {
//         min: "1000",
//         max: "2000"
//       }
//     };

//     const payload = {
//       criteria,
//       sortProperty: "designer",
//       offset: 0,
//       limit: 20
//     };

//     const response = await request(app)
//       .post(`/api/items/search`)
//       .set("Authorization", token)
//       .send(payload);

//     items = response.body.all;

//     const testItems = items.map(item => item.designer);
//     assert(
//       testItems.includes("Canali") &&
//         testItems.includes("Zegna") &&
//         !testItems.includes("Tom Ford")
//     );
//   });

//   it("POST to /api/items/search filters dataset by itemType", async () => {
//     const criteria = {
//       price: {},
//       itemType: ["Pants", "DressShirt"],
//       designer: [],
//       materials: [],
//       colors: [],
//       text: ""
//     };

//     const payload = {
//       criteria,
//       sortProperty: "designer",
//       offset: 0,
//       limit: 20
//     };

//     const response = await request(app)
//       .post(`/api/items/search`)
//       .set("Authorization", token)
//       .send(payload);

//     items = response.body.all;

//     const testItems = items.map(item => item.designer);
//     assert(
//       testItems.includes("Tom Ford") &&
//         testItems.includes("Zegna") &&
//         !testItems.includes("Canali")
//     );
//   });

//   it("POST to /api/items/search filters dataset by designer", async () => {
//     const criteria = {
//       designer: ["Zegna", "Tom Ford"]
//     };

//     const payload = {
//       criteria,
//       sortProperty: "designer",
//       offset: 0,
//       limit: 20
//     };

//     const response = await request(app)
//       .post(`/api/items/search`)
//       .set("Authorization", token)
//       .send(payload);

//     items = response.body.all;

//     const testItems = items.map(item => item.designer);
//     assert(
//       testItems.includes("Zegna") &&
//         testItems.includes("Tom Ford") &&
//         !testItems.includes("Canali")
//     );
//   });

//   it("POST to /api/items/search filters dataset by color", async () => {
//     const criteria = {
//       colors: ["Black", "Navy"]
//     };

//     const payload = {
//       criteria,
//       sortProperty: "designer",
//       offset: 0,
//       limit: 20
//     };

//     const response = await request(app)
//       .post(`/api/items/search`)
//       .set("Authorization", token)
//       .send(payload);

//     items = response.body.all;

//     const testItems = items.map(item => item.designer);
//     assert(
//       testItems.includes("Zegna") &&
//         testItems.includes("Tom Ford") &&
//         !testItems.includes("Canali")
//     );
//   });

//   it("POST to /api/items/search filters dataset by colors", async () => {
//     const criteria = {
//       materials: ["Cashmere", "Cotton"]
//     };

//     const payload = {
//       criteria,
//       sortProperty: "designer",
//       offset: 0,
//       limit: 20
//     };

//     const response = await request(app)
//       .post(`/api/items/search`)
//       .set("Authorization", token)
//       .send(payload);

//     items = response.body.all;

//     const testItems = items.map(item => item.designer);
//     assert(
//       testItems.includes("Tom Ford") &&
//         testItems.includes("Canali") &&
//         !testItems.includes("Zegna")
//     );
//   });
//   it("GET to /api/items/:itemId reads specific item from database", async () => {
//     const dressShirt = new DressShirt({
//       designer: "Tom Ford",
//       price: "700",
//       seller: mongoose.Types.ObjectId("TestId123123"),
//       collar: "15.5",
//       sleeve: "32",
//       chest: "21",
//       waist: "15",
//       length: "17",
//       colors: ["Black"],
//       materials: ["Cotton"]
//     });
//     const savedDressShirt = await dressShirt.save();
//     const dressShirtId = savedDressShirt._id.toString();

//     const response = await request(app).get(`/api/items/${dressShirtId}`);
//     assert(response.body.designer === "Tom Ford");
//   });
// });
