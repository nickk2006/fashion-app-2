// const assert = require("assert");
// const request = require("supertest");
// const mongoose = require("mongoose");
// const app = require("../../app");
// const User = mongoose.model("User");
// const MeasurementProfile = require("../../models/measurementProfiles/MeasurementProfile");
// const { loginUserAndGetToken, getIdWithToken } = require("../helpers");

// describe.skip("User measurements", function() {
//   this.timeout(10000);
//   let token;

//   before(async () => {
//     await request(app)
//       .post("/api/users/register")
//       .send({
//         email: "user1@test.com",
//         username: "user1",
//         password: "123123",
//         password2: "123123"
//       });
//     token = await loginUserAndGetToken();
//   });

//   after(async () => {
//     await Promise.all([User.remove({}), MeasurementProfile.remove({})]);
//     await mongoose.connection.collections["users"].drop();
//   });

//   it("POST to /api/users/measurements/:measurementType creates pants measurement profile", done => {
//     const payload = {
//       name: "Test Pants Profile",
//       waist: "30",
//       legOpening: "7"
//     };

//     request(app)
//       .post("/api/users/measurements/pants")
//       .set("Authorization", `Bearer ${token}`)
//       .send(payload)
//       .end((err, { body }) => {
//         const { name, waist, legOpening } = body;
//         assert(
//           name === "Test Pants Profile" && waist === 30 && legOpening === 7
//         );
//         done();
//       });
//   });

//   it("GET to /api/users/measurements/:measurementId returns single measurement profile", async () => {
//     const mock = {
//       name: "Test Pants Profile",
//       waist: "30",
//       legOpening: "7"
//     };

//     const measurement = await request(app)
//       .post("/api/users/measurements/pants")
//       .set("Authorization", `Bearer ${token}`)
//       .send(mock);

//     const measurementId = measurement.body._id;

//     const response = await request(app)
//       .get(`/api/users/measurements/${measurementId}`)
//       .set("Authorization", `Bearer ${token}`);

//     const { name, waist, legOpening } = response.body;
//     assert(name === "Test Pants Profile" && waist === 30 && legOpening === 7);
//   });

//   it("GET to /api/users/measurements gets all of the user's profiles", done => {
//     const payload = {
//       name: "Test Pants Profile",
//       waist: "30",
//       legOpening: "7"
//     };

//     request(app)
//       .post("/api/users/measurements/pants")
//       .set("Authorization", `Bearer ${token}`)
//       .send(payload)
//       .end(() => {
//         request(app)
//           .get("/api/users/measurements")
//           .set("Authorization", `Bearer ${token}`)
//           .end((err, { body }) => {
//             done();
//           });
//       });
//   });
//   // xit("POST to api/users/measurements creates pants profile", async () => {
//   //   const profile = {
//   //     type: "PantsProfile",
//   //     measurementProps: {
//   //       name: "Test Pants Profile Title",
//   //       waist: "30",
//   //       legOpening: "7"
//   //     }
//   //   };

//   //   const response = await request(app)
//   //     .post("/api/users/measurements")
//   //     .set("Authorization", token)
//   //     .send(profile);

//   //   assert(response);

//   //   const userId = await getIdWithToken(token);

//   //   const user = await User.findById(userId).populate({
//   //     path: "profile.pantsProfiles",
//   //     model: "PantsProfile"
//   //   });

//   //   const pantsProfile = user.profile.pantsProfile[0];

//   //   assert(
//   //     pantsProfile.name === "Test Pants Profile Title" &&
//   //       pantsProfile.waist === "30" &&
//   //       pantsProfile.legOpening === "7"
//   //   );
//   // });
// });
