// const assert = require("assert");
// const request = require("supertest");
// const mongoose = require("mongoose");
// const app = require("../../app");
// const User = mongoose.model("User");
// const Item = require("../../models/itemModels/Item");

// describe.skip("ItemSell Controller", () => {
//   let token;
//   before(done => {
//     request(app)
//       .post("/api/users/register")
//       .send({
//         email: "test@test.com",
//         username: "test",
//         password: "123123",
//         password2: "123123"
//       })
//       .end(() => {
//         request(app)
//           .post("/api/users/login")
//           .send({
//             email: "test@test.com",
//             password: "123123"
//           })
//           .end((err, { body }) => {
//             token = `Bearer ${body.token}`;
//             done();
//           });
//       });
//   });

//   after(done => {
//     const removeUsers = User.remove({});
//     const removeItems = Item.remove({});
//     Promise.all([removeUsers, removeItems]).then(() => {
//       mongoose.connection.collections["users"].drop().then(() => done());
//     });
//   });

//   it("POST pants object to api/items/sell creates a new pants item", done => {
//     request(app)
//       .post("/api/items")
//       .set("Authorization", token)
//       .send({
//         type: "Pants",
//         itemProps: {
//           designer: "Zegna",
//           price: "1000",
//           measurements: {
//             waist: "32",
//             legOpening: "7.5"
//           },
//           colors: ["Navy"],
//           materials: ["Wool"],
//           primaryPhoto: "http://via.placeholder.com/200x200"
//         }
//       })
//       .end((err, { body }) => {
//         assert(body.success);
//         done();
//       });
//   });

//   it("POST shirt object to api/items/sell creates a new shirt item", done => {
//     request(app)
//       .post("/api/items")
//       .set("Authorization", token)
//       .send({
//         type: "DressShirt",
//         itemProps: {
//           designer: "Tom Ford",
//           price: "700",
//           measurements: {
//             collar: "15.5",
//             sleeve: "32",
//             chest: "21",
//             waist: "15",
//             length: "17"
//           },
//           colors: ["Black"],
//           materials: ["Cotton"],
//           primaryPhoto: "http://via.placeholder.com/200x200"
//         }
//       })
//       .end((err, { body }) => {
//         assert(body.success);
//         done();
//       });
//   });

//   it("POST jacket object to api/items/sell creates a new jacket item", done => {
//     request(app)
//       .post("/api/items")
//       .set("Authorization", token)
//       .send({
//         type: "Jacket",
//         itemProps: {
//           designer: "Canali",
//           price: "2000",
//           measurements: {
//             backLength: "32",
//             chest: "21",
//             waist: "16",
//             shoulder: "17.5",
//             sleeve: "20"
//           },
//           colors: ["Grey"],
//           materials: ["Wool", "Cashmere"],
//           primaryPhoto: "http://via.placeholder.com/200x200"
//         }
//       })
//       .end((err, { body }) => {
//         assert(body.success);
//         done();
//       });
//   });
// });
