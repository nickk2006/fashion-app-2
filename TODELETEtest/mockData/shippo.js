exports.deliveryWebhook = {
  test: true,
  carrier: "shippo",
  data: {
    messages: [],
    carrier: null,
    tracking_number: "SHIPPO_DELIVERED",
    address_from: {
      city: "San Francisco",
      state: "CA",
      zip: "94103",
      country: "US"
    },
    address_to: { city: "Chicago", state: "IL", zip: "60611", country: "US" },
    eta: "2018-09-01T06:37:18.391",
    original_eta: "2018-08-31T06:37:18.391",
    servicelevel: { token: "shippo_priority", name: null },
    metadata: null,
    tracking_status: {
      object_created: "2018-09-02T06:37:18.403",
      object_updated: null,
      object_id: "db6eb3d0117a4ee687173a8dc6d5d7b7",
      status: "DELIVERED",
      status_details: "Your shipment has been delivered.",
      status_date: "2018-09-01T04:32:18.403",
      substatus: null,
      location: [Object]
    },
    tracking_history: [[Object], [Object], [Object], [Object]],
    transaction: null,
    test: true
  },
  event: "track_updated",
  metadata: "Shippo test webhook"
};
