module.exports = {
  _id: ObjectId("5b8b940d9806d620cae63089"),
  materials: {
    Cotton: 100,
    Cashmere: 0,
    Linen: 0,
    Wool: 0,
    Leather: 0,
    Denim: 0,
    Silk: 0,
    Other: 0
  },
  facets: {
    color: ["Blue", "Purple"],
    material: ["Cotton"],
    itemType: "Dress Shirt",
    designer: "Neil Barrett",
    "Dress Shirt-Chest": 10.25,
    "Dress Shirt-Length": 18.75,
    "Dress Shirt-Neck": 9.75,
    "Dress Shirt-Shoulder": 11.75,
    "Dress Shirt-Waist": 10.25
  },
  isDelivered: false,
  isTrackingSet: false,
  isClosed: false,
  holdUntil: ISODate("2018-09-02T07:48:35.755Z"),
  notifyWhenAvailable: [],
  isSold: true,
  photos: [
    "https://picsum.photos/300/400/?random_XX",
    "https://picsum.photos/300/400/?random_XX",
    "https://picsum.photos/300/400/?random_XX",
    "https://picsum.photos/300/400/?random_XX",
    "https://picsum.photos/300/400/?random_XX",
    "https://picsum.photos/300/400/?random_XX"
  ],
  price: 508,
  sellerId: ObjectId("5b8b94099806d620cae62b0a"),
  sellerUsername: "Margarete39",
  description:
    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium q",
  designerLine: "Collection Line",
  createdAt: ISODate("2018-09-02T07:41:02.556Z"),
  updatedAt: ISODate("2018-09-02T07:43:36.924Z"),
  primaryPhoto: "https://picsum.photos/300/400/?random_XX",
  __v: 0,
  holderSetBy: ObjectId("5b8b93fa9806d620cae625b0"),
  buyer: ObjectId("5b8b93fa9806d620cae625b0"),
  payment: {
    isCaptured: false,
    transfer_group: "5b8b940d9806d620cae63089",
    destination: "acct_1D4NTOFgHZucZrCT",
    transferDate: 0,
    isDisputed: false,
    amount: 47361,
    chargeId: "ch_1D5qFAKh6ah4cxtizgVhvPZW"
  },
  shipTo: {
    carrier: "",
    tracking_number: "",
    name: "Nicholas Kang",
    address: "3148 Highlander Road",
    city: "Fullerton",
    state: "CA",
    zip: 92833,
    country: "United States",
    countryCode: "US"
  }
};
