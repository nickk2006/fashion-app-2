const mongoose = require("mongoose");
// const User = require("../models/User");
const keys = require("../config/keys_dev_test");
before(done => {
  mongoose.connect(
    keys.mongoURI,
    { useNewUrlParser: true }
  );
  mongoose.connection.once("open", () => done()).on("error", err => {
    console.warn("warning", error);
  });
});
