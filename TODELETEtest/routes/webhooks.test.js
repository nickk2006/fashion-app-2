const expect = require("chai").expect;
const nock = require("nock");
const axios = require("axios");
const shippoData = require("../mockData/shippo");
describe("First test", () => {
  beforeEach(() => {
    nock("http://localhost:4040")
      .post("/api/webhooks/shippo/track-updated", shippoData.deliveryWebhook)
      .reply(200, shippoData.deliveryWebhook);
  });
  it("SHould assert true", () => {
    return axios
      .post("http://localhost:4040/api/webhooks/shippo/track-updated")
      .then(res => {
        console.log(res);
      });
  });
});
