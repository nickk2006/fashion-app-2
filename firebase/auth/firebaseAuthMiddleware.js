const verifyJwt = require("./firebaseVerifyJwt");
const { User } = require("../../models");
const util = require("util");

module.exports = function firebaseAuthMiddleware(req, res, next) {
  const token = req.header("Authorization");
  if (token) {
    verifyJwt(token)
      .then(firebaseUser => {
        if (!firebaseUser.isAnonymous) {
          User.findById(firebaseUser.uid)
            .populate("profiles")
            .populate({
              path: "itemsForSale",
              select: "facets.designer facets.itemType photos _id price"
            })
            .populate({
              path: "itemsPurchased",
              select: "facets.designer facets.itemType photos _id price"
            })
            .populate({
              path: "itemsSold",
              select: "facets.designer facets.itemType photos _id price"
            })
            .then(user => {
              if (user) {
                res.locals.user = user;
                next();
              } else {
                next(new Error("Could not find user"));
              }
            })
            .catch(e => next(new Error(e.toString())));
        } else {
          console.log("User does not have account");
          next(new Error("Incorrect auth header"));
        }
      })
      .catch(e => next(new Error(e.toString())));
  } else {
    console.log("Auth header not found");
    next(new Error("Auth header not found"));
  }
};
