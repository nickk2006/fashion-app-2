const firebaseAnonymousAuthMiddleware = require("./firebaseAnonymousAuthMiddleware");
const firebaseAuth = require("./firebaseAuthMiddleware");

module.exports = {
  firebaseAnonymousAuthMiddleware,
  firebaseAuth
};
