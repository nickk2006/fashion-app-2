const admin = require("firebase-admin");

module.exports = authorizationHeader =>
  admin.auth().verifyIdToken(authorizationHeader.split(" ")[1]);
