const verifyJwt = require("./firebaseVerifyJwt");
const { AnonymousUser } = require("../../models");

// module.exports = function(req, res, next) {
//   const token = req.header("Authorization");
//   if (token) {
//     verifyJwt(token)
//       .then(decodedToken => {
//         AnonymousUser.findOne({ firebaseUid: decodedToken.uid })
//           .populate("profiles")
//           .then(user => {
//             res.locals.user = user;
//             next();
//           });
//       })
//       .catch(e => next(new Error(e.toString())));
//   } else {
//     res.locals.user = { profiles: [] };
//   }
// };

module.exports = async function(req, res, next) {
  const token = req.header("Authorization");

  if (token) {
    try {
      const { uid } = await verifyJwt(token);

      const user = await AnonymousUser.findOneOrCreate(
        { firebaseUid: uid },
        { firebaseUid: uid }
      );

      console.log("user is", user);
      res.locals.user = user;

      next();
    } catch (e) {
      next(new Error(e.toString()));
    }
  }
};
