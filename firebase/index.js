const firebase = require("./config/firebaseSetup");
const storage = require("./config/storage");
module.exports = { firebase, storage };
