const Storage = require("@google-cloud/storage");
const credential = require("./serviceAccountKeys");
module.exports = new Storage({
  projectID: process.env.GC_PROJECT_ID,
  keyFilename: credential
});
