const admin = require("firebase-admin");
const credential = require("./serviceAccountKeys");

// const serviceAccount = require("./serviceAccountKeys");
// const serviceAccount = require("./serviceAccountKeys/firebaseKey.json");
module.exports = admin.initializeApp({
  credential: admin.credential.cert(credential),
  databaseURL: process.env.GC_DATABASE_URL
});
