// const fs = require('fs')
const path = require("path");
// const developmentKeys = require("./firebaseServiceAccountKeyDevelopment.json");
// const productionKeys = require("./firebaseServiceAccountKeyDevelopment.json");

const prodKeysPath = path.join(
  __dirname,
  "..",
  "serviceAccountKeys",
  "PRODUCTION_FIREBASE_SERVICE_KEYS.json"
);

const devKeysPath = path.join(
  __dirname,
  "..",
  "serviceAccountKeys",
  "DEVELOPMENT_FIREBASE_SERVICE_KEYS.json"
);

const serviceKeysPath =
  process.env.NODE_ENV === "production" ? prodKeysPath : devKeysPath;

// const serviceKey = require(serviceKeysPath);

// const readServiceKeys = (serviceKeysPath)

module.exports = serviceKeysPath;
