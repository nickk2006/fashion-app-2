// const mongoose = require("mongoose");
// const express = require("express");
// const firebaseAuth = require("../firebase/auth/firebaseAuthMiddleware");
// const router = express.Router();
// const { uploadPhoto } = require("../controllers/facetedSearch");
// const { storage } = require("../firebase");

// // @route   POST api/upload/test
// // @desc    Recieves and array of objects. Ojbects contain metadata of images to be uploaded (but no the images themselves). Generages signedURLs for each image and returns an array of signedURLs (in the order of the POST'ed array)
// // @access  Private
// router.post("/test", firebaseAuth, function(req, res, next) {
//   const itemPhotosBucket = storage.bucket(
//     `${process.env.GC_STORAGE_ITEM_PHOTOS_BUCKET_NAME}/large`
//   );
//   //name must include extension and must exclude the bucket name. e.g., "IMG_201231.jpg"
//   const photo = itemPhotosBucket.file("IMG_4413.jpg");
//   /*
//     const photo = itemPhotosBucket.file("IMG_4413.jpg");
//   curl -X PUT -T "/Users/macmini/Documents/IMG_4413.jpg" "https://storage.googleapis.com/walcroft-street-item-photos/IMG_4413.jpg?GoogleAccessId=fashionappiam%40fashionapp-210705.iam.gserviceaccount.com&Expires=1534990526&Signature=gYkL0T4KrqLvztvW7VrKKP6dWiAZpnuClj0mgLy3H09Wcm8cHqtc31eRYu7%2BGOp23Wo%2F7RfElhDm2RrcUNfuSFvLUoebhiExvmt3Z7YPXk0%2BtOMsXFXSGsV%2BeBOsMNfv%2BObFKkVS4iSZSPmEah1LEuEqEjiwQHJezMruNWjATagfOJrlGlDoRL0678Q%2FYqp6P3wV67DDDabip5QsbZjI5C4BB3ArmBxB2I7%2F8Gha9rCklVArK6d7cJmYzoOX7nUcAd9BaMXnHCSTNllD1jhvShZ%2FCgm5FVz55FnbEqp%2B8jMKaGS8jilPTW%2B8uArWEsTmzqZ%2FkEO5Ttv2ne8RGm5H3Q%3D%3D"
//   */
//   console.log("PHOTO NAME", req.body.photoName);
//   const expires = Date.now() + 1000000; //10 mins

//   const config = {
//     action: "write",
//     expires
//   };

//   photo
//     .getSignedUrl(config)
//     .then(url => res.status(200).json(url))
//     .catch(e => next(new Error(e.toString())));
// });

// module.exports = router;
