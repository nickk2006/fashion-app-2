const express = require("express");
const router = express.Router();
const Item2 = require("../models/itemTypes/MShirt");
const Measurement = require("../models/itemTypes/MShirtMeasurement");
// @route   GET api/test
// @desc    Get items.
// @access  Public
router.get("/", function(req, res, next) {
  return res.sendStatus(200);
});

// @route   POST api/test
// @desc    Post item.
// @access  Private
router.post("/", function(req, res, next) {
  Item2.create(req.body, function(err, item) {
    if (err) {
      return res.status(400).json({ err: err.toString() });
    } else {
      return res.status(200).json({ item });
    }
  });
});

router.post("/measurement", function(req, res, next) {
  Measurement.create(req.body, function(err, measurement) {
    if (err) {
      return res.status(400).json({ err: err.toString() });
    } else {
      return res.status(200).json({
        measurement,
        facet: measurement.facet
      });
    }
  });
});

router.post("/men/shirt");
router.post("/men/sweater");
router.post("/men/pants");
router.post("/men/jacket");

router.post("/women/shirt");
router.post("/women/sweater");
router.post("/women/pants");
router.post("/women/jacket");

module.exports = router;
