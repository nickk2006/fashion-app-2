const express = require("express");
const router = express.Router();
const stripe = require("../stripe/controllers");
const items = require("../controllers/items");

// @route   GET /api/cron/settleTransfers
// @desc    Finds transfsers due and completes them
// @access  Private
router.get(
  "/settle-transfers",
  items.findTransfersDue,
  stripe.main.settleTransfers
);

module.exports = router;
