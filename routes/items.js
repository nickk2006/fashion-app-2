const express = require("express");
const router = express.Router();
const {
  firebaseAuth,
  firebaseAnonymousAuthMiddleware
} = require("../firebase/auth");
const {
  facetedSearch,
  distinctFilters,
  populateMeasurements
} = require("../controllers/facetCatalog");
const { createItemDoc } = require("../controllers/facetedSearch");
const {
  readItem,
  readItemAndSetHold,
  updateUsersAndCatalogForPurchase,
  isShipping,
  verifySeller,
  updateItemForCaptureCharge,
  deleteItem
} = require("../controllers/items");
const { stripeControllers } = require("../stripe");
const shippoControllers = require("../shippo/controllers");
// const {
//   resizeForCatalog,
//   resizeForProductInfo
// } = require("../controllers/resizeImages");
const { sendgridControllers } = require("../sendgrid");
// @route   POST api/items/
// @desc    Creates Item doc from client form data.
// @access  Private
router.post("/", firebaseAuth, createItemDoc);

// @route   GET api/items/
// @desc    Filter functionality.
// @access  Private
router.get("/", firebaseAuth, populateMeasurements, facetedSearch);

// @route   GET api/items/anonymous
// @desc    Filters by anonymous profile. If no anonymous profile, res.locals.user = {profiles:[]}
// @access  Public
router.get(
  "/anonymous",
  firebaseAnonymousAuthMiddleware,
  populateMeasurements,
  facetedSearch
);

// @route   GET api/items/filters
// @desc    Get distinct filters. Will only be called on App mount or change in measurements and then stored in redux.
// @access  Private
router.get("/filters", firebaseAuth, populateMeasurements, distinctFilters);

// router.post("/test", cloudinary.parse.array("photos[]", 7), function(req, res) {
//   res.status(200).json(req.files);
// });

// router.post("/test/webhook", function(req, res) {
//   console.log(req.body);
//   res.sendStatus(200);
// });

// @route   GET api/items/filters
// @desc    Get distinct filters. Will only be called on App mount or change in measurements and then stored in redux.
// @access  Private
router.get(
  "/filters/anonymous",
  firebaseAnonymousAuthMiddleware,
  populateMeasurements,
  distinctFilters
);

// @route   GET api/items/:itemId/shipping
// @desc    returns {success: true} if user is buyer. returns {success: false} if user is not buyer
// @access  Private
router.get("/:itemId/shipping", firebaseAuth, isShipping);

// @route   POST api/items/:itemId/shipping
// @desc    1. Sets Item.shipTo.carrier and Item.shipTo.tracking_number. 2. captures charge from Stripe. 3. Updates Item for show charge has been captured. 4. Emails buyer tracking info. Flow for when seller inputs tracking info
// @access  Private
router.post(
  "/:itemId/shipping",
  firebaseAuth,
  verifySeller,
  shippoControllers.setupTracking,
  stripeControllers.main.captureCharge,
  updateItemForCaptureCharge,
  sendgridControllers.buyer.sendBuyerTrackingNotification
);

// @route   GET api/items/:itemId
// @desc    Get item from catalog by item id
// @access  Public
router.get(
  "/:itemId",
  readItem
  // resizeForProductInfo
);

// @route   POST api/items/:itemId
// @desc    Recieving Stripe Token and itemId. Handle purchase flow
// @access  Private
router.post(
  "/:itemId",
  firebaseAuth,
  readItemAndSetHold,
  stripeControllers.main.chargeCustomer,
  updateUsersAndCatalogForPurchase, //TODO: Use MongoDB Transactions functionality in the future
  sendgridControllers.seller.sendSellerPurchaseNotification
);

// @route   POST api/items/:itemId
// @desc    Recieving Stripe Token and itemId. Handle purchase flow
// @access  Private
router.delete("/:itemId", firebaseAuth, deleteItem);
module.exports = router;
