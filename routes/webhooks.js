const express = require("express");
const router = express.Router();
const shippoControllers = require("../shippo/controllers");
const itemsControllers = require("../controllers/items");
// @route   POST api/webhooks/shippo/track-updated
// @desc    Return current user
// @access  Private
router.post(
  "/shippo/track-updated",
  shippoControllers.validateShippoToken,
  shippoControllers.extractShippoData,
  itemsControllers.setTransferDate
);

module.exports = router;
