const router = require("express").Router();
const { main } = require("../stripe/controllers");

// @route   GET api/oauth/stripe
// @desc    Creates state param. Redirects user to stripe OAuth link with encoded state appended.
// @access  Private
router.get("/stripe", main.redirectToStripe);

// @route   POST api/oauth/stripe
// @desc    Checks state param. POSTs to stripe token endpoint to get Stripe Account ID. ID is saved into database.
// @access  Private
router.post("/stripe", main.getAccountId);

module.exports = router;
