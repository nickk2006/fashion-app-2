const express = require("express");
const router = express.Router();
const {
  firebaseAuth,
  firebaseAnonymousAuthMiddleware
} = require("../firebase/auth");
const { resizeForCatalog } = require("../controllers/resizeImages");
const {
  register,
  login,
  current,
  logout,
  isUsernameUnique,
  getUserProfile
} = require("../controllers/user");
const { sendgridControllers } = require("../sendgrid");
const {
  createMeasurement,
  createAnonymousMeasurement,
  readMeasurement,
  readMeasurements
} = require("../controllers/measurements");

// @route   POST api/users/register
// @desc    Register user
// @access  Public
router.post(
  "/register",
  register,
  sendgridControllers.auth.sendRegistrationEmail
);

// @route   POST api/users/login
// @desc    Login user / Returning JWT Token
// @access  Public
router.post("/login", login);

router.get("/check-username", isUsernameUnique);

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get("/current", firebaseAuth, current);

// @route   GET api/users/logout
// @desc    Logs out current user (if any)
// @access  Public
router.get("/logout", logout);

// @route   POST api/users/measurements/:measurementType
// @desc    Creates a measurement profile for user
// @access  Private
router.post("/measurements", firebaseAuth, createMeasurement);

// @route   POST api/users/measurements/:measurementType
// @desc    Creates a measurement profile for user
// @access  Private
router.post(
  "/measurements/anonymous",
  firebaseAnonymousAuthMiddleware,
  createAnonymousMeasurement
);

// @route   GET api/users/measurements/:measurementId
// @desc    Reads a user's single measurement profile
// @access  Private
router.get("/measurements/:measurementId", firebaseAuth, readMeasurement);

// @route   GET api/users/measurements
// @desc    Reads all of the current user's measurement profiles
// @access  Private
router.get("/measurements", firebaseAuth, readMeasurements);

// @route   GET api/users/my-profile/
// @desc    Reads all of the user's item for sale
// @access  Private
router.get("/my-profile", firebaseAuth, current);

// @route   POST api/users/measurements
// @desc    Creates item measurement specification for current user
// @access  Private
// router.post(
//   "/measurements",
//   firebaseAuth,
//   measurements
// );

// @route   GET api/users/profile/:username
// @desc    Reads itemsForSale by username
// @access  Public
router.get("/profile/:username", getUserProfile, resizeForCatalog);

module.exports = router;
