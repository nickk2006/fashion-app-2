const Validator = require("validator");
const isEmpty = require("lodash/isEmpty");

const materialsValidate = function(materials){
    const errors = {}
    if(isEmpty(materials.cotton) || isEmpty(materials.cashmere) || isEmpty(materials.linen) || isEmpty(materials.wool) || isEmpty(materials.leather) || isEmpty(materials.denim) || isEmpty(materials.silk) || isEmpty(materials.other)){
        errors.all = "Please correct materials portion"
        return errors
    }

    if(Validator.isNumeric(materials.cotton) || Validator.isNumeric(materials.cashmere) || Validator.isNumeric(materials.linen) || Validator.isNumeric(materials.wool) || Validator.isNumeric(materials.leather) || Validator.isNumeric(materials.denim) || Validator.isNumeric(materials.silk) || Validator.isNumeric(materials.other)){
    }
}

module.export = function validateSellInput(data){
    let errors = {}

    if(Validator.isEmpty(data.designer)) {
        errors.designer = "Please include the item's designer"
    }

    if(!Validator.isCurrency(price)) {
        errors.price = "Please enter a valid price"
    }

    if(typeof data.colors === 'array'){
        //TODO: check that colors is part of the enum of colors (Need to consult documentation on correct function to use)
        
        if(data.colors.length > 3) {
            errors.colors = "Please only include up to three colors"
        }
    } else {
        errors.colors = "Please enter a valid color"
    }

    if(!isEmpty(data.materials)){

    }
}

