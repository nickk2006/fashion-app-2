const express = require("express");
const router = express.Router();

// @route   POST /api/webhooks/stripe
// @desc    1. Sets Item.shipTo.carrier and Item.shipTo.tracking_number. 2. captures charge from Stripe. 3. Updates Item for show charge has been captured. 4. Emails buyer tracking info
// @access  Private
router.post("/transfer/succeeded");

module.exports = router;
