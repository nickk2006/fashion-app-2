const stripeControllers = require("./controllers");
const stripeRoutes = require("./routes");
module.exports = {
  stripeControllers,
  stripeRoutes
};
