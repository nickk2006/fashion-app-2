const request = require("superagent");
const { oauth, amountToSeller } = require("../../helpers");
const { User, Item } = require("../../models");
const stripe = require("../../config/stripe");
exports.redirectToStripe = async function(req, res) {
  const userId = res.locals.user._id;
  const { encodedState, decodedState } = oauth.getState();
  const redirect_uri = `${process.env.BASE_URL}/users/auth/callback/stripe`;
  const clientId = process.env.STRIPE_CLIENT_ID;

  await User.findByIdAndUpdate(userId, {
    $set: { stripeAuthState: decodedState }
  });

  const oauthLink = `https://connect.stripe.com/express/oauth/authorize?redirect_uri=${redirect_uri}&client_id=${clientId}&state=${encodedState}`;

  // return res.status(200).json(oauthLink);

  return res.status(200).json(oauthLink);
};

exports.getAccountId = async function(req, res, next) {
  const { code } = req.body;
  const externalState = req.body.state;
  const internalState = res.locals.user.stripeAuthState;
  const userId = res.locals.user._id;

  const isStateValid = oauth.checkState(internalState, externalState);

  if (!isStateValid) {
    const error = new Error("Stripe auth states do not match");
    error.status = 409;
    return next(error);
  }

  try {
    const credentials = await request
      .post("https://connect.stripe.com/oauth/token")
      .send({
        code,
        grant_type: "authorization_code",
        client_secret: process.env.STRIPE_SECRET_KEY
      });
    const { stripe_user_id } = JSON.parse(credentials.text);

    await User.findByIdAndUpdate(userId, {
      $set: { stripeCustomerId: stripe_user_id, stripeAuthState: "" }
    });

    return res.status(200).json({ success: true });
  } catch (e) {
    next(new Error(e.toString()));
  }
};

const getStripeId = _id =>
  User.findById(_id, "stripeCustomerId")
    .exec()
    .then(userObject => userObject.stripeCustomerId);

exports.chargeCustomer = async (req, res, next) => {
  const { stripeToken } = req.body;
  const { item, user } = res.locals;
  const grossPrice = item.price;
  const sellerAmount = amountToSeller(grossPrice) * 100;
  const sellerId = item.sellerId;
  const description = `${item.facets.designer} ${item.facets.itemType}`;
  const buyerEmail = user.email;
  try {
    const sellerStripeId = await getStripeId(sellerId);
    const transfer_group = item._id.toString();

    const charge = await stripe.charges.create({
      amount: grossPrice * 100,
      description,
      currency: "usd",
      source: stripeToken.id,
      transfer_group,
      capture: false,
      receipt_email: buyerEmail,
      metadata: {
        itemId: item._id.toString()
      }
    });

    const payment = {
      isCaptured: false,
      transfer_group,
      destination: sellerStripeId,
      amount: sellerAmount,
      chargeId: charge.id
    };

    res.locals.payment = payment;
  } catch (e) {
    return next(new Error(e.toString()));
  }
  next();
};

exports.captureCharge = async (req, res, next) => {
  const { chargeId } = res.locals.item.payment;
  const { captured } = await stripe.charges.capture(chargeId);
  if (captured) {
    // res.status(200).json({ success: true });
    return next();
  } else {
    return next(new Error(`Error capturing charge for charge id ${chargeId}`));
  }
};

exports.settleTransfers = async (req, res, next) => {
  const { items } = res.locals;

  try {
    const test = items.map(async ({ transfer_group, destination, amount }) => {
      const transfer = await stripe.transfers.create({
        amount,
        currency: "usd",
        destination,
        transfer_group
      });
      return transfer;
    });
    next();
  } catch (e) {
    console.log(e.toString());
    next(e)
  }
};
