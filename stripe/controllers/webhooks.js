const keys = require("../../config/keys");
const stripe = require("../../config/stripe");
const webhookConstants = require("../webhooks.constants");
exports.getEvent = (req, res, next) => {
  try {
    let sig = req.headers["stripe-signature"];
    let event = stripe.webhooks.constructEvent(
      req.body,
      sig,
      process.env.STRIPE_ENDPOINT_SECRET
    );
    if (event) {
      console.log("event", event);
      res.locals.event = event;
      res.sendStatus(200);
      next();
    } else {
      const error = new Error("No event dectected");
      error.status = 401;
      return next(error);
    }
  } catch (e) {
    return next(e.toString());
  }
};

exports.handleChargeSucceeded = (req, res, next) => {
  if (req.body.type !== webhookConstants.CHARGE_SUCCEEDED) {
    next();
  } else {
  }
};
