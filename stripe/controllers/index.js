const main = require("./stripe");
const webhooks = require("./webhooks");
module.exports = {
  main,
  webhooks
};
