const mongoose = require("mongoose");
const expect = require("chai").expect;

const {
  items: { WShirt, MShirt }
} = require("../../models/itemTypes");

const wShirtPayload = {
  designer: "Canali",
  photos: ["TestPhoto1", "TestPhoto2"],
  sellerId: "5b93574a82ebd209cb73be7a",
  sellerUsername: "Seller Username 1",
  color: ["Blue", "Green"],
  description: "Lorum ipsum",
  designerLine: "Exclusive Line",
  itemType: "WShirt",
  material: [
    {
      name: "Cotton",
      amount: 50
    },
    {
      name: "Cashmere",
      amount: 25
    },
    {
      name: "Silk",
      amount: 25
    }
  ],
  price: 1000,
  measurement: {
    "Sleeve Length": 11,
    Bust: 11,
    Waist: 11,
    "Body Length": 11
  }
};

const mShirtPayload = {
  designer: "Canali",
  photos: ["TestPhoto1", "TestPhoto2"],
  sellerId: "5b93574a82ebd209cb73be7a",
  sellerUsername: "Seller Username 1",
  color: ["Blue", "Green"],
  description: "Lorum ipsum",
  designerLine: "Exclusive Line",
  itemType: "MShirt",
  material: [
    {
      name: "Cotton",
      amount: 50
    },
    {
      name: "Cashmere",
      amount: 25
    },
    {
      name: "Silk",
      amount: 25
    }
  ],
  price: 1000,
  measurement: {
    "Sleeve Length": 11,
    Chest: 11,
    Waist: 11,
    "Body Length": 11
  }
};

describe.skip("Item Models", async function() {
  let db;

  before(function(done) {
    mongoose.connect(
      "mongodb://localhost/test",
      { useNewUrlParser: true }
    );
    db = mongoose.connection;
    db.on("error", console.error.bind(console, "conenction error"));
    db.once("open", function() {
      console.log("Connected to test database.");
      done();
    });
  });

  after(function(done) {
    db.close(function(e) {
      if (e) {
        throw e;
      }
      done();
    });
  });

  it("creates WShirt Item doc", async function() {
    try {
      const wShirt = await WShirt.create(wShirtPayload);
      expect(wShirt.itemType).to.equal("WShirt");
      expect(wShirt.measurement["Sleeve Length"]).to.equal(11);
      expect(wShirt.facets.female).to.equal(true);
    } catch (e) {
      throw new Error(e);
    }
  });

  it("creates MShirt Item doc", async function() {
    try {
      const mShirt = await MShirt.create(mShirtPayload);
      expect(mShirt.itemType).to.equal("MShirt");
      expect(mShirt.measurement["Sleeve Length"]).to.equal(11);
      expect(mShirt.facets.male).to.equal(true);
    } catch (e) {
      throw new Error(e);
    }
  });
});
