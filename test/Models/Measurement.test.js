const mongoose = require("mongoose");
const expect = require("chai").expect;

const {
  profiles: { WShirt, MShirt }
} = require("../../models/itemTypes");

const wShirtPayload = {
  name: "My test profile",
  itemType: "WShirtMeasurement",
  measurement: {
    "Sleeve Length.min": 11,
    "Sleeve Length.max": 12,
    "Bust.min": 11,
    "Bust.max": 12,
    "Waist.min": 11,
    "Waist.max": 12,
    "Body Length.min": 11,
    "Body Length.max": 12
  }
};

const mShirtPayload = {
  name: "My test profile",
  itemType: "MShirtMeasurement",
  measurement: {
    "Sleeve Length.min": 11,
    "Sleeve Length.max": 12,
    "Chest.min": 11,
    "Chest.max": 12,
    "Waist.min": 11,
    "Waist.max": 12,
    "Body Length.min": 11,
    "Body Length.max": 12
  }
};

describe.skip("Women Shirt Measurement Profile", async function() {
  let db;

  before(function(done) {
    mongoose.connect(
      "mongodb://localhost/test",
      { useNewUrlParser: true }
    );
    db = mongoose.connection;
    db.on("error", console.error.bind(console, "conenction error"));
    db.once("open", function() {
      console.log("Connected to test database.");
      done();
    });
  });

  after(function(done) {
    db.close(function(e) {
      if (e) {
        throw e;
      }
      done();
    });
  });
  afterEach(function(done) {
    db.dropCollection("measurementprofile2", function(err) {
      if (err) {
        throw new Error(err);
      } else {
        done();
      }
    });
  });

  it("creates WShirt Measurement doc", async function() {
    try {
      const wShirt = await WShirt.create(wShirtPayload);
      expect(wShirt.name).to.equal("My test profile");
      expect(wShirt.itemType).to.equal("WShirtMeasurement");
      expect(wShirt.measurement["Sleeve Length"].min).to.equal(11);
    } catch (e) {
      throw new Error(e);
    }
  });
  it("gets WShirt Measurement facets", async function() {
    try {
      const wShirt = await WShirt.create(wShirtPayload);
      expect(wShirt.facet.measurement.Waist["$gte"]).to.equal(11);
    } catch (e) {
      throw new Error(e);
    }
  });

  it("creates MShirt Measurement doc", async function() {
    try {
      const mShirt = await MShirt.create(mShirtPayload);
      expect(mShirt.name).to.equal("My test profile");
      expect(mShirt.itemType).to.equal("MShirtMeasurement");
      expect(mShirt.measurement["Sleeve Length"].min).to.equal(11);
    } catch (e) {
      throw new Error(e);
    }
  });
  it("gets MShirt Measurement facets", async function() {
    try {
      const mShirt = await MShirt.create(mShirtPayload);
      expect(mShirt.facet.measurement.Waist["$gte"]).to.equal(11);
    } catch (e) {
      throw new Error(e);
    }
  });
});
