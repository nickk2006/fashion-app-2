const { generateIdToken } = require("./firebase");
const token = require("./firebaseToken");
module.exports = { generateIdToken, token };
