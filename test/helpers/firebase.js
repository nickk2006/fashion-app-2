const path = require("path");
const events = require("events");
const stream = require("stream");
const _ = require("lodash");
const jwt = require("jsonwebtoken");
const certificateObject = require("./DEVELOPMENT_FIREBASE_SERVICE_KEYS");

const ONE_HOUR_IN_SECONDS = 60 * 60;
const ALGORITHM = "RS256";
const projectId = "project_id";
const uid = "someUid";
const developerClaims = {
  one: "uno",
  two: "dos"
};
/**
 * Generates a mocked Firebase ID token.
 *
 * @return {string} A mocked Firebase ID token with any provided overrides included.
 */
exports.generateIdToken = function() {
  const options = _.assign({
    audience: certificateObject.project_id,
    expiresIn: ONE_HOUR_IN_SECONDS,
    issuer: "https://securetoken.google.com/" + certificateObject.project_id,
    subject: uid,
    algorithm: ALGORITHM,
    header: {
      kid: certificateObject.private_key_id
    }
  });

  return jwt.sign(developerClaims, certificateObject.private_key, options);
};
