const react = require("../client/src/_constants/facets.constants");
const node = require("../helpers/constants/facets");
const _ = require("lodash");
const expect = require("chai").expect;

describe.only("Measurement Constants", () => {
  it("Checks that the itemTypes array in both files are equal", function(done) {
    expect(_.isEqual(react.itemTypes, node.itemTypes)).to.be.true;
    done();
  });

  it("Checks that the itemNameNapping object in both files are equal", function(done) {
    expect(_.isEqual(react.itemNameMapping, node.itemNameMapping)).to.be.true;
    done();
  });

  it("Checks that the itemTypes array and the individual item type arrays in both files are equal to one another", function(done) {
    _.map(react.itemTypes, itemType => {
      expect(_.isEqual(react[itemType], node[itemType])).to.be.true;
    });
    _.map(node.itemTypes, itemType => {
      expect(_.isEqual(react[itemType], node[itemType])).to.be.true;
    });
    done();
  });

  it("Checks that allMeasurementNames has every measurement for each item type in the itemType array", function(done) {
    _.map(node.itemTypes, itemType => {
      _.map(node[itemType], measurementName => {
        expect(node.allMeasurementNames).to.include(measurementName);
      });
    });
    done();
  });

  it("Checks that itemNameMapping object has all of the itemType's measurements", function(done) {
    _.map(react.itemTypes, itemType => {
      _.map(react[itemType], measurementName => {
        expect(react.itemNameMapping).to.have.property(measurementName);
      });
    });

    _.map(node.itemTypes, itemType => {
      _.map(node[itemType], measurementName => {
        expect(node.itemNameMapping).to.have.property(measurementName);
      });
    });

    _.map(react.itemTypes, itemType => {
      _.map(node[itemType], measurementName => {
        expect(react.itemNameMapping).to.have.property(measurementName);
      });
    });

    _.map(node.itemTypes, itemType => {
      _.map(react[itemType], measurementName => {
        expect(node.itemNameMapping).to.have.property(measurementName);
      });
    });

    done();
  });

  describe('react.itemTypeMappingWithMeasurements ("the Object") tests', function() {
    it("the Object has every measurement as a key", function(done) {
      _.map(react.itemTypes, itemType => {
        _.map(react[itemType], measurement => {
          expect(react.itemTypeMappingWithMeasurements).to.have.property(
            measurement
          );
        });
      });
      done();
    });
    it("Each property in the Object has the correct fullName value", function(done) {
      _.map(react.itemTypes, itemType => {
        _.map(react[itemType], measurement => {
          expect(
            react.itemTypeMappingWithMeasurements[measurement].fullName
          ).to.equal(measurement);
        });
      });
      done();
    });

    it("Each property in the Object has the correct name  value", function(done) {
      _.map(react.itemTypes, itemType => {
        _.map(react[itemType], measurement => {
          expect(
            react.itemTypeMappingWithMeasurements[measurement].name
          ).to.equal(react.itemNameMapping[measurement]);
        });
      });
      done();
    });

    it("Each property in the Object has a min value less than the max value", function(done) {
      _.map(react.itemTypes, itemType => {
        _.map(react[itemType], measurement => {
          expect(
            react.itemTypeMappingWithMeasurements[measurement].min
          ).to.be.lessThan(
            react.itemTypeMappingWithMeasurements[measurement].max
          );
        });
      });
      done();
    });

    it("Each key-value pair in the Object has a description key and the description value is of type string", function(done) {
      _.map(react.itemTypes, itemType => {
        _.map(react[itemType], measurement => {
          expect(
            typeof react.itemTypeMappingWithMeasurements[measurement]
              .description
          ).to.equal("string");
        });
      });
      done();
    });
  });

  it("Checks that node.measurementSchema has every measurement as a key", function(done) {
    _.map(node.itemTypes, itemType => {
      _.map(node[itemType], measurementName => {
        expect(node.measurementSchema).to.have.property(measurementName);
      });
    });
    done();
  });

  it("Checks that node.measurementProfileSchema has a min and max key for each measurement", function(done) {
    _.map(node.itemTypes, itemType => {
      _.map(node[itemType], measurementName => {
        expect(node.measurementProfileSchema).to.have.property(
          `${measurementName}.min`
        );
        expect(node.measurementProfileSchema).to.have.property(
          `${measurementName}.max`
        );
      });
    });
    done();
  });

  describe("Initial Measurement Values", () => {
    const { initialMeasurementValues } = react;
    const itemTypesPresent = Object.keys(initialMeasurementValues);
    it("Checks react.initialMeasurementValues has every item type", function(done) {
      expect(itemTypesPresent.length).to.eq(
        react.itemTypes.length,
        "initialMeasurementValues does not have the same item types as the itemType array"
      );
      itemTypesPresent.map(itemType => {
        expect(react.itemTypes).to.include(itemType);
      });
      done();
    });
    it("Checks that every item type has every size as a key value pair", function(done) {
      _.map(itemTypesPresent, itemType => {
        const sizesPresent = Object.keys(initialMeasurementValues[itemType]);
        expect(sizesPresent.length).to.eq(react.size.length);
        _.map(sizesPresent, size => {
          expect(react.size).to.include(size);
        });
      });
      done();
    });
    it("Checks that every size has correct itemType, size, and name values", function(done) {
      _.map(itemTypesPresent, itemType => {
        expect(
          Object.keys(initialMeasurementValues[itemType])
        ).to.have.lengthOf(react.size.length);

        _.map(react.size, size => {
          const initialMeasurement = initialMeasurementValues[itemType][size];
          expect(initialMeasurement.itemType).to.eq(itemType);
          expect(Object.keys(initialMeasurementValues[itemType])).to.include(
            size
          );
          expect(initialMeasurement.name).to.exist;
        });
      });
      done();
    });
    it("Checks that every size has correct measurements keys", function(done) {
      _.map(itemTypesPresent, itemType => {
        _.map(react.size, size => {
          _.map(react[itemType], measurement => {
            expect(initialMeasurementValues[itemType][size]).to.have.property(
              measurement
            );

            const measurementMinMax =
              initialMeasurementValues[itemType][size][measurement];

            expect(measurementMinMax).to.have.property("min");
            expect(measurementMinMax).to.have.property("max");
            expect(measurementMinMax.min).to.be.lessThan(measurementMinMax.max);
          });
        });
      });
      done();
    });
  });
});
