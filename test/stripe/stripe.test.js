require("dotenv").config({ path: __dirname + `/../.env.test` });

const request = require("supertest");
const expect = require("chai").expect;
const sinon = require("sinon");

describe("Stripe routes", function() {
  let app, auth, itemsController, item;

  beforeEach(function() {
    auth = require("../../firebase/auth");
    itemsController = require("../../controllers/items");
    sinon.stub(auth, "firebaseAuth").callsFake(function(req, res, next) {
      res.locals.user = {
        username: "testUnit",
        _id: "5b9749c4432d850ecffc28c5"
      };
      return next();
    });
    sinon
      .stub(itemsController, "readItemAndSetHold")
      .callsFake(function(req, res, next) {
        res.locals.item = {
          _id: "5b9c96fcb66d410cc5243aff",
          price: 100,
          sellerId: "testsellerid",
          facets: {
            designer: "Gucci",
            itemType: "Men Casual Shirt"
          },
          isSold: false
        };
        res.locals.user = {
          email: "nickk2006@gmail.com"
        };
        return next();
      });
    sinon
      .stub(itemsController, "updateUsersAndCatalogForPurchase")
      .callsFake(function(req, res, next) {
        return next();
      });
    app = require("../../app");
  });

  afterEach(function() {
    auth.firebaseAuth.restore();
    itemsController.readItemAndSetHold.restore();
    itemsController.updateUsersAndCatalogForPurchase.restore();
  });

  it("POST to /api/items/:itemId", function(done) {
    this.timeout(10000);

    request(app)
      .post("/api/items/5b9c96fcb66d410cc5243aff")
      .send({
        stripeToken: "tok_mastercard"
      })
      .expect(200)
      .end(function(err, res) {
        console.log("error is", err);
        console.log("\n\nres.body is", res.body);
        done();
      });
  });
});
