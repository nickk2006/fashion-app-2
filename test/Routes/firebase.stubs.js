const sinon = require("sinon");
const auth = require("../../firebase/auth");

exports.stubFirebaseAuthMiddleware = user =>
  sinon.stub(auth, "firebaseAuth").callsFake(function(req, res, next) {
    res.locals.user = user;
    return next();
  });

exports.stubFirebaseAnonymousAuthMiddleware = anonUser =>
  sinon
    .stub(auth, "firebaseAnonymousAuthMiddleware")
    .callsFake(function(req, res, next) {
      res.locals.user = anonUser;
      return next();
    });
