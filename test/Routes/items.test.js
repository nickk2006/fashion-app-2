require("dotenv").config({ path: __dirname + `/../.env.test` });
console.log("process env is", process.env.ENV_VERSION, __dirname);
const request = require("supertest");
const expect = require("chai").expect;
const mongoose = require("mongoose");
const sinon = require("sinon");
const { Item } = require("../../models");
describe("items routes", function() {
  let app, auth;

  beforeEach(function() {
    auth = require("../../firebase/auth");

    sinon.stub(auth, "firebaseAuth").callsFake(function(req, res, next) {
      res.locals.user = {
        username: "testUnit",
        _id: "5b9749c4432d850ecffc28c5"
      };
      return next();
    });

    app = require("../../app");
  });

  afterEach(function() {
    auth.firebaseAuth.restore();
  });

  it("POST to api/items/", function(done) {
    this.timeout(10000);

    request(app)
      .post("/api/items")
      .send({
        photos: [
          "https://res.cloudinary.com/fashionapp/image/upload/v1536987703/y7gikkiklxebw12c8zek.jpg",
          "https://res.cloudinary.com/fashionapp/image/upload/v1536987703/vb1ljtn1ep474n8gffc7.jpg"
        ],
        itemType: "Men Casual Shirt",
        measurement: {
          "Men Casual Shirt-Chest": "15",
          "Men Casual Shirt-Length": "23.75",
          "Men Casual Shirt-Shoulder": "15",
          "Men Casual Shirt-Waist": "15"
        },
        color: ["Black"],
        material: [{ name: "Linen", amount: "100" }],
        designer: "Givenchy",
        price: "1000",
        description: "Test Description"
      })
      .expect(200)
      .end(function(err, res) {
        const id = res.body._id;
        expect(mongoose.Types.ObjectId.isValid(id)).to.equal(true);
        Item.findByIdAndRemove(id).then(() => {
          done();
        });
      });
  });

  describe("POST /", function() {
    it("test", function(done) {
      expect(true).to.equal(true);
      done();
    });
  });
});
