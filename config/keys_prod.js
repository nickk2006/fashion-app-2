module.exports = {
  mongoURI: process.env.MONGO_URI,
  secretOrKey: process.env.SECRET_OR_KEY,
  stripeEndpointSecret: process.env.STRIPE_ENDPOINT_SECRET
};
