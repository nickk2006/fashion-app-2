import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../../_reducers';
import PropTypes from 'prop-types';

const Root = ({ children, initialState = {} }) => {
  const store = createStore(reducers, initialState, applyMiddleware(thunk));
  return <Provider store={store}>{children}</Provider>;
};

Root.protoTypes = {
  initialState: PropTypes.object.isRequired
};

export default Root;
