export default function(wrapper, inputSelector, newValue) {
  const input = wrapper.find(inputSelector);
  console.log('input', input.debug());
  input.simulate('change', {
    target: { value: newValue }
  });
  return wrapper.find(inputSelector);
}
