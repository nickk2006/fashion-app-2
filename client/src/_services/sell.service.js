import { instance } from "./axiosConfig";
import { getBearerHeader } from "./helpers";

const uploadSellItem = async data => {
  const headers = await getBearerHeader();
  return instance({
    method: "post",
    url: "/api/items",
    headers,
    data
  });
};
export { uploadSellItem };
