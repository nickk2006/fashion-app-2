import firebase from "../firebase";

const getBearerHeader = async () => {
  const token = await firebase.auth().currentUser.getIdToken();
  return { Authorization: `Bearer ${token}` };
};

const isAnonymous = () => firebase.auth().currentUser.isAnonymous;

export { getBearerHeader, isAnonymous };
