import { instance } from "./axiosConfig";
import { getBearerHeader, isAnonymous } from "./helpers";
const getItemsRequest = async (params = {}) => {
  const header = await getBearerHeader();
  const isAnon = isAnonymous();
  const url = isAnon ? "/api/items/anonymous" : "/api/items";
  return instance({
    method: "get",
    url,
    params,
    headers: header
  });
};

const getAllItemsForSaleByUser = async () => {
  const headers = await getBearerHeader();

  return instance.get("/api/users/my-profile", { headers });
};

const getItemRequest = id => instance.get(`/api/items/${id}`);

const createItem = async data => {
  const header = await getBearerHeader();

  return instance({
    method: "post",
    url: "/api/items",
    headers: header,
    data
  });
};

const deleteItem = async itemId => {
  const headers = await getBearerHeader();
  return instance({
    method: "delete",
    url: `/api/items/${itemId}`,
    headers
  });
};

const getFilters = async () => {
  const headers = await getBearerHeader();
  const isAnon = isAnonymous();
  const url = isAnon ? "/api/items/filters/anonymous" : "/api/items/filters";
  return instance({
    method: "get",
    url,
    headers
  });
};

const setHold = async id => {
  const headers = await getBearerHeader();
  return instance({
    method: "post",
    url: "/api/items/hold",
    headers,
    data: { id }
  });
};

const postPurchase = async ({ stripeToken, itemId, shipping }) => {
  const headers = await getBearerHeader();
  return instance({
    method: "post",
    url: `/api/items/${itemId}`,
    headers,
    data: { stripeToken, shipping }
  });
};

const getTracking = async itemId => {
  const headers = await getBearerHeader();
  return instance({
    method: "get",
    url: `/api/items/${itemId}/shipping`,
    headers
  });
};

const postTracking = async (data, itemId) => {
  const headers = await getBearerHeader();
  return instance({
    method: "post",
    url: `/api/items/${itemId}/shipping`,
    headers,
    data
  });
};

const uploadImageToCloudinary = file =>
  instance({
    method: "post",
    url: `https://api.cloudinary.com/v1_1/${
      process.env.REACT_APP_CLOUDINARY_CLOUD_NAME
    }/image/upload`,
    data: {
      file,
      upload_preset: process.env.REACT_APP_CLOUDINARY_ITEM_PRESET
    }
  });

// eslint-disable-next-line
export {
  getFilters,
  getItemsRequest,
  getItemRequest,
  getAllItemsForSaleByUser,
  createItem,
  setHold,
  postPurchase,
  postTracking,
  getTracking,
  deleteItem,
  uploadImageToCloudinary
};
