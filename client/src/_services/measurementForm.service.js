import { instance } from "./axiosConfig";
import { getBearerHeader, isAnonymous } from "./helpers";

const upload = async data => {
  const headers = await getBearerHeader();
  const isAnon = isAnonymous();
  const url = isAnon
    ? "/api/users/measurements/anonymous"
    : "/api/users/measurements";

  return instance({
    method: "post",
    url,
    headers,
    data
  });
};
const getAllMeasurementsForUser = async () => {
  const headers = await getBearerHeader();
  return instance({
    method: "get",
    url: "/api/users/measurements",
    headers
  });
};

export { upload, getAllMeasurementsForUser };
