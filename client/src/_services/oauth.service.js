import { instance } from "./axiosConfig";
import { getBearerHeader } from "./helpers";
import { completeStripeOAuthErrorHandler } from "../_actions";

const beginStripeOAuth = async () => {
  const headers = await getBearerHeader();
  return instance({
    method: "get",
    url: "/api/oauth/stripe",
    headers
  }).then(res => (window.location.href = res.data));
};

const completeStripeOAuth = ({ code, state }) =>
  getBearerHeader().then(headers =>
    instance({
      method: "post",
      url: "/api/oauth/stripe",
      headers,
      data: { code, state }
    })
  );

export { beginStripeOAuth, completeStripeOAuth };
