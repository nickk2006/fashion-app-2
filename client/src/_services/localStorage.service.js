import jwtDecode from "jwt-decode";

export const loadToken = () => {
  try {
    const serializedToken = localStorage.getItem("token");
    if (serializedToken === null) {
      return undefined;
    }
    return JSON.parse(serializedToken);
  } catch (e) {
    return undefined;
  }
};

export const saveToken = token => {
  try {
    const serializedToken = JSON.stringify(token);
    localStorage.setItem("token", serializedToken);
  } catch (e) {
    // Ignore write errors.
  }
};

export const isExpired = token => {
  try {
    const decoded = jwtDecode(token);
    const currentTime = Date.now().valueOf() / 1000;
    return decoded.exp < currentTime;
  } catch (e) {
    return true;
  }
};
