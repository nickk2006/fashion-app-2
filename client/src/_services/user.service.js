import { instance } from "./axiosConfig";
import { getBearerHeader } from "./helpers";

const login = user =>
  instance({
    method: "post",
    url: "/api/users/login",
    data: user
  });

const logout = () => {
  localStorage.removeItem("token");
};

const register = ({ username, email, password }) =>
  instance({
    method: "post",
    url: "/api/users/register",
    data: { username, email, password }
  });

const getUser = token =>
  instance({
    method: "get",
    url: "/api/users/current",
    headers: { Authorization: `Bearer ${token}` }
  });

const getStripeId = async authCode => {
  const header = await getBearerHeader();
  return instance.post(
    "/api/stripe/connect",
    {
      code: authCode,
      grant_type: "authorization_code"
    },
    {
      headers: header
    }
  );
};

const isUsernameUnique = username =>
  instance({
    method: "get",
    url: "api/users/check-username",
    params: {
      username
    }
  });

const getUserProfile = username =>
  instance({
    method: "get",
    url: `api/users/profile/${username}`
  });

export default {
  login,
  logout,
  register,
  getUser,
  getStripeId,
  isUsernameUnique,
  getUserProfile
};
