import axios from "axios";
import firebase from "../firebase";

const instance =
  process.env.NODE_ENV === "production"
    ? axios.create({
        baseURL: process.env.REACT_APP_BASE_URL
      })
    : axios.create({
        baseURL: "http://localhost:4040",
        timeout: 30000
      });

const stripeInstance = axios.create({
  baseURL: "https://connect.stripe.com"
});
export { instance, stripeInstance };
