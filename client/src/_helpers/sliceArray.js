export default (arr, size) => {
  var newArr = [];
  for (let i = 0; i < arr.length; i += size) {
    const slicedArr = arr.slice(i, i + size);
    newArr.push(slicedArr);
  }
  return newArr;
};
