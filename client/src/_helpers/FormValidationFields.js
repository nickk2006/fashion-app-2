import filter from "lodash/filter";

export default {
  required: value => (value ? undefined : "Required"),
  maxLength: max => value =>
    value && value.length > max
      ? `Must be ${max} characters or less`
      : undefined,
  minLength: min => value =>
    value && value.length < min
      ? `Must be ${min} characters or more`
      : undefined,
  number: value =>
    value && isNaN(Number(value)) ? "Must be a number" : undefined,
  positive: value =>
    value && isNaN(Number(value)) && Number(value) < 0
      ? "Must be a positive number"
      : undefined,
  isEmail: value =>
    value &&
    !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      String(value).toLowerCase()
    )
      ? "Invalid email address"
      : undefined,
  materialsEqual100: (value, { material }, props, name) => {
    const cleanMaterialArray = filter(material, material => !!material); //removes null elements
    const cleanMaterialAmountArray = filter(
      cleanMaterialArray,
      material => !!material.amount
    );
    const sum = cleanMaterialAmountArray.reduce(
      (acc, current) => acc + parseInt(current.amount),
      0
    );
    if (sum !== 100) {
      return "Must sum to 100";
    } else {
      return undefined;
    }
  }
};
