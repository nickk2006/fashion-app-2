import { stringify } from "qs";
import isEmpty from "lodash/isEmpty";
/**
 * Takes query object and returns string with '?' as first character
 */
export default queryObject =>
  !isEmpty(queryObject)
    ? stringify(queryObject, {
        arrayFormat: "brackets",
        addQueryPrefix: true
      })
    : null;
