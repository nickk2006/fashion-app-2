import ImageCompressor from "image-compressor.js";

const compressImage = (file, quality = 0.8) => {
  const compressedImagePromise = new Promise((resolve, reject) => {
    new ImageCompressor(file, {
      quality,
      success(blob) {
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onerror = function() {
          return reject(reader.error);
        };
        reader.onload = function() {
          return resolve(reader.result);
        };
      },
      error(err) {
        return reject(err);
      }
    });
  });
  return compressedImagePromise;
};

export default compressImage;
