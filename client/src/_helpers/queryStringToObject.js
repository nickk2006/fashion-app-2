import { parse } from "qs";
import isEmpty from "lodash/isEmpty";
/**
 * @param {string} queryString removes first character of string (which is normally a '?')
 */
export default queryString =>
  isEmpty(queryString) ? {} : parse(queryString, { ignoreQueryPrefix: true });
