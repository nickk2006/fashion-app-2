import React from "react";
import { Field } from "redux-form";
import FormLabel from "@material-ui/core/FormLabel";
import { TextField } from "redux-form-material-ui";

export default properties =>
  properties.map(p => (
    <div style={{ maxWidth: "10em", justifyContent: "center" }}>
      <div style={{ width: "8em" }}>
        <FormLabel>{p.label}</FormLabel>
      </div>
      <Field
        key={p.label}
        style={{ margin: "16px", maxWidth: "2em" }}
        id={p.id}
        name={p.name}
        component={TextField}
        type={p.type}
      />
    </div>
  ));
