import React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import NativeSelect from "@material-ui/core/NativeSelect";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
const styles = theme => ({
  notCompact: { width: theme.inputWidth.mobile.short },
  compact: { width: theme.inputWidth.mobile.number }
});

const NativePicker = ({
  classes,
  input,
  label,
  compact = false,
  min,
  max,
  meta: { error, active, submitFailed }
}) => {
  const optionsMap = [];
  for (let i = min; i <= max; i = i + 0.25) {
    optionsMap.push(
      <option key={i} value={i}>
        {`${i} in`}
      </option>
    );
  }

  return (
    <FormControl>
      <InputLabel htmlFor="color-picker">{label}</InputLabel>
      <NativeSelect
        error={submitFailed && !!error && !active}
        className={compact ? classes.compact : classes.notCompact}
        {...input}
      >
        <option value="" />
        {optionsMap}
      </NativeSelect>
    </FormControl>
  );
};

NativePicker.propTypes = {
  label: PropTypes.string,
  withButtons: PropTypes.bool
};

export default withStyles(styles)(NativePicker);
