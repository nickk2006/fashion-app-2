import React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import NativeSelect from "@material-ui/core/NativeSelect";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import includes from "lodash/includes";
const styles = theme => ({
  withButtons: { width: `${theme.inputWidth.mobile.long - 48}` },
  withoutButtons: { width: theme.inputWidth.mobile.long }
});

const NativePicker = ({
  classes,
  input,
  selectedOptions = [],
  label,
  options,
  isNumber = false,
  withButtons = false
}) => (
  <FormControl>
    <InputLabel htmlFor="color-picker">{label}</InputLabel>
    <NativeSelect
      className={withButtons ? classes.withButtons : classes.withoutButtons}
      {...input}
    >
      <option value="" />
      {options.map(o => (
        <option key={o} disabled={includes(selectedOptions, o)} value={o}>
          {o}
        </option>
      ))}
    </NativeSelect>
  </FormControl>
);

NativePicker.propTypes = {
  label: PropTypes.string,
  selectedOptions: PropTypes.array,
  options: PropTypes.arrayOf(PropTypes.string),
  withButtons: PropTypes.bool
};

export default withStyles(styles)(NativePicker);
