import React, { Component } from "react";
import { Field } from "redux-form";
import PropTypes from "prop-types";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

class CheckboxGroup extends Component {
  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    ).isRequired
  };

  field = ({ input, options }) => {
    const { name, onChange, onBlur, onFocus } = input;
    const inputValue = input.value;

    const checkboxes = options.map(({ label, value }, index) => {
      const handleChange = event => {
        const arr = [...inputValue];
        if (event.target.checked) {
          arr.push(value);
        } else {
          arr.splice(arr.indexOf(value), 1);
        }
        onBlur(arr);
        return onChange(arr);
      };
      const checked = inputValue.includes(value);
      return (
        <FormControlLabel
          key={`checkbox-${index}`}
          control={
            <Checkbox
              id={`${name}[${index}]`}
              name={`${name}[${index}]`}
              value={value}
              checked={checked}
              onChange={handleChange}
              onFocus={onFocus}
            />
          }
          label={label}
        />
      );
    });

    return <FormGroup>{checkboxes}</FormGroup>;
  };

  render() {
    return <Field {...this.props} type="checkbox" component={this.field} />;
  }
}

export default CheckboxGroup;
