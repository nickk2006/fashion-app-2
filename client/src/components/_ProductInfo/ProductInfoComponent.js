import React from "react";
import PhotoGallery from "../PhotoGallery";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import { Link } from "react-router-dom";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";
import TimeAgo from "react-timeago";
import isEmpty from "lodash/isEmpty";
import CheckoutButton from "../CheckoutButton";
import PropTypes from "prop-types";
import RegisterBtn from "../RegisterBtn";
const styles = theme => ({
  container: {
    // "flex-grow": 1
    // width: "100vw"
    // padding: `0px 1vw`,
    // background: "blue",
    width: "100%",
    maxWidth: "100vw"
  },
  detailsContainer: {
    display: "grid",
    margin: `0px ${theme.spacing.unit}px`
  },
  photosDetailsBreak: {
    height: "16px"
  },
  hrNoLineTall: theme.hr.noLine.tall,
  hrNoLineShort: theme.hr.noLine.short,
  checkout: {
    width: 300,
    margin: "0 auto",
    background: theme.palette.button.gradient,
    color: theme.palette.button.fontColor
  },

  photoGallery: {
    [theme.breakpoints.down("sm")]: {
      width: "100%"
    },
    [theme.breakpoints.up("md")]: {
      width: theme.breakpoints.values.sm,
      maxWidth: "100%"
    },
    maxHeight: "100%"
  }
});

const ProductInfoComponent = ({
  id,
  measurements,
  color,
  materials,
  isSold,
  itemType,
  designer,
  designerLine,
  price,
  sellerId,
  sellerUsername,
  createdAt,
  description,
  photos,
  openStripe,
  isPreview = false,
  token,
  classes: {
    container,
    detailsContainer,
    photosDetailsBreak,
    hrNoLineTall,
    hrNoLineShort,
    checkout,
    photoGallery,
    buttonContainer
  }
}) => {
  return (
    <div className={container}>
      <div className={photoGallery}>
        <PhotoGallery photos={photos} />
      </div>
      <div className={photosDetailsBreak} />

      <div className={detailsContainer}>
        <Typography variant="headline">{designer}</Typography>
        {!!designerLine && designerLine.length > 0 ? (
          <Typography variant="subheading">{designerLine}</Typography>
        ) : (
          undefined
        )}
        <Typography variant="subheading">{`$${price}`}</Typography>
        <hr className={hrNoLineTall} />
        <Typography variant="body1">{description}</Typography>
        <hr className={hrNoLineShort} />
        <List>
          <Typography variant="body2">Measurements:</Typography>
          {measurements.map(m => (
            <ListItemText
              primaryTypographyProps={{ variant: "body1" }}
              key={m.name}
            >{`${m.name} - ${m.amount} in`}</ListItemText>
          ))}
        </List>
        {!isEmpty(materials) ? (
          <List>
            <Typography variant="body2">Materials:</Typography>
            {materials.map(m => (
              <ListItemText
                primaryTypographyProps={{ variant: "body1" }}
                key={m.name}
              >{`${m.name} - ${m.amount}%`}</ListItemText>
            ))}
          </List>
        ) : null}
        <hr className={hrNoLineShort} />
        <div>
          <Typography variant="body1">Item Type: {itemType}</Typography>
        </div>
        <hr className={hrNoLineShort} />
        <div>
          <Typography variant="body1">
            Listed <TimeAgo date={createdAt} /> by{" "}
            {isPreview ? (
              `${sellerUsername}`
            ) : (
              <Link
                to={`/users/profile/${sellerUsername}`}
                style={{ color: "#000000" }}
              >
                {sellerUsername}
              </Link>
            )}{" "}
          </Typography>
        </div>
        <hr className={hrNoLineTall} />

        {isSold ? (
          <h3>Sold</h3>
        ) : isPreview ? (
          <Button disabled variant="contained" className={checkout}>
            Buy Now
          </Button>
        ) : !!token ? (
          <CheckoutButton
            disabled={isPreview}
            className={checkout}
            price={price}
            designer={designer}
            itemType={itemType}
            variant="contained"
            size="large"
            itemId={id}
          >
            <b>Buy Now</b>
          </CheckoutButton>
        ) : (
          <RegisterBtn itemId={id}>Sign up and buy</RegisterBtn>
        )}
      </div>
    </div>
  );
};

ProductInfoComponent.propTypes = {
  id: PropTypes.string,
  token: PropTypes.string,
  material: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })
  ),
  measurement: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })
  ),
  color: PropTypes.arrayOf(PropTypes.string),
  isSold: PropTypes.bool,
  itemType: PropTypes.string,
  designer: PropTypes.string,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  sellerId: PropTypes.string,
  sellerUsername: PropTypes.string,
  createdAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  description: PropTypes.string,
  photos: PropTypes.arrayOf(PropTypes.string)
};

export default withStyles(styles)(ProductInfoComponent);
