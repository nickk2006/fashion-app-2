import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import isEmpty from "lodash/isEmpty";
import { Helmet } from "react-helmet";
import ProductInfoComponent from "./ProductInfoComponent";
import { getItem as getItemActionCreator } from "../../_actions";

class ProductInfoContainer extends Component {
  componentDidMount() {
    const itemIdFromUrl = this.props.match.params.itemId;
    this.props.getItem(itemIdFromUrl);
  }

  render() {
    return !this.props.isFetchingItem && !isEmpty(this.props.id) ? (
      <div>
        <Helmet>
          <meta charSet="utf-8" />
          <title>{`${this.props.designer} ${this.props.itemType}`}</title>
        </Helmet>
        <ProductInfoComponent
          id={this.props.id}
          measurements={this.props.measurements}
          colors={this.props.colors}
          materials={this.props.materials}
          isSold={this.props.isSold}
          itemType={this.props.itemType}
          designer={this.props.designer}
          designerLine={this.props.designerLine}
          price={this.props.price}
          sellerId={this.props.sellerId}
          sellerUsername={this.props.sellerUsername}
          createdAt={this.props.createdAt}
          description={this.props.description}
          photos={this.props.photos}
          token={this.props.token}
        />
      </div>
    ) : (
      <div>
        <h3>Loading Item</h3>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  id: state.item.id,
  isFetchingItem: state.item.isFetchingItem,
  measurements: state.item.measurements,
  colors: state.item.colors,
  materials: state.item.materials,
  isSold: state.item.isSold,
  itemType: state.item.itemType,
  designer: state.item.designer,
  designerLine: state.item.designerLine,
  price: state.item.price,
  sellerId: state.item.sellerId,
  sellerUsername: state.item.sellerUsername,
  createdAt: state.item.createdAt,
  description: state.item.description,
  photos: state.item.photos,
  withFormatting: state.item.withFormatting,
  token: state.auth.user.token
});
//TODO: Create "hold" flow
const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(getItemActionCreator(id))
});

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ProductInfoContainer);
