import React, { Component } from "react";
import StripeButton from "../../assets/stripe.png";
import withStyles from "@material-ui/core/styles/withStyles";
import IconButton from "@material-ui/core/IconButton";
import { compose } from "redux";
import { connect } from "react-redux";
import { connectStripeOAuth } from "../../_actions";
import "./stripeButton.css";
class StripeConnectButton extends Component {
  render() {
    const { classes, connectStripe } = this.props;
    return (
      <div className={classes.root}>
        <button
          onClick={connectStripe}
          className="stripe-connect light-blue dark"
        >
          <span>Connect with Stripe</span>
        </button>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  connectStripe: () => dispatch(connectStripeOAuth())
});

const styles = theme => ({
  root: {
    flexGrow: 0,
    margin: `${theme.spacing.unit}px auto ${theme.spacing.unit * 3}px`
    // background: "transparent"
  }
});

export default compose(
  connect(
    null,
    mapDispatchToProps
  ),
  withStyles(styles)
)(StripeConnectButton);
