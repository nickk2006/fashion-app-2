import React, { Component } from "react";
import { Field } from "redux-form";
import NativePickerNumeric from "../../NativePicker/NativePickerNumeric";
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Modal from "@material-ui/core/Modal";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InfoIcon from "@material-ui/icons/InfoOutlined";
import { facets } from "../../../_constants";
import validations from "../../../_helpers/FormValidationFields";

const styles = theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 3,
    width: 250
  },
  textFieldFirst: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 250
  },
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  }
});

class MeasurementsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      title: "",
      description: ""
    };
  }

  handleOpen = (title, description) => {
    this.setState({
      isModalOpen: true,
      title,
      description
    });
  };

  handleClose = () => {
    this.setState({ isModalOpen: false });
  };

  render() {
    const {
      itemType,
      classes: { container, textField, textFieldFirst, paper }
    } = this.props;

    let measurements = facets[itemType];

    return (
      <div className={container}>
        {measurements.map((m, index) => (
          <div key={facets.itemTypeMappingWithMeasurements[m].name}>
            <Field
              component={NativePickerNumeric}
              min={facets.itemTypeMappingWithMeasurements[m].min}
              max={facets.itemTypeMappingWithMeasurements[m].max}
              className={index === 0 ? textFieldFirst : textField}
              id={facets.itemTypeMappingWithMeasurements[m].name}
              name={`measurement.${m}`}
              type="number"
              label={`${facets.itemTypeMappingWithMeasurements[m].name}`}
              validate={[validations.required]}
              InputProps={{
                endAdornment: <InputAdornment position="end" />
              }}
            />
            <IconButton
              aria-label="Click for more info"
              onClick={() =>
                this.handleOpen(
                  facets.itemTypeMappingWithMeasurements[m].name,
                  facets.itemTypeMappingWithMeasurements[m].description
                )
              }
            >
              <InfoIcon />
            </IconButton>
          </div>
        ))}
        <Modal
          aria-labelledby="title"
          aria-describedby="description"
          open={this.state.isModalOpen}
          onClose={this.handleClose}
        >
          <div className={paper}>
            <Typography variant="title">{this.state.title}</Typography>
            <Typography variant="subheading">
              {this.state.description}
            </Typography>
          </div>
        </Modal>
      </div>
    );
  }
}

export default withStyles(styles)(MeasurementsList);
