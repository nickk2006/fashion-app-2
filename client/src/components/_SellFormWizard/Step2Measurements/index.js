import S2Measurements from "./Step2MeasurementsComponent";
import { reduxForm, formValueSelector } from "redux-form";
import { connect } from "react-redux";
import { compose } from "redux";

const selector = formValueSelector("sell");

export default compose(
  reduxForm({
    form: "sell",
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
  }),
  connect(state => ({
    itemType: selector(state, "itemType")
  }))
)(S2Measurements);
