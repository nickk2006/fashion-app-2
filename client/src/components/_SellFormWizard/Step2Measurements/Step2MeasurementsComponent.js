import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import MeasurementsList from "./MeasurementsList";
import PrevNextButtons from "../../PrevNextButton";
import Typography from "@material-ui/core/Typography";

const styles = {
  container: {
    margin: 16
  },
  inputContainer: {
    margin: "0 0 8px 32px"
  }
};

const S2MeasurementsComponent = ({
  previousPage,
  handleSubmit,
  itemType,
  page,
  classes: { container }
}) => {
  return (
    <form className={container} onSubmit={handleSubmit}>
      <Typography variant="headline">Sell - {itemType}</Typography>
      <Typography variant="body1">
        Measure your item (in inches) and input them below
      </Typography>
      <MeasurementsList itemType={itemType} />
      <PrevNextButtons
        steps={4}
        previousPage={previousPage}
        handleSubmit={handleSubmit}
        page={page}
        nextLabel="Details"
        previousLabel="Reset"
      />
    </form>
  );
};

export default withStyles(styles)(S2MeasurementsComponent);
