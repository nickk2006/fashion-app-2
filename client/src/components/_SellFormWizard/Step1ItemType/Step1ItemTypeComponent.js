import React from "react";
import FormControl from "@material-ui/core/FormControl";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import PrevNextButtons from "../../PrevNextButton";
import ItemTypePicker from "../../ItemTypePicker";
const styles = {
  root: {
    margin: 16
  },
  formControl: {
    margin: "16px auto 0 auto"
  },
  inputContainer: {
    margin: "0 0 8px 32px"
  }
};

const StepOneItemType = ({
  handleSubmit,
  pristine,
  page,
  classes: { root, inputContainer }
}) => (
  <form className={root} onSubmit={handleSubmit}>
    <FormControl component="fieldset">
      <div className={inputContainer}>
        <Typography variant="headline">Sell</Typography>
        <Typography variant="body1">What are you trying to sell?</Typography>
        <ItemTypePicker />
      </div>
      <PrevNextButtons
        steps={4}
        pristine={pristine}
        handleSubmit={handleSubmit}
        page={page}
        nextLabel="Fit"
      />
    </FormControl>
  </form>
);

export default withStyles(styles)(StepOneItemType);
