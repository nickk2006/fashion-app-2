import { reduxForm } from "redux-form";
import S1Component from "./Step1ItemTypeComponent";
import { clearFields } from "redux-form";

export default reduxForm({
  form: "sell",
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  onChange: (values, dispatch, props, prevValues) => {
    if (values.itemType !== prevValues.itemType) {
      dispatch(clearFields("sell", false, true, "measurement"));
    }
  }
})(S1Component);
