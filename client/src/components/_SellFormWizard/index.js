import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { reset } from "redux-form";
import Step1ItemType from "./Step1ItemType";
import Step2Measurements from "./Step2Measurements";
import Step3Details from "./Step3Details";
import Step4Review from "./Step4Review";
import withStyles from "@material-ui/core/styles/withStyles";
import { connect } from "react-redux";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import StripeConnectBtn from "../StripeConnectButton";
const styles = {
  container: {
    display: "flex",
    width: "100%",
    justifyContent: "center"
  },
  stripeContainer: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    flexDirection: "column",
    marginTop: "2em"
  }
};

class SellFormWizard extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.previousPageAndRest = this.previousPageAndRest.bind(this);
    this.state = { page: 0 };
  }

  nextPage() {
    this.setState({ page: this.state.page + 1 });
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 });
  }

  previousPageAndRest() {
    this.setState({ page: 0 });
    this.props.resetForm();
  }

  render() {
    const {
      classes: { container, stripeContainer },
      isConnectedToStripe
    } = this.props;
    const { page } = this.state;

    if (isConnectedToStripe) {
      return (
        <div className={container}>
          <Helmet>
            <title>Sell An Item</title>
          </Helmet>
          {page === 0 && <Step1ItemType onSubmit={this.nextPage} page={page} />}
          {page === 1 && (
            <Step2Measurements
              previousPage={this.previousPageAndRest}
              onSubmit={this.nextPage}
              page={page}
            />
          )}
          {page === 2 && (
            <Step3Details
              previousPage={this.previousPage}
              onSubmit={this.nextPage}
              page={page}
            />
          )}
          {page === 3 && (
            <Step4Review previousPage={this.previousPage} page={page} />
          )}
        </div>
      );
    } else {
      return (
        <div className={stripeContainer}>
          <Helmet>
            <title>Connect Stripe</title>
          </Helmet>
          <Typography paragraph variant="subheading">
            <b>Walcroft Street</b> uses Stripe to get you paid quickly and keep
            your personal and payment information secure. Please connect to
            Stripe to get started selling.
          </Typography>
          <StripeConnectBtn />
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  isConnectedToStripe: state.auth.user.isConnectedToStripe
});

const mapDispatchToProps = dispatch => ({
  resetForm: () => dispatch(reset("sell"))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(SellFormWizard);
