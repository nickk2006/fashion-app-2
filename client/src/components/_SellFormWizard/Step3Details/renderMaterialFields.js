import React, { Component, Fragment } from "react";
import { Field } from "redux-form";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import withStyles from "@material-ui/core/styles/withStyles";
import NativePicker from "../../NativePicker";
import { facets } from "../../../_constants";
import validations from "../../../_helpers/FormValidationFields";
import renderTextFieldWithErrorOnSubmitFail from "../../ReduxFormRenders/renderTextFieldWithErrorOnSubmitFail";

const styles = theme => ({
  amountFieldContainer: {
    display: "flex",
    alignContent: "space-between"
  },
  amountFieldInput: {
    flexGrow: 1,
    flexShrink: 1,
    maxWidth: theme.inputWidth.mobile.number
  },
  amountFieldButton: {
    flexGrow: 0
  },
  materialContainer: {
    marginTop: theme.spacing.unit
  }
});

class renderMaterialFields extends Component {
  componentDidMount() {
    if (this.props.fields && this.props.fields.length === 0) {
      this.props.fields.push();
    }
  }
  render() {
    const materials = facets.materials;
    const {
      classes: {
        amountFieldContainer,
        amountFieldInput,
        amountFieldButton,
        materialContainer
      },
      selectedOptions,
      fields,
      meta: { error, submitFailed, active }
    } = this.props;
    return (
      <Fragment>
        {fields.map((_, index, fields) => (
          <div key={`material${index}`}>
            <div className={index === 0 ? {} : materialContainer}>
              <Field
                name={`material[${index}].name`}
                options={materials}
                component={NativePicker}
                label={`Material ${index + 1}`}
                selectedOptions={selectedOptions}
              />
            </div>
            <div className={amountFieldContainer}>
              <Field
                className={amountFieldInput}
                name={`material[${index}].amount`}
                options={materials}
                component={renderTextFieldWithErrorOnSubmitFail}
                label={`Amount ${index + 1}`}
                type="tel"
                validate={[validations.materialsEqual100]}
              />
              <div className={amountFieldButton}>
                <IconButton
                  disabled={fields.length === materials.length}
                  onClick={() => fields.push()}
                  aria-label="Add"
                >
                  <AddIcon />
                </IconButton>
              </div>
              <div className={amountFieldButton}>
                <IconButton
                  disabled={fields.length === 1}
                  onClick={() => fields.remove(index)}
                  aria-label="Remove"
                >
                  <RemoveIcon />
                </IconButton>
              </div>
            </div>
          </div>
        ))}
      </Fragment>
    );
  }
}

export default withStyles(styles)(renderMaterialFields);

// export default ({ selectedColors, fields, meta: { error } }) => (
//   <Fragment>
//     <div>
//       <Field
//         name={`color[0]`}
//         options={colors}
//         component={NativePicker}
//         label="Color 1"
//         selectedOptions={selectedColors}
//         required
//       />
//       <IconButton onClick={() => fields.push()} aria-label="Add">
//         <AddIcon />
//       </IconButton>
//     </div>
//     {fields.map((color, index, fields) => (
//       <div key={`color${index + 1}`}>
//         <Field
//           name={`color[${index + 1}]`}
//           options={colors}
//           component={NativePicker}
//           label={`Color ${index + 2}`}
//           selectedOptions={selectedColors}
//         />
//         <IconButton onClick={() => fields.push()} aria-label="Add">
//           <AddIcon />
//         </IconButton>
//         <IconButton
//           onClick={() => fields.remove(index + 1)}
//           aria-label="Remove"
//         >
//           <RemoveIcon />
//         </IconButton>
//       </div>
//     ))}
//   </Fragment>
// );
