import React, { Component, Fragment } from "react";
import { Field } from "redux-form";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import NativePicker from "../../NativePicker";
import { facets } from "../../../_constants";

export default class renderColorFields extends Component {
  componentDidMount() {
    if (this.props.fields && this.props.fields.length === 0) {
      this.props.fields.push();
    }
  }
  render() {
    const colors = facets.colors;
    const { selectedOptions, fields } = this.props;

    return (
      <Fragment>
        {fields.map((color, index, fields) => (
          <div key={`color${index}`}>
            <Field
              name={`color[${index}]`}
              options={colors}
              component={NativePicker}
              label={`Color ${index + 1}`}
              selectedOptions={selectedOptions}
              withButtons
            />
            <IconButton
              disabled={fields.length === colors.length}
              onClick={() => fields.push()}
              aria-label="Add"
            >
              <AddIcon />
            </IconButton>
            <IconButton
              disabled={fields.length === 1}
              onClick={() => fields.remove(index)}
              aria-label="Remove"
            >
              <RemoveIcon />
            </IconButton>
          </div>
        ))}
      </Fragment>
    );
  }
}
