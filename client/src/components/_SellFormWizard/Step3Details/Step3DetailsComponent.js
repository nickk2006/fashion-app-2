import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { Field, FieldArray } from "redux-form";
import { TextField } from "redux-form-material-ui";
import ImageUploadAndPreview from "../../ImageUploadAndPreview";
import PrevNextButtons from "../../PrevNextButton";
import isEmpty from "lodash/isEmpty";
import filter from "lodash/filter";
import renderColorFields from "./renderColorFields";
import renderMaterialFields from "./renderMaterialFields";
import Typography from "@material-ui/core/Typography";
import validations from "../../../_helpers/FormValidationFields";
import InputAdornment from "@material-ui/core/InputAdornment";
const styles = theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start"
  },
  inputFieldFirst: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: theme.inputWidth.mobile.long
  },
  inputField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 3
  },
  descriptionInput: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "100%",
    maxWidth: 1000,
    marginTop: theme.spacing.unit * 2
  }
});

class S3DetailsComponent extends Component {
  render() {
    const {
      page,
      handleSubmit,
      previousPage,
      color,
      description,
      itemType,
      material,
      classes: { container, inputFieldFirst, inputField, descriptionInput }
    } = this.props;

    const cleanMaterialArray = filter(material, material => !!material); //removes null elements

    const selectedMaterials = isEmpty(cleanMaterialArray)
      ? []
      : cleanMaterialArray.map(material => {
          if (material.hasOwnProperty("name")) {
            return material.name;
          }
        });

    return (
      <form className={container} onSubmit={handleSubmit}>
        <Typography
          gutterBottom
          style={{ alignSelf: "center" }}
          variant="headline"
        >
          Sell - {itemType}
        </Typography>
        <Typography variant="title">Designer</Typography>
        <Field
          component={TextField}
          className={inputFieldFirst}
          name="designer"
          type="text"
          label="Designer"
          validate={[validations.required]}
        />
        <Field
          component={TextField}
          className={inputFieldFirst}
          name="designerLine"
          type="text"
          label="Designer Line"
          helperText="e.g., Z by Zegna, H&M x Balmain"
        />
        <br />
        <Typography variant="title">Price</Typography>
        <Field
          name="price"
          label="Price"
          type="number"
          component={TextField}
          validate={[validations.required, validations.positive]}
          InputProps={{
            startAdornment: <InputAdornment position="start">$</InputAdornment>
          }}
        />
        <br />
        <div className={inputField}>
          <Typography variant="title">Colors</Typography>
          <FieldArray
            name="color"
            component={renderColorFields}
            selectedOptions={color}
            validate={[validations.required]}
          />
        </div>
        <br />
        <div className={inputField}>
          <Typography variant="title">Materials</Typography>
          <Typography gutterBottom variant="caption">
            Total must sum to 100
          </Typography>
          <FieldArray
            name="material"
            component={renderMaterialFields}
            selectedOptions={selectedMaterials}
            validate={[validations.required]}
          />
        </div>
        <br />
        <div className={descriptionInput}>
          <Typography variant="title">Description</Typography>
          <Field
            name="description"
            multiline
            rows={4}
            rowsMax={25}
            type="text"
            fullWidth
            component={TextField}
            helperText={`Character count: ${
              isEmpty(description) ? 0 : description.length
            }/280`}
            validate={[validations.required]}
          />
        </div>
        <br />
        <Typography variant="title">Photos</Typography>
        <ImageUploadAndPreview />
        <PrevNextButtons
          steps={4}
          previousPage={previousPage}
          handleSubmit={handleSubmit}
          page={page}
          nextLabel="Review"
        />
      </form>
    );
  }
}

export default withStyles(styles)(S3DetailsComponent);
