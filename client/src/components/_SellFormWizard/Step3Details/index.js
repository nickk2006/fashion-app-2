import S3Details from "./Step3DetailsComponent";
import { reduxForm, formValueSelector, clearFields, change } from "redux-form";
import { connect } from "react-redux";
import { compose } from "redux";
import isEmpty from "lodash/isEmpty";
import { facets } from "../../../_constants";

const selector = formValueSelector("sell");

export default compose(
  reduxForm({
    form: "sell",
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    onChange: (values, dispatch) => {
      if (!isEmpty(values.description) && values.description.length > 280) {
        dispatch(
          change("sell", "description", values.description.slice(0, 280))
        );
      }

      facets.materials.map(
        m =>
          isEmpty(values[m])
            ? dispatch(clearFields("sell", false, false, m))
            : null
      );
    }
  }),
  connect(state => ({
    itemType: selector(state, "itemType"),
    color: selector(state, "color"),
    material: selector(state, "material"),
    description: selector(state, "description")
  }))
)(S3Details);
