import isEmpty from "lodash/isEmpty";
import { facets } from "../../../_constants";

export default ({ measurements, itemType }) => {
  const measurementObject = [];
  for (const name in measurements) {
    if (!isEmpty(measurements[name])) {
      measurementObject.push({
        name,
        measurement: measurements[name],
        displayName: facets.itemNameMapping[name],
        itemType
      });
    }
  }
  return measurementObject;
};
