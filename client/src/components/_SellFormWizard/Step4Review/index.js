import { compose } from "redux";
import { reduxForm, formValueSelector, reset } from "redux-form";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { createItem } from "../../../_actions";
import SellFormReview from "./Step5ReviewComponent";
const selector = formValueSelector("sell");

const mapStateToProps = state => {
  return {
    measurement: selector(state, "measurement"),
    itemType: selector(state, "itemType"),
    color: selector(state, "color"),
    material: selector(state, "material"),
    designer: selector(state, "designer"),
    price: selector(state, "price"),
    description: selector(state, "description"),
    photos: selector(state, "photos"),
    username: state.auth.user.username,
    uploadCount: state.sell.uploadCount
  };
};

export default compose(
  withRouter,
  reduxForm({
    form: "sell",
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    onSubmit: (v, dispatch) => {
      const { photos, ...itemFields } = v;
      return dispatch(createItem(itemFields, v.photos));
    },
    onSubmitSuccess: (res, dispatch, props) => {
      props.history.push(`/catalog/items/${res.data._id}`);
      dispatch(reset("sell"));
    }
  }),
  connect(mapStateToProps)
)(SellFormReview);
