import React, { Fragment } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import PrevNextButtons from "../../PrevNextButton";
import ProductInfoComponent from "../../_ProductInfo/ProductInfoComponent";
import { facets } from "../../../_constants";
import CircularLoading from "../../CircularLoading";
const styles = {
  root: {
    width: "100%"
  },
  previewBox: {
    border: "3px solid black"
  },
  productInfo: {
    margin: 0,
    transform: "scale(0.9)"
  }
};

const SellFormReview = ({
  page,
  handleSubmit,
  previousPage,
  itemType,
  measurement,
  color,
  material,
  designer,
  price,
  username,
  description,
  photos,
  submitting,
  uploadCount,
  designerLine,
  classes: { root, previewBox, productInfo }
}) => {
  const transformedMeasurements = Object.keys(measurement).map(m => {
    const name = facets.itemNameMapping[m];
    const amount = measurement[m];
    return { name, amount };
  });

  const transformedPhotos = photos.map(p => p.preview);

  return (
    <Fragment>
      <form onSubmit={handleSubmit}>
        <div className={root}>
          <div className={previewBox}>
            <div className={productInfo}>
              <ProductInfoComponent
                id="Preview"
                measurements={transformedMeasurements}
                colors={color}
                material={material}
                isSold={false}
                itemType={itemType}
                designer={designer}
                designerLine={designerLine}
                price={price}
                sellerId="Preview"
                sellerUsername={username}
                createdAt={Date.now()}
                description={description}
                photos={transformedPhotos}
                isPreview
              />
            </div>
          </div>
          <PrevNextButtons
            steps={4}
            page={page}
            previousPage={previousPage}
            handleSubmit={handleSubmit}
            nextLabel="List Item!"
          />
        </div>
      </form>
      <CircularLoading isVisable={!!submitting}>
        {uploadCount === photos.length
          ? "Creating Listing"
          : `Uploading Images`}
      </CircularLoading>
    </Fragment>
  );
};

SellFormReview.propTypes = {
  id: PropTypes.string,
  materials: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })
  ),
  color: PropTypes.arrayOf(PropTypes.string),
  itemType: PropTypes.string,
  designer: PropTypes.string,
  price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  sellerId: PropTypes.string,
  sellerUsername: PropTypes.string,
  createdAt: PropTypes.string,
  description: PropTypes.string,
  primaryPhoto: PropTypes.string,
  secondaryPhotos: PropTypes.arrayOf(PropTypes.string),
  username: PropTypes.string.isRequired
};

export default withStyles(styles)(SellFormReview);
