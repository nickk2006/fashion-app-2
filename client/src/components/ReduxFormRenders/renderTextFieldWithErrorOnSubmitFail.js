import React from "react";
import { TextField } from "redux-form-material-ui";

export default ({
  input,
  label,
  meta: { error, submitFailed, active },
  ...rest
}) => (
  <TextField
    label={label}
    error={!!error && submitFailed && !active}
    helperText={!!error && submitFailed && !active && error}
    {...input}
    {...rest}
  />
);
