import React from "react";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import { Link } from "react-router-dom";
import LazyLoad from "react-lazyload";

const styles = theme => ({
  link: {
    "text-decoration": "none",
    color: "#000",
    "a:hover, a:visited, a:link, a:active": {
      "text-decoration": "none"
    }
  },
  container: {
    display: "grid",
    "text-decoration": "none",
    "a:hover, a:visited, a:link, a:active": {
      "text-decoration": "none"
    },
    [theme.breakpoints.down("sm")]: {
      "grid-template-rows": "1fr [gap-1] 4px 1em [gap-2] 4px 1em"
    },

    [theme.breakpoints.up("md")]: {
      "grid-template-rows": "1fr [gap-1] 4px 1em [gap-2] 4px 1em"
    }
  },
  itemBorder: {
    border: "1px solid #27ae60"
  },
  previewImage: {
    "max-width": "100%",
    "max-height": "100%",
    "border-radius": `${theme.spacing.unit * 0.5}px ${theme.spacing.unit *
      0.5}px ${theme.spacing.unit * 0.25}px ${theme.spacing.unit * 0.25}px`
  },
  // designerPriceRow: {
  //   display: "grid",
  //   "grid-template-columns": "3fr 1fr"
  // },
  previewImageCover: {
    "place-self": "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    //TODO: Chrome can't load pictures unless you include height. Went on safari and got exact height of <img />. Not using for sm breakpoint because the image show there.
    [theme.breakpoints.down("sm")]: {
      "max-width": "100%",
      "max-height": "100%",
      minHeight: 126
    },
    [theme.breakpoints.up("md")]: {
      height: 252
    }
  },
  designerText: {
    float: "left",
    "text-overflow": "ellipsis",
    "white-space": "nowrap",
    overflow: "hidden",
    "max-width": "70%",
    "margin-left": theme.spacing.unit * 0.5
  },
  priceText: {
    float: "right",
    "max-width": "25%",
    "margin-right": theme.spacing.unit * 0.5
  },
  title: {
    "margin-right": theme.spacing.unit * 0.5,
    "margin-left": theme.spacing.unit * 0.5
  }
});
//TODO: Remove index prop
const PPSingleProduct = ({
  item: { primaryPhoto, _id, designer, designerLine, price },
  classes: {
    previewImage,
    container,
    designerText,
    priceText,
    previewImageCover,
    designerPriceRow,
    title
  }
}) => (
  <Link className={container} to={`/catalog/items/${_id}`}>
    <div className={previewImageCover}>
      <LazyLoad height={320} offset={500}>
        <img className={previewImage} src={primaryPhoto} alt={`${designer}`} />
      </LazyLoad>
    </div>
    <div />
    <div>
      <div className={designerPriceRow}>
        <Typography
          className={designerText}
          variant="body2"
          noWrap
        >{`${designer}`}</Typography>
        <Typography
          className={priceText}
          variant="body2"
        >{`$${price}`}</Typography>
      </div>
    </div>
    <div />
    <Typography className={title} variant="caption">
      {designerLine ? null : designerLine}
    </Typography>
  </Link>
);

export default withStyles(styles)(PPSingleProduct);
