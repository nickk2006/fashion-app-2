import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { handleToken as handleTokenActionCreator } from "../../_actions";

class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      stripeLoading: true
    };
    this.onStripeUpdate = this.onStripeUpdate.bind(this);
    this.loadStripe = this.loadStripe.bind(this);
  }

  loadStripe(onload) {
    if (!window.StripeCheckout) {
      const script = document.createElement("script");
      script.onload = function() {
        onload();
      };
      script.src = "https://checkout.stripe.com/checkout.js";
      document.head.appendChild(script);
    } else {
      onload();
    }
  }

  componentDidMount() {
    this.loadStripe(() => {
      this.stripehandler = window.StripeCheckout.configure({
        key: process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY,
        image: "https://stripe.com/img/documentation/checkout/marketplace.png",
        locale: "auto",
        token: async (token, args) => {
          this.setState({ loading: true });
          await this.props.handleToken({
            stripeToken: token,
            shipping: {
              name: args.shipping_name,
              address: args.shipping_address_line1,
              city: args.shipping_address_city,
              state: args.shipping_address_state,
              zip: args.shipping_address_zip,
              country: args.shipping_address_country,
              countryCode: args.shipping_address_country_code
            },
            itemId: this.props.itemId
          });
          this.setState({ loading: false });
        }
      });
      this.setState({
        stripeLoading: false,
        loading: false
      });
    });
  }
  componentWillUnmount() {
    if (this.stripehandler) {
      this.stripehandler.close();
    }
  }

  onStripeUpdate(e) {
    this.stripehandler.open({
      amount: this.props.price * 100,
      zipCode: true,
      billingAddress: true,
      shippingAddress: true,
      name: "Walcroft Street",
      description: `${this.props.designer} ${this.props.itemType}`
    });
    e.preventDefault();
  }
  render() {
    const { stripeLoading, loading } = this.state;
    const {
      children,
      price,
      designer,
      itemId,
      itemType,
      handleToken,
      ...rest
    } = this.props;
    return loading || stripeLoading ? (
      <Button disabled>{children}</Button>
    ) : (
      <Button {...rest} onClick={this.onStripeUpdate}>
        {children}
      </Button>
    );
  }
}
//TODO: Create "hold" flow

const mapDispatchToProps = dispatch => ({
  handleToken: sellInfo => dispatch(handleTokenActionCreator(sellInfo))
});

CheckoutForm.propTypes = {
  handleToken: PropTypes.func.isRequired,
  designer: PropTypes.string.isRequired,
  itemType: PropTypes.string.isRequired,
  itemId: PropTypes.string.isRequired
};

export default connect(
  null,
  mapDispatchToProps
)(CheckoutForm);
