import React, { Fragment } from "react";
import { Field } from "redux-form";
import { TextField } from "redux-form-material-ui";
import { Helmet } from "react-helmet";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import validations from "../../_helpers/FormValidationFields";
import renderTextField from "../ReduxFormRenders/renderTextFieldWithErrorOnSubmitFail";

const styles = theme => ({
  root: {
    margin: "8px 16px",
    padding: "8px",
    display: "flex",
    "justify-content": "center",
    "flex-direction": "column"
  },
  button: {
    margin: `${theme.spacing.unit * 3}px 0px ${theme.spacing.unit}px 0px`,
    background: theme.palette.button.gradient,
    color: theme.palette.secondary.contrastText,
    fontSize: "12px"
  },
  child: {
    marginTop: theme.spacing.unit * 2
  },
  signup: {
    "text-decoration": "underline"
  }
});

const SigninComponent = ({
  classes: { button, root, child, signup },
  isAuthenticating,
  handleSubmit
}) => (
  <Fragment>
    <form onSubmit={handleSubmit}>
      <Helmet>
        <title>Sign In</title>
      </Helmet>
      <Paper elevation={2}>
        <div className={root}>
          <Typography align="center">Welcome back!</Typography>
          <Field
            className={child}
            id="email"
            name="email"
            label="Email"
            component={renderTextField}
            type="email"
            validate={[validations.required, validations.isEmail]}
          />
          <Field
            className={child}
            id="password-input"
            name="password"
            label="Password"
            component={renderTextField}
            type="password"
            validate={[validations.required]}
          />
          <Button
            variant="outlined"
            className={button}
            type="submit"
            disabled={isAuthenticating}
          >
            Sign in to your account
          </Button>
        </div>
      </Paper>
    </form>
    <br />
    <Button
      size="large"
      component={Link}
      to="/users/signup"
      fullWidth
      variant="outlined"
    >
      <Fragment>
        <Typography variant="caption">Don't have an account?&nbsp;</Typography>
        <Typography className={signup} variant="caption">
          Sign up.
        </Typography>
      </Fragment>
    </Button>
  </Fragment>
);

SigninComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isAuthenticating: PropTypes.bool
};

SigninComponent.defaultProps = { isAuthenticating: false };

export default withStyles(styles)(SigninComponent);
