import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import { compose } from "redux";
import { login } from "../../_actions";
import SigninComponent from "./SigninComponent";
import { withRouter } from "react-router-dom";
import { queryStringToObject } from "../../_helpers";
const mapStateToProps = state => ({
  isAuthenticating: state.auth.user.isAuthenticating
});
// TODO: Remove initial values

const reduxFormConfig = {
  form: "signin",
  onSubmit: (values, dispatch) => dispatch(login(values)),
  onSubmitSuccess: (result, dispatch, props) => {
    try {
      const { item } = queryStringToObject(props.location.search);
      if (typeof item !== "undefined") {
        return props.history.push(`/catalog/items/${item}`);
      }
    } catch (e) {
      return props.history.push("/");
    }
  }
};

export default compose(
  withRouter,
  reduxForm(reduxFormConfig),
  connect(
    mapStateToProps,
    null
  )
)(SigninComponent);
