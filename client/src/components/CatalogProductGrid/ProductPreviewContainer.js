import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import isEmpty from "lodash/isEmpty";
import { getItems as getItemsActionCreator } from "../../_actions";
import ProductPreviewComponent from "./ProductPreviewComponent";
import firebase from "../../firebase";
//TODO: REPLACE WITH ACTUAL STYLES

class ProductPreviewContainer extends Component {
  componentDidMount() {
    this.unregisterAuthChanged = firebase.auth().onAuthStateChanged(user => {
      if (user) {
        const { getItems, location } = this.props;
        if (location.search) {
          getItems(location.search);
        } else {
          getItems();
        }
      }
    });
  }

  componentDidUpdate(prevProps) {
    const { location, getItems } = this.props;
    if (location.search !== prevProps.location.search) {
      getItems(location.search);
    }
  }

  componentWillUnmount() {
    this.unregisterAuthChanged();
  }

  render() {
    const { items } = this.props;
    return (
      <div>
        {isEmpty(items) ? (
          <div>
            <div
              style={{ float: "left", clear: "both" }}
              ref={el => {
                this.catalogStart = el;
              }}
            />
            <h4>No Items</h4>
          </div>
        ) : (
          <div>
            <div
              style={{ float: "left", clear: "both" }}
              ref={el => {
                this.catalogStart = el;
              }}
            />
            <ProductPreviewComponent items={items} />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  items: state.items.items
});

const mapDispatchToProps = dispatch => ({
  getItems: query => dispatch(getItemsActionCreator(query))
});

ProductPreviewContainer.propTypes = {
  items: PropTypes.array.isRequired, // eslint-disable-line
  getItems: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired // eslint-disable-line
};

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ProductPreviewContainer);
