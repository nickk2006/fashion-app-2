import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import SingleProduct from "../CatalogSingleProduct";

const styles = theme => ({
  container: {
    display: "grid",
    "grid-gap": `${theme.spacing.unit}px ${theme.spacing.unit}px`,
    "justify-content": "center",
    [theme.breakpoints.down("sm")]: {
      "grid-template-columns": "1fr 1fr",
      autoRows: 300
    },

    [theme.breakpoints.up("md")]: {
      gridTemplateColumns: "1fr 1fr 1fr",
      autoRows: 400
    }
  }
});

const CatalogGridComponent = ({ items, classes: { container } }) => {
  return (
    <div className={container}>
      {items.map((item, index) => (
        <SingleProduct key={item._id} item={item} index={index} />
      ))}
    </div>
  );
};

export default withStyles(styles)(CatalogGridComponent);
