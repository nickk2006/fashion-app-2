import React, { Component } from "react";
import ReactDropzone from "react-dropzone";
import { Field, formValueSelector } from "redux-form";
import withStyles from "@material-ui/core/styles/withStyles";
import IconButton from "@material-ui/core/IconButton";
import ClearIcon from "@material-ui/icons/RemoveCircleOutline";
import Tooltip from "@material-ui/core/Tooltip";
import isEmpty from "lodash/isEmpty";
import { change } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import Lightbox from "react-images";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import AddPhotoIcon from "@material-ui/icons/AddPhotoAlternateOutlined";
import Button from "@material-ui/core/Button";
import {
  SortableContainer,
  SortableElement,
  arrayMove,
  SortableHandle
} from "react-sortable-hoc";
import { Typography } from "@material-ui/core";

const styles = {
  previewContainer: {
    display: "flex",
    flexWrap: "wrap"
  },
  previewStyle: {
    display: "block",
    maxWidth: 150,
    maxHeight: 200,
    width: "auto",
    height: "auto"
  },
  imagePreview: {
    displary: "inline-block",
    position: "relative"
  },
  clearIcon: {
    position: "absolute",
    top: 0,
    right: 0,
    zIndex: 1
  }
};

const selector = formValueSelector("sell");

const SortableItem = SortableElement(({ value }) => (
  <ListItem>{value}</ListItem>
));

const SortableList = SortableContainer(({ items }) => (
  <List disablePadding>
    {items.map((value, index) => (
      <SortableItem key={`item-${index}`} index={index} value={value} />
    ))}
  </List>
));

const renderDropzoneInput = ({ photos, ...field }) => {
  return (
    <div>
      <ReactDropzone
        name={field.name}
        accept="image/*"
        onDrop={(filesToUpload, e) => {
          const newPhotos =
            typeof photos === "undefined"
              ? filesToUpload
              : photos.concat(filesToUpload);
          return field.input.onChange(newPhotos);
        }}
        style={{
          width: 200,
          height: 75,
          borderWidth: 2,
          borderColor: "#666",
          borderStyle: "dashed",
          borderRadius: 5,
          textAlign: "center",
          margin: "8px auto"
        }}
      >
        <Button>
          <Typography variant="caption">Upload up to 7 photos</Typography>
          <AddPhotoIcon style={{ marginLeft: 8 }} />
        </Button>
      </ReactDropzone>
    </div>
  );
};

const DragHandle = SortableHandle(({ photo, index, previewStyle }) => (
  <a href={photo} onClick={e => this.openLightbox(index, e)}>
    <img alt="Preview" src={photo} className={previewStyle} />
  </a>
));

class ImageUploadAndPreview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLightboxOpen: false,
      currentImage: 0
    };
    this.closeLightbox = this.closeLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
    this.gotoImage = this.gotoImage.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
    this.onSortEnd = this.onSortEnd.bind(this);
  }

  handleDelete = index => {
    const newPhotos = this.props.photos.filter((_, i) => i !== index);
    return this.props.changeFieldValue("photos", newPhotos);
  };

  openLightbox(index, event) {
    event.preventDefault();
    this.setState({
      currentImage: index,
      isLightboxOpen: true
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      isLightboxOpen: false
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1
    });
  }
  gotoImage(index) {
    this.setState({
      currentImage: index
    });
  }
  handleClickImage() {
    if (this.state.currentImage === this.props.photos.length - 1) return;

    this.gotoNext();
  }

  onSortEnd({ oldIndex, newIndex }) {
    const newPhotoOrder = arrayMove(this.props.photos, oldIndex, newIndex);
    return this.props.changeFieldValue("photos", newPhotoOrder);
  }

  render() {
    const {
      photos,
      classes: { previewStyle, imagePreview, previewContainer, clearIcon }
    } = this.props;
    const { isLightboxOpen } = this.state;
    const photosObject = isEmpty(photos)
      ? null
      : photos.map(photo => ({ src: photo }));

    const photosArray = !isEmpty(photos)
      ? photos.map(({ preview }, index) => (
          <div key={preview} className={imagePreview}>
            <DragHandle
              photo={preview}
              index={index}
              previewStyle={previewStyle}
            />
            <div className={clearIcon}>
              <Tooltip title="Delete">
                <IconButton
                  onClick={() => this.handleDelete(index)}
                  aria-label="Delete"
                >
                  <ClearIcon style={{ color: "white" }} />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        ))
      : null;
    return (
      <div>
        {!isEmpty(photos) && (
          <div className={previewContainer}>
            <SortableList
              items={photosArray}
              onSortEnd={this.onSortEnd}
              useDragHandle
            />
          </div>
        )}
        <Field name="photos" component={renderDropzoneInput} photos={photos} />
        {isLightboxOpen && (
          <Lightbox
            currentImage={this.state.currentImage}
            images={photosObject}
            isOpen={isLightboxOpen}
            onClickImage={this.handleClickImage}
            onClickNext={this.gotoNext}
            onClickPrev={this.gotoPrevious}
            onClose={this.closeLightbox}
            backdropClosesModal
          />
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  photos: selector(state, "photos")
});
const mapDispatchToProps = dispatch => ({
  changeFieldValue: (field, value) => dispatch(change("sell", field, value))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(ImageUploadAndPreview);
