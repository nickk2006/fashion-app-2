import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { completeStripeOAuth } from "../../_actions";
import { withRouter } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import firebase from "../../firebase";
import StripeConnectButton from "../StripeConnectButton";
import withStyles from "@material-ui/core/styles/withStyles";
const styles = theme => ({
  root: {
    margin: "0 auto"
  }
});

class Stripe extends Component {
  componentDidMount() {
    const { location, connectStripe } = this.props;
    this.unregisterAuthChange = firebase.auth().onAuthStateChanged(() => {
      if (location.search) {
        connectStripe(location.search);
      }
    });
  }

  componentDidUpdate() {
    const { history, isConnectedToStripeConnect } = this.props;
    if (isConnectedToStripeConnect) {
      history.push("/sell");
    }
  }

  componentWillUnmount() {
    this.unregisterAuthChange();
  }

  render() {
    const {
      classes,
      isConnectedToStripeConnect,
      isStripeStateError
    } = this.props;

    return isStripeStateError ? (
      <div className={classes.root}>
        <Typography align="center" paragraph variant="title">
          The response from Stripe to our servers wasn't what we expected. Just
          to be safe, can you please try connecting again?
        </Typography>
        <StripeConnectButton className={classes.root} />
      </div>
    ) : isConnectedToStripeConnect ? (
      <div className={classes.root}>
        <Typography variant="title">
          Successfully Connected To Stripe!
        </Typography>
      </div>
    ) : (
      <CircularProgress
        isVisable={!!!isStripeStateError && !!!isConnectedToStripeConnect}
      >
        Connecting to Stripe...
      </CircularProgress>
    );
  }
}

const mapStateToProps = state => ({
  isConnectingToStripeConnect: state.auth.stripe.isConnectingToStripeConnect,
  isConnectedToStripeConnect: state.auth.stripe.isConnectedToStripeConnect,
  isStripeStateError: state.auth.stripe.isStripeStateError
});

const mapDispatchToProps = dispatch => ({
  connectStripe: query => dispatch(completeStripeOAuth(query))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter,
  withStyles(styles)
)(Stripe);
