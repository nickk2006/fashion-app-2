import React, { Fragment } from "react";
import { Field } from "redux-form";
import isUndefined from "lodash/isUndefined";
import { facets } from "../../_constants";
import NativePicker from "../NativePicker";
// const renderRadioGroup = ({ input, ...rest }) => (
//   <RadioGroup
//     {...input}
//     {...rest}
//     value={input.value}
//     onChange={(_, value) => input.onChange(value)}
//   />
// );

const StepOneItemType = ({ initialize, size = facets.MEDIUM }) => {
  return (
    <Fragment>
      <Field
        name="itemType"
        component={NativePicker}
        label="Item Type"
        options={facets.itemTypes}
        onChange={function(event, newValue, previousValue, name) {
          if (newValue !== previousValue && newValue.length > 0) {
            initialize(facets.initialMeasurementValues[newValue][size]);
          } else {
            initialize({});
          }
        }}
      />
    </Fragment>
  );
};
export default StepOneItemType;

// <FormControlLabel
//   value={DRESS_SHIRT}
//   control={<Radio />}
//   label="Dress Shirt"
// />
// <FormControlLabel value={PANTS} control={<Radio />} label="Pants" />
// <FormControlLabel value={JACKET} control={<Radio />} label="Jacket" />
//
// {facets.itemTypes.map(itemType => (
//   <NativePicker
//     key={itemType}
//     value={itemType}
//     control={<Radio />}
//     label={itemType}
//   />
// ))}
