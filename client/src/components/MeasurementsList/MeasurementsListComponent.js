import React, { Component, Fragment } from "react";
import { Field } from "redux-form";
import FormControl from "@material-ui/core/FormControl";
import NativePickerNumeric from "../NativePicker/NativePickerNumeric";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import { facets } from "../../_constants";
import validations from "../../_helpers/FormValidationFields";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/InfoOutlined";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";

const styles = theme => ({
  textField: {
    width: theme.inputWidth.mobile.number,
    float: "right"
  },
  table: {
    maxWidth: "100"
  },
  tableCell: {
    padding: 0,
    border: "none"
  },
  header: {
    padding: 0,
    textAlign: "left"
  },
  title: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  }
});

class MLComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      title: "",
      description: ""
    };
  }

  handleOpen = (title, description) => {
    this.setState({
      isModalOpen: true,
      title,
      description
    });
  };

  handleClose = () => {
    this.setState({
      isModalOpen: false,
      title: "",
      description: ""
    });
  };

  render() {
    const { itemType, classes } = this.props;

    const measurements = facets[itemType].map(
      fullName => facets.itemTypeMappingWithMeasurements[fullName]
    );

    return (
      <Fragment>
        <FormControl>
          <Table className={classes.table} padding="dense">
            <TableHead className={classes.tableCell}>
              <TableRow>
                <TableCell className={classes.tableCell}>Measurement</TableCell>
                <TableCell className={classes.header}>min</TableCell>
                <TableCell className={classes.header}>max</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {measurements.map((m, index) => (
                <TableRow key={m.name}>
                  <TableCell
                    className={classes.tableCell}
                    component="th"
                    scope="row"
                  >
                    <div className={classes.title}>
                      <div>{m.name}</div>
                      <IconButton
                        aria-label="Description"
                        onClick={() => this.handleOpen(m.name, m.description)}
                      >
                        <InfoIcon />
                      </IconButton>
                    </div>
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    <Field
                      tabIndex={index * 2}
                      component={NativePickerNumeric}
                      className={classes.textField}
                      min={m.min}
                      max={m.max}
                      id={`${m.name}Input`}
                      name={`${m.fullName}.min`}
                      type="number"
                      compact
                      validate={[validations.required]}
                    />
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    <Field
                      tabIndex={index * 2 + 1}
                      component={NativePickerNumeric}
                      className={classes.textField}
                      id={`${m.name}Input`}
                      min={m.min}
                      max={m.max}
                      name={`${m.fullName}.max`}
                      type="number"
                      compact
                      validate={[validations.required]}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </FormControl>
        <Modal
          aria-labelledby="title"
          aria-describedby="description"
          open={this.state.isModalOpen}
          onClose={this.handleClose}
        >
          <div className={classes.paper}>
            <Typography variant="title">{this.state.title}</Typography>
            <Typography variant="subheading">
              {this.state.description}
            </Typography>
          </div>
        </Modal>
      </Fragment>
    );
  }
}
MLComponent.propTypes = {
  itemType: PropTypes.string.isRequired
};

export default withStyles(styles)(MLComponent);
