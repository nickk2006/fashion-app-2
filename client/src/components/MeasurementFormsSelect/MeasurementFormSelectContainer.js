import MFSelectComponent from "./MeasurementFormsSelectComponent";
import { connect } from "react-redux";
import { updateItemType } from "../../_actions";

const mapStateToProps = state => ({
  itemType: state.sell.itemType
});

const mapDispatchToProps = dispatch => ({
  handleChange: event => dispatch(updateItemType(event.target.value))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MFSelectComponent);
