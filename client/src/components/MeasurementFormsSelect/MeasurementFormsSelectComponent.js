import React from "react";
import NativeSelect from "@material-ui/core/NativeSelect";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import { itemTypes } from "../../_constants";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
  selectStyle: { width: theme.inputWidth.mobile.short }
});

const MFSelectComponent = props => {
  const {
    handleChange,
    itemType,
    classes: { selectStyle }
  } = props;
  const { JACKET, PANTS, DRESS_SHIRT } = itemTypes;
  return (
    <FormControl>
      <InputLabel>Type</InputLabel>
      <NativeSelect
        className={selectStyle}
        onChange={handleChange}
        name="Item Type"
        inputProps={{ name: "Select Here", id: "item-type-select" }}
        value={itemType}
        required
      >
        <option value="" />
        <option value={JACKET}>Jacket</option>
        <option value={PANTS}>Pants</option>
        <option value={DRESS_SHIRT}>Dress Shirt</option>
      </NativeSelect>
    </FormControl>
  );
};

export default withStyles(styles)(MFSelectComponent);
