import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import SignupComponent from './SignupComponent';

describe('Signup Form Component', () => {
  let wrapped = null;
  let registering;
  let touched;
  let error;
  let reset;
  let onSave;
  let onSaveResponse;
  let handleSubmit;

  beforeEach(() => {
    registering = false;
    touched = false;
    error = null;
    reset = sinon.spy();
    onSaveResponse = Promise.resolve();
    handleSubmit = fn => fn;
  });
  const buildSubject = () => {
    onSave = sinon.stub().returns(onSaveResponse);
    const props = {
      onSave,
      registering,
      // The real redux form has many properties for each field,
      // including onChange and onBlur handlers. We only need to provide
      // the ones that will change the rendered output.
      fields: {
        email: {
          value: '',
          touched,
          error
        },
        username: {
          value: '',
          touched,
          error
        },
        password: {
          value: '',
          touched,
          error
        }
      },
      handleSubmit,
      reset
    };
    return shallow(<SignupComponent {...props} />);
  };

  it('disables button while registering', () => {
    registering = true;
    wrapped = buildSubject();
    expect(
      wrapped
        .find('button[type="submit"]')
        .at(0)
        .props().disabled
    ).toBeTruthy();
  });
});
