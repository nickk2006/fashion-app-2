import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import registration from '../../_reducers/registration.reducer';

import SignupContainer from './SignupContainer';

describe('Signup Form Container', () => {
  let store;
  let onSubmit;
  let wrapped;
  beforeEach(() => {
    store = createStore(
      combineReducers({
        form: formReducer,
        registration
      })
    );
    onSubmit = sinon.stub().returns(Promise.resolve());
    const props = {
      onSubmit
    };
    wrapped = mount(
      <Provider store={store}>
        <SignupContainer {...props} />
      </Provider>
    );
  });

  it('calls onSubmit', () => {
    const form = wrapped.find('form');
    const usernameInput = wrapped.find('input#username-input');
    const emailInput = wrapped.find('input#email-input');
    const passwordInput = wrapped.find('input#password-input');

    usernameInput.simulate('change', { target: { value: 'test' } });
    emailInput.simulate('change', { target: { value: 'test@test.com' } });
    passwordInput.simulate('change', { target: { value: '123123' } });

    form.simulate('submit');
    expect(onSubmit.callCount).toEqual(1);
  });
});
