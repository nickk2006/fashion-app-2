import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { reduxForm } from "redux-form";
import { queryStringToObject } from "../../_helpers";
import { register, isUsernameUnique } from "../../_actions";
import SignupComponent from "./SignupComponent";

const mapStateToProps = state => ({
  isAuthenticating: state.auth.user.isAuthenticating,
  token: state.auth.user.token
});

const reduxFormConfig = {
  form: "signup",
  onSubmit: (values, dispatch, props) => {
    return dispatch(register(values));
  },
  onSubmitSuccess: (res, dispatch, props) => {
    if (props.location.search) {
      try {
        const { item } = queryStringToObject(props.location.search);
        return props.history.push(`/catalog/items/${item}`);
      } catch (e) {
        return props.history.push("/");
      }
    }
    return props.history.push("/");
  },
  onSubmitFail: (errors, dispatch, submitError, props) =>
    console.log("ERROR ON REDUX FORM IS", errors),
  asyncValidate: (values, dispatch) =>
    dispatch(isUsernameUnique(values.username)),
  asyncChangeFields: ["username"]
};

export default compose(
  withRouter,
  reduxForm(reduxFormConfig),
  connect(
    mapStateToProps,
    null
  )
)(SignupComponent);
