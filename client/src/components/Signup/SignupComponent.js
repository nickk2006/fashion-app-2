import React, { Fragment } from "react";
import { Field } from "redux-form";
import { Helmet } from "react-helmet";
import PropTypes from "prop-types";
import { TextField } from "redux-form-material-ui";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Fade from "@material-ui/core/Fade";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import validations from "../../_helpers/FormValidationFields";
import { Link } from "react-router-dom";

const styles = theme => ({
  root: {
    margin: "8px 16px",
    padding: "8px",
    display: "flex",
    "justify-content": "center",
    "flex-direction": "column"
  },
  button: {
    margin: "8px auto",
    background: theme.palette.button.gradient,
    color: theme.palette.secondary.contrastText,
    fontSize: "12px"
  },
  inputField: {
    marginTop: theme.spacing.unit * 2
  },
  signin: {
    "text-decoration": "underline"
  },
  signinBtn: {
    marginTop: theme.spacing.unit * 2
  }
});

const minLength = min => value =>
  value && value.length < min ? `Must be ${min} characters or more` : undefined;
const minLength6 = minLength(6);

const SignupComponent = ({
  classes: { button, root, inputField, signin, signinBtn },
  handleSubmit,
  isAuthenticating
}) => {
  return isAuthenticating ? (
    <Fade
      in={!isAuthenticating}
      style={{ transitionDelay: "1000ms" }}
      unmountOnExit
    >
      <Helmet>
        <title>Signing Up</title>
      </Helmet>
      <CircularProgress className={root} />
    </Fade>
  ) : (
    <div>
      <Helmet>
        <title>Sign Up</title>
      </Helmet>
      <form onSubmit={handleSubmit}>
        <Paper elevation={2}>
          <div className={root}>
            <Typography variant="body1" align="center">
              Create your Walcroft account.
            </Typography>
            <div className={inputField}>
              <Field
                className="input"
                id="username"
                name="username"
                label="Display Name"
                component={TextField}
                type="text"
                placeholder="Display Name"
                fullWidth
              />
            </div>
            <div className={inputField}>
              <Field
                className="input"
                id="email-input"
                name="email"
                label="Email"
                component={TextField}
                type="email"
                placeholder="Email"
                fullWidth
                validate={[validations.isEmail]}
              />
            </div>
            <div className={inputField}>
              <Field
                className="input"
                id="password-input"
                name="password"
                label="Password"
                component={TextField}
                type="password"
                placeholder="Password"
                fullWidth
                helperText="Min 6 characters"
                validate={[minLength6]}
              />
            </div>
            <div className={inputField}>
              <Button
                className={button}
                variant="contained"
                type="submit"
                disabled={isAuthenticating}
              >
                Create your Walcroft account
              </Button>
            </div>
          </div>
        </Paper>
      </form>
      <Button
        className={signinBtn}
        size="large"
        component={Link}
        to="/users/signin"
        fullWidth
      >
        <Fragment>
          <Typography variant="caption">
            Already have an account?&nbsp;
          </Typography>
          <Typography className={signin} variant="caption">
            Sign in.
          </Typography>
        </Fragment>
      </Button>
    </div>
  );
};

SignupComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isAuthenticating: PropTypes.bool,
  token: PropTypes.string
};

export default withStyles(styles)(SignupComponent);
