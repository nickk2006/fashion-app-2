import React from "react";
import { connect } from "react-redux";
import { logout as logoutActionCreator } from "../../_actions";
import Button from "@material-ui/core/Button";
const LogoutButton = ({ isLoggingOut, logout }) => {
  return (
    <Button
      variant="outlined"
      size="small"
      onClick={() => logout()}
      disabled={isLoggingOut}
    >
      Logout
    </Button>
  );
};

const mapStateToProps = state => ({
  isLoggingOut: state.auth.user.isAuthenticating
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logoutActionCreator())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogoutButton);
