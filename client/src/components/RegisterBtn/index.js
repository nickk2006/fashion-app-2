import React from "react";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
const styles = theme => ({
  getStartedButton: {
    background: theme.palette.button.gradient,
    maxWidth: 400,
    width: "100%",
    margin: "0 auto"
  },
  btnText: {
    color: theme.palette.primary.contrastText
  }
});

const RegisterBtn = ({ classes, children, itemId }) => {
  return (
    <Button
      className={classes.getStartedButton}
      component={Link}
      to={{
        pathname: "/users/signup",
        search: itemId ? `?item=${itemId}` : ""
      }}
      size="large"
      variant="contained"
    >
      <Typography className={classes.btnText} variant="button">
        {children}
      </Typography>
    </Button>
  );
};

RegisterBtn.propTypes = {
  itemId: PropTypes.string
};

export default withStyles(styles)(RegisterBtn);
