import React, { Fragment } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Fade from "@material-ui/core/Fade";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
const styles = theme => ({
  root: {
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  },
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    height: 200
  },
  progress: {
    margin: `${theme.spacing.unit * 3}px auto`
  },
  paper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
});

const CircularLoading = ({ classes, children, isVisable }) => {
  return (
    <div className={classes.root}>
      <Fade in={isVisable}>
        <Paper className={classes.paper} elevation={1}>
          <div className={classes.container}>
            <CircularProgress className={classes.progress} thickness={5} />
            <Typography variant="title">{children}</Typography>
          </div>
        </Paper>
      </Fade>
    </div>
  );
};

CircularLoading.defaultProps = {
  isVisable: false
};

CircularLoading.propTypes = {
  isVisable: PropTypes.bool
};

export default withStyles(styles)(CircularLoading);
