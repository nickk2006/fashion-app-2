import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { Field, formValueSelector } from "redux-form";
import { Checkbox, TextField } from "redux-form-material-ui";
import InputLabel from "@material-ui/core/InputLabel";
import Fade from "@material-ui/core/Fade";
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";

const styles = theme => ({
  container: {
    display: "grid",
    gridTemplateColumns: `6em 50px ${theme.inputWidth.mobile.short}px`,
    "align-items": "center",
    justifyItems: "start"
  }
});

const selector = formValueSelector("sell");

const ShippingTable = ({
  isShippingUS,
  isShippingOutsideUS,
  classes: { container }
}) => {
  return (
    <div>
      <div className={container}>
        <InputLabel>United States</InputLabel>
        <Field name="isShippingUS" component={Checkbox} />
        {isShippingUS ? (
          <Fade in={isShippingUS}>
            <Field
              name="shippingUSPrice"
              type="tel"
              component={TextField}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">$</InputAdornment>
                )
              }}
            />
          </Fade>
        ) : (
          <div />
        )}
      </div>
      <div className={container}>
        <InputLabel>Outside U.S.</InputLabel>
        <Field name="isShippingOutsideUS" component={Checkbox} />
        <Fade in={isShippingOutsideUS}>
          <Field
            name="shippingOusideUSPrice"
            type="tel"
            component={TextField}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">$</InputAdornment>
              )
            }}
          />
        </Fade>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  isShippingUS: selector(state, "isShippingUS"),
  isShippingOutsideUS: selector(state, "isShippingOutsideUS")
});

export default compose(
  connect(mapStateToProps),
  withStyles(styles)
)(ShippingTable);
