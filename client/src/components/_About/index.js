import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import { connect } from "react-redux";
import { compose } from "redux";
import { Helmet } from "react-helmet";
import RegisterBtn from "../RegisterBtn";
import { aboutPhoto } from "../../assets";
const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing.unit * 2
  },
  image: {
    margin: `${theme.spacing.unit}px auto`,
    flexGrow: 0
  },
  centerText: {
    margin: `${theme.spacing.unit * 3}px auto ${theme.spacing.unit}px`
  },
  linkStyle: {
    color: "#000000",
    "&:visited": {
      color: "#000000"
    }
  },
  answer: { marginLeft: theme.spacing.unit * 2 }
});

class ConnectStripe extends Component {
  render() {
    const { classes, isRegistered } = this.props;

    return (
      <div className={classes.root}>
        <Helmet>
          <title>About</title>
        </Helmet>
        <Typography gutterBottom align="justify" variant="headline">
          <b>Walcroft Street</b> is a peer to peer marketplace that aims to
          solve the problem of inconsistent sizing between different fashion
          designers.
        </Typography>
        <br />
        <br />
        <Typography paragraph variant="title">
          How does <b>Walcroft Street</b> work?
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
          align="justify"
        >
          Sellers measure the key measurements of the item they want to sell.
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
          align="justify"
        >
          Buyers create measurement profiles for each type of item they're
          looking for.
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
          align="justify"
        >
          <b>Walcroft Street</b> will only show buyers clothes that match their
          measurement profiles.
        </Typography>
        <br />
        <Typography gutterBottom variant="title">
          How much do you charge?
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
          align="justify"
        >
          9% to sellers (6.1% commission + 2.9% Stripe payment processing).
          Nothing to buyers.
        </Typography>
        <br />
        <Typography gutterBottom variant="title">
          Is there buyer protection?
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
          align="justify"
        >
          Yes, buyers have two days to inspect the item after it is delivered.
        </Typography>
        <br />
        <Typography gutterBottom variant="title">
          How do payments work?
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
          align="justify"
        >
          <b>Walcroft Street</b> uses Stripe to get you paid quickly and keep
          your personal and payment information secure.
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
          align="justify"
        >
          When you buy an item, we place a hold for the purchase price on your
          card. Once the item is shipped, we charge the card, but keep the funds
          in our system. Two days after the items are delivered, if there is no
          dispute, funds are transfered to the seller.
        </Typography>
        <br />
        <Typography gutterBottom variant="title">
          When do sellers get paid?
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
          align="justify"
        >
          Our goal is to get you paid as quickly as possible. Buyers have two
          days after delivery to inspect the item. After the two days, we will
          initiate the transfer. Stripe usually takes five to ten business days
          to complete the transfer.
        </Typography>
        <br />
        <Typography gutterBottom variant="title">
          Who made this?
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
        >
          Hi! My name is Nick (on the left) and this community was created for
          the people who have ever felt the inconvenience of buying something
          that just didn't fit right (because that happens way too many times).
        </Typography>
        <img alt="Founder Picture" className={classes.image} src={aboutPhoto} />
        <br />
        <Typography gutterBottom variant="title">
          What clothing types do you currently support?
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
        >
          We currently support pants, dress shirts, and jackets (the items that
          compose a suit). In the coming days/weeks, we plan on addding support
          for more items (both men's and women's clothing).
        </Typography>
        <br />
        <Typography gutterBottom variant="title">
          I have questions/suggestions?
        </Typography>
        <Typography
          className={classes.answer}
          gutterBottom
          variant="subheading"
        >
          YES! Feel free to shoot me an email at{" "}
          <a href="mailto:nick@walcroftstreet.com">nick@walcroftstreet.com</a>{" "}
          and I'll respond as quickly as possible.
        </Typography>
        <br />
        {isRegistered ? (
          undefined
        ) : (
          <RegisterBtn>Create your Walcroft account</RegisterBtn>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isRegistered: state.auth.user.token
});

export default compose(
  connect(mapStateToProps),
  withStyles(styles)
)(ConnectStripe);
