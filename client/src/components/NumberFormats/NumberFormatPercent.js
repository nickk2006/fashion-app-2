import React from "react";
import NumberFormat from "react-number-format";

export default ({ inputRef, onChange, ...other }) => (
  <NumberFormat
    {...other}
    getInputRef={inputRef}
    onValueChange={(_, e) => onChange(e)}
    suffix="%"
    type="tel"
    decimalScale={0}
  />
);
