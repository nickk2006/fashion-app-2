import React, { Component, Fragment } from "react";
import CatalogProductGridComponent from "../CatalogProductGrid/ProductPreviewComponent";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import { Helmet } from "react-helmet";
import isEmpty from "lodash/isEmpty";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import { getUserProfile } from "../../_actions";
import PaginationBar from "../PaginationBar";

const styles = {};

class UserProfile extends Component {
  componentDidMount() {
    const { match, getItems } = this.props;
    getItems(match.params.username);
  }

  render() {
    const {
      match: {
        params: { username }
      },
      items
    } = this.props;
    return (
      <div>
        <Helmet>
          <title>{`${username}'s Profile`}</title>
        </Helmet>
        <Typography variant="display3">
          {username}
          's Items For Sale
        </Typography>
        {isEmpty(items) ? (
          <Typography>No Items</Typography>
        ) : (
          <Fragment>
            <CatalogProductGridComponent items={items} />
            <PaginationBar />
          </Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  items: state.items.items
});

const mapDispatchToProps = dispatch => ({
  getItems: username => dispatch(getUserProfile(username))
});

UserProfile.propTypes = {
  items: PropTypes.array.isRequired, // eslint-disable-line
  getItems: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired // eslint-disable-line
};

export default compose(
  withRouter,
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(UserProfile);
