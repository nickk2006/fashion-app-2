import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import isEmpty from "lodash/isEmpty";
import PropTypes from "prop-types";
import PaginationComponent from "./PaginationBarComponent";
import { queryStringToObject, queryObjectToString } from "../../_helpers";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = {
  root: {
    display: "flex",
    "justify-content": "center",
    width: "100%"
  }
};

class PaginationContainer extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(newPage) {
    const { history, location } = this.props;

    try {
      const oldQuery = queryStringToObject(location.search);
      const newQuery = Object.assign({}, oldQuery, { pn: newPage });
      const newQueryString = queryObjectToString(newQuery);
      history.push({ pathname: location.pathname, search: newQueryString });
    } catch (e) {
      console.log("ERROR WITH PAGINATION BAR", e.toString());
    }
  }

  render() {
    const {
      location,
      totalCount,
      isFetchingItem,
      isItemsFetched,
      classes: { root }
    } = this.props;
    const totalPages = Math.ceil(totalCount / 30);
    const currentPage = isEmpty(queryStringToObject(location.search).pn)
      ? 1
      : totalPages < queryStringToObject(location.search).pn
        ? Math.ceil(totalPages / 30)
        : Math.floor(queryStringToObject(location.search).pn);

    return !isItemsFetched ? (
      <div />
    ) : isFetchingItem ? (
      <div className={root}>Loading...</div>
    ) : totalCount === 0 ? (
      <div className={root}>
        <PaginationComponent
          currentPage={1}
          totalPages={1}
          onChange={this.handleChange}
        />
      </div>
    ) : (
      <div>
        <div className={root}>
          <PaginationComponent
            currentPage={currentPage}
            totalPages={totalPages}
            onChange={this.handleChange}
            hideEllipsis={true}
            boundaryPagesRange={0}
            siblingPagesRange={2}
          />
        </div>
      </div>
    );
  }
}

PaginationContainer.propTypes = {
  totalCount: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired, //eslint-disable-line
  location: PropTypes.object.isRequired //eslint-disable-line
};

const mapStateToProps = state => ({
  isItemsFetched: state.items.isItemsFetched,
  totalCount: state.items.totalCount,
  isFetchingItem: state.items.isFetchingItem
});

export default compose(
  withRouter,
  connect(mapStateToProps),
  withStyles(styles)
)(PaginationContainer);
