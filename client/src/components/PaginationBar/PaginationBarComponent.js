/*eslint-disable*/
import React from "react";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import LastPageIcon from "@material-ui/icons/LastPage";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import withStyles from "@material-ui/core/styles/withStyles";
import {
  createUltimatePagination,
  ITEM_TYPES
} from "react-ultimate-pagination";

const activePageStyle = { fontWeight: "bold" };
const styles = {
  button: {
    display: "inline-block",
    padding: "0.75em",
    minHeight: 0,
    minWidth: 0
  }
};
const Page = withStyles(styles)(
  ({ value, isActive, onClick, isDisabled, classes: { button } }) => (
    <Button
      className={button}
      variant="flat"
      style={isActive ? activePageStyle : null}
      onClick={onClick}
      disabled={isDisabled}
      size="small"
    >
      {value.toString()}
    </Button>
  )
);

const Ellipsis = ({ onClick, isDisabled }) => (
  <Button variant="flat" onClick={onClick} disabled={isDisabled}>
    ...
  </Button>
);

const FirstPageLink = ({ isActive, onClick, isDisabled }) => (
  <IconButton
    variant="flat"
    onClick={onClick}
    disabled={isDisabled}
    label="..."
  >
    <FirstPageIcon />
  </IconButton>
);

const PreviousPageLink = ({ isActive, onClick, isDisabled }) => (
  <IconButton onClick={onClick} disabled={isDisabled}>
    <ChevronLeftIcon />
  </IconButton>
);

const NextPageLink = ({ isActive, onClick, isDisabled }) => (
  <IconButton onClick={onClick} disabled={isDisabled}>
    <ChevronRightIcon />
  </IconButton>
);

const LastPageLink = ({ isActive, onClick, isDisabled }) => (
  <IconButton onClick={onClick} disabled={isDisabled}>
    <LastPageIcon />
  </IconButton>
);

const itemTypeToComponent = {
  [ITEM_TYPES.PAGE]: Page,
  [ITEM_TYPES.ELLIPSIS]: Ellipsis,
  [ITEM_TYPES.FIRST_PAGE_LINK]: FirstPageLink,
  [ITEM_TYPES.PREVIOUS_PAGE_LINK]: PreviousPageLink,
  [ITEM_TYPES.NEXT_PAGE_LINK]: NextPageLink,
  [ITEM_TYPES.LAST_PAGE_LINK]: LastPageLink
};

export default createUltimatePagination({
  itemTypeToComponent
});

// const PaginationBarContainer = props => {
//   const { count, handleChange, limit, offset } = props;
//   const currentPage = Math.ceil(offset / limit);
//   const totalPages = Math.ceil(count / limit);
//   return (
//     <div>
//       <Pagination currentPage={currentPage} totalPages={totalPages} />
//     </div>
//   );
// };

// PaginationBarContainer.propTypes = {
//   count: PropTypes.number.isRequired,
//   limit: PropTypes.number.isRequired,
//   offset: PropTypes.number.isRequired,
//   handleChange: PropTypes.func.isRequired
// };

// const mapStateToProps = state => ({
//   count: state.items.count,
//   next: state.items.next,
//   limit: state.items.limit,
//   offset: state.items.offset
// });

// const mapDispatchToProps = dispatch => ({
//   handleChange: searchQuery => dispatch(paginate(searchQuery))
// });

// export default compose(
//   withRouter,
//   connect(
//     mapStateToProps,
//     mapDispatchToProps
//   )
// )(PaginationBarContainer);
