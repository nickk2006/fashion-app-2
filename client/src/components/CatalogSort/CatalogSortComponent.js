import React from "react";
import { Field } from "redux-form";
import Radio from "@material-ui/core/Radio";
import { RadioGroup } from "redux-form-material-ui";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
  root: {}
});

const CatalogSortComponent = () => (
  <form>
    <Field name="sort" component={RadioGroup}>
      <Radio value="new" label="Newest Arrivals" />
      <Radio value="priceLowToHigh" label="Price: Low to High" />
      <Radio value="priceHighToLow" label="Price: High to Low" />
    </Field>
  </form>
);

export default CatalogSortComponent;
