import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { Helmet } from "react-helmet";
import CatalogProductGrid from "../CatalogProductGrid";
import PaginationBar from "../PaginationBar";
import Filter from "../CatalogFilter";
import PropTypes from "prop-types";
const styles = {
  container: {
    "max-width": 980,
    display: "block",
    "margin-left": "auto",
    "margin-right": "auto"
  }
};

class CatalogContainer extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  componentDidUpdate() {
    if (!this.props.isLanding) {
      const node = this.myRef.current;
      node.scrollIntoView();
    }
  }
  render() {
    const {
      classes: { container }
    } = this.props;
    return (
      <div className={container}>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Walcroft Street Catalog</title>
        </Helmet>
        <div ref={this.myRef} />
        <Filter />
        <CatalogProductGrid />
        <PaginationBar />
      </div>
    );
  }
}

CatalogContainer.defaultProps = {
  isLanding: false
};

CatalogContainer.propTypes = {
  isLanding: PropTypes.bool
};

export default withStyles(styles)(CatalogContainer);
