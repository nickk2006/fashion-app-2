import React, { Component } from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import firebase from "firebase/app";
import "firebase/auth";
import "./firebaseui-styling.global.css"; // Import globally.

class SignInScreen extends Component {
  uiConfig = {
    signInFlow: "popup",
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      {
        provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
        requireDisplayName: true
      }
    ],
    callbacks: {
      signInSuccessWithAuthResult: () => false
    }
  };

  state = {
    isSignedIn: undefined
  };

  // /**
  //  * @inheritDoc
  //  */
  // componentDidMount() {
  //   this.unregisterAuthObserver = firebaseApp
  //     .auth()
  //     .onAuthStateChanged(user => {
  //       this.setState({ isSignedIn: !!user });
  //     });
  // }

  // /**
  //  * @inheritDoc
  //  */
  // componentWillUnmount() {
  //   this.unregisterAuthObserver();
  // }

  // /**
  //  * @inheritDoc
  //  */
  render() {
    return (
      <StyledFirebaseAuth
        uiConfig={this.uiConfig}
        firebaseAuth={firebase.auth()}
      />
    );
  }
}

export default SignInScreen;
