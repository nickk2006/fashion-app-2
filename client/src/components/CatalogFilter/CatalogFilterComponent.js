import React, { Component } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import CheckboxGroup from "../CheckboxGroup";
import firebase from "../../firebase";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
const styles = theme => ({
  root: {
    width: "100vw",
    maxWidth: "100%",
    "margin-bottom": `${theme.spacing.unit}px`
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "33.33%",
    flexShrink: 0
  }
});

class CatalogFilterExpansionPanels extends Component {
  state = {
    expanded: null
  };

  componentDidMount() {
    this.unregisterAuthChanged = firebase
      .auth()
      .onAuthStateChanged(() => this.props.getFilters());
  }

  componentWillUnmount() {
    this.unregisterAuthChanged();
  }

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };

  render() {
    const {
      classes: { root, heading },
      color,
      material,
      itemType,
      designer,
      profile
    } = this.props;
    const { expanded } = this.state;

    return (
      <div className={root}>
        <ExpansionPanel
          expanded={expanded === "designer"}
          onChange={this.handleChange("designer")}
          CollapseProps={{ timeout: 400 }}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={heading}>Designers</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {designer.length > 0 ? (
              <CheckboxGroup name="designer" options={designer} />
            ) : (
              <Typography variant="caption">
                No filter available for Designers
              </Typography>
            )}
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel
          expanded={expanded === "color"}
          onChange={this.handleChange("color")}
          CollapseProps={{ timeout: 400 }}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={heading}>Colors</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {color.length > 0 ? (
              <CheckboxGroup name="color" options={color} />
            ) : (
              <Typography variant="caption">
                No filter available for Colors
              </Typography>
            )}
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel
          expanded={expanded === "itemType"}
          onChange={this.handleChange("itemType")}
          CollapseProps={{ timeout: 400 }}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={heading}>Item Type</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {itemType.length > 0 ? (
              <CheckboxGroup name="itemType" options={itemType} />
            ) : (
              <Typography variant="caption">
                No filter available for Item Type
              </Typography>
            )}
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel
          expanded={expanded === "material"}
          onChange={this.handleChange("material")}
          CollapseProps={{ timeout: 400 }}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={heading}>Materials</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {material.length > 0 ? (
              <CheckboxGroup name="material" options={material} />
            ) : (
              <Typography variant="caption">
                No filter available for Materials
              </Typography>
            )}
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel
          expanded={expanded === "myProfile"}
          onChange={this.handleChange("myProfile")}
          CollapseProps={{ timeout: 400 }}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={heading}>My Profiles</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {profile.length > 0 ? (
              <CheckboxGroup name="profile" options={profile} />
            ) : (
              <Button
                component={Link}
                to="/measurements"
                fullWidth
                variant="outlined"
              >
                Create A Profile
              </Button>
            )}
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    );
  }
}

export default withStyles(styles)(CatalogFilterExpansionPanels);
