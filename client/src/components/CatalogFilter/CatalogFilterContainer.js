import { connect } from "react-redux";
import { compose } from "redux";
import CatalogFilterComponent from "./CatalogFilterComponent";
import { reduxForm } from "redux-form";
import { withRouter } from "react-router-dom";
import { queryStringToObject, queryObjectToString } from "../../_helpers";
import { getFilters } from "../../_actions";

const mapStateToProps = (
  {
    filter: {
      colorFilters,
      materialFilters,
      itemTypeFilters,
      designerFilters,
      profile
    }
  },
  { location: { search } }
) => {
  const color = colorFilters.map(color => ({
    label: color,
    value: color
  }));

  const material = materialFilters.map(material => ({
    label: material,
    value: material
  }));

  const itemType = itemTypeFilters.map(itemType => ({
    label: itemType,
    value: itemType
  }));

  const designer = designerFilters.map(designer => ({
    label: designer,
    value: designer
  }));

  const initialValues = queryStringToObject(search);
  return {
    color,
    material,
    itemType,
    designer,
    profile,
    initialValues: {
      designer: initialValues.designer,
      color: initialValues.color,
      itemType: initialValues.itemType,
      material: initialValues.material,
      profile: initialValues.profile
    }
  };
};

const mapDispatchToProps = dispatch => ({
  getFilters: () => dispatch(getFilters())
});

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  reduxForm({
    form: "filter",
    onChange: (values, dispatch, props) => {
      const newQueryString = queryObjectToString(values);
      props.history.push({
        pathname: props.location.pathname,
        search: newQueryString
      });
    }
  })
)(CatalogFilterComponent);
