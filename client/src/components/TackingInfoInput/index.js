import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import withStyles from "@material-ui/core/styles/withStyles";
import { connect } from "react-redux";
import { reduxForm, Field } from "redux-form";
import { TextField } from "redux-form-material-ui";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {
  postTracking as postTrackingActionCreator,
  getTracking as getTrackingActionCreator
} from "../../_actions";
import CircularProgress from "@material-ui/core/CircularProgress";
import firebase from "../../firebase";

const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    maxWidth: `${theme.inputWidth.pc.short}px`
  }
});

class TrackingInfoInput extends Component {
  componentDidMount() {
    const {
      getTracking,
      match: {
        params: { itemId }
      },
      history
    } = this.props;
    this.unregisterAuthChange = firebase
      .auth()
      .onAuthStateChanged(user => getTracking(itemId, history));
  }

  componentWillUnmount() {
    this.unregisterAuthChange();
  }

  render() {
    const { classes, isLoading, isSuccess, error, handleSubmit } = this.props;
    if (isLoading || !isSuccess) {
      return (
        <div>
          <CircularProgress thickness={5} />
          <Typography variant="body1">Loading...</Typography>
        </div>
      );
    }
    if (isSuccess) {
      return (
        <form onSubmit={handleSubmit} className={classes.root}>
          <Typography variant="body1">
            After you've shipped the item, please submit the delivery service
            name and the tracking id below. We'll let the buyer know!
          </Typography>
          <Field
            component={TextField}
            name="carrier"
            type="text"
            label="Carrier Name"
          />
          <Field
            component={TextField}
            name="tracking_number"
            type="text"
            label="Tracking ID"
          />
          <br />
          <Button type="submit" variant="contained">
            Submit
          </Button>
          <Typography variant="body1">
            If you have any issues, feel free to contact me at{" "}
            <a href="mailto:nick@walcroftstreet.com">nick@walcroftstreet.com</a>
          </Typography>
          {!!error && <Typography variant="body1">error</Typography>}
        </form>
      );
    }
  }
}

const mapStateToProps = state => ({
  isLoading: state.tracking.isLoading,
  isSuccess: state.tracking.isSuccess,
  error: state.tracking.error
});

const mapDispatchToProps = dispatch => ({
  getTracking: (itemId, history) =>
    dispatch(getTrackingActionCreator(itemId, history)),
  postTracking: (data, itemId) =>
    dispatch(postTrackingActionCreator(data, itemId))
});

const reduxFormConfig = {
  form: "trackingInfo",
  onSubmit: (values, dispatch, { match: { params }, postTracking }) => {
    if (params.itemId) {
      return postTracking(values, params.itemId);
    }
  }
};

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles),
  reduxForm(reduxFormConfig)
)(TrackingInfoInput);
