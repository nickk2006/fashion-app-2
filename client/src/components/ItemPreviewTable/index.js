import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import map from "lodash/map";
import isEmpty from "lodash/isEmpty";
import { Typography } from "@material-ui/core";
const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
});

const ItemPreviewTable = ({ classes, items }) => {
  if (isEmpty(items)) {
    return <Typography variant="body1">No Items</Typography>;
  } else {
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Item</TableCell>
              <TableCell>Price</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {map(items, item => (
              <TableRow key={item._id}>
                <TableCell>
                  <Link to={`/catalog/items/${item._id}`}>{`${
                    item.facets.designer
                  } ${item.facets.itemType}`}</Link>
                </TableCell>
                <TableCell>{`$${item.price}`}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
};

ItemPreviewTable.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      facets: PropTypes.shape({
        designer: PropTypes.string,
        itemType: PropTypes.string
      }),
      photos: PropTypes.arrayOf(PropTypes.string),
      _id: PropTypes.string,
      price: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })
  )
};

export default withStyles(styles)(ItemPreviewTable);
