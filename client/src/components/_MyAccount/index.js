import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import MyAccountComponent from "./MyAccountComponent";
import { getStripeId as getStripeIdActionCreator } from "../../_actions";

class MyAccountContainer extends Component {
  componentDidMount() {
    const { location, getStripeId } = this.props;
    if (location.search) {
      getStripeId(location.search);
    }
  }

  render() {
    return <MyAccountComponent />;
  }
}

const mapDispatchToProps = dispatch => ({
  getStripeId: queryString => dispatch(getStripeIdActionCreator(queryString))
});

export default compose(
  withRouter,
  connect(
    null,
    mapDispatchToProps
  )
)(MyAccountContainer);
