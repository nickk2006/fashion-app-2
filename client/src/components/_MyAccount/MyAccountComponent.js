import React from "react";
import { Helmet } from "react-helmet";
import UserMeasurementsList from "../UserMeasurementsList";
import LogoutButton from "../LogoutButton";
import UserItemsForSaleList from "../UserItemsForSaleList";
import Typography from "@material-ui/core/Typography";
import StripeButton from "../StripeConnectButton";
const MyAccount = () => {
  return (
    <div>
      <Helmet>
        <title>My Account</title>
      </Helmet>
      <UserItemsForSaleList />
      <Typography variant="headline">Measurement Profiles</Typography>
      <UserMeasurementsList />
      <Typography variant="headline">
        Connect to Stripe to Start Selling
      </Typography>
      <StripeButton />
      <br />
      <LogoutButton />
    </div>
  );
};

export default MyAccount;
