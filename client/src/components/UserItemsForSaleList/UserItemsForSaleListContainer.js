import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { getAllItemsForSaleByUser } from "../../_actions";
import ItemPreviewTable from "../ItemPreviewTable";
import ItemPreviewTableDeletable from "../ItemPreviewTableDeletable";
import Typography from "@material-ui/core/Typography";
class UserItemsForSaleListContainer extends Component {
  componentDidMount() {
    const { getItems } = this.props;
    // const displayName = history.userId || username;
    getItems();
  }
  //TODO: Change userId to username
  render() {
    const { itemsForSale, itemsSold, itemsPurchased } = this.props;

    return (
      <Fragment>
        <Typography gutterBottom variant="title">
          Items For Sale
        </Typography>
        <ItemPreviewTableDeletable items={itemsForSale} />
        <br />
        <Typography gutterBottom variant="title">
          Items Purchased
        </Typography>
        <ItemPreviewTable items={itemsPurchased} />
        <br />
        <Typography gutterBottom variant="title">
          Items Sold
        </Typography>
        <ItemPreviewTable items={itemsSold} />
      </Fragment>
    );
  }
}

UserItemsForSaleListContainer.propTypes = {
  itemsForSale: PropTypes.array.isRequired,
  itemsSold: PropTypes.array.isRequired,
  itemsPurchased: PropTypes.array.isRequired,
  getItems: PropTypes.func.isRequired,
  isItemsEmpty: PropTypes.oneOfType([PropTypes.any, PropTypes.bool]),
  username: PropTypes.string,
  history: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  itemsForSale: state.userItems.itemsForSale,
  itemsPurchased: state.userItems.itemsPurchased,
  itemsSold: state.userItems.itemsSold,
  isItemsEmpty: state.userItems.isItemsEmpty,
  username: state.auth.user.username
});

const mapDispatchToProps = dispatch => ({
  getItems: () => dispatch(getAllItemsForSaleByUser())
});

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(UserItemsForSaleListContainer);
