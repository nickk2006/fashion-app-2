import React from "react";
import isEmpty from "lodash/isEmpty";
import locationHelperBuilder from "redux-auth-wrapper/history4/locationHelper";
import { connectedRouterRedirect } from "redux-auth-wrapper/history4/redirect";
import connectedAuthWrapper from "redux-auth-wrapper/connectedAuthWrapper";

const locationHelper = locationHelperBuilder({});

const userIsAuthenticatedDefaults = {
  authenticatingSelector: state => state.auth.user.isAuthenticating,
  authenticatedSelector: state => !isEmpty(state.auth.user.token),
  wrapperDisplayName: "UserIsAuthenticated"
};

export const userIsAuthenticated = connectedAuthWrapper(
  userIsAuthenticatedDefaults
);

const LoadingPlaceholder = () => <div>LOADING...</div>;

export const userIsAuthenticatedRedir = connectedRouterRedirect({
  ...userIsAuthenticatedDefaults,
  AuthenticatingComponent: LoadingPlaceholder,
  redirectPath: "/users/signin"
});

const userIsNotAuthenticatedDefaults = {
  authenticatedSelector: state => isEmpty(state.auth.user.token),
  wrapperDisplayName: "UserIsNotAuthenticated"
};

export const userIsNotAuthenticated = connectedAuthWrapper(
  userIsNotAuthenticatedDefaults
);

export const userIsNotAuthenticatedRedirSignin = connectedRouterRedirect({
  ...userIsNotAuthenticatedDefaults,
  redirectPath: (state, ownProps) =>
    locationHelper.getRedirectQueryParam(ownProps) || "/",
  allowRedirectBack: false
});

export const userIsNotAuthenticatedRedirSignup = connectedRouterRedirect({
  ...userIsNotAuthenticatedDefaults,
  redirectPath: (state, ownProps) =>
    locationHelper.getRedirectQueryParam(ownProps) || "/measurements",
  allowRedirectBack: false
});
