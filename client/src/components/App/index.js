import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  setUserIntoRedux as setUserIntoReduxActionCreator,
  logout as logoutActionCreator,
  setTokenIntoRedux as setTokenIntoReduxActionCreator,
  getUser as getUserActionCreator
} from "../../_actions";
import Header from "../Header";
import Footer from "../Footer";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import {
  userIsAuthenticatedRedir,
  userIsNotAuthenticatedRedirSignup,
  userIsNotAuthenticatedRedirSignin
} from "../AuthHOC";

import Landing from "../Landing";
import Signup from "../Signup";
import Signin from "../Signin";
import MeasurementsForm from "../_MeasurementsFormWizard";
import MyAccount from "../_MyAccount";
import Catalog from "../_Catalog";
import UserProfile from "../UserProfile";
import ProductInfo from "../_ProductInfo";
import SellFormWizard from "../_SellFormWizard";
import firebase from "../../firebase";
import About from "../_About";
import TrackingInfoInput from "../TackingInfoInput";
import CallbackStripe from "../callbackComponents/Stripe";
import "./stripeButton.css";

const styles = theme => ({
  "@global": {
    html: {
      height: "100%",
      "box-sizing": "border-box"
    },
    "*": {
      "box-sizing": "inherit",
      "&:before, &:after": {
        "box-sizing": "inherit"
      }
    },

    body: {
      position: "relative",
      margin: 0,
      minHeight: "100%"
    }
  },
  header: {
    flexGrow: 0
  },
  container: {
    maxWidth: theme.breakpoints.values.md,
    display: " flex",
    alignItems: "stretch",
    flexDirection: "column",
    margin: "auto"
  },
  body: {
    position: "relative",
    boxSizing: "inherit",
    paddingBottom: theme.spacing.unit * 4,
    margin: `${theme.spacing.unit}px auto 50px`,
    maxWidth: theme.breakpoints.values.sm,
    minWidth: `${theme.breakpoints.values.sm / 2}px`
  },
  footer: {
    boxSizing: "inherit",
    position: "absolute",
    maxWidth: theme.breakpoints.values.md,
    margin: "0 auto",
    right: 0,
    bottom: 0,
    left: 0,
    height: 50
  }
});

class App extends Component {
  componentDidMount() {
    const { setUserIntoRedux, setTokenIntoRedux, getUser } = this.props;
    this.unregisterAuthStateChanged = firebase
      .auth()
      .onAuthStateChanged(user => {
        if (user && !user.isAnonymous) {
          setUserIntoRedux(user);
          user.getIdToken().then(token => {
            setTokenIntoRedux(token);
            getUser(token);
          });
        } else {
          firebase.auth().signInAnonymously();
        }
      });
  }

  componentWillUnmount() {
    this.unregisterAuthStateChanged();
  }

  render() {
    const {
      classes: { body, container, header, footer }
    } = this.props;
    return (
      <Router>
        <div className={container}>
          <div className={header}>
            <Header />
          </div>
          <div className={body}>
            <Route exact path="/" component={Landing} />
            <Route
              exact
              path="/catalog"
              component={userIsAuthenticatedRedir(Catalog)}
            />
            <Route exact path="/about" component={About} />
            <Route
              exact
              path="/catalog/items/:itemId"
              component={ProductInfo}
            />
            <Route path="/measurements" component={MeasurementsForm} />
            <Route
              exact
              path="/users/signup"
              component={userIsNotAuthenticatedRedirSignup(Signup)}
            />
            <Route
              exact
              path="/users/signin"
              component={userIsNotAuthenticatedRedirSignin(Signin)}
            />
            <Route
              exact
              path="/sell"
              component={userIsAuthenticatedRedir(SellFormWizard)}
            />
            <Route
              exact
              path="/users/profile"
              component={userIsAuthenticatedRedir(MyAccount)}
            />
            <Route
              exact
              path="/users/profile/:username"
              component={UserProfile}
            />
            <Route
              exact
              path="/catalog/items/:itemId/shipping"
              component={userIsAuthenticatedRedir(TrackingInfoInput)}
            />
            <Route
              exact
              path="/users/auth/callback/stripe"
              component={CallbackStripe}
            />
          </div>
          <div className={footer}>
            <Footer />
          </div>
        </div>
      </Router>
    );
  }
}

App.propTypes = {
  user: PropTypes.object, // eslint-disable-line
  token: PropTypes.string,
  setUserIntoRedux: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  user: state.auth.user.user,
  token: state.auth.user.token,
  loggingIn: state.auth.user.loggingIn,
  isAuthenticating: state.auth.user.isAuthenticating
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logoutActionCreator),
  setTokenIntoRedux: token => dispatch(setTokenIntoReduxActionCreator(token)),
  setUserIntoRedux: user => dispatch(setUserIntoReduxActionCreator(user)),
  getUser: token => dispatch(getUserActionCreator(token))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(App);
