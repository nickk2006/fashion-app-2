import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { reset } from "redux-form";
import { connect } from "react-redux";
import { compose } from "redux";
import withStyles from "@material-ui/core/styles/withStyles";
import Step1ItemType from "./Step1ItemType";
import Step2Measurements from "./Step2Measurements";

const styles = {
  container: {
    display: "flex",
    width: "100%",
    justifyContent: "center"
  }
};

class MeasurementFormWizard extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.previousPageAndRest = this.previousPageAndRest.bind(this);
    this.state = { page: 0 };
  }

  nextPage() {
    this.setState({ page: this.state.page + 1 });
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 });
  }

  previousPageAndRest() {
    this.setState({ page: 0 });
    this.props.resetForm();
  }

  render() {
    const {
      classes: { container }
    } = this.props;
    const { page } = this.state;
    return (
      <div className={container}>
        <Helmet>
          <title>Create Measurement Profile</title>
        </Helmet>
        {page === 0 && (
          <Step1ItemType handleSubmit={this.nextPage} page={page} steps={2} />
        )}
        {page === 1 && (
          <Step2Measurements
            previousPage={this.previousPageAndRest}
            page={page}
            steps={2}
          />
        )}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  resetForm: () => dispatch(reset("sell"))
});

export default compose(
  connect(
    null,
    mapDispatchToProps
  ),
  withStyles(styles)
)(MeasurementFormWizard);
