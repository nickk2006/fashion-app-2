// import React, { Component } from "react";
import {
  reduxForm,
  reset,
  formValueSelector,
  initialize as initializeAC
} from "redux-form";
import S1Component from "./Step1ItemTypeComponent";
import { compose } from "redux";
import { connect } from "react-redux";
import { facets } from "../../../_constants";

const selector = formValueSelector("createMeasurement");

// class S1Container extends Component {
//   componentDidMount() {
//     this.props.resetForm();
//     // this.props.initializeForm();
//   }

//   render() {
//     const { handleSubmit, page, steps, itemType, resetForm } = this.props;
//     return (
//       <S1Component
//         handleSubmit={handleSubmit}
//         page={page}
//         steps={steps}
//         itemType={itemType}
//         handleReset={resetForm}
//       />
//     );
//   }
// }

const mapStateToProps = state => ({
  itemType: selector(state, "itemType"),
  size: selector(state, "size")
});

const mapDispatchToProps = dispatch => ({
  resetForm: () => dispatch(reset("createMeasurement")),
  initialize: measurements =>
    dispatch(initializeAC("createMeasurement", measurements, true))
  // initializeForm: () =>
  //   dispatch(
  //     initialize(
  //       "createMeasurement",
  //       {},
  //       {
  //         keepDirty: false
  //       }
  //     )
  //   )
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  reduxForm({
    form: "createMeasurement",
    enableReinitialize: true
  })
)(S1Component);
