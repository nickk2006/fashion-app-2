import React, { Fragment } from "react";
import NativePicker from "../../NativePicker";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import PrevNextButtons from "../../PrevNextButton";
import ItemTypePicker from "../../ItemTypePicker";
import validations from "../../../_helpers/FormValidationFields";
import { Field } from "redux-form";
import { renderTextFieldWithErrorOnSubmitFail } from "../../ReduxFormRenders";
import { facets } from "../../../_constants";
import Collapse from "@material-ui/core/Collapse";
import MeasurementsList from "../../MeasurementsList";

const styles = {
  container: {
    margin: "16 auto"
  },
  root: {
    margin: 16
  },
  formControl: {
    margin: "16px auto 0 auto"
  },
  inputContainer: {
    padding: 8,
    display: "flex",
    flexDirection: "column"
  }
};

const StepOneItemType = ({
  handleSubmit,
  pristine,
  page,
  steps,
  itemType = false,
  classes,
  initialize,
  size
}) => (
  <form className={classes.container} onSubmit={handleSubmit}>
    <div className={classes.inputContainer}>
      <Typography variant="title" gutterBottom>
        Create A Measurement Profile
      </Typography>
      <Typography variant="body1">What are you looking for?</Typography>
      <ItemTypePicker itemType={itemType} size={size} initialize={initialize} />
      <br />
      <Field
        name="size"
        component={NativePicker}
        label="Size"
        options={facets.size}
        defaultValue={facets.MEDIUM}
        onChange={function(event, newValue, previousValue, name) {
          if (
            newValue !== previousValue &&
            typeof previousValue !== "undefined"
          ) {
            initialize(facets.initialMeasurementValues[itemType][newValue]);
          } else {
            initialize({});
          }
        }}
      />
      <br />
      <Field
        component={renderTextFieldWithErrorOnSubmitFail}
        name="name"
        type="text"
        label="Profile Name"
        validate={[validations.required]}
      />
      <br />
      <Collapse in={!!itemType}>
        {itemType ? <MeasurementsList itemType={itemType} /> : undefined}
      </Collapse>
    </div>
    <PrevNextButtons
      steps={steps}
      pristine={pristine}
      handleSubmit={handleSubmit}
      page={page}
      disableNext={!itemType}
    />
  </form>
);

export default withStyles(styles)(StepOneItemType);
