import S2Measurements from "./Step2MeasurementsComponent";
import { reduxForm, formValueSelector, reset} from "redux-form";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { uploadMFAction } from "../../../_actions";
import { facets, routes } from "../../../_constants";
const selector = formValueSelector("createMeasurement");

export default compose(
  withRouter,
  reduxForm({
    form: "createMeasurement",
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    onSubmit: (values, dispatch) => {
      dispatch(uploadMFAction(values));
    },
    onSubmitSuccess: (values, dispatch, props) => {
      props.history.push("/users/profile")
      dispatch(reset('createMeasurement'))
    }
  }),
  connect(state => ({
    itemType: selector(state, "itemType")
  }))
)(S2Measurements);
