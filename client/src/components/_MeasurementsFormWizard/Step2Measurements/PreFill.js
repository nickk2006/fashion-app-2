import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import FormLabel from "@material-ui/core/FormLabel";
import { Field } from "redux-form";
import { TextField } from "redux-form-material-ui";
import MeasurementsList from "../../MeasurementsList";
import PrevNextButtons from "../../PrevNextButton";
import PulseLoader from "react-spinners/PulseLoader";
import Typography from "@material-ui/core/Typography";
import NativeSelect from "@material-ui/core/NativeSelect";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";

export default () => {
  return (
    <div>
      <Typography variant="title">Prefill</Typography>
      <Typography variant="body1">
        Optional. Select a prefill and adjust it to your perfect fit below.
      </Typography>
      <FormControl>
        <InputLabel htmlFor="prefill">Prefill</InputLabel>
        <NativeSelect fullWidth>
          <options value="small">Small</options>
          <options value="medium">Medium</options>
          <options value="large">Large</options>
        </NativeSelect>
      </FormControl>
    </div>
  );
};
