import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { Field } from "redux-form";
import { renderTextFieldWithErrorOnSubmitFail } from "../../ReduxFormRenders";
import MeasurementsList from "../../MeasurementsList";
import PrevNextButtons from "../../PrevNextButton";
import PulseLoader from "react-spinners/PulseLoader";
import Typography from "@material-ui/core/Typography";
import validations from "../../../_helpers/FormValidationFields";

const styles = theme => ({
  container: {
    margin: "16 auto"
  },
  inputContainer: {
    padding: 8,
    display: "flex",
    flexDirection: "column"
  },
  titleTextField: {
    width: theme.inputWidth.mobile.long,
    float: "right"
  },
  measurementsStyle: {
    margin: "0 auto",
    "margin-top": theme.spacing.unit * 2
  },
  formLabel: theme.formLabel,
  padding: {
    padding: `${theme.spacing.unit}px 0`
  },
  lineBreak: theme.hr.line.short,
  loading: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  }
});

const S2MeasurementsComponent = ({
  previousPage,
  handleSubmit,
  itemType,
  page,
  submitting,
  steps,
  classes: {
    container,
    inputContainer,
    measurementsStyle,
    formLabel,
    titleTextField,
    padding,
    lineBreak,
    loading
  }
}) => {
  return (
    <form className={container} onSubmit={handleSubmit}>
      <div className={inputContainer}>
        <Typography variant="title" gutterBottom>
          Measurements - {itemType}
        </Typography>
        <Field
          component={renderTextFieldWithErrorOnSubmitFail}
          name="name"
          type="text"
          label="Profile Name"
          helperText="eg, Relaxed Fit Casual Shirt, Work Pants, Going Out Jacket"
          validate={[validations.required]}
        />
        <br />
        <Typography gutterBottom>
          Get your best fitting item and measure each measurement below. Input a
          minimum and maximum that you would like the system to search for.
        </Typography>
        <MeasurementsList itemType={itemType} />
      </div>
      <PrevNextButtons
        steps={steps}
        handleSubmit={handleSubmit}
        previousPage={previousPage}
        page={page}
        submitting={submitting}
        nextLabel="Submit"
        previousLabel="Reset"
      />
      <div className={loading}>
        <PulseLoader size={10} loading={submitting} />
      </div>
    </form>
  );
};

export default withStyles(styles)(S2MeasurementsComponent);
