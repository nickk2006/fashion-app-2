import React from "react";
import Button from "@material-ui/core/Button";
import MobileStepper from "@material-ui/core/MobileStepper";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";

const styles = {
  stepper: {
    width: 300,
    margin: "0 auto"
  }
};

const PrevNextButtonsComponent = ({
  steps,
  page,
  previousPage,
  handleSubmit,
  submitting = false,
  pristine = false,
  disableNext = false,
  previousLabel = "Back",
  nextLabel = "Next",
  classes: { stepper },
  theme
}) => {
  return (
    <MobileStepper
      variant="dots"
      steps={steps}
      position="static"
      activeStep={page}
      className={stepper}
      backButton={
        <Button
          variant="outlined"
          disabled={page === 0}
          tabIndex="-1"
          onClick={previousPage}
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
          {previousLabel}
        </Button>
      }
      nextButton={
        page === steps - 1 ? (
          <Button
            disabled={pristine || submitting || disableNext}
            type="submit"
            variant="contained"
            onClick={handleSubmit}
          >
            {nextLabel}
          </Button>
        ) : (
          <Button
            disabled={pristine || disableNext}
            type="submit"
            variant="contained"
            onClick={handleSubmit}
          >
            {nextLabel}
            {theme.direction === "rtl" ? (
              <KeyboardArrowLeft />
            ) : (
              <KeyboardArrowRight />
            )}
          </Button>
        )
      }
    />
  );
};

PrevNextButtonsComponent.propTypes = {
  steps: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  previousPage: PropTypes.func,
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  nextLabel: PropTypes.string,
  previousLabel: PropTypes.string
};

export default withStyles(styles, { withTheme: true })(
  PrevNextButtonsComponent
);
