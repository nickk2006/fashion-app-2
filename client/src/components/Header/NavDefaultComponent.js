import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = {
  flex: {
    flexGrow: 1
  }
};

const NavDefault = ({ classes: { flex }, isLoggedIn }) => {
  return (
    <Fragment>
      <div className={flex}>
        <Button component={Link} to="/" color="inherit">
          <Typography variant="title" color="inherit">
            Walcroft Street{" "}
          </Typography>
        </Button>
      </div>
      <Button component={Link} to="/about" color="inherit">
        ABOUT
      </Button>
      <Button component={Link} to="/sell" color="inherit">
        SELL
      </Button>
      <Button component={Link} to="/measurements" color="inherit">
        MEASUREMENTS
      </Button>
      {isLoggedIn ? (
        <Button component={Link} to="/users/profile" color="inherit">
          My Account
        </Button>
      ) : (
        <Fragment>
          <Button component={Link} to="/users/signup" color="inherit">
            SIGNUP
          </Button>
          <Button component={Link} to="/users/signin" color="inherit">
            SIGNIN
          </Button>
        </Fragment>
      )}
    </Fragment>
  );
};

export default withStyles(styles)(NavDefault);
