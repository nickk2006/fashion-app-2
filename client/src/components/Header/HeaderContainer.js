import React, { Component } from "react";
import { connect } from "react-redux";
import HeaderComponent from "./HeaderComponent";
import firebase from "../../firebase";
const mapStateToProps = state => ({
  token: state.auth.user.token
});

class HeaderContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAnonymous: true
    };
  }
  componentDidMount() {
    this.unregisterAuthChanged = firebase.auth().onAuthStateChanged(user => {
      if (user && !user.isAnonymous) {
        this.setState({
          isAnonymous: false
        });
      } else {
        this.setState({
          isAnonymous: true
        });
      }
    });
  }
  render() {
    return <HeaderComponent isAnonymous={this.state.isAnonymous} />;
  }
}

export default connect(
  mapStateToProps,
  null
)(HeaderContainer);
