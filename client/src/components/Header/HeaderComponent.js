import React from "react";
import { compose } from "redux";
import withTheme from "@material-ui/core/styles/withTheme";
import Hidden from "@material-ui/core/Hidden";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import withStyles from "@material-ui/core/styles/withStyles";
import NavDefaultComponent from "./NavDefaultComponent";
import NavMobileComponent from "./NavMobileComponent";

const styles = {
  root: {
    flexGrow: 1,
    borderBottom: "1px solid #D3D3D3"
  }
};

const HeaderComponent = ({ classes: { root }, isAnonymous }) => (
  <header className={root}>
    <AppBar color="inherit" position="static" elevation={0}>
      <Toolbar>
        <Hidden mdUp>
          <NavMobileComponent isLoggedIn={!isAnonymous} />
        </Hidden>
        <Hidden smDown>
          <NavDefaultComponent isLoggedIn={!isAnonymous} />
        </Hidden>
      </Toolbar>
    </AppBar>
  </header>
);

export default compose(
  withTheme(),
  withStyles(styles)
)(HeaderComponent);
