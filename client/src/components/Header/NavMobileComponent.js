import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/MenuOutlined";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

const styles = {
  flex: {
    display: "flex",
    justifyContent: "space-between"
  },
  title: {
    flexGrow: 1
  },
  list: {
    width: 225
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

class NavMobileComponent extends Component {
  state = {
    isOpen: false
  };

  toggleDrawer = isOpen => () => {
    this.setState({
      isOpen: isOpen
    });
  };

  render() {
    const {
      classes: { flex, menuButton, title, list },
      isLoggedIn
    } = this.props;

    return (
      <div>
        <Drawer open={this.state.isOpen} onClose={this.toggleDrawer(false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer(false)}
            onKeyDown={this.toggleDrawer(false)}
          >
            <div className={list}>
              <List>
                <div>
                  <ListItem
                    button
                    component={Link}
                    onClick={this.toggleDrawer(false)}
                    to="/about"
                    color="inherit"
                  >
                    <ListItemText primary="ABOUT" />
                  </ListItem>
                  <ListItem
                    button
                    component={Link}
                    onClick={this.toggleDrawer(false)}
                    to="/sell"
                    color="inherit"
                  >
                    <ListItemText primary="SELL" />
                  </ListItem>
                  <ListItem
                    button
                    component={Link}
                    onClick={this.toggleDrawer(false)}
                    to="/measurements"
                    color="inherit"
                  >
                    <ListItemText primary="MEASUREMENTS" />
                  </ListItem>
                  {isLoggedIn ? (
                    <ListItem
                      button
                      component={Link}
                      onClick={this.toggleDrawer(false)}
                      to="/users/profile"
                      color="inherit"
                    >
                      <ListItemText primary="MY ACCOUNT" />
                    </ListItem>
                  ) : (
                    <Fragment>
                      <ListItem
                        button
                        component={Link}
                        onClick={this.toggleDrawer(false)}
                        to="/users/signup"
                        color="inherit"
                      >
                        <ListItemText primary="SIGN UP" />
                      </ListItem>
                      <ListItem
                        button
                        component={Link}
                        onClick={this.toggleDrawer(false)}
                        to="/users/signin"
                        color="inherit"
                      >
                        <ListItemText primary="SIGN IN" />
                      </ListItem>
                    </Fragment>
                  )}
                </div>
              </List>
            </div>
          </div>
        </Drawer>
        <div className={flex}>
          <IconButton
            color="inherit"
            className={menuButton}
            onClick={this.toggleDrawer(true)}
          >
            <MenuIcon />
          </IconButton>

          <Button
            className={title}
            component={Link}
            onClick={this.toggleDrawer(false)}
            to="/"
            color="inherit"
          >
            <Typography variant="title" color="inherit">
              Walcroft Street
            </Typography>
          </Button>
        </div>
      </div>
    );
  }
}

NavMobileComponent.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired
};
export default withStyles(styles)(NavMobileComponent);
