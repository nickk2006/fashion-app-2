import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import firebase from "../../firebase";
import LandingComponent from "./LandingComponent";

class LandingContainer extends Component {
  componentDidMount() {
    this.unregisterAuthChanged = firebase.auth().onAuthStateChanged(user => {
      if (user && !user.isAnonymous) {
        this.props.history.push("/catalog");
      }
    });
  }

  componentWillUnmount() {
    this.unregisterAuthChanged();
  }

  render() {
    return <LandingComponent />;
  }
}

export default withRouter(LandingContainer);
