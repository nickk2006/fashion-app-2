import React, { Fragment } from "react";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import Catalog from "../_Catalog";
import { Link } from "react-router-dom";
const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  divider: {
    margin: theme.spacing.unit * 6,
    width: "75%"
  },
  description: {
    marginLeft: theme.spacing.unit
  },
  btnContainer: {
    margin: `${theme.spacing.unit * 4}px 0px`
  },
  btn: {
    background: theme.palette.button.gradient
  },
  btnText: {
    color: theme.palette.primary.contrastText
  }
});

const LandingComponent = ({ classes }) => {
  return (
    <Fragment>
      <div className={classes.root}>
        <Typography align="center" variant="display3">
          A curated clothing catalog where everything fits YOU
        </Typography>
        <Divider light className={classes.divider} />
        <Typography paragraph variant="display2">
          How it works
        </Typography>
        <div className={classes.description}>
          <Typography gutterBottom variant="display1">
            Measure your best fitting clothes
          </Typography>
          <Typography gutterBottom variant="display1">
            Create a profile for each one
          </Typography>
          <Typography gutterBottom variant="display1">
            Discover clothes with similar fits
          </Typography>
        </div>
        <div className={classes.btnContainer}>
          <Button
            className={classes.btn}
            component={Link}
            to="/measurements"
            size="large"
            variant="contained"
          >
            <Typography className={classes.btnText} variant="button">
              Create My Catalog
            </Typography>
          </Button>
        </div>
      </div>
      <Divider light className={classes.divider} />

      <Catalog isLanding />
    </Fragment>
  );
};

export default withStyles(styles)(LandingComponent);
