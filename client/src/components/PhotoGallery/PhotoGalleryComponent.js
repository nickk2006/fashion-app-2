import React from "react";
import ImageGallery from "react-image-gallery";
import "../../styles/image-gallery.css";

export default ({ photos }) => {
  const images = photos.map(photoUrl => {
    const photoObject = {
      original: photoUrl,
      thumbnail: photoUrl
    };
    return photoObject;
  });

  return (
    <ImageGallery
      showFullscreenButton={false}
      showPlayButton={false}
      slideDuration={400}
      items={images}
    />
  );
};
