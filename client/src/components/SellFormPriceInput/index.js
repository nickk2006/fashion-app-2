import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import { Field } from "redux-form";
import { TextField } from "redux-form-material-ui";

const styles = theme => ({
  container: {
    display: "grid",
    gridTemplateColumns: `6em ${theme.inputWidth.mobile.short}px 50px`,
    "align-items": "center",
    justifyItems: "start"
  }
});
//TODO: Create input mask for price.
const PriceInput = ({ classes: { container } }) => (
  <div className={container}>
    <InputLabel>Price</InputLabel>
    <Field name="price" type="number" component={TextField} />
  </div>
);

export default withStyles(styles)(PriceInput);
