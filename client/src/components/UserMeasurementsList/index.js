/* eslint react/forbid-prop-types: 0 */
import React, { Component } from "react";
import { compose } from "redux";
import PropTypes from "prop-types";
import isEmpty from "lodash/isEmpty";
import map from "lodash/map";
import { connect } from "react-redux";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import withStyles from "@material-ui/core/styles/withStyles";
import { getAllMeasurementsForUser as getAllMeasurementsForUserAC } from "../../_actions";
import Typography from "@material-ui/core/Typography";
import firebase from "../../firebase";

const styles = {
  centerText: {
    textAlign: "center"
  }
};

class UserMeasurementsList extends Component {
  componentDidMount() {
    this.unregisterAuthChanged = firebase.auth().onAuthStateChanged(user => {
      this.props.getAllMeasurementsForUser();
    });
  }

  componentWillUnmount() {
    this.unregisterAuthChanged();
  }
  render() {
    const {
      measurementsList,
      classes: { centerText }
    } = this.props;
    return isEmpty(measurementsList) ? (
      <Typography className="content">No Measurements</Typography>
    ) : (
      <div>
        <div className="content">
          {map(measurementsList, (singleProfile, index) => (
            <div key={`${singleProfile[0].itemType}-${index}`}>
              <h3>{singleProfile[0].displayName}</h3>
              <Table aria-labelledby="tableTitle">
                <TableHead>
                  <TableRow>
                    <TableCell>Measurement</TableCell>
                    <TableCell className={centerText}>{"Min (in)"}</TableCell>
                    <TableCell className={centerText}>{"Max (in)"}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {map(singleProfile, (singleMeasurement, index) => (
                    <TableRow key={`${singleMeasurement.itemType}-${index}`}>
                      <TableCell component="th" scope="row">
                        {singleMeasurement.name}
                      </TableCell>
                      <TableCell className={centerText} numeric>
                        {singleMeasurement.min}
                      </TableCell>
                      <TableCell className={centerText} numeric>
                        {singleMeasurement.max}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

UserMeasurementsList.defaultProps = {
  measurementsList: []
};

UserMeasurementsList.propTypes = {
  measurementsList: PropTypes.array,
  getAllMeasurementsForUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  measurementsList: state.measurementForm.userMeasurementsList
});

const mapDispatchToProps = dispatch => ({
  getAllMeasurementsForUser: () => dispatch(getAllMeasurementsForUserAC())
});

export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(UserMeasurementsList);
