import React from "react";
import { compose } from "redux";
import withTheme from "@material-ui/core/styles/withTheme";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import withStyles from "@material-ui/core/styles/withStyles";
import instagram from "../../assets/instagram.png";
import facebook from "../../assets/facebook.png";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
const styles = theme => ({
  container: {
    display: "flex",
    justifyContent: "center",
    margin: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px`
  },
  madeIn: {
    lineHeight: theme.typography.body1.lineHeight,
    fontSize: "0.875rem"
  }
});

const FooterCompnoent = ({ classes }) => (
  <footer>
    <Divider />
    <div className={classes.container}>
      <Link style={{ textDecoration: "none" }} to="/about">
        <Typography variant="body1">Walcroft Street</Typography>
      </Link>
      <Typography className={classes.madeIn} variant="caption">
        &nbsp;- Made in Los Angeles, CA
      </Typography>
    </div>
  </footer>
);

export default compose(
  withTheme(),
  withStyles(styles)
)(FooterCompnoent);

//<img className={logo} alt="Instagram" src={instagram} />
//<img className={logo} alt="Facebook" src={facebook} />
