import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

export default createMuiTheme({
  typography: {
    // eslint-disable-next-line
    fontFamily: '"Lora", "serif"' + '"Open Sans", "sans-serif"'
  },
  palette: {
    primary: {
      light: "#58a5f0",
      main: "#0277bd",
      dark: "#004c8c",
      contrastText: "#ffffff"
    },
    secondary: {
      light: "#5e92f3",
      main: "#1565c0",
      dark: "#003c8f",
      contrastText: "#ffffff"
    },
    background: {
      default: "#ffffff"
    },
    button: {
      gradient: "linear-gradient(45deg, #0277bd 30%, #1565c0 90%)",
      fontColor: "#ffffff"
    }
  },
  hr: {
    line: {
      tall: {
        border: 0,
        "border-top": "1px solid #eee",
        margin: "0.5em 0"
      },
      short: {
        border: 0,
        "border-top": "1px solid #eee",
        margin: "0.1em 0"
      }
    },
    noLine: {
      tall: {
        border: 0,
        margin: "0.5em 0"
      },
      short: {
        border: 0,
        margin: "0.1em 0"
      }
    }
  },
  inputWidth: {
    mobile: { long: 200, short: 100, number: "5.25em" },
    pc: { long: 600, short: 300 }
  },
  formLabel: {
    "line-height": 2
  }
});
