// import dotenv from "dotenv";

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { StripeProvider } from "react-stripe-elements";

import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducers from "./_reducers";
import initialState from "./_reducers/initialState";
import CssBaseline from "@material-ui/core/CssBaseline";

import theme from "./styles/theme";

import App from "./components/App";

// import AppLanding from "./components/AppLanding";

if (process.env.NODE_ENV !== "production") {
  console.log(".env version:", process.env.REACT_APP_ENV_VERSION);
}

const composeEnhancers =
  process.env.NODE_ENV === "development"
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    : compose;

const store = createStore(
  reducers,
  initialState,
  composeEnhancers(applyMiddleware(thunk))
);

ReactDOM.render(
  <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY}>
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </MuiThemeProvider>
    </Provider>
  </StripeProvider>,
  document.querySelector("#root")
);

// ReactDOM.render(
//   <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY}>
//     <Provider store={store}>
//       <MuiThemeProvider theme={theme}>
//         <CssBaseline />
//         <AppLanding />
//       </MuiThemeProvider>
//     </Provider>
//   </StripeProvider>,
//   document.querySelector("#root")
// );
