const PANTS = "Pants";
const JACKET = "Jacket";
const DRESS_SHIRT = "Dress Shirt";
const CASUAL_SHIRT = "Casual Shirt";
const SWEATER = "Sweater";
const BLOUSE = "Blouse";

const M_PANTS = `Men ${PANTS}`;
const M_JACKET = `Men ${JACKET}`;
const M_DRESS_SHIRT = `Men ${DRESS_SHIRT}`;
const M_CASUAL_SHIRT = `Men ${CASUAL_SHIRT}`;
const M_SWEATER = `Men ${SWEATER}`;
const W_PANTS = `Women ${PANTS}`;
const W_JACKET = `Women ${JACKET}`;
const W_BLOUSE = `Women ${BLOUSE}`;
const W_SWEATER = `Women ${SWEATER}`;
const W_CASUAL_SHIRT = `Women ${CASUAL_SHIRT}`;
const W_DRESS_SHIRT = `Women ${DRESS_SHIRT}`;

const FRONT_RISE = "Front Rise";
const INSEAM = "Inseam";
const KNEE = "Knee";
const LEG_OPENING = "Leg Opening";
const TOP_THIGH = "Top Thigh";
const WAIST = "Waist";
const CHEST = "Chest";
const BODY_LENGTH = "Body Length";
const NECK = "Neck";
const SHOULDER = "Shoulder";
const SLEEVE = "Shoulder/Sleeve";
const BUST = "Bust";

const SMALL = "Small";
const MEDIUM = "Medium";
const LARGE = "Large";

const M_PANTS_FRONT_RISE = `${M_PANTS}-${FRONT_RISE}`;
const M_PANTS_INSEAM = `${M_PANTS}-${INSEAM}`;
const M_PANTS_KNEE = `${M_PANTS}-${KNEE}`;
const M_PANTS_LEG_OPENING = `${M_PANTS}-${LEG_OPENING}`;
const M_PANTS_TOP_THIGH = `${M_PANTS}-${TOP_THIGH}`;
const M_PANTS_WAIST = `${M_PANTS}-${WAIST}`;

const M_DRESS_SHIRT_CHEST = `${M_DRESS_SHIRT}-${CHEST}`;
const M_DRESS_SHIRT_BODY_LENGTH = `${M_DRESS_SHIRT}-${BODY_LENGTH}`;
const M_DRESS_SHIRT_SLEEVE = `${M_DRESS_SHIRT}-${SLEEVE}`;
const M_DRESS_SHIRT_NECK = `${M_DRESS_SHIRT}-${NECK}`;
const M_DRESS_SHIRT_SHOULDER = `${M_DRESS_SHIRT}-${SHOULDER}`;
const M_DRESS_SHIRT_WAIST = `${M_DRESS_SHIRT}-${WAIST}`;

const M_CASUAL_SHIRT_CHEST = `${M_CASUAL_SHIRT}-${CHEST}`;
const M_CASUAL_SHIRT_BODY_LENGTH = `${M_CASUAL_SHIRT}-${BODY_LENGTH}`;
const M_CASUAL_SHIRT_SLEEVE = `${M_CASUAL_SHIRT}-${SLEEVE}`;
const M_CASUAL_SHIRT_SHOULDER = `${M_CASUAL_SHIRT}-${SHOULDER}`;
const M_CASUAL_SHIRT_WAIST = `${M_CASUAL_SHIRT}-${WAIST}`;

const M_JACKET_CHEST = `${M_JACKET}-${CHEST}`;
const M_JACKET_BODY_LENGTH = `${M_JACKET}-${BODY_LENGTH}`;
const M_JACKET_SHOULDER = `${M_JACKET}-${SHOULDER}`;
const M_JACKET_SLEEVE = `${M_JACKET}-${SLEEVE}`;
const M_JACKET_WAIST = `${M_JACKET}-${WAIST}`;

const M_SWEATER_CHEST = `${M_SWEATER}-${CHEST}`;
const M_SWEATER_BODY_LENGTH = `${M_SWEATER}-${BODY_LENGTH}`;
const M_SWEATER_SLEEVE = `${M_SWEATER}-${SLEEVE}`;
const M_SWEATER_SHOULDER = `${M_SWEATER}-${SHOULDER}`;
const M_SWEATER_WAIST = `${M_SWEATER}-${WAIST}`;

const W_PANTS_FRONT_RISE = `${W_PANTS}-${FRONT_RISE}`;
const W_PANTS_INSEAM = `${W_PANTS}-${INSEAM}`;
const W_PANTS_KNEE = `${W_PANTS}-${KNEE}`;
const W_PANTS_LEG_OPENING = `${W_PANTS}-${LEG_OPENING}`;
const W_PANTS_TOP_THIGH = `${W_PANTS}-${TOP_THIGH}`;
const W_PANTS_WAIST = `${W_PANTS}-${WAIST}`;

const W_JACKET_BUST = `${W_JACKET}-${BUST}`;
const W_JACKET_BODY_LENGTH = `${W_JACKET}-${BODY_LENGTH}`;
const W_JACKET_SHOULDER = `${W_JACKET}-${SHOULDER}`;
const W_JACKET_SLEEVE = `${W_JACKET}-${SLEEVE}`;
const W_JACKET_WAIST = `${W_JACKET}-${WAIST}`;

const W_BLOUSE_BUST = `${W_BLOUSE}-${BUST}`;
const W_BLOUSE_BODY_LENGTH = `${W_BLOUSE}-${BODY_LENGTH}`;
const W_BLOUSE_SLEEVE = `${W_BLOUSE}-${SLEEVE}`;
const W_BLOUSE_WAIST = `${W_BLOUSE}-${WAIST}`;

const W_SWEATER_BUST = `${W_SWEATER}-${BUST}`;
const W_SWEATER_BODY_LENGTH = `${W_SWEATER}-${BODY_LENGTH}`;
const W_SWEATER_SLEEVE = `${W_SWEATER}-${SLEEVE}`;
const W_SWEATER_SHOULDER = `${W_SWEATER}-${SHOULDER}`;
const W_SWEATER_WAIST = `${W_SWEATER}-${WAIST}`;

const W_CASUAL_SHIRT_BUST = `${W_CASUAL_SHIRT}-${BUST}`;
const W_CASUAL_SHIRT_BODY_LENGTH = `${W_CASUAL_SHIRT}-${BODY_LENGTH}`;
const W_CASUAL_SHIRT_SLEEVE = `${W_CASUAL_SHIRT}-${SLEEVE}`;
const W_CASUAL_SHIRT_SHOULDER = `${W_CASUAL_SHIRT}-${SHOULDER}`;
const W_CASUAL_SHIRT_WAIST = `${W_CASUAL_SHIRT}-${WAIST}`;

const W_DRESS_SHIRT_BUST = `${W_DRESS_SHIRT}-${BUST}`;
const W_DRESS_SHIRT_BODY_LENGTH = `${W_DRESS_SHIRT}-${BODY_LENGTH}`;
const W_DRESS_SHIRT_SLEEVE = `${W_DRESS_SHIRT}-${SLEEVE}`;
const W_DRESS_SHIRT_SHOULDER = `${W_DRESS_SHIRT}-${SHOULDER}`;
const W_DRESS_SHIRT_WAIST = `${W_DRESS_SHIRT}-${WAIST}`;

exports.itemTypes = [
  M_CASUAL_SHIRT,
  M_DRESS_SHIRT,
  M_JACKET,
  M_PANTS,
  M_SWEATER,
  W_BLOUSE,
  W_CASUAL_SHIRT,
  W_DRESS_SHIRT,
  W_JACKET,
  W_PANTS,
  W_SWEATER
];

exports[M_PANTS] = [
  M_PANTS_FRONT_RISE,
  M_PANTS_INSEAM,
  M_PANTS_KNEE,
  M_PANTS_LEG_OPENING,
  M_PANTS_TOP_THIGH,
  M_PANTS_WAIST
];

exports[M_DRESS_SHIRT] = [
  M_DRESS_SHIRT_CHEST,
  M_DRESS_SHIRT_BODY_LENGTH,
  M_DRESS_SHIRT_SLEEVE,
  M_DRESS_SHIRT_NECK,
  M_DRESS_SHIRT_SHOULDER,
  M_DRESS_SHIRT_WAIST
];

exports[M_CASUAL_SHIRT] = [
  M_CASUAL_SHIRT_CHEST,
  M_CASUAL_SHIRT_BODY_LENGTH,
  M_CASUAL_SHIRT_SLEEVE,
  M_CASUAL_SHIRT_SHOULDER,
  M_CASUAL_SHIRT_WAIST
];

exports[M_JACKET] = [
  M_JACKET_CHEST,
  M_JACKET_BODY_LENGTH,
  M_JACKET_SHOULDER,
  M_JACKET_SLEEVE,
  M_JACKET_WAIST
];

exports[M_SWEATER] = [
  M_SWEATER_CHEST,
  M_SWEATER_BODY_LENGTH,
  M_SWEATER_SLEEVE,
  M_SWEATER_SHOULDER,
  M_SWEATER_WAIST
];

exports[W_PANTS] = [
  W_PANTS_FRONT_RISE,
  W_PANTS_INSEAM,
  W_PANTS_KNEE,
  W_PANTS_LEG_OPENING,
  W_PANTS_TOP_THIGH,
  W_PANTS_WAIST
];

exports[W_JACKET] = [
  W_JACKET_BUST,
  W_JACKET_BODY_LENGTH,
  W_JACKET_SHOULDER,
  W_JACKET_SLEEVE,
  W_JACKET_WAIST
];

exports[W_BLOUSE] = [
  W_BLOUSE_BUST,
  W_BLOUSE_BODY_LENGTH,
  W_BLOUSE_SLEEVE,
  W_BLOUSE_WAIST
];

exports[W_SWEATER] = [
  W_SWEATER_BUST,
  W_SWEATER_BODY_LENGTH,
  W_SWEATER_SLEEVE,
  W_SWEATER_SHOULDER,
  W_SWEATER_WAIST
];

exports[W_CASUAL_SHIRT] = [
  W_CASUAL_SHIRT_BUST,
  W_CASUAL_SHIRT_BODY_LENGTH,
  W_CASUAL_SHIRT_SLEEVE,
  W_CASUAL_SHIRT_SHOULDER,
  W_CASUAL_SHIRT_WAIST
];

exports[W_DRESS_SHIRT] = [
  W_DRESS_SHIRT_BUST,
  W_DRESS_SHIRT_BODY_LENGTH,
  W_DRESS_SHIRT_SLEEVE,
  W_DRESS_SHIRT_SHOULDER,
  W_DRESS_SHIRT_WAIST
];

exports.itemNameMapping = {
  [M_PANTS_WAIST]: WAIST,
  [M_PANTS_FRONT_RISE]: FRONT_RISE,
  [M_PANTS_INSEAM]: INSEAM,
  [M_PANTS_TOP_THIGH]: TOP_THIGH,
  [M_PANTS_KNEE]: KNEE,
  [M_PANTS_LEG_OPENING]: LEG_OPENING,

  [M_DRESS_SHIRT_NECK]: NECK,
  [M_DRESS_SHIRT_CHEST]: CHEST,
  [M_DRESS_SHIRT_WAIST]: WAIST,
  [M_DRESS_SHIRT_BODY_LENGTH]: BODY_LENGTH,
  [M_DRESS_SHIRT_SLEEVE]: SLEEVE,
  [M_DRESS_SHIRT_SHOULDER]: SHOULDER,

  [M_CASUAL_SHIRT_CHEST]: CHEST,
  [M_CASUAL_SHIRT_BODY_LENGTH]: BODY_LENGTH,
  [M_CASUAL_SHIRT_SLEEVE]: SLEEVE,
  [M_CASUAL_SHIRT_SHOULDER]: SHOULDER,
  [M_CASUAL_SHIRT_WAIST]: WAIST,

  [M_JACKET_BODY_LENGTH]: BODY_LENGTH,
  [M_JACKET_CHEST]: CHEST,
  [M_JACKET_WAIST]: WAIST,
  [M_JACKET_SHOULDER]: SHOULDER,
  [M_JACKET_SLEEVE]: SLEEVE,

  [M_SWEATER_CHEST]: CHEST,
  [M_SWEATER_BODY_LENGTH]: BODY_LENGTH,
  [M_SWEATER_SLEEVE]: SLEEVE,
  [M_SWEATER_SHOULDER]: SHOULDER,
  [M_SWEATER_WAIST]: WAIST,

  [W_PANTS_WAIST]: WAIST,
  [W_PANTS_FRONT_RISE]: FRONT_RISE,
  [W_PANTS_INSEAM]: INSEAM,
  [W_PANTS_LEG_OPENING]: LEG_OPENING,
  [W_PANTS_TOP_THIGH]: TOP_THIGH,
  [W_PANTS_KNEE]: KNEE,

  [W_JACKET_BODY_LENGTH]: BODY_LENGTH,
  [W_JACKET_BUST]: BUST,
  [W_JACKET_WAIST]: WAIST,
  [W_JACKET_SHOULDER]: SHOULDER,
  [W_JACKET_SLEEVE]: SLEEVE,

  [W_BLOUSE_BODY_LENGTH]: BODY_LENGTH,
  [W_BLOUSE_BUST]: BUST,
  [W_BLOUSE_WAIST]: WAIST,
  [W_BLOUSE_SLEEVE]: SLEEVE,

  [W_SWEATER_BUST]: BUST,
  [W_SWEATER_BODY_LENGTH]: BODY_LENGTH,
  [W_SWEATER_SHOULDER]: SHOULDER,
  [W_SWEATER_SLEEVE]: SLEEVE,
  [W_SWEATER_WAIST]: WAIST,

  [W_CASUAL_SHIRT_BUST]: BUST,
  [W_CASUAL_SHIRT_BODY_LENGTH]: BODY_LENGTH,
  [W_CASUAL_SHIRT_SLEEVE]: SLEEVE,
  [W_CASUAL_SHIRT_SHOULDER]: SHOULDER,
  [W_CASUAL_SHIRT_WAIST]: WAIST,

  [W_DRESS_SHIRT_BUST]: BUST,
  [W_DRESS_SHIRT_WAIST]: WAIST,
  [W_DRESS_SHIRT_BODY_LENGTH]: BODY_LENGTH,
  [W_DRESS_SHIRT_SLEEVE]: SLEEVE,
  [W_DRESS_SHIRT_SHOULDER]: SHOULDER
};

exports.itemTypeMappingWithMeasurements = {
  [M_PANTS_WAIST]: {
    fullName: M_PANTS_WAIST,
    name: WAIST,
    min: 13,
    max: 25,
    description:
      "Measure flat across the back waistband from one side to the other with the natural dip."
  },
  [M_PANTS_FRONT_RISE]: {
    fullName: M_PANTS_FRONT_RISE,
    name: FRONT_RISE,
    min: 9,
    max: 12,
    description:
      "Measure from the crotch seam to the top of the front waistband."
  },

  [M_PANTS_INSEAM]: {
    fullName: M_PANTS_INSEAM,
    name: INSEAM,
    min: 26,
    max: 40,
    description:
      "Measure from the crotch seam to the bottom of the leg on the inside seam."
  },
  [M_PANTS_TOP_THIGH]: {
    fullName: M_PANTS_TOP_THIGH,
    name: TOP_THIGH,
    min: 10,
    max: 15,
    description:
      "Measure width starting at the crotch seam and ending 1-2 inches below the hip area."
  },
  [M_PANTS_KNEE]: {
    fullName: M_PANTS_KNEE,
    name: KNEE,
    min: 6,
    max: 12,
    description: "Measure across the knee area."
  },
  [M_PANTS_LEG_OPENING]: {
    fullName: M_PANTS_LEG_OPENING,
    name: LEG_OPENING,
    min: 5,
    max: 12,
    description: "Measure across the leg opening from side to side."
  },
  [M_CASUAL_SHIRT_CHEST]: {
    fullName: M_CASUAL_SHIRT_CHEST,
    name: CHEST,
    min: 15,
    max: 30,
    description:
      "Measure from the bottom of one armhole across the chest to the bottom of the opposite armhole."
  },
  [M_CASUAL_SHIRT_WAIST]: {
    fullName: M_CASUAL_SHIRT_WAIST,
    name: WAIST,
    min: 15,
    max: 30,
    description:
      "Place the measuring tape where the natural waist would fall (around 6-8 inches below chest), usually a bit lower than halfway down the shirt, and measure from one side of the shirt across to the other."
  },
  [M_CASUAL_SHIRT_BODY_LENGTH]: {
    fullName: M_CASUAL_SHIRT_BODY_LENGTH,
    name: BODY_LENGTH,
    min: 10,
    max: 40,
    description:
      "Place the measuring tape in the middle of the collar and pull it down vertically to the bottom of the shirt."
  },
  [M_CASUAL_SHIRT_SLEEVE]: {
    fullName: M_CASUAL_SHIRT_SLEEVE,
    name: SLEEVE,
    min: 5,
    max: 45,
    description:
      "Start the tape at the center back of your neck, extend the tape over the top of your shoulder and down to the end of the sleeve cuff."
  },
  [M_CASUAL_SHIRT_SHOULDER]: {
    fullName: M_CASUAL_SHIRT_SHOULDER,
    name: SHOULDER,
    min: 10,
    max: 25,
    description:
      "Place the measuring tape at the corner of one shoulder and pull across to the other shoulder. This is an important measurement as some people typically wear the same size but have different shoulder widths."
  },
  [M_DRESS_SHIRT_NECK]: {
    fullName: M_DRESS_SHIRT_NECK,
    name: NECK,
    min: 10,
    max: 25,
    description:
      "Unbutton the shirt and spread it out on its back. Place one end of the measuring tape at the center of the button where the button stitching comes through to the inside of the collar band and measure along the collar up to the button."
  },
  [M_DRESS_SHIRT_CHEST]: {
    fullName: M_DRESS_SHIRT_CHEST,
    name: CHEST,
    min: 15,
    max: 30,
    description:
      "Measure from the bottom of one armhole across the chest to the bottom of the opposite armhole."
  },
  [M_DRESS_SHIRT_WAIST]: {
    fullName: M_DRESS_SHIRT_WAIST,
    name: WAIST,
    min: 15,
    max: 30,
    description:
      "Place the measuring tape where the natural waist would fall (around 6-8 inches below chest), usually a bit lower than halfway down the shirt, and measure from one side of the shirt across to the other."
  },
  [M_DRESS_SHIRT_BODY_LENGTH]: {
    fullName: M_DRESS_SHIRT_BODY_LENGTH,
    name: BODY_LENGTH,
    min: 10,
    max: 40,
    description:
      "Place the measuring tape in the middle of the collar and pull it down vertically to the bottom of the shirt."
  },
  [M_DRESS_SHIRT_SLEEVE]: {
    fullName: M_DRESS_SHIRT_SLEEVE,
    name: SLEEVE,
    min: 10,
    max: 45,
    description:
      "Start the tape at the center back of your neck, extend the tape over the top of your shoulder and down to the end of the sleeve cuff."
  },
  [M_DRESS_SHIRT_SHOULDER]: {
    fullName: M_DRESS_SHIRT_SHOULDER,
    name: SHOULDER,
    min: 10,
    max: 30,
    description:
      "Place the measuring tape at the corner of one shoulder and pull across to the other shoulder. This is an important measurement as some people typically wear the same size but have different shoulder widths."
  },
  [M_JACKET_BODY_LENGTH]: {
    fullName: M_JACKET_BODY_LENGTH,
    name: BODY_LENGTH,
    min: 10,
    max: 40,
    description:
      "Place the measuring tape in the middle of the collar and pull it down vertically to the bottom of the jacket."
  },
  [M_JACKET_CHEST]: {
    fullName: M_JACKET_CHEST,
    name: CHEST,
    min: 15,
    max: 30,
    description:
      "Measure from the bottom of one armhole across the chest to the bottom of the opposite armhole."
  },
  [M_JACKET_WAIST]: {
    fullName: M_JACKET_WAIST,
    name: WAIST,
    min: 15,
    max: 30,
    description:
      "Place the measuring tape where the natural waist would fall, usually a bit lower than halfway down the jacket, and measure from one side of the jacket across to the other."
  },
  [M_JACKET_SHOULDER]: {
    fullName: M_JACKET_SHOULDER,
    name: SHOULDER,
    min: 10,
    max: 30,
    description:
      "Place the measuring tape at the corner of one shoulder and pull across to the other shoulder. This is an important measurement as some people typically wear the same size but have different shoulder widths."
  },
  [M_JACKET_SLEEVE]: {
    fullName: M_JACKET_SLEEVE,
    name: SLEEVE,
    min: 20,
    max: 45,
    description:
      "Start the tape at the center back of your neck, extend the tape over the top of your shoulder and down to the end of the sleeve cuff."
  },
  [M_SWEATER_CHEST]: {
    fullName: M_SWEATER_CHEST,
    name: CHEST,
    min: 15,
    max: 30,
    description:
      "Measure from the bottom of one armhole across the chest to the bottom of the opposite armhole."
  },
  [M_SWEATER_WAIST]: {
    fullName: M_SWEATER_WAIST,
    name: WAIST,
    min: 15,
    max: 25,
    description:
      "Place the measuring tape where the natural waist would fall (around 6-8 inches below chest), usually a bit lower than halfway down the sweater, and measure from one side of the sweater across to the other."
  },
  [M_SWEATER_BODY_LENGTH]: {
    fullName: M_SWEATER_BODY_LENGTH,
    name: BODY_LENGTH,
    min: 10,
    max: 40,
    description:
      "Place the measuring tape in the middle of the collar and pull it down vertically to the bottom of the sweater."
  },
  [M_SWEATER_SLEEVE]: {
    fullName: M_SWEATER_SLEEVE,
    name: SLEEVE,
    min: 10,
    max: 45,
    description:
      "Start the tape at the center back of your neck, extend the tape over the top of your shoulder and down to the end of the sleeve cuff."
  },
  [M_SWEATER_SHOULDER]: {
    fullName: M_SWEATER_SHOULDER,
    name: SHOULDER,
    min: 10,
    max: 30,
    description:
      "Place the measuring tape at the corner of one shoulder and pull across to the other shoulder. This is an important measurement as some people typically wear the same size but have different shoulder widths."
  },
  [W_PANTS_WAIST]: {
    fullName: W_PANTS_WAIST,
    name: WAIST,
    min: 10,
    max: 25,
    description:
      "Measure flat across the back waistband from one side to the other with the natural dip."
  },
  [W_PANTS_FRONT_RISE]: {
    fullName: W_PANTS_FRONT_RISE,
    name: FRONT_RISE,
    min: 5,
    max: 15,
    description:
      "Measure from the crotch seam to the top of the front waistband."
  },

  [W_PANTS_INSEAM]: {
    fullName: W_PANTS_INSEAM,
    name: INSEAM,
    min: 20,
    max: 40,
    description:
      "Measure from the crotch seam to the bottom of the leg on the inside seam."
  },
  [W_PANTS_TOP_THIGH]: {
    fullName: W_PANTS_TOP_THIGH,
    name: TOP_THIGH,
    min: 8,
    max: 15,
    description:
      "Measure width starting at the crotch seam and ending 1-2 inches below the hip area."
  },
  [W_PANTS_KNEE]: {
    fullName: W_PANTS_KNEE,
    name: KNEE,
    min: 5,
    max: 15,
    description: "Measure across the knee area."
  },
  [W_PANTS_LEG_OPENING]: {
    fullName: W_PANTS_LEG_OPENING,
    name: LEG_OPENING,
    min: 5,
    max: 15,
    description: "Measure across the leg opening from side to side."
  },
  [W_JACKET_BODY_LENGTH]: {
    fullName: W_JACKET_BODY_LENGTH,
    name: BODY_LENGTH,
    min: 10,
    max: 40,
    description:
      "Place the measuring tape in the middle of the collar and pull it down vertically to the bottom of the jacket."
  },
  [W_JACKET_BUST]: {
    fullName: W_JACKET_BUST,
    name: BUST,
    min: 10,
    max: 30,
    description:
      "Button the item. Measure across the chest with the item lying flat, not stretched, but 'relaxed'. The measurement is taken from side seam to side seam directly under the armhole or sleeve."
  },
  [W_JACKET_WAIST]: {
    fullName: W_JACKET_WAIST,
    name: WAIST,
    min: 10,
    max: 30,
    description:
      "Place the measuring tape where the natural waist would fall, usually a bit lower than halfway down the jacket, and measure from one side of the jacket across to the other."
  },
  [W_JACKET_SHOULDER]: {
    fullName: W_JACKET_SHOULDER,
    name: SHOULDER,
    min: 10,
    max: 30,
    description:
      "Place the measuring tape at the corner of one shoulder and pull across to the other shoulder. This is an important measurement as some people typically wear the same size but have different shoulder widths."
  },
  [W_JACKET_SLEEVE]: {
    fullName: W_JACKET_SLEEVE,
    name: SLEEVE,
    min: 10,
    max: 45,
    description:
      "Start the tape at the center back of your neck, extend the tape over the top of your shoulder and down to the end of the sleeve cuff."
  },
  [W_BLOUSE_BODY_LENGTH]: {
    fullName: W_BLOUSE_BODY_LENGTH,
    name: BODY_LENGTH,
    min: 10,
    max: 40,
    description:
      "Place the measuring tape in the middle of the item and pull it down vertically to the bottom of the blouse."
  },
  [W_BLOUSE_BUST]: {
    fullName: W_BLOUSE_BUST,
    name: BUST,
    min: 10,
    max: 30,
    description:
      "Measure across the chest with the item lying flat, not stretched, but 'relaxed'. The measurement is taken from side seam to side seam directly under the armhole or sleeve."
  },
  [W_BLOUSE_WAIST]: {
    fullName: W_BLOUSE_WAIST,
    name: WAIST,
    min: 10,
    max: 25,
    description:
      "Place the measuring tape where the natural waist would fall, usually a bit lower than halfway down the blouse, and measure from one side of the blouse across to the other."
  },
  [W_BLOUSE_SLEEVE]: {
    fullName: W_BLOUSE_SLEEVE,
    name: SLEEVE,
    min: 5,
    max: 45,
    description:
      "Start the tape at the center back of your neck, extend the tape over the top of your shoulder and down to the end of the sleeve cuff."
  },
  [W_SWEATER_BUST]: {
    fullName: W_SWEATER_BUST,
    name: BUST,
    min: 10,
    max: 30,
    description:
      "Measure from the bottom of one armhole across the bust to the bottom of the opposite armhole."
  },
  [W_SWEATER_WAIST]: {
    fullName: W_SWEATER_WAIST,
    name: WAIST,
    min: 10,
    max: 30,
    description:
      "Place the measuring tape where the natural waist would fall (around 6-8 inches below chest), usually a bit lower than halfway down the sweater, and measure from one side of the sweater across to the other."
  },
  [W_SWEATER_BODY_LENGTH]: {
    fullName: W_SWEATER_BODY_LENGTH,
    name: BODY_LENGTH,
    min: 10,
    max: 40,
    description:
      "Place the measuring tape in the middle of the collar and pull it down vertically to the bottom of the sweater."
  },
  [W_SWEATER_SLEEVE]: {
    fullName: W_SWEATER_SLEEVE,
    name: SLEEVE,
    min: 5,
    max: 45,
    description:
      "Start the tape at the center back of your neck, extend the tape over the top of your shoulder and down to the end of the sleeve cuff."
  },
  [W_SWEATER_SHOULDER]: {
    fullName: W_SWEATER_SHOULDER,
    name: SHOULDER,
    min: 10,
    max: 30,
    description:
      "Place the measuring tape at the corner of one shoulder and pull across to the other shoulder. This is an important measurement as some people typically wear the same size but have different shoulder widths."
  },
  [W_CASUAL_SHIRT_BUST]: {
    fullName: W_CASUAL_SHIRT_BUST,
    name: BUST,
    min: 10,
    max: 30,
    description:
      "Measure from the bottom of one armhole across the bust to the bottom of the opposite armhole."
  },
  [W_CASUAL_SHIRT_WAIST]: {
    fullName: W_CASUAL_SHIRT_WAIST,
    name: WAIST,
    min: 10,
    max: 25,
    description:
      "Place the measuring tape where the natural waist would fall (around 6-8 inches below chest), usually a bit lower than halfway down the shirt, and measure from one side of the shirt across to the other."
  },
  [W_CASUAL_SHIRT_BODY_LENGTH]: {
    fullName: W_CASUAL_SHIRT_BODY_LENGTH,
    name: BODY_LENGTH,
    min: 10,
    max: 40,
    description:
      "Place the measuring tape in the middle of the collar and pull it down vertically to the bottom of the shirt."
  },
  [W_CASUAL_SHIRT_SLEEVE]: {
    fullName: W_CASUAL_SHIRT_SLEEVE,
    name: SLEEVE,
    min: 5,
    max: 45,
    description:
      "Start the tape at the center back of your neck, extend the tape over the top of your shoulder and down to the end of the sleeve cuff."
  },
  [W_CASUAL_SHIRT_SHOULDER]: {
    fullName: W_CASUAL_SHIRT_SHOULDER,
    name: SHOULDER,
    min: 10,
    max: 30,
    description:
      "Place the measuring tape at the corner of one shoulder and pull across to the other shoulder. This is an important measurement as some people typically wear the same size but have different shoulder widths."
  },
  [W_DRESS_SHIRT_BUST]: {
    fullName: W_DRESS_SHIRT_BUST,
    name: BUST,
    min: 10,
    max: 30,
    description:
      "Measure from the bottom of one armhole across the bust to the bottom of the opposite armhole."
  },
  [W_DRESS_SHIRT_WAIST]: {
    fullName: W_DRESS_SHIRT_WAIST,
    name: WAIST,
    min: 10,
    max: 25,
    description:
      "Place the measuring tape where the natural waist would fall (around 6-8 inches below chest), usually a bit lower than halfway down the shirt, and measure from one side of the shirt across to the other."
  },
  [W_DRESS_SHIRT_BODY_LENGTH]: {
    fullName: W_DRESS_SHIRT_BODY_LENGTH,
    name: BODY_LENGTH,
    min: 10,
    max: 40,
    description:
      "Place the measuring tape in the middle of the collar and pull it down vertically to the bottom of the shirt."
  },
  [W_DRESS_SHIRT_SLEEVE]: {
    fullName: W_DRESS_SHIRT_SLEEVE,
    name: SLEEVE,
    min: 5,
    max: 45,
    description:
      "Start the tape at the center back of your neck, extend the tape over the top of your shoulder and down to the end of the sleeve cuff."
  },
  [W_DRESS_SHIRT_SHOULDER]: {
    fullName: W_DRESS_SHIRT_SHOULDER,
    name: SHOULDER,
    min: 10,
    max: 30,
    description:
      "Place the measuring tape at the corner of one shoulder and pull across to the other shoulder. This is an important measurement as some people typically wear the same size but have different shoulder widths."
  }
};

exports.initialMeasurementValues = {
  [M_CASUAL_SHIRT]: {
    [SMALL]: {
      itemType: M_CASUAL_SHIRT,
      size: SMALL,
      name: `My ${CASUAL_SHIRT} Profile`,
      [M_CASUAL_SHIRT_CHEST]: { min: 18, max: 22 },
      [M_CASUAL_SHIRT_BODY_LENGTH]: { min: 26, max: 29 },
      [M_CASUAL_SHIRT_SLEEVE]: { min: 31, max: 33 },
      [M_CASUAL_SHIRT_SHOULDER]: { min: 16, max: 18 },
      [M_CASUAL_SHIRT_WAIST]: { min: 17, max: 22 }
    },
    [MEDIUM]: {
      itemType: M_CASUAL_SHIRT,
      size: MEDIUM,
      name: `My ${CASUAL_SHIRT} Profile`,
      [M_CASUAL_SHIRT_CHEST]: { min: 21.5, max: 23.5 },
      [M_CASUAL_SHIRT_BODY_LENGTH]: { min: 27, max: 30 },
      [M_CASUAL_SHIRT_SLEEVE]: { min: 32, max: 34 },
      [M_CASUAL_SHIRT_SHOULDER]: { min: 17, max: 19 },
      [M_CASUAL_SHIRT_WAIST]: { min: 19, max: 23.5 }
    },
    [LARGE]: {
      itemType: M_CASUAL_SHIRT,
      size: LARGE,
      name: `My ${CASUAL_SHIRT} Profile`,
      [M_CASUAL_SHIRT_CHEST]: { min: 23, max: 25 },
      [M_CASUAL_SHIRT_BODY_LENGTH]: { min: 27.5, max: 30.5 },
      [M_CASUAL_SHIRT_SLEEVE]: { min: 32.5, max: 34.5 },
      [M_CASUAL_SHIRT_SHOULDER]: { min: 18, max: 21 },
      [M_CASUAL_SHIRT_WAIST]: { min: 21, max: 25 }
    }
  },
  [M_DRESS_SHIRT]: {
    [SMALL]: {
      itemType: M_DRESS_SHIRT,
      size: SMALL,
      name: `My ${DRESS_SHIRT} Profile`,
      [M_DRESS_SHIRT_CHEST]: { min: 18, max: 22 },
      [M_DRESS_SHIRT_BODY_LENGTH]: { min: 26, max: 29 },
      [M_DRESS_SHIRT_SLEEVE]: { min: 31, max: 33 },
      [M_DRESS_SHIRT_NECK]: { min: 15, max: 16 },
      [M_DRESS_SHIRT_SHOULDER]: { min: 16, max: 18 },
      [M_DRESS_SHIRT_WAIST]: { min: 17, max: 22 }
    },
    [MEDIUM]: {
      itemType: M_DRESS_SHIRT,
      size: MEDIUM,
      name: `My ${DRESS_SHIRT} Profile`,
      [M_DRESS_SHIRT_CHEST]: { min: 21.5, max: 23.5 },
      [M_DRESS_SHIRT_BODY_LENGTH]: { min: 27, max: 30 },
      [M_DRESS_SHIRT_SLEEVE]: { min: 32, max: 34 },
      [M_DRESS_SHIRT_NECK]: { min: 16, max: 17 },
      [M_DRESS_SHIRT_SHOULDER]: { min: 17, max: 19 },
      [M_DRESS_SHIRT_WAIST]: { min: 19, max: 23.5 }
    },
    [LARGE]: {
      itemType: M_DRESS_SHIRT,
      size: LARGE,
      name: `My ${DRESS_SHIRT} Profile`,
      [M_DRESS_SHIRT_CHEST]: { min: 23, max: 25 },
      [M_DRESS_SHIRT_BODY_LENGTH]: { min: 27.5, max: 30.5 },
      [M_DRESS_SHIRT_SLEEVE]: { min: 32.5, max: 34.5 },
      [M_DRESS_SHIRT_NECK]: { min: 16.25, max: 17.5 },
      [M_DRESS_SHIRT_SHOULDER]: { min: 18, max: 21 },
      [M_DRESS_SHIRT_WAIST]: { min: 21, max: 25 }
    }
  },
  [M_JACKET]: {
    [SMALL]: {
      itemType: M_JACKET,
      size: SMALL,
      name: `My ${JACKET} Profile`,
      [M_JACKET_CHEST]: { min: 17.5, max: 19.5 },
      [M_JACKET_BODY_LENGTH]: { min: 27, max: 29.75 },
      [M_JACKET_SHOULDER]: { min: 16, max: 17.25 },
      [M_JACKET_SLEEVE]: { min: 23.5, max: 25 },
      [M_JACKET_WAIST]: { min: 16.5, max: 19.5 }
    },
    [MEDIUM]: {
      itemType: M_JACKET,
      size: MEDIUM,
      name: `My ${JACKET} Profile`,
      [M_JACKET_CHEST]: { min: 19, max: 21 },
      [M_JACKET_BODY_LENGTH]: { min: 28.5, max: 31.5 },
      [M_JACKET_SHOULDER]: { min: 17, max: 18.25 },
      [M_JACKET_SLEEVE]: { min: 24, max: 25.5 },
      [M_JACKET_WAIST]: { min: 17.5, max: 21 }
    },
    [LARGE]: {
      itemType: M_JACKET,
      size: LARGE,
      name: `My ${JACKET} Profile`,
      [M_JACKET_CHEST]: { min: 20.5, max: 22 },
      [M_JACKET_BODY_LENGTH]: { min: 29.5, max: 32 },
      [M_JACKET_SHOULDER]: { min: 18, max: 20 },
      [M_JACKET_SLEEVE]: { min: 24.5, max: 26 },
      [M_JACKET_WAIST]: { min: 18.5, max: 22 }
    }
  },
  [M_PANTS]: {
    [SMALL]: {
      itemType: M_PANTS,
      size: SMALL,
      name: `My ${PANTS} Profile`,
      [M_PANTS_FRONT_RISE]: { min: 9, max: 10 },
      [M_PANTS_INSEAM]: { min: 30, max: 36 },
      [M_PANTS_KNEE]: { min: 8, max: 8.75 },
      [M_PANTS_LEG_OPENING]: { min: 6.75, max: 8.75 },
      [M_PANTS_TOP_THIGH]: { min: 10, max: 11.75 },
      [M_PANTS_WAIST]: { min: 14, max: 16 }
    },
    [MEDIUM]: {
      itemType: M_PANTS,
      size: MEDIUM,
      name: `My ${PANTS} Profile`,
      [M_PANTS_FRONT_RISE]: { min: 9, max: 10.5 },
      [M_PANTS_INSEAM]: { min: 30, max: 36 },
      [M_PANTS_KNEE]: { min: 8.25, max: 9.25 },
      [M_PANTS_LEG_OPENING]: { min: 7, max: 9.25 },
      [M_PANTS_TOP_THIGH]: { min: 11, max: 12.5 },
      [M_PANTS_WAIST]: { min: 15, max: 17 }
    },
    [LARGE]: {
      itemType: M_PANTS,
      size: LARGE,
      name: `My ${PANTS} Profile`,
      [M_PANTS_FRONT_RISE]: { min: 9.5, max: 10.5 },
      [M_PANTS_INSEAM]: { min: 30, max: 36 },
      [M_PANTS_KNEE]: { min: 8.5, max: 10 },
      [M_PANTS_LEG_OPENING]: { min: 7.25, max: 10 },
      [M_PANTS_TOP_THIGH]: { min: 11.5, max: 13.5 },
      [M_PANTS_WAIST]: { min: 16, max: 20 }
    }
  },
  [M_SWEATER]: {
    [SMALL]: {
      itemType: M_SWEATER,
      size: SMALL,
      name: `My ${SWEATER} Profile`,
      [M_SWEATER_CHEST]: { min: 17, max: 22 },
      [M_SWEATER_BODY_LENGTH]: { min: 24, max: 27 },
      [M_SWEATER_SLEEVE]: { min: 31, max: 33 },
      [M_SWEATER_SHOULDER]: { min: 16.5, max: 18 },
      [M_SWEATER_WAIST]: { min: 17, max: 22 }
    },
    [MEDIUM]: {
      itemType: M_SWEATER,
      size: MEDIUM,
      name: `My ${SWEATER} Profile`,
      [M_SWEATER_CHEST]: { min: 21, max: 23.5 },
      [M_SWEATER_BODY_LENGTH]: { min: 25, max: 28 },
      [M_SWEATER_SLEEVE]: { min: 32, max: 34 },
      [M_SWEATER_SHOULDER]: { min: 17, max: 19.25 },
      [M_SWEATER_WAIST]: { min: 18, max: 23 }
    },
    [LARGE]: {
      itemType: M_SWEATER,
      size: LARGE,
      name: `My ${SWEATER} Profile`,
      [M_SWEATER_CHEST]: { min: 22, max: 25 },
      [M_SWEATER_BODY_LENGTH]: { min: 26, max: 29 },
      [M_SWEATER_SLEEVE]: { min: 32, max: 36 },
      [M_SWEATER_SHOULDER]: { min: 18, max: 21 },
      [M_SWEATER_WAIST]: { min: 19, max: 25 }
    }
  },
  [W_BLOUSE]: {
    [SMALL]: {
      itemType: W_BLOUSE,
      size: SMALL,
      name: `My ${BLOUSE} Profile`,
      [W_BLOUSE_BUST]: { min: 17, max: 20 },
      [W_BLOUSE_BODY_LENGTH]: { min: 10, max: 30 },
      [W_BLOUSE_SLEEVE]: { min: 10, max: 32 },
      [W_BLOUSE_WAIST]: { min: 14, max: 19 }
    },
    [MEDIUM]: {
      itemType: W_BLOUSE,
      size: MEDIUM,
      name: `My ${BLOUSE} Profile`,
      [W_BLOUSE_BUST]: { min: 18, max: 21 },
      [W_BLOUSE_BODY_LENGTH]: { min: 10, max: 30 },
      [W_BLOUSE_SLEEVE]: { min: 10, max: 34 },
      [W_BLOUSE_WAIST]: { min: 17, max: 22 }
    },
    [LARGE]: {
      itemType: W_BLOUSE,
      size: LARGE,
      name: `My ${BLOUSE} Profile`,
      [W_BLOUSE_BUST]: { min: 19, max: 23 },
      [W_BLOUSE_BODY_LENGTH]: { min: 10, max: 30 },
      [W_BLOUSE_SLEEVE]: { min: 10, max: 36 },
      [W_BLOUSE_WAIST]: { min: 19, max: 24 }
    }
  },
  [W_CASUAL_SHIRT]: {
    [SMALL]: {
      itemType: W_CASUAL_SHIRT,
      size: SMALL,
      name: `My ${CASUAL_SHIRT} Profile`,
      [W_CASUAL_SHIRT_BUST]: { min: 17, max: 20 },
      [W_CASUAL_SHIRT_BODY_LENGTH]: { min: 10, max: 30 },
      [W_CASUAL_SHIRT_SLEEVE]: { min: 10, max: 36 },
      [W_CASUAL_SHIRT_SHOULDER]: { min: 13.5, max: 15 },
      [W_CASUAL_SHIRT_WAIST]: { min: 14, max: 19 }
    },
    [MEDIUM]: {
      itemType: W_CASUAL_SHIRT,
      size: MEDIUM,
      name: `My ${CASUAL_SHIRT} Profile`,
      [W_CASUAL_SHIRT_BUST]: { min: 18, max: 21 },
      [W_CASUAL_SHIRT_BODY_LENGTH]: { min: 10, max: 30 },
      [W_CASUAL_SHIRT_SLEEVE]: { min: 10, max: 36 },
      [W_CASUAL_SHIRT_SHOULDER]: { min: 14.5, max: 16.5 },
      [W_CASUAL_SHIRT_WAIST]: { min: 17, max: 22 }
    },
    [LARGE]: {
      itemType: W_CASUAL_SHIRT,
      size: LARGE,
      name: `My ${CASUAL_SHIRT} Profile`,
      [W_CASUAL_SHIRT_BUST]: { min: 19, max: 23 },
      [W_CASUAL_SHIRT_BODY_LENGTH]: { min: 10, max: 30 },
      [W_CASUAL_SHIRT_SLEEVE]: { min: 10, max: 36 },
      [W_CASUAL_SHIRT_SHOULDER]: { min: 15, max: 17 },
      [W_CASUAL_SHIRT_WAIST]: { min: 19, max: 24 }
    }
  },
  [W_DRESS_SHIRT]: {
    [SMALL]: {
      itemType: W_DRESS_SHIRT,
      size: SMALL,
      name: `My ${DRESS_SHIRT} Profile`,
      [W_DRESS_SHIRT_BUST]: { min: 17, max: 20 },
      [W_DRESS_SHIRT_BODY_LENGTH]: { min: 25, max: 34 },
      [W_DRESS_SHIRT_SLEEVE]: { min: 29, max: 32 },
      [W_DRESS_SHIRT_SHOULDER]: { min: 13.5, max: 15 },
      [W_DRESS_SHIRT_WAIST]: { min: 14, max: 19 }
    },
    [MEDIUM]: {
      itemType: W_DRESS_SHIRT,
      size: MEDIUM,
      name: `My ${DRESS_SHIRT} Profile`,
      [W_DRESS_SHIRT_BUST]: { min: 18, max: 21 },
      [W_DRESS_SHIRT_BODY_LENGTH]: { min: 25, max: 34 },
      [W_DRESS_SHIRT_SLEEVE]: { min: 30, max: 33 },
      [W_DRESS_SHIRT_SHOULDER]: { min: 14.5, max: 16.5 },
      [W_DRESS_SHIRT_WAIST]: { min: 17, max: 22 }
    },
    [LARGE]: {
      itemType: W_DRESS_SHIRT,
      size: LARGE,
      name: `My ${DRESS_SHIRT} Profile`,
      [W_DRESS_SHIRT_BUST]: { min: 19, max: 23 },
      [W_DRESS_SHIRT_BODY_LENGTH]: { min: 25, max: 34 },
      [W_DRESS_SHIRT_SLEEVE]: { min: 30, max: 33 },
      [W_DRESS_SHIRT_SHOULDER]: { min: 15, max: 17 },
      [W_DRESS_SHIRT_WAIST]: { min: 19, max: 24 }
    }
  },
  [W_JACKET]: {
    [SMALL]: {
      itemType: W_JACKET,
      size: SMALL,
      name: `My ${JACKET} Profile`,
      [W_JACKET_BUST]: { min: 17, max: 21 },
      [W_JACKET_BODY_LENGTH]: { min: 10, max: 34 },
      [W_JACKET_SHOULDER]: { min: 13.5, max: 15 },
      [W_JACKET_SLEEVE]: { min: 28, max: 32 },
      [W_JACKET_WAIST]: { min: 14, max: 19 }
    },
    [MEDIUM]: {
      itemType: W_JACKET,
      size: MEDIUM,
      name: `My ${JACKET} Profile`,
      [W_JACKET_BUST]: { min: 18, max: 22 },
      [W_JACKET_BODY_LENGTH]: { min: 10, max: 34 },
      [W_JACKET_SHOULDER]: { min: 14.5, max: 16.5 },
      [W_JACKET_SLEEVE]: { min: 30, max: 33 },
      [W_JACKET_WAIST]: { min: 17, max: 22 }
    },
    [LARGE]: {
      itemType: W_JACKET,
      size: LARGE,
      name: `My ${JACKET} Profile`,
      [W_JACKET_BUST]: { min: 19, max: 24 },
      [W_JACKET_BODY_LENGTH]: { min: 10, max: 34 },
      [W_JACKET_SHOULDER]: { min: 15, max: 17 },
      [W_JACKET_SLEEVE]: { min: 30, max: 33 },
      [W_JACKET_WAIST]: { min: 17, max: 22 }
    }
  },
  [W_PANTS]: {
    [SMALL]: {
      itemType: W_PANTS,
      size: SMALL,
      name: `My ${PANTS} Profile`,
      [W_PANTS_FRONT_RISE]: { min: 7, max: 10 },
      [W_PANTS_INSEAM]: { min: 26, max: 36 },
      [W_PANTS_KNEE]: { min: 6, max: 10 },
      [W_PANTS_LEG_OPENING]: { min: 5, max: 10 },
      [W_PANTS_TOP_THIGH]: { min: 8, max: 12 },
      [W_PANTS_WAIST]: { min: 11, max: 14 }
    },
    [MEDIUM]: {
      itemType: W_PANTS,
      size: MEDIUM,
      name: `My ${PANTS} Profile`,
      [W_PANTS_FRONT_RISE]: { min: 8, max: 10.5 },
      [W_PANTS_INSEAM]: { min: 28, max: 36 },
      [W_PANTS_KNEE]: { min: 7, max: 12 },
      [W_PANTS_LEG_OPENING]: { min: 6, max: 12 },
      [W_PANTS_TOP_THIGH]: { min: 9, max: 13 },
      [W_PANTS_WAIST]: { min: 12, max: 16 }
    },
    [LARGE]: {
      itemType: W_PANTS,
      size: LARGE,
      name: `My ${PANTS} Profile`,
      [W_PANTS_FRONT_RISE]: { min: 8.5, max: 10.5 },
      [W_PANTS_INSEAM]: { min: 28, max: 36 },
      [W_PANTS_KNEE]: { min: 7.5, max: 14 },
      [W_PANTS_LEG_OPENING]: { min: 6.5, max: 14 },
      [W_PANTS_TOP_THIGH]: { min: 9.5, max: 14 },
      [W_PANTS_WAIST]: { min: 14, max: 17 }
    }
  },
  [W_SWEATER]: {
    [SMALL]: {
      itemType: W_SWEATER,
      size: SMALL,
      name: `My ${SWEATER} Profile`,
      [W_SWEATER_BUST]: { min: 17, max: 21 },
      [W_SWEATER_BODY_LENGTH]: { min: 10, max: 34 },
      [W_SWEATER_SLEEVE]: { min: 10, max: 33 },
      [W_SWEATER_SHOULDER]: { min: 13.5, max: 15 },
      [W_SWEATER_WAIST]: { min: 14, max: 20 }
    },
    [MEDIUM]: {
      itemType: W_SWEATER,
      size: MEDIUM,
      name: `My ${SWEATER} Profile`,
      [W_SWEATER_BUST]: { min: 18, max: 22 },
      [W_SWEATER_BODY_LENGTH]: { min: 10, max: 34 },
      [W_SWEATER_SLEEVE]: { min: 10, max: 33 },
      [W_SWEATER_SHOULDER]: { min: 14.5, max: 16.5 },
      [W_SWEATER_WAIST]: { min: 17, max: 22 }
    },
    [LARGE]: {
      itemType: W_SWEATER,
      size: LARGE,
      name: `My ${SWEATER} Profile`,
      [W_SWEATER_BUST]: { min: 19, max: 24 },
      [W_SWEATER_BODY_LENGTH]: { min: 10, max: 34 },
      [W_SWEATER_SLEEVE]: { min: 10, max: 33 },
      [W_SWEATER_SHOULDER]: { min: 15, max: 17 },
      [W_SWEATER_WAIST]: { min: 17, max: 22 }
    }
  }
};

exports.size = [SMALL, MEDIUM, LARGE];

exports.MEDIUM = MEDIUM;

exports.colors = [
  "Black",
  "Brown",
  "Blue",
  "Green",
  "Grey",
  "Orange",
  "Pink",
  "Purple",
  "Red",
  "White",
  "Yellow"
];

exports.materials = [
  "Cotton",
  "Cashmere",
  "Linen",
  "Wool",
  "Leather",
  "Denim",
  "Silk",
  "Other"
];
