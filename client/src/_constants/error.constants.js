export default {
  ERROR: "ALERT_ERROR",

  "AUTH_EMAIL_ALREADY-IN-USE": "auth/email-already-in-use",
  AUTH_WEAK_PASSWORD: "auth/weak-password",
  AUTH_INVALID_EMAIL: "auth/invalid-email",
  AUTH_USER_DISABLED: "auth/user-disabled",
  AUTH_USER_NOT_FOUND: "auth/user-not-found",
  AUTH_WRONG_PASSWORD: "auth/wrong-password",
  DISPLAY_NAME_TAKEN: "Display name is already taken",
  EMAIL_TAKEN: "Email is already in user"
};
