import { reduxConstants } from "../_constants";

export default function(state = {}, action) {
  switch (action.type) {
    case reduxConstants.SELL_ITEM_REQUEST:
      return {
        ...state,
        isUploading: true,
        uploadCount: 1
      };
    case reduxConstants.SELL_ITEM_SUCCESS:
      return {
        ...state,
        isUploading: false,
        isSuccess: true
      };
    case reduxConstants.UPLOAD_SINGLE_IMAGE_SUCCESS:
      return {
        ...state,
        uploadCount: state.uploadCount + 1
      };
    case reduxConstants.SELL_ITEM_FAILURE:
      return {
        ...state,
        isUploading: false,
        uploadCount: 1
      };
    case reduxConstants.UPDATE_ITEM_TYPE:
      return {
        ...state,
        itemType: action.itemType
      };
    default:
      return state;
  }
}
