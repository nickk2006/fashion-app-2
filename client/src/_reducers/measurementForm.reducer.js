import { reduxConstants } from "../_constants";

export default function(state = {}, action) {
  switch (action.type) {
    case reduxConstants.MF_UPLOAD_REQUEST:
      return {
        ...state,
        isUploadingMeasurement: true
      };

    case reduxConstants.MF_UPLOAD_SUCCESS:
      return {
        ...state,
        isUploadingMeasurement: false,
        profile: action.profile
      };
    case reduxConstants.MF_UPLOAD_FAILURE:
      return {
        ...state,
        isUploadingMeasurement: false,
        profile: undefined,
        error: action.error
      };
    case reduxConstants.GET_ALL_MF_REQUEST:
      return {
        ...state,
        isGettingMF: true
      };
    case reduxConstants.GET_ALL_MF_SUCCESS:
      return {
        ...state,
        isGettingMF: false,
        userMeasurementsList: action.userMeasurementsList
      };
    case reduxConstants.GET_ALL_MF_FAILURE:
      return {
        ...state,
        isGettingMF: false,
        error: action.error
      };
    default:
      return state;
  }
}
