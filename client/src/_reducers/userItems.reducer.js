import { reduxConstants } from "../_constants";

export default function(state = {}, action) {
  switch (action.type) {
    case reduxConstants.USER_ITEMS_REQUEST:
      return {
        ...state,
        isFetchingItems: true,
        isItemsEmpty: undefined
      };
    case reduxConstants.USER_ITEMS_SUCCESS_NO_ITEMS:
      return {
        ...state,
        isFetchingItems: false,
        isItemsEmpty: true
      };
    case reduxConstants.USER_ITEMS_SUCCESS:
      return {
        ...state,
        isFetchingItems: false,
        isItemsEmpty: false,
        itemsForSale: action.itemsForSale,
        itemsPurchased: action.itemsPurchased,
        itemsSold: action.itemsSold,
        ...action
      };
    case reduxConstants.DELETE_ITEM_SUCCESS:
      return {
        ...state,
        itemsForSale: action.itemsForSale
      };
    default:
      return state;
  }
}
