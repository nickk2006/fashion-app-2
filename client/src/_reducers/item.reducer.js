import { reduxConstants } from "../_constants";

export default function(state = {}, action) {
  switch (action.type) {
    case reduxConstants.ITEM_REQUEST:
      return {
        isFetchingItem: true
      };
    case reduxConstants.ITEM_SUCCESS:
      return {
        isFetchingItem: false,
        id: action.item._id,
        measurements: action.item.measurement,
        colors: action.item.color,
        materials: action.item.material,
        isSold: action.item.isSold,
        itemType: action.item.itemType,
        designer: action.item.designer,
        designerLine: action.item.designerLine,
        price: action.item.price,
        sellerId: action.item.sellerId,
        sellerUsername: action.item.sellerUsername,
        createdAt: action.item.createdAt,
        description: action.item.description,
        photos: action.item.photos
      };
    // case reduxConstants.SET_HOLD_SUCCESS:
    //   return {
    //     ...state,
    //     holdUntil: action.holdUntil
    //   };
    default:
      return state;
  }
}
