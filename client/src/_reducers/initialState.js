export default {
  auth: {
    user: {
      isAuthenticating: false,
      token: "",
      username: "",
      isConnectedToStripe: false
    },
    stripe: {
      isConnectedToStripeConnect: false,
      isConnectingToStripeConnect: false,
      isAlreadyConnectedToStripe: false,
      isDeauthorizingStripe: false,
      isDeauthorizedStripe: false,
      isStripeStateError: false
    }
  },
  registration: {},
  measurementForm: {
    isUploadingMeasurement: false,
    isGettingMF: false,
    userMeasurementsList: []
  },
  items: {
    isFetchingItem: false,
    isItemsFetched: false,
    limit: 20,
    offset: 0,
    totalCount: 0,
    items: []
  },
  item: {},
  sell: {
    isUploading: false,
    isSuccess: false,
    uploadCount: 1,
    itemType: "",
    photos: [],
    error: ""
  },
  userItems: {
    isFetchingItems: false,
    isItemsEmpty: undefined,
    itemsForSale: [],
    itemsPurchased: [],
    itemsSold: []
  },
  filter: {
    colorFilters: [],
    materialFilters: [],
    itemTypeFilters: [],
    designerFilters: [],
    profile: []
  },
  tracking: {
    isLoading: true,
    isSuccess: false,
    error: ""
  }
};
