import { userConstants } from '../_constants';

export default function(state = {}, action) {
  switch (action.type) {
    case userConstants.REGISTER_REQUEST:
      return { registering: true };
    case userConstants.REGISTER_SUCCESS:
    case userConstants.REGISTER_FAILURE:
      return {};
    default:
      return state;
  }
}
