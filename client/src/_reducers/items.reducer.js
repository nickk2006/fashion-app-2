import { reduxConstants } from "../_constants";

export default function(state = {}, action) {
  switch (action.type) {
    case reduxConstants.ITEMS_REQUEST:
      return {
        ...state,
        isFetchingItem: true,
        isItemsFetched: false,
        error: null
      };
    case reduxConstants.ITEMS_SUCCESS:
      return {
        ...state,
        isFetchingItem: false,
        isItemsFetched: true,
        items: action.items,
        totalCount: action.totalCount,
        error: null
      };
    case reduxConstants.ITEMS_FAILURE:
      return {
        ...state,
        isFetchingItem: false,
        isItemsFetched: false,

        error: action.error
      };
    case reduxConstants.GET_USER_PROFILE_REQUEST:
      return {
        ...state,
        isFetchingItem: true,
        isItemsFetched: false,
        error: null,
        items: []
      };
    case reduxConstants.GET_USER_PROFILE_SUCCESS:
      return {
        ...state,
        isFetchingItem: false,
        isItemsFetched: true,
        items: action.items,
        totalCount: action.totalCount,
        error: null
      };
    default:
      return state;
  }
}
