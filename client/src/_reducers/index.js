import { combineReducers } from "redux";

import { reducer as formReducer } from "redux-form";
import auth from "./user";
import registration from "./registration.reducer";
import measurementForm from "./measurementForm.reducer";
import item from "./item.reducer";
import items from "./items.reducer";
import userItems from "./userItems.reducer";
import sell from "./sell.reducer";
import filter from "./filters.reducer";
import tracking from "./tracking.reducer";
import { userConstants } from "../_constants";
import initialState from "./initialState";
const appReducer = combineReducers({
  auth,
  registration,
  measurementForm,
  items,
  item,
  sell,
  userItems,
  filter,
  tracking,
  form: formReducer
});

export default (state, action) => {
  if (action.type === userConstants.USERS_LOGOUT_REQUEST) {
    state = initialState;
  }
  return appReducer(state, action);
};
