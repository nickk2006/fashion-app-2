import { reduxConstants } from "../_constants";

export default function(state = {}, action) {
  switch (action.type) {
    case reduxConstants.FILTER_REQUEST:
      return {
        ...state,
        isFetchingFilters: true
      };
    case reduxConstants.FILTER_SUCCESS:
      return {
        ...state,
        isFetchingFilters: false,
        colorFilters: action.colorFilters,
        materialFilters: action.materialFilters,
        itemTypeFilters: action.itemTypeFilters,
        designerFilters: action.designerFilters,
        profile: action.profile
      };

    default:
      return {
        ...state,
        isFetchingFilters: false
      };
  }
}
