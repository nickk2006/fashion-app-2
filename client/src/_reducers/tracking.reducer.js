import { reduxConstants } from "../_constants";

export default function(state = {}, action) {
  switch (action.type) {
    case reduxConstants.GET_TRACKING_INFO_REQUEST:
    case reduxConstants.POSTING_TRACKING_INFO_REQUEST:
      return {
        ...state,
        isLoading: true,
        isSuccess: false,
        error: ""
      };
    case reduxConstants.GET_TRACKING_INFO_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isSuccess: true,
        error: "",
        item: action.item
      };
    case reduxConstants.POSTING_TRACKING_INFO_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isSuccess: true,
        error: ""
      };
    case reduxConstants.POSTING_TRACKING_INFO_FAILURE:
      return {
        ...state,
        isLoading: false,
        isSuccess: false,
        error: action.errorMessage || reduxConstants.DEFAULT_ERROR_MESSAGE
      };
    default:
      return state;
  }
}
