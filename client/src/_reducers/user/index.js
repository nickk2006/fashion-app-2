import { combineReducers } from "redux";
import user from "./authentication.reducer";
import stripe from "./stripe.reducer";

export default combineReducers({
  user,
  stripe
});
