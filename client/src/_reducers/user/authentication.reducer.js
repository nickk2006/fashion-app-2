import { userConstants, reduxConstants } from "../../_constants";

export default function(state = {}, action) {
  switch (action.type) {
    case userConstants.GET_TOKEN_REQUEST:
      return {
        ...state,
        isAuthenticating: true,
        token: ""
      };
    case userConstants.GET_TOKEN_SUCCESS:
      return {
        ...state,
        isAuthenticating: false,
        token: action.token
      };

    case userConstants.GET_TOKEN_FAILURE:
    case userConstants.USERS_LOGOUT_SUCCESS:
    case userConstants.USERS_LOGOUT_FAILURE:
    case userConstants.LOGIN_FAILURE:
      return {
        ...state,
        isAuthenticating: false,
        token: ""
      };
    case userConstants.GET_USER_SUCCESS:
      return {
        ...state,
        ...action.user
      };
    case userConstants.LOGIN_REQUEST:
      return {
        ...state,
        isAuthenticating: true,
        token: ""
      };
    case userConstants.LOGIN_SUCCESS:
    case userConstants.REGISTER_SUCCESS:
      return {
        ...state,
        isAuthenticating: false,
        username: action.username,
        token: action.token
      };
    case reduxConstants.SEND_STRIPE_AUTH_CODE_TO_SERVER_SUCCESS:
      return {
        ...state,
        isConnectedToStripe: true
      };

    case userConstants.SET_USER_INTO_REDUX:
      return {
        ...state,
        username: action.username
      };
    default:
      return state;
  }
}
