import { reduxConstants } from "../../_constants";

export default function(state = {}, action) {
  switch (action.type) {
    case reduxConstants.SEND_STRIPE_AUTH_CODE_TO_SERVER_REQUEST:
      return {
        ...state,
        isConnectingToStripeConnect: true,
        isConnectedToStripeConnect: false
      };
    case reduxConstants.SEND_STRIPE_AUTH_CODE_TO_SERVER_SUCCESS:
      return {
        ...state,
        isConnectingToStripeConnect: false,
        isConnectedToStripeConnect: true
      };
    case reduxConstants.SEND_STRIPE_AUTH_CODE_TO_SERVER_FAILURE:
      return {
        ...state,
        isConnectingToStripeConnect: false,
        isConnectedToStripeConnect: false
      };
    case reduxConstants.ALREADY_CONNECT_TO_STRIPE:
      return {
        ...state,
        isConnectedToStripeConnect: true,
        isConnectingToStripeConnect: false,
        isAlreadyConnectedToStripe: true
      };
    case reduxConstants.DEAUTHORIZE_STRIPE_REQUEST:
      return {
        ...state,
        isDeauthorizingStripe: true
      };
    case reduxConstants.DEAUTHORIZE_STRIPE_SUCCESS:
      return {
        ...state,
        isDeauthorizingStripe: false,
        isDeauthorizedStripe: true
      };
    case reduxConstants.DEAUTHORIZE_STRIPE_FAILURE:
      return {
        ...state,
        isDeauthorizingStripe: false,
        isDeauthorizedStripe: false,
        isConnectedToStripeConnect: false
      };
    case reduxConstants.STRIPE_CONNECT_STATE_ERROR:
      return {
        ...state,
        isConnectingToStripeConnect: false,
        isConnectedToStripeConnect: false,
        isStripeStateError: true
      };
    case reduxConstants.STRIPE_REDIRECT_TO_OAUTH_REQUEST:
    case reduxConstants.STRIPE_REDIRECT_TO_OAUTH_SUCCESS:
    case reduxConstants.STRIPE_REDIRECT_TO_OAUTH_FAILURE:
    default:
      return state;
  }
}
