import { reduxConstants, errorConstants } from "../_constants";

/**
 *
 * @param {object} data replaces values of photos key with primaryPhoto and secondayPhoto s3 urls
 * @param {*} history
 */
const uploadSellItem = ({ itemWithoutPhotos, binaryPhotos }) => dispatch => {
  const { SELL_ITEM_REQUEST } = reduxConstants;

  dispatch({ type: SELL_ITEM_REQUEST });
};

const updateItemType = value => ({
  type: reduxConstants.UPDATE_ITEM_TYPE,
  itemType: value
});
export { uploadSellItem, updateItemType };
