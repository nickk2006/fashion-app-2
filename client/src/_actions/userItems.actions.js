import { reduxConstants } from "../_constants";
import { getAllItemsForSaleByUser as getItems } from "../_services";

const getAllItemsForSaleByUser = () => async dispatch => {
  const {
    USER_ITEMS_REQUEST,
    USER_ITEMS_SUCCESS,
    USER_ITEMS_SUCCESS_NO_ITEMS,
    USER_ITEMS_FAILURE
  } = reduxConstants;

  dispatch({ type: USER_ITEMS_REQUEST });

  try {
    const res = await getItems();

    if (res.status === 200) {
      return dispatch({
        type: USER_ITEMS_SUCCESS,
        ...res.data
      });
    } else if (res.status === 204) {
      return dispatch({
        type: USER_ITEMS_SUCCESS_NO_ITEMS
      });
    }
  } catch (e) {
    dispatch({
      type: USER_ITEMS_FAILURE,
      error: e.toString()
    });
  }
};

export { getAllItemsForSaleByUser };
