import isEmpty from "lodash/isEmpty";
import {
  beginStripeOAuth,
  completeStripeOAuth as completeStripeOAuthService
} from "../../_services";
import { queryStringToObject } from "../../_helpers";
import { reduxConstants } from "../../_constants";

const connectStripeOAuth = () => async dispatch => {
  dispatch({ type: reduxConstants.STRIPE_REDIRECT_TO_OAUTH_REQUEST });
  try {
    await beginStripeOAuth();
    // const redirectLink = res.data.url
    dispatch({ type: reduxConstants.STRIPE_REDIRECT_TO_OAUTH_SUCCESS });
    // window.location.replace(redirectLink)
  } catch (e) {
    dispatch({ type: reduxConstants.ERROR, error: e.toString() });
    dispatch({ type: reduxConstants.STRIPE_REDIRECT_TO_OAUTH_FAILURE });
  }
};

const completeStripeOAuth = queryString => async dispatch => {
  dispatch({ type: reduxConstants.SEND_STRIPE_AUTH_CODE_TO_SERVER_REQUEST });
  try {
    const queryObject = queryStringToObject(queryString);
    if (!isEmpty(queryObject)) {
      const { code, state } = queryObject;
      const res = await completeStripeOAuthService({ code, state });
      if (res.status === 200) {
        dispatch({
          type: reduxConstants.SEND_STRIPE_AUTH_CODE_TO_SERVER_SUCCESS
        });
      }
    } else {
      dispatch({
        type: reduxConstants.SEND_STRIPE_AUTH_CODE_TO_SERVER_FAILURE
      });
    }
  } catch (err) {
    if (err.response.status === 409) {
      dispatch({ type: reduxConstants.STRIPE_CONNECT_STATE_ERROR });
    } else {
      dispatch({
        type: reduxConstants.SEND_STRIPE_AUTH_CODE_TO_SERVER_FAILURE
      });
    }
  }
};

const completeStripeOAuthErrorHandler = res => async dispatch => {
  if (res.status === 409) {
    dispatch({ type: reduxConstants.STRIPE_CONNECT_STATE_ERROR });
  } else {
    dispatch({
      type: reduxConstants.SEND_STRIPE_AUTH_CODE_TO_SERVER_FAILURE
    });
  }
};

export {
  connectStripeOAuth,
  completeStripeOAuth,
  completeStripeOAuthErrorHandler
};
