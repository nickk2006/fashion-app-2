import {
  userConstants,
  errorConstants,
  reduxConstants
} from "../../_constants";
import { userService } from "../../_services";
import { SubmissionError } from "redux-form";
import firebase from "../../firebase";

function login({ email, password }) {
  return async dispatch => {
    const { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE } = userConstants;

    dispatch({
      type: LOGIN_REQUEST
    });

    try {
      const res = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);

      // TODO: history.push('/')
      const token = await firebase.auth().currentUser.getIdToken();

      return dispatch({
        type: LOGIN_SUCCESS,
        username: res.user.displayName,
        token
      });
    } catch (e) {
      dispatch({ type: LOGIN_FAILURE });
      switch (e.code) {
        case errorConstants.AUTH_INVALID_EMAIL:
        case errorConstants.AUTH_USER_DISABLED:
        case errorConstants.AUTH_USER_NOT_FOUND:
        case errorConstants.AUTH_WRONG_PASSWORD:
          throw new SubmissionError({
            password: "Please try again",
            email: "Please try again",
            _error: "Login failed"
          });
        default:
          dispatch({ type: errorConstants.ERROR, error: e.toString() });
          throw new SubmissionError({
            _error: "Login failed"
          });
      }
    }
  };
}

function register({ email, password, username }) {
  return async dispatch => {
    dispatch({
      type: userConstants.REGISTER_REQUEST
    });

    try {
      const user = await userService.register({ username, email, password });
      await firebase.auth().signInWithEmailAndPassword(email, password);
      const token = await firebase.auth().currentUser.getIdToken();
      return dispatch({
        type: userConstants.REGISTER_SUCCESS,
        username: user.data.username,
        token
      });
    } catch (e) {
      dispatch({
        type: userConstants.REGISTER_FAILURE
      });

      const { code } = e.response.data;
      switch (code) {
        case errorConstants.EMAIL_TAKEN:
          throw new SubmissionError({
            email: "Email is in use",
            _error: "Registration failed"
          });
        default:
          dispatch({ type: errorConstants.ERROR, error: e.toString() });
          throw new SubmissionError({
            _error: "Registration failed"
          });
      }
    }
  };
}

const isUsernameUnique = username => async dispatch => {
  dispatch({ type: userConstants.CHECK_USERNAME_REQUEST });
  try {
    await userService.isUsernameUnique(username);
    dispatch({ type: userConstants.CHECK_USERNAME_SUCCESS });
    return;
  } catch (e) {
    const { code } = e.response.data;
    switch (code) {
      case errorConstants.DISPLAY_NAME_TAKEN:
        throw {
          username: errorConstants.DISPLAY_NAME_TAKEN
        };
    }
  }
};

const setTokenIntoRedux = token => ({
  type: userConstants.GET_TOKEN_SUCCESS,
  token
});

const setUserIntoRedux = user => ({
  type: userConstants.SET_USER_INTO_REDUX,
  username: user.displayName
});
function getToken() {
  return async dispatch => {
    try {
      dispatch({
        type: userConstants.GET_TOKEN_REQUEST
      });

      const token = await firebase.auth().currentUser.getIdToken();

      dispatch({
        type: userConstants.GET_TOKEN_SUCCESS,
        token
      });
    } catch (e) {
      dispatch({
        type: userConstants.GET_TOKEN_FAILURE
      });

      dispatch({
        type: errorConstants.ERROR,
        error: e.toString()
      });
    }
  };
}
const logout = () => async dispatch => {
  dispatch({
    type: userConstants.USERS_LOGOUT_REQUEST
  });
  try {
    await firebase.auth().signOut();
    dispatch({
      type: userConstants.USERS_LOGOUT_SUCCESS
    });
  } catch (e) {
    dispatch({
      type: userConstants.USERS_LOGOUT_FAILURE
    });
    dispatch({
      type: errorConstants.ERROR,
      error: e.toString()
    });
  }
};

const getUser = token => async dispatch => {
  const {
    GET_USER_REQUEST,
    GET_USER_SUCCESS,
    GET_USER_FAILURE
  } = userConstants;

  dispatch({
    type: GET_USER_REQUEST
  });

  try {
    const { data } = await userService.getUser(token);
    dispatch({
      type: GET_USER_SUCCESS,
      user: data
    });
  } catch (e) {
    dispatch({
      type: GET_USER_FAILURE
    });
    dispatch({
      type: errorConstants.ERROR,
      error: e.toString()
    });
  }
};

const getUserProfile = username => async dispatch => {
  dispatch({
    type: reduxConstants.GET_USER_PROFILE_REQUEST
  });
  try {
    const {
      data: { data }
    } = await userService.getUserProfile(username);
    dispatch({
      type: reduxConstants.GET_USER_PROFILE_SUCCESS,
      items: data,
      totalCount: data.length
    });
  } catch (e) {
    dispatch({
      type: errorConstants.ERROR,
      error: e.toString()
    });
    dispatch({ type: reduxConstants.GET_USER_PROFILE_FAILURE });
  }
};

export {
  login,
  register,
  setUserIntoRedux,
  setTokenIntoRedux,
  getToken,
  logout,
  getUser,
  isUsernameUnique,
  getUserProfile
};
