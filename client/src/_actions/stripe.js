import isString from "lodash/isString";
import { reduxConstants } from "../_constants";
import { userService } from "../_services";
import { queryStringToObject } from "../_helpers";

const getStripeId = queryString => async dispatch => {
  const { scope, state, code } = queryStringToObject(queryString);

  if (scope === "read_write" && state === "walcroft" && isString(code)) {
    dispatch({
      type: reduxConstants.FETCH_STRIPE_USER_CREDENTIALS_REQUEST
    });

    try {
      await userService.getStripeId(code);
      dispatch({
        type: reduxConstants.FETCH_STRIPE_USER_CREDENTIALS_SUCCESS
      });
    } catch (e) {
      dispatch({
        type: reduxConstants.ERROR,
        error: e.toString()
      });
      dispatch({
        type: reduxConstants.FETCH_STRIPE_USER_CREDENTIALS_FAILURE
      });
    }
  }
};

export { getStripeId };
