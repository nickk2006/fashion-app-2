import { reduxConstants } from "../_constants";
import {
  uploadMF,
  getAllMeasurementsForUser as getMeasurements
} from "../_services";

const uploadMFAction = data => async dispatch => {
  const {
    MF_UPLOAD_REQUEST,
    MF_UPLOAD_SUCCESS,
    MF_UPLOAD_FAILURE
  } = reduxConstants;

  dispatch({
    type: MF_UPLOAD_REQUEST
  });

  try {
    const profile = await uploadMF(data);
    dispatch({
      type: MF_UPLOAD_SUCCESS,
      profile: profile.data
    });
  } catch (e) {
    dispatch({
      type: MF_UPLOAD_FAILURE,
      error: e.toString()
    });
  }
};

const getAllMeasurementsForUser = () => async dispatch => {
  const {
    GET_ALL_MF_REQUEST,
    GET_ALL_MF_SUCCESS,
    GET_ALL_MF_FAILURE
  } = reduxConstants;

  dispatch({
    type: GET_ALL_MF_REQUEST
  });
  try {
    const measurements = await getMeasurements();
    if (measurements.status === 204) {
      dispatch({
        type: GET_ALL_MF_SUCCESS
      });
    } else {
      dispatch({
        type: GET_ALL_MF_SUCCESS,
        userMeasurementsList: measurements.data
      });
    }
  } catch (e) {
    dispatch({
      type: GET_ALL_MF_FAILURE,
      error: e.toString()
    });
  }
};

export { uploadMFAction, getAllMeasurementsForUser };
