import { errorConstants, reduxConstants as constants } from "../_constants";
import {
  getItemsRequest,
  getItemRequest,
  createItem as createItemRequest,
  getFilters as getFiltersRequest,
  setHold as setHoldRequest,
  postPurchase,
  postTracking as postTrackingRequest,
  getTracking as getTrackingRequest,
  deleteItem as deleteItemRequest,
  uploadImageToCloudinary
} from "../_services";
import { parse } from "qs";
import { resizePhoto } from "../_helpers";
import { reject } from "async";

const getItems = query => async dispatch => {
  const { ITEMS_REQUEST, ITEMS_SUCCESS, ITEMS_FAILURE } = constants;

  dispatch({
    type: ITEMS_REQUEST
  });
  try {
    let res;
    if (query) {
      const queryObj = parse(query.substring(1));
      res = await getItemsRequest(queryObj);
    } else {
      res = await getItemsRequest();
    }
    if (res.status === 204) {
      dispatch({
        type: ITEMS_SUCCESS,
        items: [],
        totalCount: 0
      });
    } else {
      const data = res.data.data;
      const metaData = res.data.metaData[0];
      dispatch({
        type: ITEMS_SUCCESS,
        items: data,
        totalCount: parseInt(metaData.total, 10)
      });
    }
  } catch (e) {
    dispatch({
      type: ITEMS_FAILURE,
      error: e.toString()
    });
  }
};

const getFilters = () => async dispatch => {
  const { FILTER_REQUEST, FILTER_SUCCESS, FILTER_FAILURE } = constants;
  dispatch({ type: FILTER_REQUEST });

  try {
    const res = await getFiltersRequest();
    const filters = res.data;
    dispatch({
      type: FILTER_SUCCESS,
      colorFilters: filters.color || [],
      materialFilters: filters.material || [],
      itemTypeFilters: filters.itemType || [],
      designerFilters: filters.designer || [],
      profile: filters.myProfile || []
    });
  } catch (e) {
    dispatch({ type: FILTER_FAILURE });
    dispatch({
      type: errorConstants.ERROR,
      error: e.toString()
    });
  }
};

const getItem = id => async dispatch => {
  const { ITEM_REQUEST, ITEM_SUCCESS } = constants;
  const { ERROR } = errorConstants;

  dispatch({
    type: ITEM_REQUEST
  });
  try {
    const item = await getItemRequest(id);
    dispatch({
      type: ITEM_SUCCESS,
      item: item.data
    });
  } catch (e) {
    dispatch({
      type: ERROR,
      error: e.toString()
    });
  }
};

const createItem = (itemFields, photos) => async dispatch => {
  dispatch({
    type: constants.SELL_ITEM_REQUEST
  });

  const getCompressPhotoPromise = photo =>
    new Promise((res, rej) => {
      dispatch({ type: constants.RESIZE_SINGLE_IMAGE_REQUEST });
      return resizePhoto(photo)
        .then(resizedPhotoDataUrl => {
          dispatch({ type: constants.RESIZE_SINGLE_IMAGE_SUCCESS });
          return res(resizedPhotoDataUrl);
        })
        .catch(e => {
          dispatch({ type: constants.RESIZE_SINGLE_IMAGE_FAILURE });
          dispatch({ type: constants.ERROR, error: e.toString() });
          return rej(e);
        });
    });

  const getUploadSinglePhotoRequest = dataUrl =>
    new Promise((res, rej) => {
      dispatch({ type: constants.UPLOAD_SINGLE_IMAGE_REQUEST });
      return uploadImageToCloudinary(dataUrl)
        .then(({ data }) => {
          const fileName = `${data.version}/${data.public_id}.jpg`;
          dispatch({ type: constants.UPLOAD_SINGLE_IMAGE_SUCCESS });
          return res(fileName);
        })
        .catch(e => {
          dispatch({ type: constants.UPLOAD_SINGLE_IMAGE_FAILURE });
          dispatch({ type: constants.ERROR, error: e.toString() });
          return rej(e);
        });
    });

  const photoUrls = await Promise.all(
    photos.map(
      photo =>
        new Promise((res, rej) =>
          getCompressPhotoPromise(photo)
            .then(dataUrl => getUploadSinglePhotoRequest(dataUrl))
            .then(filename => res(filename))
            .catch(e => rej(e))
        )
    )
  );

  const payload = {
    photos: photoUrls,
    ...itemFields
  };

  return createItemRequest(payload)
    .then(res => {
      dispatch({
        type: constants.SELL_ITEM_SUCCESS
      });
      return res;
    })
    .catch(e => {
      dispatch({ type: constants.SELL_ITEM_FAILURE });
      dispatch({ type: constants.ERROR, error: e.toString() });
    });
};

const deleteItem = itemId => async dispatch => {
  dispatch({
    type: constants.DELETE_ITEM_REQUEST
  });
  try {
    const res = await deleteItemRequest(itemId);
    dispatch({
      type: constants.DELETE_ITEM_SUCCESS,
      itemsForSale: res.data.itemsForSale
    });
  } catch (e) {
    dispatch({ type: constants.ERROR, error: e.toString() });
  }
};

const handleToken = ({ stripeToken, itemId, shipping }) => async dispatch => {
  dispatch({
    type: constants.SUBMIT_PURCHASE_REQUEST
  });

  try {
    const res = await postPurchase({ stripeToken, itemId, shipping });
    dispatch({
      type: constants.SUBMIT_PURCHASE_SUCCESS,
      response: res.data
    });
  } catch (e) {
    dispatch({ type: constants.ERROR, error: e.toString() });
    dispatch({ type: constants.SUBMIT_PURCHASE_FAILURE });
  }
};

// const setHold = id => async dispatch => {
//   dispatch({
//     type: constants.SET_HOLD_REQUEST
//   });
//   try {
//     const holdUntil = await setHoldRequest(id);
//     dispatch({
//       type: constants.SET_HOLD_SUCCESS,
//       holdUntil
//     });
//   } catch (e) {
//     dispatch({ type: constants.ERROR, error: e.toString() });
//     dispatch({ type: constants.SET_HOLD_FAILURE });
//   }
// };

const getTracking = (itemId, history) => async dispatch => {
  dispatch({ type: constants.GET_TRACKING_INFO_REQUEST });
  const {
    data: { isSuccess, item }
  } = await getTrackingRequest(itemId);
  if (!!isSuccess) {
    dispatch({
      type: constants.GET_TRACKING_INFO_SUCCESS,
      item
    });
  } else {
    dispatch({
      type: constants.GET_TRACKING_INFO_FAILURE
    });
    history.push(`/catalog/items/${itemId}`);
  }
};

const postTracking = (data, itemId) => async dispatch => {
  dispatch({
    type: constants.POSTING_TRACKING_INFO_REQUEST
  });
  try {
    await postTrackingRequest(data, itemId);
    dispatch({ type: constants.POSTING_TRACKING_INFO_SUCCESS });
  } catch (e) {
    dispatch({ type: constants.ERROR, error: e.toString() });
    dispatch({ type: constants.POSTING_TRACKING_INFO_FAILURE });
  }
};

export {
  getItems,
  getItem,
  createItem,
  getFilters,
  handleToken,
  getTracking,
  postTracking,
  deleteItem
};
