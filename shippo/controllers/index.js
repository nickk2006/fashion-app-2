const isString = require("lodash/isString");
const request = require("superagent");
const { Item } = require("../../models");
const {
  carriersList,
  carriersMap
} = require("../../helpers/constants/shipping");
exports.validateShippoToken = (req, res, next) => {
  if (
    !isString(req.query.token) ||
    req.query.token !== process.env.SHIPPO_WEBHOOK_PARAM_TOKEN
  ) {
    return next(new Error());
  } else if (req.query.token === process.env.SHIPPO_WEBHOOK_PARAM_TOKEN) {
    next();
  }
};

exports.extractShippoData = (req, res, next) => {
  const trackingStatus = req.body.data.tracking_status.status;
  const deliveryDate = req.body.data.tracking_status.status_date;
  res.locals.deliveryDate = Date.parse(deliveryDate);
  res.locals.itemId = req.body.data.metadata;
  if (trackingStatus === "DELIVERED") {
    return next();
  } else {
    return res.sendStatus(200);
  }
};

exports.setupTracking = async (req, res, next) => {
  const { seller, item } = res.locals;
  const { itemId } = req.params;
  const { carrier, tracking_number } = req.body;
  if (process.env.NODE_ENV === "production") {
    if (!carriersList.includes(carrier)) {
      return next(new Error("Invalid carrier selected"));
    }
  }
  try {
    const response = await request
      .post("https://api.goshippo.com/tracks/")
      .set("Authorization", `ShippoToken ${process.env.SHIPPO_API_TOKEN}`)
      .send({
        carrier,
        tracking_number,
        metadata: itemId
      });

    const newItem = await Item.findByIdAndUpdate(
      itemId,
      {
        $set: {
          "shipTo.carrier": carrier,
          "shipTo.tracking_number": tracking_number
        }
      },
      { new: true }
    );

    res.locals.item = newItem;

    next();
  } catch (e) {
    next(e.toString());
  }
};
