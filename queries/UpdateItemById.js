const { Item } = require("../models");

module.exports = ({ condition, update, options }) => {
  return Item.findOneAndUpdate(condition, update, options).exec();
};
