const Item = require("../models/Item");
/**
 * @param {string} type - Type of item to be created
 * @param {object} itemProps
 * @return {Promise} adds item to database
 */
module.exports = item => {
  const newItem = new Item(item);
  return newItem.save();
};
