const { User } = require("../models");

/**
 * Finds single user in user collection.
 * @param {string} email - The email of the record to find
 * @param {boolean} includePassword - default to false. If true, will return password with User
 * @return {promise} promise that resolves with User that matches the email
 */

module.exports = (email, includePassword = false) => {
  let query = User.findOne({ email });
  if (includePassword) {
    return query.select("email +password").exec();
  } else {
    return query.exec();
  }
};
