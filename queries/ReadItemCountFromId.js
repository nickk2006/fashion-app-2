const { Item } = require("../models");

module.exports = id => Item.find({ _id: { $lte: id } }).count();
