const { MeasurementProfile } = require("../models");
/**
 *
 * @param {string} measurementId Can be string or ObjectId.
 * @returns {Promise} resolves into measurement profile
 */
module.exports = measurementId => {
  const query = MeasurementProfile.findById(measurementId);
  return new Promise((resolve, reject) =>
    query.exec(function(err, measurement) {
      if (err) {
        return reject(err.message);
      } else {
        return resolve(measurement);
      }
    })
  );
};
