const { Item } = require("../models");
/**
 *
 * @param {String} itemId
 * @returns {Promise}
 */
module.exports = (itemId, select) => {
  return Item.findById(itemId).select(select);
};
