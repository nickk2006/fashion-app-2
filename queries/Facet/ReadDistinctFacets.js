const filterByMeasurements = require("./FilterByMeasurement");
const { Item } = require("../../models");
const isEmpty = require("lodash/isEmpty");
module.exports = (measurementsArray = [], cb) => {
  console.log("\n\n\n\nmeasurements array !");
  console.log(measurementsArray);

  if (!isEmpty(measurementsArray)) {
    const ItemsFilteredByMeasurement = filterByMeasurements(measurementsArray);

    return (
      ItemsFilteredByMeasurement.unwind("$facets.color")
        .unwind("$facets.material")
        .group({
          _id: 1,
          material: { $addToSet: "$facets.material" },
          designer: { $addToSet: "$facets.designer" },
          itemType: { $addToSet: "$facets.itemType" },
          color: { $addToSet: "$facets.color" }
        })
        // .project({
        //   color: "$color[0]"
        // })
        .exec(cb)
    );
  } else {
    return Item.aggregate([{ $match: { isSold: false } }])
      .unwind("$facets.color")
      .unwind("$facets.material")
      .group({
        _id: 1,
        material: { $addToSet: "$facets.material" },
        designer: { $addToSet: "$facets.designer" },
        itemType: { $addToSet: "$facets.itemType" },
        color: { $addToSet: "$facets.color" }
      })
      .exec(cb);
  }
};
