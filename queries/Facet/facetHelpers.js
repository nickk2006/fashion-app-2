const map = require("lodash/map");
const uniq = require("lodash/uniq");
const facetConstants = require("../../helpers/constants/facets");
/**
 *
 * @param {Array} measurementsArray
 * @return {Array} itemTypes present
 */
const itemTypesPresent = function(measurementsArray) {
  return uniq(map(measurementsArray, singleProfile => singleProfile.itemType));
};

const filtersPresent = function(filters) {
  return Object.keys(filters).filter(function(filterName) {
    return facetConstants.availableFilters.includes(filterName);
  });
};
module.exports = {
  itemTypesPresent,
  filtersPresent
};
