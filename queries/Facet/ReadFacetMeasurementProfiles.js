const mongoose = require("mongoose");
const facets = require("../../helpers/constants/facets");

/**
 * Expected parameter shape:
 *[{
    itemType: "Jacket",
    measurements: [
      { "Jacket-Length": { min: XX, max: XX } },
      { "Jacket-Chest": { min: XX, max: XX } },
      ...
    ]
  }, ....];
 */

module.exports = function(measurementProfileArray) {
  const facet = {};

  const pantsProfiles = [];
  const dressShirtProfiles = [];
  const jacketProfiles = [];

  measurementProfileArray.map(({ itemType, measurements }) => {
    if (itemType === "Pants") {
      pantsProfiles.push(measurements);
    } else if (itemType === "Dress Shirt") {
      dressShirtProfiles.push(measurements);
    } else if (itemType === "Jacket") {
      jacketProfiles.push(measurements);
    }
  });

  if (pantsProfiles.length > 0) {
    pantsProfiles.map(measurements => {
      const measurementExpression = buildMeasurementExpression(measurements);
    });
  }
};

const buildMeasurementExpression = function(measurements) {
  const expression = Object.keys(measurements).map(singleMeasurement => ({
    $gte: [singleMeasurement, measurements.singleMeasurement.min],
    $lte: [singleMeasurement, measurements.singleMeasurement.max]
  }));

  return { $match: { $and: expression } };
};
