const mongoose = require("mongoose");
const map = require("lodash/map");
const isEmpty = require("lodash/isEmpty");
const facetConstants = require("../../helpers/constants/facets");
const facetHelpers = require("./facetHelpers");
const filterByMeasurements = require("./FilterByMeasurement");
const { Item } = require("../../models");
/**
 * Expected parameter shape:
 *[{
    itemType: "Jacket",
    measurements: [
      { "Jacket-Length": { min: XX, max: XX } },
      { "Jacket-Chest": { min: XX, max: XX } },
      ...
    ]
  }, ....];
 */
module.exports = ({ measurementsArray, filters, skip, limit, page }, cb) => {
  const excludedFieldsForFilters = {};

  const ItemsFilteredByMeasurement = filterByMeasurements(measurementsArray);

  // Object of fields to exclude as pipeline leaves measurements stage and enters filters stage. Excluding measurement facets.
  map(
    facetConstants.allMeasurementNames,
    name => (excludedFieldsForFilters[`facets.${name}`] = 0)
  );

  //check if there are filters. If not, return query.
  if (isEmpty(filters)) {
    return ItemsFilteredByMeasurement.sort({ createdAt: -1 })
      .project({
        _id: 1,
        price: 1,
        designerLine: 1,
        designer: "$facets.designer",
        primaryPhoto: 1
      })
      .facet({
        data: [{ $skip: skip }, { $limit: limit }],
        metaData: [
          { $count: "total" },
          { $addFields: { page: parseInt(page), offset: parseInt(skip) } }
        ]
      })
      .exec(cb); //TODO: Add pagination functionality
  }
  const filtersPresent = facetHelpers.filtersPresent(filters);

  const selectedFilters = [];

  if (filtersPresent.includes("color")) {
    selectedFilters.push({ "facets.color": { $in: filters.color } });
  }

  if (filtersPresent.includes("material")) {
    selectedFilters.push({ "facets.material": { $in: filters.material } });
  }

  if (filtersPresent.includes("itemType")) {
    selectedFilters.push({ "facets.itemType": { $in: filters.itemType } });
  }

  if (filtersPresent.includes("designer")) {
    selectedFilters.push({ "facets.designer": { $in: filters.designer } });
  }

  return ItemsFilteredByMeasurement.project(excludedFieldsForFilters)
    .match({ $and: selectedFilters })
    .sort({ createdAt: -1 })
    .project({
      _id: 1,
      price: 1,
      designerLine: 1,
      designer: "$facets.designer",
      primaryPhoto: 1
    })
    .facet({
      metaData: [
        { $count: "total" },
        { $addFields: { page: parseInt(page), offset: parseInt(skip) } }
      ],
      data: [{ $skip: skip }, { $limit: limit }]
    })
    .exec(cb);
};
