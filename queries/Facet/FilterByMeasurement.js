const map = require("lodash/map");
const isEmpty = require("lodash/isEmpty");
const { Item } = require("../../models");
const facetConstants = require("../../helpers/constants/facets");
const facetHelpers = require("./facetHelpers");
// const util = require("util");
module.exports = measurementsArray => {
  if (isEmpty(measurementsArray)) {
    return Item.aggregate([{ $match: { isSold: false } }]);
  }
  const projectFieldsMatchItemTypes = [{ $match: { isSold: false } }];
  const matchByMeasurements = {};
  const measurementFacetList = [];

  projectFieldsMatchItemTypes.push({
    $project: {
      _id: 1,
      facets: 1,
      price: 1,
      createdAt: 1,
      designerLine: 1,
      primaryPhoto: 1
    }
  });

  const itemTypesPresent = facetHelpers.itemTypesPresent(measurementsArray);
  if (itemTypesPresent.length) {
    projectFieldsMatchItemTypes.push({
      $match: { "facets.itemType": { $in: itemTypesPresent } }
    });
  }

  // Facets for each measurement profile created
  map(measurementsArray, (singleProfile, index) => {
    const matchTypeMatchMeasurements = [];
    const measurementNamesToMatch = [];

    // Match for itemType of measurement profile
    matchTypeMatchMeasurements.push({
      $match: { "facets.itemType": { $eq: singleProfile.itemType } }
    });

    // Uses measurements for singleProfile's itemType to push any non empty measurement to array
    facetConstants[singleProfile.itemType].map(measurementName => {
      if (!isEmpty(singleProfile[measurementName])) {
        measurementNamesToMatch.push(measurementName);
      }
    });

    // Match by measurements
    map(measurementNamesToMatch, measurementName => {
      matchTypeMatchMeasurements.push({
        $match: {
          [`facets.${measurementName}`]: {
            $gte: singleProfile[measurementName].min,
            $lte: singleProfile[measurementName].max
          }
        }
      });
    });

    // Creates new pipeline for this measurement profile
    matchByMeasurements[
      `${singleProfile.itemType}${index}`
    ] = matchTypeMatchMeasurements;

    // Need a list of all the pipelines to merge them together after. Appended the '$' at begining.
    measurementFacetList.push(`$${singleProfile.itemType}${index}`);
  });

  return Item.aggregate(projectFieldsMatchItemTypes)
    .facet(matchByMeasurements)
    .project({ filteredByMeasurements: { $setUnion: measurementFacetList } }) // Combines faceted pipelines into an object with the root key as "filteredByMeasurements"
    .unwind("filteredByMeasurements")
    .replaceRoot("filteredByMeasurements"); // unwind and replaceRoot stages above are to remove the "filteredByMeasurements" root key
};
