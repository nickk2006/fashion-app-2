const { User } = require("../models");

module.exports = ({ condition, update, options }) => {
  return User.findOneAndUpdate(condition, update, options).exec();
};
