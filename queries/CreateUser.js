const { User } = require("../models");

/**
 * Creates a user
 * @param {object} userProps - Object containing email and password (not hashed yet)
 * @return {promise} A promise that resolves with the User that was created.
 */

module.exports = userProps => {
  const user = new User(userProps);
  return user.save();
};
