const { User } = require("../models");

module.exports = id =>
  User.findById(id)
    .select("itemsForSale")
    .populate(
      "itemsForSale",
      "_id designer price itemType colors materials primaryPhoto"
    );
