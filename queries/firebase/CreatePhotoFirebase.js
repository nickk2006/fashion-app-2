const { storage } = require("../../firebase");

module.exports = ({ file, name }) => {
  const itemPhotosBucket = storage.bucket(
    `${process.env.GC_STORAGE_ITEM_PHOTOS_BUCKET_NAME}`
  );

  return itemPhotosBucket.upload(file, {
    gzip: true,
    predefinedAcl: "publicRead"
  });
};
