const mongoose = require("mongoose");
const { MeasurementProfile, User } = require("../models");
const isEmpty = require("lodash/isEmpty");
/**
 *
 * @param {string} userId Can be string or ObjectId.
 * @returns {Promise} resolves into array of objects. Objects will have measurement name and measurementId. {name: ____, measurementId: ___}
 */
module.exports = userId =>
  User.findById(userId, "profiles")
    .populate("profiles", "-user -__v")
    .lean()
    .exec();
