const mongoose = require("mongoose");
const { MeasurementProfile, User } = require("../models");
// const map = require("lodash/map");
// const { facets } = require("../helpers");
module.exports = ({ measurementProps, _id }) => {
  // const user = mongoose.Types.ObjectId(_id);

  // const propsWithUser = Object.assign(measurementProps, { user });

  const measurementProfile = new MeasurementProfile(measurementProps);
  return measurementProfile
    .save()
    .then(measurement =>
      User.findByIdAndUpdate(_id, {
        $push: { profiles: measurement._id }
      }).then(() => measurement)
    );
};
