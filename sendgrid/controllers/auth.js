const sgMail = require("../../config/sendgrid");
const { REGISTRATION_CONFIRMATION } = require("../sendgrid.constants");
const sendRegistrationEmail = async (req, res, next) => {
  const { username, email } = res.locals;
  sgMail
    .send({
      to: email,
      from: "nick@walcroftstreet.com",
      templateId: REGISTRATION_CONFIRMATION,
      dynamic_template_data: {
        username
      }
    })
    .then(() => {
      return next();
    })
    .catch(e => next(e));
};

module.exports = { sendRegistrationEmail };
