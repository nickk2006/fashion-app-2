const sgMail = require("../../config/sendgrid");
const { BUYER_ITEM_TRACKING_TEMPLATE_ID } = require("../sendgrid.constants");
const sendBuyerTrackingNotification = async (req, res, next) => {
  const {
    facets: { designer, itemType },
    sellerUsername,
    carrier,
    tracking_number,
    photos
  } = res.locals.item;
  const { buyerEmail } = res.locals;
  const primaryPhotoUrlSm = photos[0].replace("_XX", "_sm");

  sgMail
    .send({
      to: buyerEmail,
      from: "nick@walcroftstreet.com",
      templateId: BUYER_ITEM_TRACKING_TEMPLATE_ID,
      dynamic_template_data: {
        designer,
        itemType: itemType.toLowerCase(),
        sellerUsername,
        carrier,
        tracking_number,
        primaryPhotoUrlSm
      }
    })
    .then(() => {
      res.status(200).json({ success: true });
      return next();
    });
};

module.exports = { sendBuyerTrackingNotification };
