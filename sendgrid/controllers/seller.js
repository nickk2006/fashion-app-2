const sgMail = require("../../config/sendgrid");
const { SELLER_RECEIPT_TEMPLATE_ID } = require("../sendgrid.constants");
const { amountToSeller } = require("../../helpers");
const sendSellerPurchaseNotification = async (req, res, next) => {
  const { buyer, seller, item } = res.locals;
  const primaryPhotoUrlSm = item.photos[0].replace("_XX", "_sm");
  const price = item.price;
  const msg = {
    to: seller.email,
    from: "nick@walcroftstreet.com",
    templateId: SELLER_RECEIPT_TEMPLATE_ID,
    dynamic_template_data: {
      deliveryInputUrl: `${process.env.BASE_URL}/catalog/items/${
        item._id
      }/shipping`,
      primaryPhotoUrlSm,
      name: item.shipTo.name,
      address: item.shipTo.address,
      city: item.shipTo.city,
      state: item.shipTo.state,
      zip: item.shipTo.zip,
      brand: item.designer,
      itemType: item.itemType.toLowerCase(),
      price: price.toFixed(2),
      platformFee: price - amountToSeller(price).toFixed(2),
      netPrice: amountToSeller(price).toFixed(2)
    }
  };
  sgMail.send(msg).then(() => {
    res.status(200).json({ success: true });
    next();
  });
};

module.exports = { sendSellerPurchaseNotification };
