const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const MeasurementProfile = require("./MeasurementProfile");
const options = require("./options");

const dressShirtProperties = {
  collar: {
    type: Number,
    required: true
  },
  sleeve: {
    type: Number,
    required: true
  },
  chest: {
    type: Number,
    required: true
  },
  waist: {
    type: Number,
    required: true
  },
  length: {
    type: Number,
    required: true
  }
};

const DressShirtSchema = new Schema(dressShirtProperties, options);

const DressShirt = MeasurementProfile.discriminator(
  "DressShirtMeasurement",
  DressShirtSchema,
  options
);

module.exports = DressShirt;
