const JacketProfile = require("./JacketProfile");
const DressShirtProfile = require("./DressShirtProfile");
const PantsProfile = require("./PantsProfile");

module.exports = {
  JacketProfile,
  DressShirtProfile,
  PantsProfile
};
