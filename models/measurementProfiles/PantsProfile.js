const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const MeasurementProfile = require("./MeasurementProfile");
const options = require("./options");

const pantsProperties = {
  waist: {
    type: Number,
    required: true
  },
  legOpening: {
    type: Number
  }
};
const PantsMeasurement = new Schema(pantsProperties, options);

const PantsProfile = MeasurementProfile.discriminator(
  "PantsMeasurement",
  PantsMeasurement
);

module.exports = PantsProfile;
