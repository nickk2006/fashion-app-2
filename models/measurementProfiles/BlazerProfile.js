const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const MeasurementProfile = require("./MeasurementProfile");
const options = require("./options");
const isEmpty = require("lodash/isEmpty");

const jacketProperties = {
  backLength: {
    type: Number,
    required: true
  },
  chest: {
    type: Number,
    required: true
  },
  waist: {
    type: Number,
    required: true
  },
  shoulder: {
    type: Number,
    required: true
  },
  sleeve: {
    type: Number,
    required: true
  }
};

const JacketSchmea = new Schema(jacketProperties, options);

const Jacket = MeasurementProfile.discriminator(
  "JacketMeasurement",
  JacketSchmea,
  options
);

module.exports = Jacket;
