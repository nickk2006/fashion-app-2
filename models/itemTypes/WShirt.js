const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Item = require("../Item2");
const options = require("./_itemOptions");
const { itemMeasurementType } = require("./item.constants");
const WShirtSchema = new Schema(
  {
    measurement: {
      "Sleeve Length": itemMeasurementType,
      Bust: itemMeasurementType,
      Waist: itemMeasurementType,
      "Body Length": itemMeasurementType
    }
  },
  options
);

// MCasualShirtItemSchema.virtual("itemType").get(function() {
//   return " Shirt";
// });

WShirtSchema.pre("save", function() {
  this.facets.female = true;
});

const WShirt = Item.discriminator("WShirt", WShirtSchema);

module.exports = WShirt;
