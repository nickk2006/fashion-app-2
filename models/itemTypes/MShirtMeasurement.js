const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const MeasurementProfile = require("../MeasurementProfile2");
const options = require("./_measurementOptions");
const { itemMeasurementType } = require("./item.constants");

const json = {
  designer: "Canali",
  photos: ["TestPhoto1", "TestPhoto2"],
  sellerId: "5b93574a82ebd209cb73be7a",
  sellerUsername: "Seller Username 1",
  color: ["blue", "green"],
  description: "Lorum ipsum",
  designerLine: "Exclusive Line",
  itemType: "Casual Shirt",
  material: [
    { name: "Cotton", amount: 50 },
    { name: "Cashmere", amount: 25 },
    { name: "Silk", amount: 25 }
  ],
  price: 1000,
  measurement: {
    Sleeve: 32,
    Chest: 21,
    Waist: 19,
    Length: 30
  }
};

const json2 = {
  measurement: {
    "Sleeve.max": 10,
    "Sleeve.min": 10,
    "Chest.min": 10,
    "Chest.max": 10,
    "Waist.min": 10,
    "Waist.max": 10,
    "Length.min": 10,
    "Length.max": 10
  }
};

const MShirtSchema = new Schema(
  {
    measurement: {
      "Sleeve Length.max": itemMeasurementType,
      "Sleeve Length.min": itemMeasurementType,
      "Chest.min": itemMeasurementType,
      "Chest.max": itemMeasurementType,
      "Waist.min": itemMeasurementType,
      "Waist.max": itemMeasurementType,
      "Body Length.min": itemMeasurementType,
      "Body Length.max": itemMeasurementType
    }
  },
  options
);

// MCasualShirtItemSchema.virtual("itemType").get(function() {
//   return " Shirt";
// });

MShirtSchema.virtual("facet").get(function() {
  return {
    itemType: this.itemType,
    measurement: {
      "Sleeve Length": {
        $gte: this.measurement["Sleeve Length"].min,
        $lte: this.measurement["Sleeve Length"].max
      },
      Chest: {
        $gte: this.measurement.Chest.min,
        $lte: this.measurement.Chest.max
      },
      Waist: {
        $gte: this.measurement.Waist.min,
        $lte: this.measurement.Waist.max
      },
      "Body Length": {
        $gte: this.measurement["Body Length"].min,
        $lte: this.measurement["Body Length"].max
      }
    }
  };
});

const MShirt = MeasurementProfile.discriminator(
  "MShirtMeasurement",
  MShirtSchema
);

module.exports = MShirt;
