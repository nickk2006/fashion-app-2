const MShirt = require("./MShirt");
const MShirtMeasurement = require("./MShirtMeasurement");
const WShirt = require("./WShirt");
const WShirtMeasurement = require("./WShirtMeasurement");

const items = {
  MShirt: MShirt,
  WShirt: WShirt
};

const profiles = {
  MShirt: MShirtMeasurement,
  WShirt: WShirtMeasurement
};

module.exports = {
  items,
  profiles
};
