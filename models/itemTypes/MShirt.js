const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Item = require("../Item2");
const options = require("./_itemOptions");
const { itemMeasurementType } = require("./item.constants");
const MShirtSchema = new Schema(
  {
    measurement: {
      "Sleeve Length": itemMeasurementType,
      Chest: itemMeasurementType,
      Waist: itemMeasurementType,
      "Body Length": itemMeasurementType
    }
  },
  options
);

// MCasualShirtItemSchema.virtual("itemType").get(function() {
//   return " Shirt";
// });

MShirtSchema.pre("save", function() {
  this.facets.male = true;
});

const MShirt = Item.discriminator("MShirt", MShirtSchema);

module.exports = MShirt;
