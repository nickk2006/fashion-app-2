const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const MeasurementProfile = require("../MeasurementProfile2");
const options = require("./_measurementOptions");
const { itemMeasurementType } = require("./item.constants");

const WShirtSchema = new Schema(
  {
    measurement: {
      "Sleeve Length.max": itemMeasurementType,
      "Sleeve Length.min": itemMeasurementType,
      "Bust.min": itemMeasurementType,
      "Bust.max": itemMeasurementType,
      "Waist.min": itemMeasurementType,
      "Waist.max": itemMeasurementType,
      "Body Length.min": itemMeasurementType,
      "Body Length.max": itemMeasurementType
    }
  },
  options
);

// MCasualShirtItemSchema.virtual("itemType").get(function() {
//   return " Shirt";
// });

WShirtSchema.virtual("facet").get(function() {
  return {
    itemType: this.itemType,
    measurement: {
      "Sleeve Length": {
        $gte: this.measurement["Sleeve Length"].min,
        $lte: this.measurement["Sleeve Length"].max
      },
      Bust: {
        $gte: this.measurement.Bust.min,
        $lte: this.measurement.Bust.max
      },
      Waist: {
        $gte: this.measurement.Waist.min,
        $lte: this.measurement.Waist.max
      },
      "Body Length": {
        $gte: this.measurement["Body Length"].min,
        $lte: this.measurement["Body Length"].max
      }
    }
  };
});

const WShirt = MeasurementProfile.discriminator(
  "WShirtMeasurement",
  WShirtSchema
);

module.exports = WShirt;
