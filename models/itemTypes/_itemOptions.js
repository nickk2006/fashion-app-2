module.exports = {
  timestamps: true,
  discriminatorKey: "itemType",
  toJSON: {
    getters: true,
    virtuals: true
  },
  toObject: {
    getters: true,
    virtuals: true
  }
};
