const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MeasurementProfileSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    }
  },
  {
    discriminatorKey: "itemType",
    timestamps: true,
    toJSON: {
      getters: true,
      virtuals: true
    },
    toObject: {
      getters: true,
      virtuals: true
    }
  }
);
const MeasurementProfile = mongoose.model(
  "MeasurementProfile2",
  MeasurementProfileSchema
);
module.exports = MeasurementProfile;
