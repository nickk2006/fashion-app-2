const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const anonUserSchema = new Schema(
  {
    firebaseUid: {
      type: String,
      required: true,
      unique: true
    },
    profiles: [
      {
        type: Schema.Types.ObjectId,
        ref: "MeasurementProfile"
      }
    ]
  },
  { timestamps: true }
);

anonUserSchema.index({
  firebaseUid: 1
});

anonUserSchema.statics.findOneOrCreate = async function(condition, doc) {
  const self = this;
  try {
    const user = await self
      .findOne(condition)
      .populate("profiles")
      .exec();
    if (user) {
      return user;
    } else {
      const newUser = await self.create(doc);
      return newUser;
    }
  } catch (e) {
    return e;
  }
};

const AnonymousUser = mongoose.model("AnonymousUser", anonUserSchema);

module.exports = AnonymousUser;
