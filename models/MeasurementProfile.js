const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const facets = require("../helpers/constants/facets");

const MeasurementProfileSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  itemType: {
    type: String,
    required: true,
    enum: { values: facets.itemTypes, message: "Not a valid item type." }
  },
  ...facets.measurementProfileSchema
});

MeasurementProfileSchema.pre("validate", function(next) {
  // console.log("\n\nthis is", this, "\n\n");
  const { itemType, ...measurements } = this;
  facets[itemType].map(fullName => {
    measurements[`${fullName}.min`] > measurements[`${fullName}.max`]
      ? next(new Error("Min measurement must be smaller than max measurement."))
      : null;
  });
  next();
});

const MeasurementProfile = mongoose.model(
  "MeasurementProfile",
  MeasurementProfileSchema
);

module.exports = MeasurementProfile;
