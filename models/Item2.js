const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const User = require("./User");
const { PaymentSchema, AddressSchema } = require("./Schemas");
const facets = require("../helpers/constants/facets");
// const isEqual = require("lodash/isEqual");
// const map = require("lodash/map");
const uniq = require("lodash/uniq");

const Item2Schema = new Schema(
  {
    price: {
      type: Number,
      required: true
    },
    sellerId: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    isDelivered: {
      type: Boolean,
      default: false
    },
    sellerUsername: {
      type: String,
      required: true
    },
    buyer: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    primaryPhoto: {
      type: String
    },
    payment: PaymentSchema,
    isTrackingSet: {
      type: Boolean,
      default: false
    },
    isClosed: {
      type: Boolean,
      default: false
    },
    materials: {
      Cotton: {
        type: Number,
        default: 0,
        min: [0, "May not include negative material percentage."]
      },
      Cashmere: {
        type: Number,
        default: 0,
        min: [0, "May not include negative material percentage."]
      },
      Linen: {
        type: Number,
        default: 0,
        min: [0, "May not include negative material percentage."]
      },
      Wool: {
        type: Number,
        default: 0,
        min: [0, "May not include negative material percentage."]
      },
      Leather: {
        type: Number,
        default: 0,
        min: [0, "May not include negative material percentage."]
      },
      Denim: {
        type: Number,
        default: 0,
        min: [0, "May not include negative material percentage."]
      },
      Silk: {
        type: Number,
        default: 0,
        min: [0, "May not include negative material percentage."]
      },
      Other: {
        type: Number,
        default: 0,
        min: [0, "May not include negative material percentage."]
      }
    },
    shipTo: AddressSchema,

    holdUntil: {
      type: Date,
      default: "0"
    },
    holderSetBy: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    notifyWhenAvailable: [
      {
        type: Schema.Types.ObjectId,
        ref: "User"
      }
    ],
    isSold: {
      type: Boolean,
      default: false
    },
    photos: [
      {
        type: String,
        required: true
      }
    ],
    description: {
      type: String,
      maxlength: [280, "Descriptions can only be 280 characters long"]
    },
    designerLine: {
      type: String,
      maxLength: [200, "Designer Line character length limited to 200."]
    },
    facets: {
      male: {
        type: Boolean,
        default: false
      },
      female: {
        type: Boolean,
        default: false
      },
      color: [
        {
          type: String,
          enum: {
            values: [
              "Black",
              "Brown",
              "Blue",
              "Green",
              "Grey",
              "Orange",
              "Pink",
              "Purple",
              "Red",
              "White",
              "Yellow"
            ]
          }
        }
      ],
      designer: {
        type: String,
        required: true
      },
      // itemType: {
      //   type: String,
      //   enum: {
      //     values: facets.itemTypes
      //   },
      //   required: true
      // },
      material: [
        {
          type: String,
          enum: {
            values: [
              "Cotton",
              "Cashmere",
              "Linen",
              "Wool",
              "Leather",
              "Denim",
              "Silk",
              "Other"
            ]
          }
        }
      ]
    }
  },
  {
    timestamps: true,
    discriminatorKey: "itemType",
    toJSON: {
      getters: true,
      virtuals: true
    },
    toObject: {
      getters: true,
      virtuals: true
    }
  }
);

Item2Schema.index(
  {
    isSold: 1,
    createdAt: 1,
    "facets.itemType": 1,
    "facets.designer": 1,
    // "facets.color": 1
    "facets.material": 1
  },
  {
    name: "itemIndex"
  }
);
//http://www.companiondenim.com/how-to-measure/

Item2Schema.virtual("material")
  .set(function(material) {
    const includedMaterial = [];
    material.map(({ name, amount }) => {
      this.materials[name] = amount;
      if (amount > 0) {
        includedMaterial.push(name);
      }
    });
    this.facets.material = includedMaterial;
  })
  .get(function() {
    const material = {};

    [
      { name: "Cotton", amount: this.materials.Cotton },
      { name: "Cashmere", amount: this.materials.Cashmere },
      { name: "Linen", amount: this.materials.Linen },
      { name: "Wool", amount: this.materials.Wool },
      { name: "Leather", amount: this.materials.Leather },
      { name: "Denim", amount: this.materials.Denim },
      { name: "Silk", amount: this.materials.Silk },
      { name: "Other", amount: this.materials.Other }
    ].map(
      ({ name, amount }) =>
        amount > 0 ? Object.assign(material, { [name]: amount }) : null
    );

    return material;
  });

Item2Schema.virtual("color")
  .set(function(color) {
    this.facets.color = uniq(color);
  })
  .get(function() {
    return this.facets.color;
  });

// Item2Schema.virtual("measurement")
//   .set(function(measurement) {
//     Object.keys(measurement).map(
//       measurementName =>
//         (this.facets[measurementName] = measurement[measurementName])
//     );
//   })
//   .get(function() {
//     return facets[this.facets.itemType].map(measurementName => ({
//       name: measurementName,
//       amount: this.facets[measurementName]
//     }));
//   });

Item2Schema.virtual("designer")
  .set(function(designer) {
    this.facets.designer = designer;
  })
  .get(function() {
    return this.facets.designer;
  });

Item2Schema.virtual("itemType")
  // .set(function(itemType) {
  //   this.facets.itemType = itemType;
  // })
  .get(function() {
    return this.facets.itemType;
  });

Item2Schema.pre("validate", function(next) {
  return facets.materials.reduce(
    (acc, materialName) => this.materials[materialName] + acc,
    0
  ) === 100
    ? next()
    : next(new Error(`Sum of materials should equal 100%.`));
});

Item2Schema.pre("save", function(next) {
  this.primaryPhoto = this.photos[0];
  next();
});

Item2Schema.post("save", function(item, next) {
  const itemId = item._id;
  const sellerId = item.sellerId;

  const update = { $push: { itemsForSale: itemId } };

  User.findByIdAndUpdate(sellerId, update, function(err, doc) {
    if (err) {
      console.log("Error updating user profile for itemsForSale: ", err);
      return next(err);
    }
    next();
  });
});

const Item2 = mongoose.model("Item2", Item2Schema);

module.exports = Item2;
