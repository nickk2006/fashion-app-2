const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const User = require("../User");

const AddressSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    address: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    state: {
      type: String,
      required: true,
      enum: {
        values: [
          "AK",
          "AL",
          "AR",
          "AZ",
          "CA",
          "CO",
          "CT",
          "DC",
          "DE",
          "FL",
          "GA",
          "HI",
          "IA",
          "ID",
          "IL",
          "IN",
          "KS",
          "KY",
          "LA",
          "MA",
          "MD",
          "ME",
          "MI",
          "MN",
          "MO",
          "MS",
          "MT",
          "NC",
          "ND",
          "NE",
          "NH",
          "NJ",
          "NM",
          "NV",
          "NY",
          "OH",
          "OK",
          "OR",
          "PA",
          "RI",
          "SC",
          "SD",
          "TN",
          "TX",
          "UT",
          "VA",
          "VT",
          "WA",
          "WI",
          "WV",
          "WY"
        ],
        message: "Please check the state abbreviation."
      }
    },
    zip: {
      type: Number,
      required: true
    },
    country: String,
    countryCode: String,
    carrier: {
      type: String,
      default: ""
    },
    tracking_number: {
      type: String,
      default: ""
    }
  },
  { _id: false }
);
module.exports = AddressSchema;
