const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PaymentSchema = new Schema(
  {
    isCaptured: {
      type: Boolean,
      default: false
    },
    transfer_group: {
      type: String,
      required: true,
      default: ""
    },
    destination: {
      type: String,
      required: true,
      default: ""
    },
    amount: {
      type: Number,
      required: true
    },
    chargeId: String,
    transferDate: {
      type: Number,
      default: 0
    },
    isDisputed: {
      type: Boolean,
      default: false
    }
  },
  { _id: false }
);

module.exports = PaymentSchema;
