const PaymentSchema = require("./PaymentSchema");
const AddressSchema = require("./AddressSchema");
module.exports = {
  PaymentSchema,
  AddressSchema
};
