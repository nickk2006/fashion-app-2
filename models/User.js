const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: {
      type: String,
      required: true,
      validate: {
        isAsync: true,
        validator: function(v, cb) {
          User.find({ email: v }, function(err, docs) {
            cb(docs.length === 0);
          });
        },
        message: "Email is already taken"
      }
    },
    username: {
      type: String,
      required: true,
      validate: {
        isAsync: true,
        validator: function(v, cb) {
          User.find({ username: v }, function(err, docs) {
            cb(docs.length === 0);
          });
        },
        message: "Display name is already taken"
      }
    },
    stripeCustomerId: {
      type: String,
      default: ""
    },
    stripeAuthState: String,
    itemsForSale: [
      {
        type: Schema.Types.ObjectId,
        ref: "Item"
      }
    ],
    itemsSold: [
      {
        type: Schema.Types.ObjectId,
        ref: "Item"
      }
    ],
    itemsPurchased: [
      {
        type: Schema.Types.ObjectId,
        ref: "Item"
      }
    ],
    address: [
      {
        type: Schema.Types.ObjectId,
        ref: "Address"
      }
    ],
    profiles: [
      {
        type: Schema.Types.ObjectId,
        ref: "MeasurementProfile"
      }
    ],
    singleCollections: [
      {
        type: Schema.Types.ObjectId,
        ref: "SingleCollection"
      }
    ]
  },
  { timestamps: true }
);

userSchema.statics.isUsernameUnique = async function(username) {
  const users = await User.find({ username }).exec();
  if (users.length) {
    return false;
  } else {
    return true;
  }
};

userSchema.statics.isEmailUnique = async function(email) {
  const users = await User.find({ email }).exec();
  if (users.length) {
    return false;
  } else {
    return true;
  }
};

userSchema.virtual("isConnectedToStripe").get(function() {
  return !!this.stripeCustomerId;
});
// userSchema.pre("save", function save(next) {
//   const user = this;
//   if (!user.isModified("password")) {
//     console.log("is modified");
//     return next();
//   }
//   bcrypt.genSalt(12, (err, salt) => {
//     if (err) {
//       return next(err);
//     }
//     bcrypt.hash(user.password, salt, (err, hash) => {
//       if (err) {
//         return next(err);
//       }
//       user.password = hash;
//       next();
//     });
//   });
// });
// // Helper method for validating user's password
// userSchema.methods.comparePassword = function comparePassword(
//   candidatePassword,
//   cb
// ) {
//   bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
//     cb(err, isMatch);
//   });
// };

const User = mongoose.model("User", userSchema);

module.exports = User;
