const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Item = require("../itemModels/Item");
const options = require("./itemOptions");

const pantsProperties = {
  measurements: {
    waist: {
      type: Number,
      required: true
    },
    legOpening: {
      type: Number
    }
  }
};
const PantsSchema = new Schema(pantsProperties, options);

const Pants = Item.discriminator("Pants", PantsSchema, options);

module.exports = Pants;
