const Jacket = require("./Jacket");
const DressShirt = require("./DressShirt");
const Pants = require("./Pants");

module.exports = { Jacket, DressShirt, Pants };
