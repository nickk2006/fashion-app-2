const CLOTHES_TYPE = {
  JACKET: "Jacket",
  DRESS_SHIRT: "DressShirt",
  PANTS: "Pants"
};

module.exports = {
  CLOTHES_TYPE
};
