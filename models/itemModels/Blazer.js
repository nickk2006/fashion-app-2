const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Item = require("../itemModels/Item");
const WithFormating = require("./WithFormatting");
const options = require("./itemOptions");
const jacketProperties = {
  measurements: {
    backLength: {
      type: Number,
      required: true
    },
    chest: {
      type: Number,
      required: true
    },
    waist: {
      type: Number,
      required: true
    },
    shoulder: {
      type: Number,
      required: true
    },
    sleeve: {
      type: Number,
      required: true
    }
  },
  measurementsWithFormating: [
    {
      type: Schema.Types.ObjectId,
      ref: "WithFormating"
    }
  ]
};

const JacketSchema = new Schema(jacketProperties, options);

const Jacket = Item.discriminator("Jacket", JacketSchema, options);

module.exports = Jacket;
