const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const withFormatingSchema = new Schema({
  backEndName: {
    type: String,
    required: true
  },
  frontEndName: {
    type: String,
    required: true
  },
  detailsText: {
    type: String,
    required: true
  },
  detailsImage: {
    type: String,
    required: true
  },
  measurement: {
    type: Number,
    required: true
  }
});

const WithFormating = mongoose.model("WithFormating", withFormatingSchema);

module.exports = WithFormating;
