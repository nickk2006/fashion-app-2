const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Item = require("../itemModels/Item");
const options = require("./itemOptions");
const dressShirtProperties = {
  measurements: {
    collar: {
      type: Number,
      required: true
    },
    sleeve: {
      type: Number,
      required: true
    },
    chest: {
      type: Number,
      required: true
    },
    waist: {
      type: Number,
      required: true
    },
    length: {
      type: Number,
      required: true
    }
  }
};

const DressShirtSchema = new Schema(dressShirtProperties, options);

const DressShirt = Item.discriminator("DressShirt", DressShirtSchema, options);

module.exports = DressShirt;
