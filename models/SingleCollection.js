const mongoose = require("mongoose");
const { User } = require("../models");
const Schema = mongoose.Schema;

const SingleCollectionSchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    items: [
      {
        type: Schema.Types.ObjectId,
        ref: "Item"
      }
    ],
    isViewed: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: true }
);

SingleCollectionSchema.post("save", function(singleCollection, next) {
  const { userId, _id } = singleCollection;

  const singleCollectionId = _id.toString();

  const payload = { $push: { singleCollections: singleCollectionId } };

  User.findByIdAndUpdate(userId, payload, function(err, doc) {
    if (err) {
      return next(err);
    }
    return next();
  });
});

const SingleCollection = mongoose.model(
  "SingleCollection",
  SingleCollectionSchema
);

module.exports = SingleCollection;
