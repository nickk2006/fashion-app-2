const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const helmet = require("helmet");
const { stripeControllers, stripeRoutes } = require("./stripe");
const winston = require("./config/winston");
const { users, items, oauth, webhooks } = require("./routes");
const firebaseAuth = require("./firebase/auth/firebaseAuthMiddleware");
const cron = require("./controllers/cron");
const cronRoutes = require("./routes/cron");
const path = require("path");
const CORS_WHITELIST = require("./helpers/constants/frontend");
const corsOptions = {
  origin: CORS_WHITELIST,
  optionsSuccessStatus: 200
};

if (process.env.NODE_ENV === "production") {
  app.use(helmet());
  app.use(function(req, res, next) {
    const allowedOrigins = [
      "https://www.walcroftstreet.com",
      "https://connect.stripe.com"
    ];
    const origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
      res.setHeader("Access-Control-Allow-Origin", origin);
    }
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
  });
  app.use(cors(corsOptions));
} else if (process.env.NODE_ENV === "development") {
  console.log("hit cors dev");
  app.use(cors());

  const morgan = require("morgan");
  app.use(morgan("short"));
  // mongoose.set("debug", true);
  // mongoose.set("debug", function(collectionName, method, query, doc) {
  //   console.log(
  //     "Mongoose: ".cyan +
  //       collectionName +
  //       "." +
  //       method +
  //       " (" +
  //       JSON.stringify(query, null, 2) +
  //       ")"
  //   );
  // });

  // app.use(cors({ origin: true, credentials: true }));
  // app.use(function(req, res, next) {
  //   if (req.method === "OPTIONS") {
  //     res.header("Access-Control-Allow-Origin", req.headers.origin);
  //   } else {
  //     res.header("Access-Control-Allow-Origin", "*");
  //   }
  // });

  // var whitelist = ["http://localhost:3000"];
  // const corsOptions = {
  //   origin: function(origin, callback) {
  //     var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
  //     callback(null, originIsWhitelisted);
  //   },
  //   credentials: true
  // };
  // app.options("*", cors()); // include before other routes
  // app.all("", function(req, res, next) {
  //   res.header("Access-Control-Allow-Origin", "");
  //   res.header(
  //     "Access-Control-Allow-Methods",
  //     "PUT, GET, POST, DELETE, OPTIONS"
  //   );
  //   res.header(
  //     "Access-Control-Allow-Headers",
  //     "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  //   );
  //   //Auth Each API Request created by user.
  //   next();
  // });
}
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );
//   next();
// });
mongoose
  .connect(
    process.env.MONGO_URI,
    {
      useNewUrlParser: true
      // dbName: process.env.MONGODB_DATABASE_NAME
    }
  )
  .then(
    () => console.log("Database connection established!"),
    err => console.log("Error connecting to Database instance due to:", err)
  );

// app.use(express.static("public"));

app.use(
  "/api/webhooks/stripe",
  bodyParser.raw({ type: "*/*" }),
  stripeControllers.webhooks.getEvent,
  stripeRoutes.webhooks
);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/api/webhooks", webhooks);
app.use("/api/users", users);
app.use("/api/items", items);
app.use("/api/oauth", firebaseAuth, oauth);
app.use("/api/cron", cron.checkCronHeader, cronRoutes);
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

app.use((err, req, res, next) => {
  console.log("errstat is", err.status);
  winston.error(
    `${err.status || 422} - ${err.message} - ${req.originalUrl} - ${
      req.method
    } - ${req.ip}`
  );
  if (typeof err.status === "undefined") {
    console.log(err.stack);
    console.log(err.message);
    return res.sendStatus(422);
  } else {
    console.log(err.stack);
    return res.status(err.status || 422).send(err);
  }
});

module.exports = app;
