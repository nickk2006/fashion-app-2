const MEASUREMENTS = {
  JACKET_MEASUREMENTS: {
    BACK_LENGTH: "measurements.backLength",
    CHEST: "measurements.chest",
    WAIST: "measurements.waist",
    SHOULDER: "measurements.shoulder",
    SLEEVE: "measurements.sleeve"
  },
  DRESS_SHIRT_MEASUREMENTS: {
    COLLAR: "measurements.collar",
    CHEST: "measurements.chest",
    SLEEVE: "measurements.sleeve",
    WAIST: "measurements.waist",
    LENGTH: "measurements.length"
  },
  PANTS_MEASUREMENTS: {
    WAIST: "measurements.waist",
    LEG_OPENING: "measurements.legOpening"
  }
};

module.exports = MEASUREMENTS;
