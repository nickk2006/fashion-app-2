const M_PANTS = "Men Pants";
const M_JACKET = "Men Jacket";
const M_DRESS_SHIRT = "Men Dress Shirt";
const M_CASUAL_SHIRT = "Men Casual Shirt";
const M_SWEATER = "Men Sweater";
const W_PANTS = "Women Pants";
const W_JACKET = "Women Jacket";
const W_BLOUSE = "Women Blouse";
const W_SWEATER = "Women Sweater";
const W_CASUAL_SHIRT = "Women Casual Shirt";
const W_DRESS_SHIRT = "Women Dress Shirt";

const FRONT_RISE = "Front Rise";
const INSEAM = "Inseam";
const KNEE = "Knee";
const LEG_OPENING = "Leg Opening";
const TOP_THIGH = "Top Thigh";
const WAIST = "Waist";
const CHEST = "Chest";
const BODY_LENGTH = "Body Length";
const NECK = "Neck";
const SHOULDER = "Shoulder";
const SLEEVE = "Shoulder/Sleeve";
const BUST = "Bust";

const M_PANTS_FRONT_RISE = `${M_PANTS}-${FRONT_RISE}`;
const M_PANTS_INSEAM = `${M_PANTS}-${INSEAM}`;
const M_PANTS_KNEE = `${M_PANTS}-${KNEE}`;
const M_PANTS_LEG_OPENING = `${M_PANTS}-${LEG_OPENING}`;
const M_PANTS_TOP_THIGH = `${M_PANTS}-${TOP_THIGH}`;
const M_PANTS_WAIST = `${M_PANTS}-${WAIST}`;

const M_DRESS_SHIRT_CHEST = `${M_DRESS_SHIRT}-${CHEST}`;
const M_DRESS_SHIRT_BODY_LENGTH = `${M_DRESS_SHIRT}-${BODY_LENGTH}`;
const M_DRESS_SHIRT_SLEEVE = `${M_DRESS_SHIRT}-${SLEEVE}`;
const M_DRESS_SHIRT_NECK = `${M_DRESS_SHIRT}-${NECK}`;
const M_DRESS_SHIRT_SHOULDER = `${M_DRESS_SHIRT}-${SHOULDER}`;
const M_DRESS_SHIRT_WAIST = `${M_DRESS_SHIRT}-${WAIST}`;

const M_CASUAL_SHIRT_CHEST = `${M_CASUAL_SHIRT}-${CHEST}`;
const M_CASUAL_SHIRT_BODY_LENGTH = `${M_CASUAL_SHIRT}-${BODY_LENGTH}`;
const M_CASUAL_SHIRT_SLEEVE = `${M_CASUAL_SHIRT}-${SLEEVE}`;
const M_CASUAL_SHIRT_SHOULDER = `${M_CASUAL_SHIRT}-${SHOULDER}`;
const M_CASUAL_SHIRT_WAIST = `${M_CASUAL_SHIRT}-${WAIST}`;

const M_JACKET_CHEST = `${M_JACKET}-${CHEST}`;
const M_JACKET_BODY_LENGTH = `${M_JACKET}-${BODY_LENGTH}`;
const M_JACKET_SHOULDER = `${M_JACKET}-${SHOULDER}`;
const M_JACKET_SLEEVE = `${M_JACKET}-${SLEEVE}`;
const M_JACKET_WAIST = `${M_JACKET}-${WAIST}`;

const M_SWEATER_CHEST = `${M_SWEATER}-${CHEST}`;
const M_SWEATER_BODY_LENGTH = `${M_SWEATER}-${BODY_LENGTH}`;
const M_SWEATER_SLEEVE = `${M_SWEATER}-${SLEEVE}`;
const M_SWEATER_SHOULDER = `${M_SWEATER}-${SHOULDER}`;
const M_SWEATER_WAIST = `${M_SWEATER}-${WAIST}`;

const W_PANTS_FRONT_RISE = `${W_PANTS}-${FRONT_RISE}`;
const W_PANTS_INSEAM = `${W_PANTS}-${INSEAM}`;
const W_PANTS_KNEE = `${W_PANTS}-${KNEE}`;
const W_PANTS_LEG_OPENING = `${W_PANTS}-${LEG_OPENING}`;
const W_PANTS_TOP_THIGH = `${W_PANTS}-${TOP_THIGH}`;
const W_PANTS_WAIST = `${W_PANTS}-${WAIST}`;

const W_JACKET_BUST = `${W_JACKET}-${BUST}`;
const W_JACKET_BODY_LENGTH = `${W_JACKET}-${BODY_LENGTH}`;
const W_JACKET_SHOULDER = `${W_JACKET}-${SHOULDER}`;
const W_JACKET_SLEEVE = `${W_JACKET}-${SLEEVE}`;
const W_JACKET_WAIST = `${W_JACKET}-${WAIST}`;

const W_BLOUSE_BUST = `${W_BLOUSE}-${BUST}`;
const W_BLOUSE_BODY_LENGTH = `${W_BLOUSE}-${BODY_LENGTH}`;
const W_BLOUSE_SLEEVE = `${W_BLOUSE}-${SLEEVE}`;
const W_BLOUSE_WAIST = `${W_BLOUSE}-${WAIST}`;

const W_SWEATER_BUST = `${W_SWEATER}-${BUST}`;
const W_SWEATER_BODY_LENGTH = `${W_SWEATER}-${BODY_LENGTH}`;
const W_SWEATER_SLEEVE = `${W_SWEATER}-${SLEEVE}`;
const W_SWEATER_SHOULDER = `${W_SWEATER}-${SHOULDER}`;
const W_SWEATER_WAIST = `${W_SWEATER}-${WAIST}`;

const W_CASUAL_SHIRT_BUST = `${W_CASUAL_SHIRT}-${BUST}`;
const W_CASUAL_SHIRT_BODY_LENGTH = `${W_CASUAL_SHIRT}-${BODY_LENGTH}`;
const W_CASUAL_SHIRT_SLEEVE = `${W_CASUAL_SHIRT}-${SLEEVE}`;
const W_CASUAL_SHIRT_SHOULDER = `${W_CASUAL_SHIRT}-${SHOULDER}`;
const W_CASUAL_SHIRT_WAIST = `${W_CASUAL_SHIRT}-${WAIST}`;

const W_DRESS_SHIRT_BUST = `${W_DRESS_SHIRT}-${BUST}`;
const W_DRESS_SHIRT_BODY_LENGTH = `${W_DRESS_SHIRT}-${BODY_LENGTH}`;
const W_DRESS_SHIRT_SLEEVE = `${W_DRESS_SHIRT}-${SLEEVE}`;
const W_DRESS_SHIRT_SHOULDER = `${W_DRESS_SHIRT}-${SHOULDER}`;
const W_DRESS_SHIRT_WAIST = `${W_DRESS_SHIRT}-${WAIST}`;

exports.itemTypes = [
  M_CASUAL_SHIRT,
  M_DRESS_SHIRT,
  M_JACKET,
  M_PANTS,
  M_SWEATER,
  W_BLOUSE,
  W_CASUAL_SHIRT,
  W_DRESS_SHIRT,
  W_JACKET,
  W_PANTS,
  W_SWEATER
];

exports[M_PANTS] = [
  M_PANTS_FRONT_RISE,
  M_PANTS_INSEAM,
  M_PANTS_KNEE,
  M_PANTS_LEG_OPENING,
  M_PANTS_TOP_THIGH,
  M_PANTS_WAIST
];

exports[M_DRESS_SHIRT] = [
  M_DRESS_SHIRT_CHEST,
  M_DRESS_SHIRT_BODY_LENGTH,
  M_DRESS_SHIRT_SLEEVE,
  M_DRESS_SHIRT_NECK,
  M_DRESS_SHIRT_SHOULDER,
  M_DRESS_SHIRT_WAIST
];

exports[M_CASUAL_SHIRT] = [
  M_CASUAL_SHIRT_CHEST,
  M_CASUAL_SHIRT_BODY_LENGTH,
  M_CASUAL_SHIRT_SLEEVE,
  M_CASUAL_SHIRT_SHOULDER,
  M_CASUAL_SHIRT_WAIST
];

exports[M_JACKET] = [
  M_JACKET_CHEST,
  M_JACKET_BODY_LENGTH,
  M_JACKET_SHOULDER,
  M_JACKET_SLEEVE,
  M_JACKET_WAIST
];

exports[M_SWEATER] = [
  M_SWEATER_CHEST,
  M_SWEATER_BODY_LENGTH,
  M_SWEATER_SLEEVE,
  M_SWEATER_SHOULDER,
  M_SWEATER_WAIST
];

exports[W_PANTS] = [
  W_PANTS_FRONT_RISE,
  W_PANTS_INSEAM,
  W_PANTS_KNEE,
  W_PANTS_LEG_OPENING,
  W_PANTS_TOP_THIGH,
  W_PANTS_WAIST
];

exports[W_JACKET] = [
  W_JACKET_BUST,
  W_JACKET_BODY_LENGTH,
  W_JACKET_SHOULDER,
  W_JACKET_SLEEVE,
  W_JACKET_WAIST
];

exports[W_BLOUSE] = [
  W_BLOUSE_BUST,
  W_BLOUSE_BODY_LENGTH,
  W_BLOUSE_SLEEVE,
  W_BLOUSE_WAIST
];

exports[W_SWEATER] = [
  W_SWEATER_BUST,
  W_SWEATER_BODY_LENGTH,
  W_SWEATER_SLEEVE,
  W_SWEATER_SHOULDER,
  W_SWEATER_WAIST
];

exports[W_CASUAL_SHIRT] = [
  W_CASUAL_SHIRT_BUST,
  W_CASUAL_SHIRT_BODY_LENGTH,
  W_CASUAL_SHIRT_SLEEVE,
  W_CASUAL_SHIRT_SHOULDER,
  W_CASUAL_SHIRT_WAIST
];

exports[W_DRESS_SHIRT] = [
  W_DRESS_SHIRT_BUST,
  W_DRESS_SHIRT_BODY_LENGTH,
  W_DRESS_SHIRT_SLEEVE,
  W_DRESS_SHIRT_SHOULDER,
  W_DRESS_SHIRT_WAIST
];

exports.itemNameMapping = {
  [M_PANTS_WAIST]: WAIST,
  [M_PANTS_FRONT_RISE]: FRONT_RISE,
  [M_PANTS_INSEAM]: INSEAM,
  [M_PANTS_TOP_THIGH]: TOP_THIGH,
  [M_PANTS_KNEE]: KNEE,
  [M_PANTS_LEG_OPENING]: LEG_OPENING,

  [M_DRESS_SHIRT_NECK]: NECK,
  [M_DRESS_SHIRT_CHEST]: CHEST,
  [M_DRESS_SHIRT_WAIST]: WAIST,
  [M_DRESS_SHIRT_BODY_LENGTH]: BODY_LENGTH,
  [M_DRESS_SHIRT_SLEEVE]: SLEEVE,
  [M_DRESS_SHIRT_SHOULDER]: SHOULDER,

  [M_CASUAL_SHIRT_CHEST]: CHEST,
  [M_CASUAL_SHIRT_BODY_LENGTH]: BODY_LENGTH,
  [M_CASUAL_SHIRT_SLEEVE]: SLEEVE,
  [M_CASUAL_SHIRT_SHOULDER]: SHOULDER,
  [M_CASUAL_SHIRT_WAIST]: WAIST,

  [M_JACKET_BODY_LENGTH]: BODY_LENGTH,
  [M_JACKET_CHEST]: CHEST,
  [M_JACKET_WAIST]: WAIST,
  [M_JACKET_SHOULDER]: SHOULDER,
  [M_JACKET_SLEEVE]: SLEEVE,

  [M_SWEATER_CHEST]: CHEST,
  [M_SWEATER_BODY_LENGTH]: BODY_LENGTH,
  [M_SWEATER_SLEEVE]: SLEEVE,
  [M_SWEATER_SHOULDER]: SHOULDER,
  [M_SWEATER_WAIST]: WAIST,

  [W_PANTS_WAIST]: WAIST,
  [W_PANTS_FRONT_RISE]: FRONT_RISE,
  [W_PANTS_INSEAM]: INSEAM,
  [W_PANTS_LEG_OPENING]: LEG_OPENING,
  [W_PANTS_TOP_THIGH]: TOP_THIGH,
  [W_PANTS_KNEE]: KNEE,

  [W_JACKET_BODY_LENGTH]: BODY_LENGTH,
  [W_JACKET_BUST]: BUST,
  [W_JACKET_WAIST]: WAIST,
  [W_JACKET_SHOULDER]: SHOULDER,
  [W_JACKET_SLEEVE]: SLEEVE,

  [W_BLOUSE_BODY_LENGTH]: BODY_LENGTH,
  [W_BLOUSE_BUST]: BUST,
  [W_BLOUSE_WAIST]: WAIST,
  [W_BLOUSE_SLEEVE]: SLEEVE,

  [W_SWEATER_BUST]: BUST,
  [W_SWEATER_BODY_LENGTH]: BODY_LENGTH,
  [W_SWEATER_SHOULDER]: SHOULDER,
  [W_SWEATER_SLEEVE]: SLEEVE,
  [W_SWEATER_WAIST]: WAIST,

  [W_CASUAL_SHIRT_BUST]: BUST,
  [W_CASUAL_SHIRT_BODY_LENGTH]: BODY_LENGTH,
  [W_CASUAL_SHIRT_SLEEVE]: SLEEVE,
  [W_CASUAL_SHIRT_SHOULDER]: SHOULDER,
  [W_CASUAL_SHIRT_WAIST]: WAIST,

  [W_DRESS_SHIRT_BUST]: BUST,
  [W_DRESS_SHIRT_WAIST]: WAIST,
  [W_DRESS_SHIRT_BODY_LENGTH]: BODY_LENGTH,
  [W_DRESS_SHIRT_SLEEVE]: SLEEVE,
  [W_DRESS_SHIRT_SHOULDER]: SHOULDER
};
exports.allMeasurementNames = [
  M_PANTS_FRONT_RISE,
  M_PANTS_INSEAM,
  M_PANTS_KNEE,
  M_PANTS_LEG_OPENING,
  M_PANTS_TOP_THIGH,
  M_PANTS_WAIST,

  M_DRESS_SHIRT_CHEST,
  M_DRESS_SHIRT_BODY_LENGTH,
  M_DRESS_SHIRT_SLEEVE,
  M_DRESS_SHIRT_NECK,
  M_DRESS_SHIRT_SHOULDER,
  M_DRESS_SHIRT_WAIST,

  M_CASUAL_SHIRT_CHEST,
  M_CASUAL_SHIRT_BODY_LENGTH,
  M_CASUAL_SHIRT_SLEEVE,
  M_CASUAL_SHIRT_SHOULDER,
  M_CASUAL_SHIRT_WAIST,

  M_JACKET_CHEST,
  M_JACKET_BODY_LENGTH,
  M_JACKET_SHOULDER,
  M_JACKET_SLEEVE,
  M_JACKET_WAIST,

  M_SWEATER_CHEST,
  M_SWEATER_BODY_LENGTH,
  M_SWEATER_SLEEVE,
  M_SWEATER_SHOULDER,
  M_SWEATER_WAIST,

  W_PANTS_FRONT_RISE,
  W_PANTS_INSEAM,
  W_PANTS_KNEE,
  W_PANTS_LEG_OPENING,
  W_PANTS_TOP_THIGH,
  W_PANTS_WAIST,

  W_JACKET_BUST,
  W_JACKET_BODY_LENGTH,
  W_JACKET_SHOULDER,
  W_JACKET_SLEEVE,
  W_JACKET_WAIST,

  W_BLOUSE_BUST,
  W_BLOUSE_BODY_LENGTH,
  W_BLOUSE_SLEEVE,
  W_BLOUSE_WAIST,

  W_SWEATER_BUST,
  W_SWEATER_BODY_LENGTH,
  W_SWEATER_SLEEVE,
  W_SWEATER_SHOULDER,
  W_SWEATER_WAIST,

  W_CASUAL_SHIRT_BUST,
  W_CASUAL_SHIRT_BODY_LENGTH,
  W_CASUAL_SHIRT_SLEEVE,
  W_CASUAL_SHIRT_SHOULDER,
  W_CASUAL_SHIRT_WAIST,

  W_DRESS_SHIRT_BUST,
  W_DRESS_SHIRT_BODY_LENGTH,
  W_DRESS_SHIRT_SLEEVE,
  W_DRESS_SHIRT_SHOULDER,
  W_DRESS_SHIRT_WAIST
];

exports.colors = [
  "Black",
  "Brown",
  "Blue",
  "Green",
  "Grey",
  "Orange",
  "Pink",
  "Purple",
  "Red",
  "White",
  "Yellow"
];

exports.materials = [
  "Cotton",
  "Cashmere",
  "Linen",
  "Wool",
  "Leather",
  "Denim",
  "Silk",
  "Other"
];

exports.availableFilters = ["color", "material", "itemType", "designer"];

const measurementSchemaType = {
  type: Number,
  min: [0, "May not include negative measurements"]
};

exports.measurementSchema = {
  _id: false,
  [M_CASUAL_SHIRT_CHEST]: measurementSchemaType,
  [M_CASUAL_SHIRT_BODY_LENGTH]: measurementSchemaType,
  [M_CASUAL_SHIRT_SLEEVE]: measurementSchemaType,
  [M_CASUAL_SHIRT_SHOULDER]: measurementSchemaType,
  [M_CASUAL_SHIRT_WAIST]: measurementSchemaType,

  [M_PANTS_WAIST]: measurementSchemaType,
  [M_PANTS_FRONT_RISE]: measurementSchemaType,
  [M_PANTS_INSEAM]: measurementSchemaType,
  [M_PANTS_TOP_THIGH]: measurementSchemaType,
  [M_PANTS_KNEE]: measurementSchemaType,
  [M_PANTS_LEG_OPENING]: measurementSchemaType,

  [M_DRESS_SHIRT_NECK]: measurementSchemaType,
  [M_DRESS_SHIRT_CHEST]: measurementSchemaType,
  [M_DRESS_SHIRT_WAIST]: measurementSchemaType,
  [M_DRESS_SHIRT_BODY_LENGTH]: measurementSchemaType,
  [M_DRESS_SHIRT_SLEEVE]: measurementSchemaType,
  [M_DRESS_SHIRT_SHOULDER]: measurementSchemaType,

  [M_JACKET_BODY_LENGTH]: measurementSchemaType,
  [M_JACKET_CHEST]: measurementSchemaType,
  [M_JACKET_WAIST]: measurementSchemaType,
  [M_JACKET_SHOULDER]: measurementSchemaType,
  [M_JACKET_SLEEVE]: measurementSchemaType,

  [M_SWEATER_CHEST]: measurementSchemaType,
  [M_SWEATER_BODY_LENGTH]: measurementSchemaType,
  [M_SWEATER_SLEEVE]: measurementSchemaType,
  [M_SWEATER_SHOULDER]: measurementSchemaType,
  [M_SWEATER_WAIST]: measurementSchemaType,

  [W_PANTS_WAIST]: measurementSchemaType,
  [W_PANTS_FRONT_RISE]: measurementSchemaType,
  [W_PANTS_INSEAM]: measurementSchemaType,
  [W_PANTS_TOP_THIGH]: measurementSchemaType,
  [W_PANTS_KNEE]: measurementSchemaType,
  [W_PANTS_LEG_OPENING]: measurementSchemaType,

  [W_JACKET_BODY_LENGTH]: measurementSchemaType,
  [W_JACKET_BUST]: measurementSchemaType,
  [W_JACKET_WAIST]: measurementSchemaType,
  [W_JACKET_SHOULDER]: measurementSchemaType,
  [W_JACKET_SLEEVE]: measurementSchemaType,

  [W_BLOUSE_BODY_LENGTH]: measurementSchemaType,
  [W_BLOUSE_BUST]: measurementSchemaType,
  [W_BLOUSE_WAIST]: measurementSchemaType,
  [W_BLOUSE_SLEEVE]: measurementSchemaType,

  [W_SWEATER_BUST]: measurementSchemaType,
  [W_SWEATER_BODY_LENGTH]: measurementSchemaType,
  [W_SWEATER_SLEEVE]: measurementSchemaType,
  [W_SWEATER_SHOULDER]: measurementSchemaType,
  [W_SWEATER_WAIST]: measurementSchemaType,

  [W_CASUAL_SHIRT_BUST]: measurementSchemaType,
  [W_CASUAL_SHIRT_BODY_LENGTH]: measurementSchemaType,
  [W_CASUAL_SHIRT_SLEEVE]: measurementSchemaType,
  [W_CASUAL_SHIRT_SHOULDER]: measurementSchemaType,
  [W_CASUAL_SHIRT_WAIST]: measurementSchemaType,

  [W_DRESS_SHIRT_BUST]: measurementSchemaType,
  [W_DRESS_SHIRT_WAIST]: measurementSchemaType,
  [W_DRESS_SHIRT_BODY_LENGTH]: measurementSchemaType,
  [W_DRESS_SHIRT_SLEEVE]: measurementSchemaType,
  [W_DRESS_SHIRT_SHOULDER]: measurementSchemaType
};

exports.measurementProfileSchema = {
  [`${M_PANTS_WAIST}.min`]: measurementSchemaType,
  [`${M_PANTS_WAIST}.max`]: measurementSchemaType,

  [`${M_PANTS_FRONT_RISE}.min`]: measurementSchemaType,
  [`${M_PANTS_FRONT_RISE}.max`]: measurementSchemaType,

  [`${M_PANTS_INSEAM}.min`]: measurementSchemaType,
  [`${M_PANTS_INSEAM}.max`]: measurementSchemaType,

  [`${M_PANTS_TOP_THIGH}.min`]: measurementSchemaType,
  [`${M_PANTS_TOP_THIGH}.max`]: measurementSchemaType,

  [`${M_PANTS_KNEE}.min`]: measurementSchemaType,
  [`${M_PANTS_KNEE}.max`]: measurementSchemaType,

  [`${M_PANTS_LEG_OPENING}.min`]: measurementSchemaType,
  [`${M_PANTS_LEG_OPENING}.max`]: measurementSchemaType,

  [`${M_CASUAL_SHIRT_CHEST}.min`]: measurementSchemaType,
  [`${M_CASUAL_SHIRT_CHEST}.max`]: measurementSchemaType,

  [`${M_CASUAL_SHIRT_BODY_LENGTH}.min`]: measurementSchemaType,
  [`${M_CASUAL_SHIRT_BODY_LENGTH}.max`]: measurementSchemaType,

  [`${M_CASUAL_SHIRT_SLEEVE}.min`]: measurementSchemaType,
  [`${M_CASUAL_SHIRT_SLEEVE}.max`]: measurementSchemaType,

  [`${M_CASUAL_SHIRT_SHOULDER}.min`]: measurementSchemaType,
  [`${M_CASUAL_SHIRT_SHOULDER}.max`]: measurementSchemaType,

  [`${M_CASUAL_SHIRT_WAIST}.min`]: measurementSchemaType,
  [`${M_CASUAL_SHIRT_WAIST}.max`]: measurementSchemaType,

  [`${M_DRESS_SHIRT_NECK}.min`]: measurementSchemaType,
  [`${M_DRESS_SHIRT_NECK}.max`]: measurementSchemaType,

  [`${M_DRESS_SHIRT_CHEST}.min`]: measurementSchemaType,
  [`${M_DRESS_SHIRT_CHEST}.max`]: measurementSchemaType,

  [`${M_DRESS_SHIRT_WAIST}.min`]: measurementSchemaType,
  [`${M_DRESS_SHIRT_WAIST}.max`]: measurementSchemaType,

  [`${M_DRESS_SHIRT_BODY_LENGTH}.min`]: measurementSchemaType,
  [`${M_DRESS_SHIRT_BODY_LENGTH}.max`]: measurementSchemaType,

  [`${M_DRESS_SHIRT_SLEEVE}.min`]: measurementSchemaType,
  [`${M_DRESS_SHIRT_SLEEVE}.max`]: measurementSchemaType,

  [`${M_DRESS_SHIRT_SHOULDER}.min`]: measurementSchemaType,
  [`${M_DRESS_SHIRT_SHOULDER}.max`]: measurementSchemaType,

  [`${M_JACKET_BODY_LENGTH}.min`]: measurementSchemaType,
  [`${M_JACKET_BODY_LENGTH}.max`]: measurementSchemaType,

  [`${M_JACKET_SLEEVE}.min`]: measurementSchemaType,
  [`${M_JACKET_SLEEVE}.max`]: measurementSchemaType,

  [`${M_JACKET_CHEST}.min`]: measurementSchemaType,
  [`${M_JACKET_CHEST}.max`]: measurementSchemaType,

  [`${M_JACKET_WAIST}.min`]: measurementSchemaType,
  [`${M_JACKET_WAIST}.max`]: measurementSchemaType,

  [`${M_JACKET_SHOULDER}.min`]: measurementSchemaType,
  [`${M_JACKET_SHOULDER}.max`]: measurementSchemaType,

  [`${M_JACKET_SLEEVE}.min`]: measurementSchemaType,
  [`${M_JACKET_SLEEVE}.max`]: measurementSchemaType,

  [`${M_SWEATER_CHEST}.min`]: measurementSchemaType,
  [`${M_SWEATER_CHEST}.max`]: measurementSchemaType,

  [`${M_SWEATER_BODY_LENGTH}.min`]: measurementSchemaType,
  [`${M_SWEATER_BODY_LENGTH}.max`]: measurementSchemaType,

  [`${M_SWEATER_SLEEVE}.min`]: measurementSchemaType,
  [`${M_SWEATER_SLEEVE}.max`]: measurementSchemaType,

  [`${M_SWEATER_SHOULDER}.min`]: measurementSchemaType,
  [`${M_SWEATER_SHOULDER}.max`]: measurementSchemaType,

  [`${M_SWEATER_WAIST}.min`]: measurementSchemaType,
  [`${M_SWEATER_WAIST}.max`]: measurementSchemaType,

  [`${W_PANTS_WAIST}.min`]: measurementSchemaType,
  [`${W_PANTS_WAIST}.max`]: measurementSchemaType,

  [`${W_PANTS_FRONT_RISE}.min`]: measurementSchemaType,
  [`${W_PANTS_FRONT_RISE}.max`]: measurementSchemaType,

  [`${W_PANTS_INSEAM}.min`]: measurementSchemaType,
  [`${W_PANTS_INSEAM}.max`]: measurementSchemaType,

  [`${W_PANTS_TOP_THIGH}.min`]: measurementSchemaType,
  [`${W_PANTS_TOP_THIGH}.max`]: measurementSchemaType,

  [`${W_PANTS_KNEE}.min`]: measurementSchemaType,
  [`${W_PANTS_KNEE}.max`]: measurementSchemaType,

  [`${W_PANTS_LEG_OPENING}.min`]: measurementSchemaType,
  [`${W_PANTS_LEG_OPENING}.max`]: measurementSchemaType,

  [`${W_JACKET_BODY_LENGTH}.min`]: measurementSchemaType,
  [`${W_JACKET_BODY_LENGTH}.max`]: measurementSchemaType,

  [`${W_JACKET_SLEEVE}.min`]: measurementSchemaType,
  [`${W_JACKET_SLEEVE}.max`]: measurementSchemaType,

  [`${W_JACKET_BUST}.min`]: measurementSchemaType,
  [`${W_JACKET_BUST}.max`]: measurementSchemaType,

  [`${W_JACKET_WAIST}.min`]: measurementSchemaType,
  [`${W_JACKET_WAIST}.max`]: measurementSchemaType,

  [`${W_JACKET_SHOULDER}.min`]: measurementSchemaType,
  [`${W_JACKET_SHOULDER}.max`]: measurementSchemaType,

  [`${W_JACKET_SLEEVE}.min`]: measurementSchemaType,
  [`${W_JACKET_SLEEVE}.max`]: measurementSchemaType,

  [`${W_BLOUSE_BODY_LENGTH}.min`]: measurementSchemaType,
  [`${W_BLOUSE_BODY_LENGTH}.max`]: measurementSchemaType,

  [`${W_BLOUSE_SLEEVE}.min`]: measurementSchemaType,
  [`${W_BLOUSE_SLEEVE}.max`]: measurementSchemaType,

  [`${W_BLOUSE_BUST}.min`]: measurementSchemaType,
  [`${W_BLOUSE_BUST}.max`]: measurementSchemaType,

  [`${W_BLOUSE_WAIST}.min`]: measurementSchemaType,
  [`${W_BLOUSE_WAIST}.max`]: measurementSchemaType,

  [`${W_BLOUSE_SLEEVE}.min`]: measurementSchemaType,
  [`${W_BLOUSE_SLEEVE}.max`]: measurementSchemaType,

  [`${W_SWEATER_BUST}.min`]: measurementSchemaType,
  [`${W_SWEATER_BUST}.max`]: measurementSchemaType,

  [`${W_SWEATER_BODY_LENGTH}.min`]: measurementSchemaType,
  [`${W_SWEATER_BODY_LENGTH}.max`]: measurementSchemaType,

  [`${W_SWEATER_SLEEVE}.min`]: measurementSchemaType,
  [`${W_SWEATER_SLEEVE}.max`]: measurementSchemaType,

  [`${W_SWEATER_SHOULDER}.min`]: measurementSchemaType,
  [`${W_SWEATER_SHOULDER}.max`]: measurementSchemaType,

  [`${W_SWEATER_WAIST}.min`]: measurementSchemaType,
  [`${W_SWEATER_WAIST}.max`]: measurementSchemaType,

  [`${W_CASUAL_SHIRT_BUST}.min`]: measurementSchemaType,
  [`${W_CASUAL_SHIRT_BUST}.max`]: measurementSchemaType,

  [`${W_CASUAL_SHIRT_BODY_LENGTH}.min`]: measurementSchemaType,
  [`${W_CASUAL_SHIRT_BODY_LENGTH}.max`]: measurementSchemaType,

  [`${W_CASUAL_SHIRT_SLEEVE}.min`]: measurementSchemaType,
  [`${W_CASUAL_SHIRT_SLEEVE}.max`]: measurementSchemaType,

  [`${W_CASUAL_SHIRT_SHOULDER}.min`]: measurementSchemaType,
  [`${W_CASUAL_SHIRT_SHOULDER}.max`]: measurementSchemaType,

  [`${W_CASUAL_SHIRT_WAIST}.min`]: measurementSchemaType,
  [`${W_CASUAL_SHIRT_WAIST}.max`]: measurementSchemaType,

  [`${W_DRESS_SHIRT_BUST}.min`]: measurementSchemaType,
  [`${W_DRESS_SHIRT_BUST}.max`]: measurementSchemaType,

  [`${W_DRESS_SHIRT_WAIST}.min`]: measurementSchemaType,
  [`${W_DRESS_SHIRT_WAIST}.max`]: measurementSchemaType,

  [`${W_DRESS_SHIRT_BODY_LENGTH}.min`]: measurementSchemaType,
  [`${W_DRESS_SHIRT_BODY_LENGTH}.max`]: measurementSchemaType,

  [`${W_DRESS_SHIRT_SLEEVE}.min`]: measurementSchemaType,
  [`${W_DRESS_SHIRT_SLEEVE}.max`]: measurementSchemaType,

  [`${W_DRESS_SHIRT_SHOULDER}.min`]: measurementSchemaType,
  [`${W_DRESS_SHIRT_SHOULDER}.max`]: measurementSchemaType
};
