const FRONTEND_DEV_URLS = [
  "http://localhost:3000",
  "https://connect.stripe.com"
];

const FRONTEND_PROD_URLS = [
  "https://www.walcroftstreet.com",
  "https://connect.stripe.com"
];

module.exports =
  process.env.NODE_ENV === "production"
    ? FRONTEND_PROD_URLS
    : FRONTEND_DEV_URLS;
