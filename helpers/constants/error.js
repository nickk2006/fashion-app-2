const DISPLAY_NAME_TAKEN = "Display name is already taken";
const EMAIL_TAKEN = "Email is already in user";
module.exports = {
  DISPLAY_NAME_TAKEN,
  EMAIL_TAKEN
};
