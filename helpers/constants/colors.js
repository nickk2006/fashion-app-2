exports.names = [
  "black",
  "brown",
  "blue",
  "green",
  "grey",
  "orange",
  "pink",
  "purple",
  "red",
  "white",
  "yellow"
];

exports.displayNames = {
  black: "Black",
  brown: "Brown",
  blue: "Blue",
  green: "Green",
  grey: "Grey",
  orange: "Orange",
  pink: "Pink",
  purple: "Purple",
  red: "Red",
  white: "White",
  yellow: "Yellow"
};
