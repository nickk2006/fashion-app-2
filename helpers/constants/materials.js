exports.names = [
  "cotton",
  "cashmere",
  "linen",
  "wool",
  "leather",
  "denim",
  "silk",
  "other"
];

exports.displayNames = {
  cotton: "Cotton",
  cashmere: "Cashmere",
  linen: "Linen",
  wool: "Wool",
  leather: "Leather",
  denim: "Denim",
  silk: "Silk",
  other: "Other"
};
