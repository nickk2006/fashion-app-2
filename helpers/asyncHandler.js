const boom = require("boom");
const asyncUtil = fn => (req, res, next, ...args) => {
  Promise.resolve(fn(req, res, next, ...args)).catch(
    err => (err.isBoom ? next(err) : next(boom.badImplementation(err)))
  );
};

module.exports = asyncUtil;
