const uuid = require("uuid/v4");

exports.getState = () => {
  const state = uuid();
  const buff = new Buffer(state);
  const encodedState = buff.toString("base64");
  return {
    encodedState,
    decodedState: state
  };
};

exports.checkState = (internalState, externalState) => {
  const externalStateBuff = new Buffer(externalState, "base64");

  const decodedExternalState = externalStateBuff.toString("ascii");

  return internalState === decodedExternalState;
};
