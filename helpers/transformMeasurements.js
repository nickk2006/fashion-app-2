const map = require("lodash/map");
const isEmpty = require("lodash/isEmpty");
const facets = require("../helpers/constants/facets");

/*
Converts:
 { 'Pants-Front Rise': {min: XX, max: XX},
       'Pants-Hip': [Object],
       'Pants-Inseam': [Object],
       'Pants-Top Thigh': [Object],
       'Pants-Knee': [Object],
       'Pants-Leg Opening': [Object],
       _id: 5b82bdc470b977042d76053a,
       itemType: 'Pants' }
To:

*/

const fullNameObjToFrontEndArray = fullNameObject => {
  const itemType = fullNameObject.itemType;
  const displayName = fullNameObject.name;
  const measurementArray = [];
  map(facets[itemType], fullName => {
    if (!isEmpty(fullNameObject[fullName])) {
      const amount = fullNameObject[fullName];
      measurementArray.push({
        name: facets.itemNameMapping[fullName],
        min: amount.min,
        max: amount.max,
        itemType,
        displayName
      });
    }
  });
  return measurementArray;
};

module.exports = { fullNameObjToFrontEndArray };
