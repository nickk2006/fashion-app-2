module.exports = {
  API_IDENTIFIER: "http://localhost:4040/api",

  JACKET: "Jacket",
  DRESS_SHIRT: "DressShirt",
  PANTS: "Pants",

  JACKET_MEASUREMENT: "JacketMeasurement",
  DRESS_SHIRT_MEASUREMENT: "DressShirtMeasurement",
  PANTS_MEASUREMENT: "PantsMeasurement",

  FILTER_URL: {
    MIN_PRICE: "min_price",
    MAX_PRICE: "max_price",
    ITEM_TYPE: "item_type",
    DESIGNER: "designer",
    COLORS: "colors",
    MATERIALS: "materials"
  },
  FILTER_MONGODB: {
    PRICE: "price",
    ITEM_TYPE: "itemType",
    DESIGNER: "designer",
    COLORS: "colors",
    MATERIALS: "materials"
  }
};
