module.exports = price => {
  const FLAT_FEE = 5;
  const PERCENTAGE = 0.09;
  const CUT_OFF = 50;

  let amountToSeller;

  if (price <= CUT_OFF) {
    amountToSeller = price - FLAT_FEE;
  } else {
    amountToSeller =
      price -
      (FLAT_FEE * 100 + Math.round((price - CUT_OFF) * 100 * PERCENTAGE)) / 100;
  }
  return amountToSeller;
};
